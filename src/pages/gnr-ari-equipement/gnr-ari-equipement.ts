import { GnrAriEquipementDispositifdederivationPage } from './../gnr-ari-equipement-dispositifdederivation/gnr-ari-equipement-dispositifdederivation';
import { GnrAriEquipementTableaudecontrolePage } from './../gnr-ari-equipement-tableaudecontrole/gnr-ari-equipement-tableaudecontrole';
import { GnrAriEquipementLiaisonpersonnellePage } from './../gnr-ari-equipement-liaisonpersonnelle/gnr-ari-equipement-liaisonpersonnelle';
import { GnrAriEquipementCornedappelPage } from './../gnr-ari-equipement-cornedappel/gnr-ari-equipement-cornedappel';
import { GnrAriEquipementSoupapealademandePage } from './../gnr-ari-equipement-soupapealademande/gnr-ari-equipement-soupapealademande';
import { GnrAriEquipementDetendeurhpPage } from './../gnr-ari-equipement-detendeurhp/gnr-ari-equipement-detendeurhp';
import { GnrAriEquipementHarnaisPage } from './../gnr-ari-equipement-harnais/gnr-ari-equipement-harnais';
import { GnrAriEquipementPiecefacialePage } from './../gnr-ari-equipement-piecefaciale/gnr-ari-equipement-piecefaciale';
import { GnrAriEquipementBouteillePage } from './../gnr-ari-equipement-bouteille/gnr-ari-equipement-bouteille';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrAriEquipementLigneguidePage } from '../gnr-ari-equipement-ligneguide/gnr-ari-equipement-ligneguide';
import { GnrAriEquipementPlaquedecontrolePage } from '../gnr-ari-equipement-plaquedecontrole/gnr-ari-equipement-plaquedecontrole';
import { GnrAriEquipementBalisesonorePage } from '../gnr-ari-equipement-balisesonore/gnr-ari-equipement-balisesonore';

/**
 * Generated class for the GnrAriEquipementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-equipement',
  templateUrl: 'gnr-ari-equipement.html',
})
export class GnrAriEquipementPage {

  list: Array<{image: string ,title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {image: 'assets/icon/bouteille.jpg', title:'BOUTEILLE',component: GnrAriEquipementBouteillePage},
      {image: 'assets/icon/masque.jpg' ,title:'PIECE FACIALE (MASQUE)',component: GnrAriEquipementPiecefacialePage},
      {image: 'assets/icon/GNR_Harnais.png' ,title:'HARNAIS',component: GnrAriEquipementHarnaisPage},
      {image: 'assets/icon/GNR_DetenteurHP.png',title:'DETENDEUR HAUTE PRESSION',component: GnrAriEquipementDetendeurhpPage},
      {image: 'assets/icon/GNR_Soupape1.png',title:'SOUPAPE A LA DEMANDE',component: GnrAriEquipementSoupapealademandePage},
      {image: 'assets/icon/GNR_CORNE.jpg',title:'CORNE D APPEL',component: GnrAriEquipementCornedappelPage},
      {image: 'assets/icon/GNR_Balise1.png',title:'BALISE SONORE DE LOCALISATION',component: GnrAriEquipementBalisesonorePage},
      {image: 'assets/icon/GNR_LiaisonPErso.png',title:'LIAISON PERSONNELLE',component: GnrAriEquipementLiaisonpersonnellePage},
      {image: 'assets/icon/Ligne_guide2.png',title:'LIGNE GUIDE',component: GnrAriEquipementLigneguidePage},
      {image: 'assets/icon/Tableau_controle.png',title:'TABLEAU DE CONTROLE',component: GnrAriEquipementTableaudecontrolePage},
      {image: 'assets/icon/GNR_PlaqueControle.png',title:'PLAQUE DE CONTROLE',component: GnrAriEquipementPlaquedecontrolePage},
      {image: 'assets/icon/GNR_derivation2.png',title:'DISPOSITIFS DE DERIVATION',component: GnrAriEquipementDispositifdederivationPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriEquipementPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
