import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementPage } from './gnr-ari-equipement';

@NgModule({
  declarations: [
    GnrAriEquipementPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementPage),
  ],
})
export class GnrAriEquipementPageModule {}
