import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaiesPage } from './plaies';

@NgModule({
  declarations: [
    PlaiesPage,
  ],
  imports: [
    IonicPageModule.forChild(PlaiesPage),
  ],
})
export class PlaiesPageModule {}
