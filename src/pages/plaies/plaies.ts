import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapPlaiesSignesComponent } from '../../components/sap-plaies/sap-plaies-signes/sap-plaies-signes';
import { SapPlaiesBilansComponent } from '../../components/sap-plaies/sap-plaies-bilans/sap-plaies-bilans';
import { SapPlaiesActionsComponent } from '../../components/sap-plaies/sap-plaies-actions/sap-plaies-actions';
import { SapPlaiesImportantComponent } from '../../components/sap-plaies/sap-plaies-important/sap-plaies-important';

/**
 * Generated class for the PlaiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-plaies',
  templateUrl: 'plaies.html',
})
export class PlaiesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaiesPage');
  }
  tab7Root=SapPlaiesSignesComponent;
  tab8Root=SapPlaiesBilansComponent;
  tab9Root=SapPlaiesActionsComponent;
  tab10Root=SapPlaiesImportantComponent;
}
