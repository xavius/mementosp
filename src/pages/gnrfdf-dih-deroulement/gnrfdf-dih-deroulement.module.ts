import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfDihDeroulementPage } from './gnrfdf-dih-deroulement';

@NgModule({
  declarations: [
    GnrfdfDihDeroulementPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfDihDeroulementPage),
  ],
})
export class GnrfdfDihDeroulementPageModule {}
