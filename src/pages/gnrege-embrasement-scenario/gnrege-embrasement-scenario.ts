import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeEmbrasementScenarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-embrasement-scenario',
  templateUrl: 'gnrege-embrasement-scenario.html',
})
export class GnregeEmbrasementScenarioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeEmbrasementScenarioPage');
  }

}
