import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEmbrasementScenarioPage } from './gnrege-embrasement-scenario';

@NgModule({
  declarations: [
    GnregeEmbrasementScenarioPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEmbrasementScenarioPage),
  ],
})
export class GnregeEmbrasementScenarioPageModule {}
