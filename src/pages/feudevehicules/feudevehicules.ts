import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxVehiculesAccesComponent } from '../../components/feux-vehicules/feux-vehicules-acces/feux-vehicules-acces';
import { FeuxVehiculesActionComponent } from '../../components/feux-vehicules/feux-vehicules-action/feux-vehicules-action';
import { FeuxVehiculesAttentionComponent } from '../../components/feux-vehicules/feux-vehicules-attention/feux-vehicules-attention';
import { FeuxVehiculesInterditComponent } from '../../components/feux-vehicules/feux-vehicules-interdit/feux-vehicules-interdit';
import { FeuxVehiculesRisquesComponent } from '../../components/feux-vehicules/feux-vehicules-risques/feux-vehicules-risques';
import { FeuxVehiculesStrategieComponent } from '../../components/feux-vehicules/feux-vehicules-strategie/feux-vehicules-strategie';

/**
 * Generated class for the FeudevehiculesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudevehicules',
  templateUrl: 'feudevehicules.html',
})
export class FeudevehiculesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudevehiculesPage');
  }
  tab1Root=FeuxVehiculesRisquesComponent;
  tab2Root=FeuxVehiculesStrategieComponent;
  tab3Root=FeuxVehiculesAccesComponent;
  tab4Root=FeuxVehiculesActionComponent;
  tab5Root=FeuxVehiculesAttentionComponent;
  tab6Root=FeuxVehiculesInterditComponent;
}
