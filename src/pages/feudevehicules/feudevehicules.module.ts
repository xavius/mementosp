import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudevehiculesPage } from './feudevehicules';

@NgModule({
  declarations: [
    FeudevehiculesPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudevehiculesPage),
  ],
})
export class FeudevehiculesPageModule {}
