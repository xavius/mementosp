import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriAugmentationStressPage } from './gnr-ari-augmentation-stress';

@NgModule({
  declarations: [
    GnrAriAugmentationStressPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriAugmentationStressPage),
  ],
})
export class GnrAriAugmentationStressPageModule {}
