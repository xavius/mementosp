import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfCcfBasePage } from './gnrfdf-ccf-base';

@NgModule({
  declarations: [
    GnrfdfCcfBasePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfCcfBasePage),
  ],
})
export class GnrfdfCcfBasePageModule {}
