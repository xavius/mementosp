import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapPolytraumatiseSignesComponent } from '../../components/sap-polytraumatise/sap-polytraumatise-signes/sap-polytraumatise-signes';
import { SapPolytraumatiseBilansComponent } from '../../components/sap-polytraumatise/sap-polytraumatise-bilans/sap-polytraumatise-bilans';
import { SapPolytraumatiseActionsComponent } from '../../components/sap-polytraumatise/sap-polytraumatise-actions/sap-polytraumatise-actions';
import { SapPolytraumatiseImportantComponent } from '../../components/sap-polytraumatise/sap-polytraumatise-important/sap-polytraumatise-important';

/**
 * Generated class for the PolytraumatisePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-polytraumatise',
  templateUrl: 'polytraumatise.html',
})
export class PolytraumatisePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PolytraumatisePage');
  }
  tab7Root=SapPolytraumatiseSignesComponent;
  tab8Root=SapPolytraumatiseBilansComponent;
  tab9Root=SapPolytraumatiseActionsComponent;
  tab10Root=SapPolytraumatiseImportantComponent;
}
