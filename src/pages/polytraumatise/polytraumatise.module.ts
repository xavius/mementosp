import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PolytraumatisePage } from './polytraumatise';

@NgModule({
  declarations: [
    PolytraumatisePage,
  ],
  imports: [
    IonicPageModule.forChild(PolytraumatisePage),
  ],
})
export class PolytraumatisePageModule {}
