import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfUiffDefensivesPage } from './gnrfdf-uiff-defensives';

@NgModule({
  declarations: [
    GnrfdfUiffDefensivesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfUiffDefensivesPage),
  ],
})
export class GnrfdfUiffDefensivesPageModule {}
