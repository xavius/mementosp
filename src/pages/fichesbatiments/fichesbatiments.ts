import { FeudechemineePage } from './../feudecheminee/feudecheminee';
import { FeudecavePage } from './../feudecave/feudecave';
import { FeuIghPage } from './../feu-igh/feu-igh';
import { FeuErPtypeJuPage } from './../feu-er-ptype-ju/feu-er-ptype-ju';
import { FeuErPtypeSvytPage } from './../feu-er-ptype-svyt/feu-er-ptype-svyt';
import { FeuErPtypeMPage } from './../feu-er-ptype-m/feu-er-ptype-m';
import { FeudemagasinPage } from './../feudemagasin/feudemagasin';
import { FeudeparkingPage } from './../feudeparking/feudeparking';
import { FeudejointdedilatationPage } from './../feudejointdedilatation/feudejointdedilatation';
import { FeudeplanchersPage } from './../feudeplanchers/feudeplanchers';
import { FeudeterassePage } from './../feudeterasse/feudeterasse';
import { FeudecomblePage } from './../feudecomble/feudecomble';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeudemaisonPage } from '../feudemaison/feudemaison';
import { FeudappartementPage } from '../feudappartement/feudappartement';
import { FeudecagedescalierPage } from '../feudecagedescalier/feudecagedescalier';
import { FeudentrepôtPage } from '../feudentrep\u00F4t/feudentrep\u00F4t';

/**
 * Generated class for the FichesbatimentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichesbatiments',
  templateUrl: 'fichesbatiments.html',
})
export class FichesbatimentsPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Feu d\'Appartement', component: FeudappartementPage},
      {title:'Feu de Maison', component: FeudemaisonPage},
      {title:'Feu de Cave', component: FeudecavePage},
      {title:'Feu de Comble', component: FeudecomblePage},
      {title:'Feu de Terrasse', component: FeudeterassePage},
      {title:'Feu de Cage d Escalier', component: FeudecagedescalierPage},
      {title:'Feu de Cheminée', component: FeudechemineePage},
      {title:'Feu de Plancher,murs,cloisons', component: FeudeplanchersPage},
      {title:'Feu de Joint de dilatation', component: FeudejointdedilatationPage},
      {title:'Feu de Parking sous-terrain', component: FeudeparkingPage},
      {title:'Feu de Magasin', component: FeudemagasinPage},
      {title:'Feu d\'Entrepôt', component: FeudentrepôtPage},
      {title:'Feu d\'ERP type M', component: FeuErPtypeMPage},
      {title:'Feu d\'ERP type S-V-Y-T', component: FeuErPtypeSvytPage},
      {title:'Feu d\'ERP type J-U', component: FeuErPtypeJuPage},
      {title:'Feu dans un IGH', component: FeuIghPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichesbatimentsPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
  }

}
