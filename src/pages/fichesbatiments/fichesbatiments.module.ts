import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichesbatimentsPage } from './fichesbatiments';

@NgModule({
  declarations: [
    FichesbatimentsPage,
  ],
  imports: [
    IonicPageModule.forChild(FichesbatimentsPage),
  ],
})
export class FichesbatimentsPageModule {}
