import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichesautresPage } from './fichesautres';

@NgModule({
  declarations: [
    FichesautresPage,
  ],
  imports: [
    IonicPageModule.forChild(FichesautresPage),
  ],
})
export class FichesautresPageModule {}
