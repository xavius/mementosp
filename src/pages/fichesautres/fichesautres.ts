import { AutresTunnelsPage } from './../autres-tunnels/autres-tunnels';
import { AutresTramwayPage } from './../autres-tramway/autres-tramway';
import { AutresFerroviairePage } from './../autres-ferroviaire/autres-ferroviaire';
import { AutresPgrPage } from './../autres-pgr/autres-pgr';
import { AutresViolancesurbainesPage } from './../autres-violancesurbaines/autres-violancesurbaines';
import { PlanorsecPage } from './../planorsec/planorsec';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FichesautresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichesautres',
  templateUrl: 'fichesautres.html',
})
export class FichesautresPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Plan ORSEC NOVI', component: PlanorsecPage},
      {title:'Violances Urbaines', component: AutresViolancesurbainesPage},
      {title:'Procédure Gaz Renforcée (PGR)', component: AutresPgrPage},
      {title:'Réseaux Ferroviaires', component: AutresFerroviairePage},
      {title:'Réseaux Tramway', component: AutresTramwayPage},
      {title:'Equipements des Tunnels Routiers du Groupement', component: AutresTunnelsPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichesautresPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
