import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrariDefinitionsPage } from './gnrari-definitions';

@NgModule({
  declarations: [
    GnrariDefinitionsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrariDefinitionsPage),
  ],
})
export class GnrariDefinitionsPageModule {}
