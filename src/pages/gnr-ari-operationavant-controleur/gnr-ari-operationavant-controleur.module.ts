import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationavantControleurPage } from './gnr-ari-operationavant-controleur';

@NgModule({
  declarations: [
    GnrAriOperationavantControleurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationavantControleurPage),
  ],
})
export class GnrAriOperationavantControleurPageModule {}
