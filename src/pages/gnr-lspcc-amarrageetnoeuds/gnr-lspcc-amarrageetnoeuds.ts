import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccAmarrageetnoeudsPointsfixesComponent } from '../../components/gnr-lspcc-amarrageetnoeuds/gnr-lspcc-amarrageetnoeuds-pointsfixes/gnr-lspcc-amarrageetnoeuds-pointsfixes';
import { GnrLspccAmarrageetnoeudsNoeudsComponent } from '../../components/gnr-lspcc-amarrageetnoeuds/gnr-lspcc-amarrageetnoeuds-noeuds/gnr-lspcc-amarrageetnoeuds-noeuds';

/**
 * Generated class for the GnrLspccAmarrageetnoeudsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-amarrageetnoeuds',
  templateUrl: 'gnr-lspcc-amarrageetnoeuds.html',
})
export class GnrLspccAmarrageetnoeudsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccAmarrageetnoeudsPage');
  }
  tab59Root=GnrLspccAmarrageetnoeudsPointsfixesComponent;
  tab60Root=GnrLspccAmarrageetnoeudsNoeudsComponent;
}
