import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccAmarrageetnoeudsPage } from './gnr-lspcc-amarrageetnoeuds';

@NgModule({
  declarations: [
    GnrLspccAmarrageetnoeudsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccAmarrageetnoeudsPage),
  ],
})
export class GnrLspccAmarrageetnoeudsPageModule {}
