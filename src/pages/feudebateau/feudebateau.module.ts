import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudebateauPage } from './feudebateau';

@NgModule({
  declarations: [
    FeudebateauPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudebateauPage),
  ],
})
export class FeudebateauPageModule {}
