import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxBateauAccesComponent } from '../../components/feux-bateau/feux-bateau-acces/feux-bateau-acces';
import { FeuxBateauActionComponent } from '../../components/feux-bateau/feux-bateau-action/feux-bateau-action';
import { FeuxBateauAttentionComponent } from '../../components/feux-bateau/feux-bateau-attention/feux-bateau-attention';
import { FeuxBateauInterditComponent } from '../../components/feux-bateau/feux-bateau-interdit/feux-bateau-interdit';
import { FeuxBateauRisquesComponent } from '../../components/feux-bateau/feux-bateau-risques/feux-bateau-risques';
import { FeuxBateauStrategieComponent } from '../../components/feux-bateau/feux-bateau-strategie/feux-bateau-strategie';

/**
 * Generated class for the Feudebateau7Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudebateau',
  templateUrl: 'feudebateau.html',
})
export class FeudebateauPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Feudebateau7Page');
  }
  tab1Root=FeuxBateauRisquesComponent;
  tab2Root=FeuxBateauStrategieComponent;
  tab3Root=FeuxBateauAccesComponent;
  tab4Root=FeuxBateauActionComponent;
  tab5Root=FeuxBateauAttentionComponent;
  tab6Root=FeuxBateauInterditComponent;
}
