import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementPiecefacialePage } from './gnr-ari-equipement-piecefaciale';

@NgModule({
  declarations: [
    GnrAriEquipementPiecefacialePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementPiecefacialePage),
  ],
})
export class GnrAriEquipementPiecefacialePageModule {}
