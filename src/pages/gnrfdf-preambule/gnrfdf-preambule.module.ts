import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfPreambulePage } from './gnrfdf-preambule';

@NgModule({
  declarations: [
    GnrfdfPreambulePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfPreambulePage),
  ],
})
export class GnrfdfPreambulePageModule {}
