import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapHemorragieSignesComponent } from '../../components/sap-hemorragie/sap-hemorragie-signes/sap-hemorragie-signes';
import { SapHemorragieBilansComponent } from '../../components/sap-hemorragie/sap-hemorragie-bilans/sap-hemorragie-bilans';
import { SapHemorragieActionsComponent } from '../../components/sap-hemorragie/sap-hemorragie-actions/sap-hemorragie-actions';
import { SapHemorragieImportantComponent } from '../../components/sap-hemorragie/sap-hemorragie-important/sap-hemorragie-important';

/**
 * Generated class for the HemorragiePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hemorragie',
  templateUrl: 'hemorragie.html',
})
export class HemorragiePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HemorragiePage');
  }
  tab7Root=SapHemorragieSignesComponent;
  tab8Root=SapHemorragieBilansComponent;
  tab9Root=SapHemorragieActionsComponent;
  tab10Root=SapHemorragieImportantComponent;
}
