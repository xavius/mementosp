import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HemorragiePage } from './hemorragie';

@NgModule({
  declarations: [
    HemorragiePage,
  ],
  imports: [
    IonicPageModule.forChild(HemorragiePage),
  ],
})
export class HemorragiePageModule {}
