import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuaeronefPage } from './feuaeronef';

@NgModule({
  declarations: [
    FeuaeronefPage,
  ],
  imports: [
    IonicPageModule.forChild(FeuaeronefPage),
  ],
})
export class FeuaeronefPageModule {}
