import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxAeronefAccesComponent } from '../../components/feux-aeronef/feux-aeronef-acces/feux-aeronef-acces';
import { FeuxAeronefActionComponent } from '../../components/feux-aeronef/feux-aeronef-action/feux-aeronef-action';
import { FeuxAeronefAttentionComponent } from '../../components/feux-aeronef/feux-aeronef-attention/feux-aeronef-attention';
import { FeuxAeronefInterditComponent } from '../../components/feux-aeronef/feux-aeronef-interdit/feux-aeronef-interdit';
import { FeuxAeronefRisquesComponent } from '../../components/feux-aeronef/feux-aeronef-risques/feux-aeronef-risques';
import { FeuxAeronefStrategieComponent } from '../../components/feux-aeronef/feux-aeronef-strategie/feux-aeronef-strategie';

/**
 * Generated class for the FeuaeronefPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feuaeronef',
  templateUrl: 'feuaeronef.html',
})
export class FeuaeronefPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuaeronefPage');
  }
  tab1Root=FeuxAeronefRisquesComponent;
  tab2Root=FeuxAeronefStrategieComponent;
  tab3Root=FeuxAeronefAccesComponent;
  tab4Root=FeuxAeronefActionComponent;
  tab5Root=FeuxAeronefAttentionComponent;
  tab6Root=FeuxAeronefInterditComponent;
}
