import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OedemepoumonPage } from './oedemepoumon';

@NgModule({
  declarations: [
    OedemepoumonPage,
  ],
  imports: [
    IonicPageModule.forChild(OedemepoumonPage),
  ],
})
export class OedemepoumonPageModule {}
