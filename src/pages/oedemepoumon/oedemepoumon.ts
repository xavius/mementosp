import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapOedemepoumonSignesComponent } from '../../components/sap-oedemepoumon/sap-oedemepoumon-signes/sap-oedemepoumon-signes';
import { SapOedemepoumonBilansComponent } from '../../components/sap-oedemepoumon/sap-oedemepoumon-bilans/sap-oedemepoumon-bilans';
import { SapOedemepoumonActionsComponent } from '../../components/sap-oedemepoumon/sap-oedemepoumon-actions/sap-oedemepoumon-actions';
import { SapOedemepoumonImportantComponent } from '../../components/sap-oedemepoumon/sap-oedemepoumon-important/sap-oedemepoumon-important';

/**
 * Generated class for the OedemepoumonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-oedemepoumon',
  templateUrl: 'oedemepoumon.html',
})
export class OedemepoumonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OedemepoumonPage');
  }
  tab7Root=SapOedemepoumonSignesComponent;
  tab8Root=SapOedemepoumonBilansComponent;
  tab9Root=SapOedemepoumonActionsComponent;
  tab10Root=SapOedemepoumonImportantComponent;
}
