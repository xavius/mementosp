import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeSyntheseApparitionPage } from './gnrege-synthese-apparition';

@NgModule({
  declarations: [
    GnregeSyntheseApparitionPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeSyntheseApparitionPage),
  ],
})
export class GnregeSyntheseApparitionPageModule {}
