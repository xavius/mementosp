import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementHarnaisPage } from './gnr-ari-equipement-harnais';

@NgModule({
  declarations: [
    GnrAriEquipementHarnaisPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementHarnaisPage),
  ],
})
export class GnrAriEquipementHarnaisPageModule {}
