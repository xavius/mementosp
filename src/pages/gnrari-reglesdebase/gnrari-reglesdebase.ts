import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrAriReglesdebase_1Component } from '../../components/gnr-ari-reglesdebase/gnr-ari-reglesdebase-1/gnr-ari-reglesdebase-1';
import { GnrAriReglesdebase_2Component } from '../../components/gnr-ari-reglesdebase/gnr-ari-reglesdebase-2/gnr-ari-reglesdebase-2';
import { GnrAriReglesdebase_3Component } from '../../components/gnr-ari-reglesdebase/gnr-ari-reglesdebase-3/gnr-ari-reglesdebase-3';

/**
 * Generated class for the GnrariReglesdebasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrari-reglesdebase',
  templateUrl: 'gnrari-reglesdebase.html',
})
export class GnrariReglesdebasePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrariReglesdebasePage');
  }
  tab22Root=GnrAriReglesdebase_1Component;
  tab23Root=GnrAriReglesdebase_2Component;
  tab24Root=GnrAriReglesdebase_3Component;
}
