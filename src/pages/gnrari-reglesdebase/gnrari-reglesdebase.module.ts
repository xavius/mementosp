import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrariReglesdebasePage } from './gnrari-reglesdebase';

@NgModule({
  declarations: [
    GnrariReglesdebasePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrariReglesdebasePage),
  ],
})
export class GnrariReglesdebasePageModule {}
