import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudetransformateurPage } from './feudetransformateur';

@NgModule({
  declarations: [
    FeudetransformateurPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudetransformateurPage),
  ],
})
export class FeudetransformateurPageModule {}
