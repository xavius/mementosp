import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxTransformateurAccesComponent } from '../../components/feux-transformateur/feux-transformateur-acces/feux-transformateur-acces';
import { FeuxTransformateurActionComponent } from '../../components/feux-transformateur/feux-transformateur-action/feux-transformateur-action';
import { FeuxTransformateurAttentionComponent } from '../../components/feux-transformateur/feux-transformateur-attention/feux-transformateur-attention';
import { FeuxTransformateurInterditComponent } from '../../components/feux-transformateur/feux-transformateur-interdit/feux-transformateur-interdit';
import { FeuxTransformateurRisquesComponent } from '../../components/feux-transformateur/feux-transformateur-risques/feux-transformateur-risques';
import { FeuxTransformateurStrategieComponent } from '../../components/feux-transformateur/feux-transformateur-strategie/feux-transformateur-strategie';

/**
 * Generated class for the FeudetransformateurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudetransformateur',
  templateUrl: 'feudetransformateur.html',
})
export class FeudetransformateurPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudetransformateurPage');
  }
  tab1Root=FeuxTransformateurRisquesComponent;
  tab2Root=FeuxTransformateurStrategieComponent;
  tab3Root=FeuxTransformateurAccesComponent;
  tab4Root=FeuxTransformateurActionComponent;
  tab5Root=FeuxTransformateurAttentionComponent;
  tab6Root=FeuxTransformateurInterditComponent;

}
