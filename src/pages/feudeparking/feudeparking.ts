import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxParkingAccesComponent } from '../../components/feux-parking/feux-parking-acces/feux-parking-acces';
import { FeuxParkingActionComponent } from '../../components/feux-parking/feux-parking-action/feux-parking-action';
import { FeuxParkingAttentionComponent } from '../../components/feux-parking/feux-parking-attention/feux-parking-attention';
import { FeuxParkingInterditComponent } from '../../components/feux-parking/feux-parking-interdit/feux-parking-interdit';
import { FeuxParkingRisquesComponent } from '../../components/feux-parking/feux-parking-risques/feux-parking-risques';
import { FeuxParkingStrategieComponent } from '../../components/feux-parking/feux-parking-strategie/feux-parking-strategie';

/**
 * Generated class for the FeudeparkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudeparking',
  templateUrl: 'feudeparking.html',
})
export class FeudeparkingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudeparkingPage');
  }
  tab1Root=FeuxParkingRisquesComponent;
  tab2Root=FeuxParkingStrategieComponent;
  tab3Root=FeuxParkingAccesComponent;
  tab4Root=FeuxParkingActionComponent;
  tab5Root=FeuxParkingAttentionComponent;
  tab6Root=FeuxParkingInterditComponent;
}
