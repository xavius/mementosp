import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudeparkingPage } from './feudeparking';

@NgModule({
  declarations: [
    FeudeparkingPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudeparkingPage),
  ],
})
export class FeudeparkingPageModule {}
