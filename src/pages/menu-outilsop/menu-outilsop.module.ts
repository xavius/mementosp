import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuOutilsopPage } from './menu-outilsop';

@NgModule({
  declarations: [
    MenuOutilsopPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuOutilsopPage),
  ],
})
export class MenuOutilsopPageModule {}
