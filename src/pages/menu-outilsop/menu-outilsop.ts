import { OutilsopAricoPage } from './../outilsop-arico/outilsop-arico';
import { OutilsopO2Page } from './../outilsop-o2/outilsop-o2';
import { OutilsopExplosimetriePage } from './../outilsop-explosimetrie/outilsop-explosimetrie';
import { OutilsopLifPage } from './../outilsop-lif/outilsop-lif';
import { OutilsopHydroliquePage } from './../outilsop-hydrolique/outilsop-hydrolique';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MenuOutilsopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-outilsop',
  templateUrl: 'menu-outilsop.html',
})
export class MenuOutilsopPage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  list3: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Calcul Hydrauliques', component: OutilsopHydroliquePage},
      {title:'Calcul L.I.F', component: OutilsopLifPage},
      {title:'Autonomie ARICO', component: OutilsopAricoPage},
      {title:'Abaques Explosimétre', component: OutilsopExplosimetriePage}
            //** */{title:'Procédure PGR', component: },*/

    ];
    this.list2 = [
      {title:'Durée d une bouteille O2', component: OutilsopO2Page},
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuOutilsopPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
