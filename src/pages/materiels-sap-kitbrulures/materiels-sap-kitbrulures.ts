import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapKitbruluresDescriptionComponent } from '../../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-description/materiels-sap-kitbrulures-description';
import { MaterielsSapKitbruluresIndicationsComponent } from '../../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-indications/materiels-sap-kitbrulures-indications';
import { MaterielsSapKitbruluresRisquesComponent } from '../../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-risques/materiels-sap-kitbrulures-risques';
import { MaterielsSapKitbruluresUtilisationComponent } from '../../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-utilisation/materiels-sap-kitbrulures-utilisation';
import { MaterielsSapKitbruluresPointsclesComponent } from '../../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-pointscles/materiels-sap-kitbrulures-pointscles';


/**
 * Generated class for the MaterielsSapKitbruluresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-kitbrulures',
  templateUrl: 'materiels-sap-kitbrulures.html',
})
export class MaterielsSapKitbruluresPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapKitbruluresPage');
  }
  tab65Root=MaterielsSapKitbruluresDescriptionComponent;
  tab66Root=MaterielsSapKitbruluresIndicationsComponent;
  tab67Root=MaterielsSapKitbruluresRisquesComponent;
  tab68Root=MaterielsSapKitbruluresUtilisationComponent;
  tab69Root=MaterielsSapKitbruluresPointsclesComponent;
}
