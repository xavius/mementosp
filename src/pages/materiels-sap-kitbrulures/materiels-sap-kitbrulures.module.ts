import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapKitbruluresPage } from './materiels-sap-kitbrulures';

@NgModule({
  declarations: [
    MaterielsSapKitbruluresPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapKitbruluresPage),
  ],
})
export class MaterielsSapKitbruluresPageModule {}
