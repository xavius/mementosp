import { EmailComposer } from '@ionic-native/email-composer';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  subject='';
  body='';
  to='';

  constructor(public navCtrl: NavController, public navParams: NavParams, private emailComposer: EmailComposer) {

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }
  /**send() {
    let email = {
      to: this.to,
      cc: [],
      bcc:[],
      attachements:[],
      subject: this.subject,
      body: this.subject,
      isHtml: true,

    }
    this.emailComposer.open(email);
  }*/
}
