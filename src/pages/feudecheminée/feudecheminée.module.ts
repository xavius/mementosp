import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudecheminéePage } from './feudecheminée';

@NgModule({
  declarations: [
    FeudecheminéePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudecheminéePage),
  ],
})
export class FeudecheminéePageModule {}
