import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutilsopExplosimetriePage } from './outilsop-explosimetrie';

@NgModule({
  declarations: [
    OutilsopExplosimetriePage,
  ],
  imports: [
    IonicPageModule.forChild(OutilsopExplosimetriePage),
  ],
})
export class OutilsopExplosimetriePageModule {}
