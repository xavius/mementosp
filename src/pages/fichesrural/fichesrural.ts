import { FeudengraisPage } from './../feudengrais/feudengrais';
import { FeuderecoltePage } from './../feuderecolte/feuderecolte';
import { FeudecentreequestrePage } from './../feudecentreequestre/feudecentreequestre';
import { FeudefermePage } from './../feudeferme/feudeferme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FichesruralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichesrural',
  templateUrl: 'fichesrural.html',
})
export class FichesruralPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Feu de Ferme', component: FeudefermePage},
      {title:'Feu de Centre Equestre', component: FeudecentreequestrePage},
      {title:'Feu de Récolte', component: FeuderecoltePage},
      {title:'Feu d\'Engrais', component: FeudengraisPage}
      
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichesbatimentsPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
  }

}
