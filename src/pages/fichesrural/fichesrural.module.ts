import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichesruralPage } from './fichesrural';

@NgModule({
  declarations: [
    FichesruralPage,
  ],
  imports: [
    IonicPageModule.forChild(FichesruralPage),
  ],
})
export class FichesruralPageModule {}
