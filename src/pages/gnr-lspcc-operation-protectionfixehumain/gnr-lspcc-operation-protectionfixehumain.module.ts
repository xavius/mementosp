import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationProtectionfixehumainPage } from './gnr-lspcc-operation-protectionfixehumain';

@NgModule({
  declarations: [
    GnrLspccOperationProtectionfixehumainPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationProtectionfixehumainPage),
  ],
})
export class GnrLspccOperationProtectionfixehumainPageModule {}
