import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccOperationProtectionfixehumainChefdagresComponent } from '../../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-chefdagres/gnr-lspcc-operation-protectionfixehumain-chefdagres';
import { GnrLspccOperationProtectionfixehumainChefdequipeComponent } from '../../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-chefdequipe/gnr-lspcc-operation-protectionfixehumain-chefdequipe';
import { GnrLspccOperationProtectionfixehumainEquipierComponent } from '../../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-equipier/gnr-lspcc-operation-protectionfixehumain-equipier';
import { GnrLspccOperationProtectionfixehumain_2ebinomeComponent } from '../../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-2ebinome/gnr-lspcc-operation-protectionfixehumain-2ebinome';

/**
 * Generated class for the GnrLspccOperationProtectionfixehumainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-protectionfixehumain',
  templateUrl: 'gnr-lspcc-operation-protectionfixehumain.html',
})
export class GnrLspccOperationProtectionfixehumainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationProtectionfixehumainPage');
  }
  tab61Root=GnrLspccOperationProtectionfixehumainChefdagresComponent;
  tab62Root=GnrLspccOperationProtectionfixehumainChefdequipeComponent;
  tab63Root=GnrLspccOperationProtectionfixehumainEquipierComponent;
  tab64Root=GnrLspccOperationProtectionfixehumain_2ebinomeComponent;
}
