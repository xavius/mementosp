import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeExplosiondefumeeParametresPage } from './gnrege-explosiondefumee-parametres';

@NgModule({
  declarations: [
    GnregeExplosiondefumeeParametresPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeExplosiondefumeeParametresPage),
  ],
})
export class GnregeExplosiondefumeeParametresPageModule {}
