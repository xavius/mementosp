import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxChaufferieindustrielleAccesComponent } from '../../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-acces/feux-chaufferieindustrielle-acces';
import { FeuxChaufferieindustrielleActionComponent } from '../../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-action/feux-chaufferieindustrielle-action';
import { FeuxChaufferieindustrielleAttentionComponent } from '../../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-attention/feux-chaufferieindustrielle-attention';
import { FeuxChaufferieindustrielleInterditComponent } from '../../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-interdit/feux-chaufferieindustrielle-interdit';
import { FeuxChaufferieindustrielleRisquesComponent } from '../../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-risques/feux-chaufferieindustrielle-risques';
import { FeuxChaufferieindustrielleStrategieComponent } from '../../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-strategie/feux-chaufferieindustrielle-strategie';

/**
 * Generated class for the FeudechaufferieindustriellePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudechaufferieindustrielle',
  templateUrl: 'feudechaufferieindustrielle.html',
})
export class FeudechaufferieindustriellePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudechaufferieindustriellePage');
  }
  tab1Root=FeuxChaufferieindustrielleRisquesComponent;
  tab2Root=FeuxChaufferieindustrielleStrategieComponent;
  tab3Root=FeuxChaufferieindustrielleAccesComponent;
  tab4Root=FeuxChaufferieindustrielleActionComponent;
  tab5Root=FeuxChaufferieindustrielleAttentionComponent;
  tab6Root=FeuxChaufferieindustrielleInterditComponent;
}
