import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudechaufferieindustriellePage } from './feudechaufferieindustrielle';

@NgModule({
  declarations: [
    FeudechaufferieindustriellePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudechaufferieindustriellePage),
  ],
})
export class FeudechaufferieindustriellePageModule {}
