import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevClassementdesbatimentsErpComponent } from '../../components/prev-classementdesbatiments/prev-classementdesbatiments-erp/prev-classementdesbatiments-erp';
import { PrevClassementdesbatimentsIghComponent } from '../../components/prev-classementdesbatiments/prev-classementdesbatiments-igh/prev-classementdesbatiments-igh';
import { PrevClassementdesbatimentsHabitationComponent } from '../../components/prev-classementdesbatiments/prev-classementdesbatiments-habitation/prev-classementdesbatiments-habitation';
import { PrevClassementdesbatimentsTravailComponent } from '../../components/prev-classementdesbatiments/prev-classementdesbatiments-travail/prev-classementdesbatiments-travail';

/**
 * Generated class for the PrevclassementdesbatimentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevclassementdesbatiments',
  templateUrl: 'prevclassementdesbatiments.html',
})
export class PrevclassementdesbatimentsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevclassementdesbatimentsPage');
  }
  tab42Root=PrevClassementdesbatimentsErpComponent;
  tab43Root=PrevClassementdesbatimentsIghComponent;
  tab44Root=PrevClassementdesbatimentsHabitationComponent;
  tab45Root=PrevClassementdesbatimentsTravailComponent;
}
