import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevclassementdesbatimentsPage } from './prevclassementdesbatiments';

@NgModule({
  declarations: [
    PrevclassementdesbatimentsPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevclassementdesbatimentsPage),
  ],
})
export class PrevclassementdesbatimentsPageModule {}
