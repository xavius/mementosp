import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrAriContraintesCorporelComponent } from '../../components/gnr-ari-contraintes/gnr-ari-contraintes-corporel/gnr-ari-contraintes-corporel';
import { GnrAriContraintesSensorielComponent } from '../../components/gnr-ari-contraintes/gnr-ari-contraintes-sensoriel/gnr-ari-contraintes-sensoriel';
import { GnrAriContraintesRelationComponent } from '../../components/gnr-ari-contraintes/gnr-ari-contraintes-relation/gnr-ari-contraintes-relation';

/**
 * Generated class for the GnrAriContraintesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-contraintes',
  templateUrl: 'gnr-ari-contraintes.html',
})
export class GnrAriContraintesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriContraintesPage');
  }
  tab56Root= GnrAriContraintesCorporelComponent;
  tab57Root= GnrAriContraintesSensorielComponent;
  tab58Root= GnrAriContraintesRelationComponent;

}
