import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriContraintesPage } from './gnr-ari-contraintes';

@NgModule({
  declarations: [
    GnrAriContraintesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriContraintesPage),
  ],
})
export class GnrAriContraintesPageModule {}
