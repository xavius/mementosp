import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesReglesdebasePage } from './gnretablissementlances-reglesdebase';

@NgModule({
  declarations: [
    GnretablissementlancesReglesdebasePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesReglesdebasePage),
  ],
})
export class GnretablissementlancesReglesdebasePageModule {}
