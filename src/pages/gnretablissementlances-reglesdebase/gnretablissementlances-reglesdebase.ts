import { GnretablissementlancesReglesdebaseDevoirsdubinomePage } from './../gnretablissementlances-reglesdebase-devoirsdubinome/gnretablissementlances-reglesdebase-devoirsdubinome';
import { GnretablissementlancesReglesdebaseTypedetablissementPage } from './../gnretablissementlances-reglesdebase-typedetablissement/gnretablissementlances-reglesdebase-typedetablissement';
import { GnretablissementlancesReglesdebaseMaterieldebasePage } from './../gnretablissementlances-reglesdebase-materieldebase/gnretablissementlances-reglesdebase-materieldebase';
import { GnretablissementlancesReglesdebasePrisedeauPage } from './../gnretablissementlances-reglesdebase-prisedeau/gnretablissementlances-reglesdebase-prisedeau';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesReglesdebasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-reglesdebase',
  templateUrl: 'gnretablissementlances-reglesdebase.html',
})
export class GnretablissementlancesReglesdebasePage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'La Prise d Eau', component: GnretablissementlancesReglesdebasePrisedeauPage},
      {title:'Le Matériel de Base', component: GnretablissementlancesReglesdebaseMaterieldebasePage},
      {title:'Les Types d Etablissement', component: GnretablissementlancesReglesdebaseTypedetablissementPage},
      {title:'Précautions et Devoirs du Binôme', component: GnretablissementlancesReglesdebaseDevoirsdubinomePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesReglesdebasePage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
