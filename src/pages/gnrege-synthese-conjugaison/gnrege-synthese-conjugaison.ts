import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeSyntheseConjugaisonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-synthese-conjugaison',
  templateUrl: 'gnrege-synthese-conjugaison.html',
})
export class GnregeSyntheseConjugaisonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeSyntheseConjugaisonPage');
  }

}
