import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeSyntheseConjugaisonPage } from './gnrege-synthese-conjugaison';

@NgModule({
  declarations: [
    GnregeSyntheseConjugaisonPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeSyntheseConjugaisonPage),
  ],
})
export class GnregeSyntheseConjugaisonPageModule {}
