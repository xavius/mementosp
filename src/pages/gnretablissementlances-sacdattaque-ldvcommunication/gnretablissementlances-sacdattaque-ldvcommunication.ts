import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesSacdattaqueLdvcommunicationChefdagresComponent } from '../../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-chefdagres/gnretablissementlances-sacdattaque-ldvcommunication-chefdagres';
import { GnretablissementlancesSacdattaqueLdvcommunicationChefdequipeComponent } from '../../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-chefdequipe/gnretablissementlances-sacdattaque-ldvcommunication-chefdequipe';
import { GnretablissementlancesSacdattaqueLdvcommunicationEquipierComponent } from '../../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-equipier/gnretablissementlances-sacdattaque-ldvcommunication-equipier';
import { GnretablissementlancesSacdattaqueLdvcommunicationBalComponent } from '../../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-bal/gnretablissementlances-sacdattaque-ldvcommunication-bal';

/**
 * Generated class for the GnretablissementlancesSacdattaqueLdvcommunicationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-sacdattaque-ldvcommunication',
  templateUrl: 'gnretablissementlances-sacdattaque-ldvcommunication.html',
})
export class GnretablissementlancesSacdattaqueLdvcommunicationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesSacdattaqueLdvcommunicationPage');
  }
  tab61Root=GnretablissementlancesSacdattaqueLdvcommunicationChefdagresComponent;
  tab62Root=GnretablissementlancesSacdattaqueLdvcommunicationChefdequipeComponent;
  tab63Root=GnretablissementlancesSacdattaqueLdvcommunicationEquipierComponent;
  tab64Root=GnretablissementlancesSacdattaqueLdvcommunicationBalComponent;
}
