import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesSacdattaqueLdvcommunicationPage } from './gnretablissementlances-sacdattaque-ldvcommunication';

@NgModule({
  declarations: [
    GnretablissementlancesSacdattaqueLdvcommunicationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesSacdattaqueLdvcommunicationPage),
  ],
})
export class GnretablissementlancesSacdattaqueLdvcommunicationPageModule {}
