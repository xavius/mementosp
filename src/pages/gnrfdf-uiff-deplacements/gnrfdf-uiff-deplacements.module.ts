import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfUiffDeplacementsPage } from './gnrfdf-uiff-deplacements';

@NgModule({
  declarations: [
    GnrfdfUiffDeplacementsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfUiffDeplacementsPage),
  ],
})
export class GnrfdfUiffDeplacementsPageModule {}
