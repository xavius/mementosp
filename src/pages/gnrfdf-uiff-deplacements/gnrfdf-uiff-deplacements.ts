import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrfdfUiffDeplacementsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrfdf-uiff-deplacements',
  templateUrl: 'gnrfdf-uiff-deplacements.html',
})
export class GnrfdfUiffDeplacementsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrfdfUiffDeplacementsPage');
  }

}
