import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriAugmentationPoidsPage } from './gnr-ari-augmentation-poids';

@NgModule({
  declarations: [
    GnrAriAugmentationPoidsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriAugmentationPoidsPage),
  ],
})
export class GnrAriAugmentationPoidsPageModule {}
