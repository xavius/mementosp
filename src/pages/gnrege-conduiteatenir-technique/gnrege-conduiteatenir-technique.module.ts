import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeConduiteatenirTechniquePage } from './gnrege-conduiteatenir-technique';

@NgModule({
  declarations: [
    GnregeConduiteatenirTechniquePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeConduiteatenirTechniquePage),
  ],
})
export class GnregeConduiteatenirTechniquePageModule {}
