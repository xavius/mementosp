import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationPage } from './gnr-lspcc-operation-sauvetageexcavation';

@NgModule({
  declarations: [
    GnrLspccOperationSauvetageexcavationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationSauvetageexcavationPage),
  ],
})
export class GnrLspccOperationSauvetageexcavationPageModule {}
