import { GnrLspccOperationSauvetageexcavationIllustrationPage } from './../gnr-lspcc-operation-sauvetageexcavation-illustration/gnr-lspcc-operation-sauvetageexcavation-illustration';
import { GnrLspccOperationSauvetageexcavationRemontePage } from './../gnr-lspcc-operation-sauvetageexcavation-remonte/gnr-lspcc-operation-sauvetageexcavation-remonte';
import { GnrLspccOperationSauvetageexcavationExplorationPage } from './../gnr-lspcc-operation-sauvetageexcavation-exploration/gnr-lspcc-operation-sauvetageexcavation-exploration';
import { GnrLspccOperationSauvetageexcavationChefdagresPage } from './../gnr-lspcc-operation-sauvetageexcavation-chefdagres/gnr-lspcc-operation-sauvetageexcavation-chefdagres';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-sauvetageexcavation',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation.html',
})
export class GnrLspccOperationSauvetageexcavationPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Rôle du Chef d Agrès', component: GnrLspccOperationSauvetageexcavationChefdagresPage},
      {title:'Binôme d Exploration', component: GnrLspccOperationSauvetageexcavationExplorationPage},
      {title:'Binôme de Remontée', component: GnrLspccOperationSauvetageexcavationRemontePage},
      {title:'Illustration', component: GnrLspccOperationSauvetageexcavationIllustrationPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationSauvetageexcavationPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
