import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriEquipementLigneguidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-equipement-ligneguide',
  templateUrl: 'gnr-ari-equipement-ligneguide.html',
})
export class GnrAriEquipementLigneguidePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriEquipementLigneguidePage');
  }

}
