import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementLigneguidePage } from './gnr-ari-equipement-ligneguide';

@NgModule({
  declarations: [
    GnrAriEquipementLigneguidePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementLigneguidePage),
  ],
})
export class GnrAriEquipementLigneguidePageModule {}
