import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationIllustrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-sauvetageexcavation-illustration',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-illustration.html',
})
export class GnrLspccOperationSauvetageexcavationIllustrationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationSauvetageexcavationIllustrationPage');
  }

}
