import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationIllustrationPage } from './gnr-lspcc-operation-sauvetageexcavation-illustration';

@NgModule({
  declarations: [
    GnrLspccOperationSauvetageexcavationIllustrationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationSauvetageexcavationIllustrationPage),
  ],
})
export class GnrLspccOperationSauvetageexcavationIllustrationPageModule {}
