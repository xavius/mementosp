import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuGnrPage } from './menu-gnr';

@NgModule({
  declarations: [
    MenuGnrPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuGnrPage),
  ],
})
export class MenuGnrPageModule {}
