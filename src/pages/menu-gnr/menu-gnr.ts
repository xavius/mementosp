import { GdoPreventionrisquesdetoxicitePage } from './../gdo-preventionrisquesdetoxicite/gdo-preventionrisquesdetoxicite';
import { GdoExerciceducommandementPage } from './../gdo-exerciceducommandement/gdo-exerciceducommandement';
import { GdoVentilationoperationnellePage } from './../gdo-ventilationoperationnelle/gdo-ventilationoperationnelle';
import { GdoEtablissementsettechniquesdextinctionPage } from './../gdo-etablissementsettechniquesdextinction/gdo-etablissementsettechniquesdextinction';
import { GdoIcendiesdestructuresPage } from './../gdo-icendiesdestructures/gdo-icendiesdestructures';
import { GnrfdfPage } from './../gnrfdf/gnrfdf';
import { GnregePage } from './../gnrege/gnrege';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrariPage } from '../gnrari/gnrari';
import { GnrlspccPage } from '../gnrlspcc/gnrlspcc';
import { GnretablissementlancesPage } from '../gnretablissementlances/gnretablissementlances';


/**
 * Generated class for the MenuGnrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-gnr',
  templateUrl: 'menu-gnr.html',
})
export class MenuGnrPage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'A.R.I', component: GnrariPage},
      {title:'LSPCC', component: GnrlspccPage},
      {title:'ETABLISSEMENT LANCES', component: GnretablissementlancesPage},
      {title:'Explosion - Embrassement des fumées', component: GnregePage},
      {title:'F.D.F', component: GnrfdfPage}
    ];
    this.list2 = [
      {title:'Incendies de Structures', component: GdoIcendiesdestructuresPage},
      {title:'Etablissements et Techniques d Extinction', component: GdoEtablissementsettechniquesdextinctionPage},
      {title:'Ventilation Opérationnelle', component: GdoVentilationoperationnellePage},
      {title:'Exercice du commandement et conduite des opérations', component: GdoExerciceducommandementPage},
      {title:'Prévention contre les risques de toxicité liés aux fumées d incendie', component: GdoPreventionrisquesdetoxicitePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuGnrPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
