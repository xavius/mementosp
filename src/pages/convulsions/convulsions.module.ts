import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConvulsionsPage } from './convulsions';

@NgModule({
  declarations: [
    ConvulsionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConvulsionsPage),
  ],
})
export class ConvulsionsPageModule {}
