import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapConvulsionsSignesComponent } from '../../components/sap-convulsions/sap-convulsions-signes/sap-convulsions-signes';
import { SapConvulsionsBilansComponent } from '../../components/sap-convulsions/sap-convulsions-bilans/sap-convulsions-bilans';
import { SapConvulsionsActionsComponent } from '../../components/sap-convulsions/sap-convulsions-actions/sap-convulsions-actions';
import { SapConvulsionsImportantComponent } from '../../components/sap-convulsions/sap-convulsions-important/sap-convulsions-important';

/**
 * Generated class for the ConvulsionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-convulsions',
  templateUrl: 'convulsions.html',
})
export class ConvulsionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConvulsionsPage');
  }
  tab7Root=SapConvulsionsSignesComponent;
  tab8Root=SapConvulsionsBilansComponent;
  tab9Root=SapConvulsionsActionsComponent;
  tab10Root=SapConvulsionsImportantComponent;
}
