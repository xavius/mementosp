import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM1ChefdagresComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-chefdagres/gnretablissementlances-manoeuvres-gnr-m1-chefdagres';
import { GnretablissementlancesManoeuvresGnrM1ChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-chefdequipe/gnretablissementlances-manoeuvres-gnr-m1-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM1EquipierComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-equipier/gnretablissementlances-manoeuvres-gnr-m1-equipier';
import { GnretablissementlancesManoeuvresGnrM1BalComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-bal/gnretablissementlances-manoeuvres-gnr-m1-bal';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrM1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-m1',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-m1.html',
})
export class GnretablissementlancesManoeuvresGnrM1Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrM1Page');
  }
  tab61Root=GnretablissementlancesManoeuvresGnrM1ChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvresGnrM1ChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvresGnrM1EquipierComponent;
  tab64Root=GnretablissementlancesManoeuvresGnrM1BalComponent;
}
