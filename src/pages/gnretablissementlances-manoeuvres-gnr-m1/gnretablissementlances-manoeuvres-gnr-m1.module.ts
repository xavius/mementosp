import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM1Page } from './gnretablissementlances-manoeuvres-gnr-m1';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrM1Page,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrM1Page),
  ],
})
export class GnretablissementlancesManoeuvresGnrM1PageModule {}
