import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NrbcRtnEtiquettesdedanger2AnciensPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nrbc-rtn-etiquettesdedanger2-anciens',
  templateUrl: 'nrbc-rtn-etiquettesdedanger2-anciens.html',
})
export class NrbcRtnEtiquettesdedanger2AnciensPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NrbcRtnEtiquettesdedanger2AnciensPage');
  }

}
