import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NrbcRtnEtiquettesdedanger2AnciensPage } from './nrbc-rtn-etiquettesdedanger2-anciens';

@NgModule({
  declarations: [
    NrbcRtnEtiquettesdedanger2AnciensPage,
  ],
  imports: [
    IonicPageModule.forChild(NrbcRtnEtiquettesdedanger2AnciensPage),
  ],
})
export class NrbcRtnEtiquettesdedanger2AnciensPageModule {}
