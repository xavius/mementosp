import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapGarrotDescriptionComponent } from '../../components/materiels-sap-garrot/materiels-sap-garrot-description/materiels-sap-garrot-description';
import { MaterielsSapGarrotIndicationsComponent } from '../../components/materiels-sap-garrot/materiels-sap-garrot-indications/materiels-sap-garrot-indications';
import { MaterielsSapGarrotRisquesComponent } from '../../components/materiels-sap-garrot/materiels-sap-garrot-risques/materiels-sap-garrot-risques';
import { MaterielsSapGarrotUtilisationComponent } from '../../components/materiels-sap-garrot/materiels-sap-garrot-utilisation/materiels-sap-garrot-utilisation';
import { MaterielsSapGarrotPointsclesComponent } from '../../components/materiels-sap-garrot/materiels-sap-garrot-pointscles/materiels-sap-garrot-pointscles';
import { MaterielsSapGarrotEfficaciteComponent } from '../../components/materiels-sap-garrot/materiels-sap-garrot-efficacite/materiels-sap-garrot-efficacite';


/**
 * Generated class for the MaterielsSapGarrotarterielPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-garrotarteriel',
  templateUrl: 'materiels-sap-garrotarteriel.html',
})
export class MaterielsSapGarrotarterielPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapGarrotarterielPage');
  }
  tab65Root=MaterielsSapGarrotDescriptionComponent;
  tab66Root=MaterielsSapGarrotIndicationsComponent;
  tab67Root=MaterielsSapGarrotRisquesComponent;
  tab68Root=MaterielsSapGarrotUtilisationComponent;
  tab69Root=MaterielsSapGarrotPointsclesComponent;
  tab70Root=MaterielsSapGarrotEfficaciteComponent;
}
