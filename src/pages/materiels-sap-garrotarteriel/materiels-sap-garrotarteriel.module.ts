import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapGarrotarterielPage } from './materiels-sap-garrotarteriel';

@NgModule({
  declarations: [
    MaterielsSapGarrotarterielPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapGarrotarterielPage),
  ],
})
export class MaterielsSapGarrotarterielPageModule {}
