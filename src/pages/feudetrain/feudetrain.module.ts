import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudetrainPage } from './feudetrain';

@NgModule({
  declarations: [
    FeudetrainPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudetrainPage),
  ],
})
export class FeudetrainPageModule {}
