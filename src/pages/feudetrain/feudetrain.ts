import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxTrainAccesComponent } from '../../components/feux-train/feux-train-acces/feux-train-acces';
import { FeuxTrainActionComponent } from '../../components/feux-train/feux-train-action/feux-train-action';
import { FeuxTrainAttentionComponent } from '../../components/feux-train/feux-train-attention/feux-train-attention';
import { FeuxTrainInterditComponent } from '../../components/feux-train/feux-train-interdit/feux-train-interdit';
import { FeuxTrainRisquesComponent } from '../../components/feux-train/feux-train-risques/feux-train-risques';
import { FeuxTrainStrategieComponent } from '../../components/feux-train/feux-train-strategie/feux-train-strategie';

/**
 * Generated class for the FeudetrainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudetrain',
  templateUrl: 'feudetrain.html',
})
export class FeudetrainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudetrainPage');
  }
  tab1Root=FeuxTrainRisquesComponent;
  tab2Root=FeuxTrainStrategieComponent;
  tab3Root=FeuxTrainAccesComponent;
  tab4Root=FeuxTrainActionComponent;
  tab5Root=FeuxTrainAttentionComponent;
  tab6Root=FeuxTrainInterditComponent;
}
