import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationpendantControleurPage } from './gnr-ari-operationpendant-controleur';

@NgModule({
  declarations: [
    GnrAriOperationpendantControleurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationpendantControleurPage),
  ],
})
export class GnrAriOperationpendantControleurPageModule {}
