import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfMoyensTerrestresPage } from './gnrfdf-moyens-terrestres';

@NgModule({
  declarations: [
    GnrfdfMoyensTerrestresPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfMoyensTerrestresPage),
  ],
})
export class GnrfdfMoyensTerrestresPageModule {}
