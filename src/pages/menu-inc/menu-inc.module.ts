import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuIncPage } from './menu-inc';

@NgModule({
  declarations: [
    MenuIncPage
  ],
  imports: [
    IonicPageModule.forChild(MenuIncPage),
  ],
})
export class MenuIncPageModule {}
