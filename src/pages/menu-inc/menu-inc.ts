import { FichesautresPage } from './../fichesautres/fichesautres';
import { MgoPage } from './../mgo/mgo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FichesbatimentsPage } from '../fichesbatiments/fichesbatiments';
import { FichesruralPage } from '../fichesrural/fichesrural';
import { FichesfeuxspeciauxPage } from '../fichesfeuxspeciaux/fichesfeuxspeciaux';
import { FichesvehiculesPage } from '../fichesvehicules/fichesvehicules';

/**
 * Generated class for the MenuIncPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-inc',
  templateUrl: 'menu-inc.html',
})
export class MenuIncPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'M.G.O.', component: MgoPage},
      {title:'Fiches Reflexes "Bâtiments"', component: FichesbatimentsPage},
      {title:'Fiches Reflexes "Rural"', component: FichesruralPage},
      {title:'Fiches Reflexes "Feux Spéciaux"', component: FichesfeuxspeciauxPage},
      {title:'Fiches Reflexes "Véhicules"', component: FichesvehiculesPage},
      {title:'Fiches Reflexes "Autres"', component: FichesautresPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuIncPage');
  }

  itemSelected(item){
      this.navCtrl.push(item.component);
  }

}
