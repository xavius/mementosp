import { MaterielsSapPage } from './../materiels-sap/materiels-sap';
import { FichessrPage } from './../fichessr/fichessr';
import { FichessapPage } from './../fichessap/fichessap';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MenuSapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-sap',
  templateUrl: 'menu-sap.html',
})
export class MenuSapPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Fiches Reflexes "S.A.P"', component: FichessapPage},
      {title:'Fiches Reflexes "S.R"', component: FichessrPage},
      {title:'Matériels "S.A.P"', component: MaterielsSapPage},

    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuSapPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
}

}
