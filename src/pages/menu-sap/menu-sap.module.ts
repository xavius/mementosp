import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuSapPage } from './menu-sap';

@NgModule({
  declarations: [
    MenuSapPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuSapPage),
  ],
})
export class MenuSapPageModule {}
