import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionAnneauxcoususPage } from './gnr-lspcc-composition-anneauxcousus';

@NgModule({
  declarations: [
    GnrLspccCompositionAnneauxcoususPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionAnneauxcoususPage),
  ],
})
export class GnrLspccCompositionAnneauxcoususPageModule {}
