import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapThermometreDescriptionComponent } from '../../components/materiels-sap-thermometre/materiels-sap-thermometre-description/materiels-sap-thermometre-description';
import { MaterielsSapThermometreIndicationsComponent } from '../../components/materiels-sap-thermometre/materiels-sap-thermometre-indications/materiels-sap-thermometre-indications';
import { MaterielsSapThermometreRisquesComponent } from '../../components/materiels-sap-thermometre/materiels-sap-thermometre-risques/materiels-sap-thermometre-risques';
import { MaterielsSapThermometreUtilisationComponent } from '../../components/materiels-sap-thermometre/materiels-sap-thermometre-utilisation/materiels-sap-thermometre-utilisation';
import { MaterielsSapThermometrePointsclesComponent } from '../../components/materiels-sap-thermometre/materiels-sap-thermometre-pointscles/materiels-sap-thermometre-pointscles';
import { MaterielsSapThermometreEfficaciteComponent } from '../../components/materiels-sap-thermometre/materiels-sap-thermometre-efficacite/materiels-sap-thermometre-efficacite';


/**
 * Generated class for the MaterielsSapThermometrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-thermometre',
  templateUrl: 'materiels-sap-thermometre.html',
})
export class MaterielsSapThermometrePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapThermometrePage');
  }
  tab65Root=MaterielsSapThermometreDescriptionComponent;
  tab66Root=MaterielsSapThermometreIndicationsComponent;
  tab67Root=MaterielsSapThermometreRisquesComponent;
  tab68Root=MaterielsSapThermometreUtilisationComponent;
  tab69Root=MaterielsSapThermometrePointsclesComponent;
  tab70Root=MaterielsSapThermometreEfficaciteComponent;
}
