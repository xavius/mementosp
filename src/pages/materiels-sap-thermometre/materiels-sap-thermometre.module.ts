import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapThermometrePage } from './materiels-sap-thermometre';

@NgModule({
  declarations: [
    MaterielsSapThermometrePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapThermometrePage),
  ],
})
export class MaterielsSapThermometrePageModule {}
