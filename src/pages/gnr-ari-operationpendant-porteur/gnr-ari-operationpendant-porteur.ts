import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationpendantPorteurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationpendant-porteur',
  templateUrl: 'gnr-ari-operationpendant-porteur.html',
})
export class GnrAriOperationpendantPorteurPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationpendantPorteurPage');
  }

}
