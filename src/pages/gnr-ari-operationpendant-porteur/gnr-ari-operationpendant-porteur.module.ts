import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationpendantPorteurPage } from './gnr-ari-operationpendant-porteur';

@NgModule({
  declarations: [
    GnrAriOperationpendantPorteurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationpendantPorteurPage),
  ],
})
export class GnrAriOperationpendantPorteurPageModule {}
