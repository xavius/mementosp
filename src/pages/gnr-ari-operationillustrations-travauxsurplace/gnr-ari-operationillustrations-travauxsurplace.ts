import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationillustrationsTravauxsurplacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationillustrations-travauxsurplace',
  templateUrl: 'gnr-ari-operationillustrations-travauxsurplace.html',
})
export class GnrAriOperationillustrationsTravauxsurplacePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationillustrationsTravauxsurplacePage');
  }

}
