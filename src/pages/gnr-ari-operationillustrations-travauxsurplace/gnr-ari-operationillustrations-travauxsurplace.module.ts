import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationillustrationsTravauxsurplacePage } from './gnr-ari-operationillustrations-travauxsurplace';

@NgModule({
  declarations: [
    GnrAriOperationillustrationsTravauxsurplacePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationillustrationsTravauxsurplacePage),
  ],
})
export class GnrAriOperationillustrationsTravauxsurplacePageModule {}
