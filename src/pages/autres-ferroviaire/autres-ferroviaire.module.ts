import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutresFerroviairePage } from './autres-ferroviaire';

@NgModule({
  declarations: [
    AutresFerroviairePage,
  ],
  imports: [
    IonicPageModule.forChild(AutresFerroviairePage),
  ],
})
export class AutresFerroviairePageModule {}
