import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrariPage } from './gnrari';

@NgModule({
  declarations: [
    GnrariPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrariPage),
  ],
})
export class GnrariPageModule {}
