import { GnrAriAugmentationPage } from './../gnr-ari-augmentation/gnr-ari-augmentation';
import { GnrAriContraintesPage } from './../gnr-ari-contraintes/gnr-ari-contraintes';
import { GnrAriEquipementPage } from './../gnr-ari-equipement/gnr-ari-equipement';
import { GnrAriOperationpointsclesPage } from './../gnr-ari-operationpointscles/gnr-ari-operationpointscles';
import { GnrAriOperationillustrationsPage } from './../gnr-ari-operationillustrations/gnr-ari-operationillustrations';
import { GnrAriOperationpendantPage } from './../gnr-ari-operationpendant/gnr-ari-operationpendant';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrariDefinitionsPage } from '../gnrari-definitions/gnrari-definitions';
import { GnrariFumeesPage } from '../gnrari-fumees/gnrari-fumees';
import { GnrariAtmospherePage } from '../gnrari-atmosphere/gnrari-atmosphere';
import { GnrariReglesdebasePage } from '../gnrari-reglesdebase/gnrari-reglesdebase';
import { GnrAriOperationavantPage } from '../gnr-ari-operationavant/gnr-ari-operationavant';


/**
 * Generated class for the GnrariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrari',
  templateUrl: 'gnrari.html',
})
export class GnrariPage {

  
  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  list3: Array<{title: string, component: any}>;
  list4: Array<{title: string, component: any}>;
  list5: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Définitions', component: GnrariDefinitionsPage},
      {title:'Fumées d Incendie', component: GnrariFumeesPage},
      {title:'Atmosphères Toxiques', component: GnrariAtmospherePage}
    ];
    this.list2 = [
      {title:'Règles de Base', component: GnrariReglesdebasePage},
    ];
    this.list3 = [
      {title:'Avant Engagement', component: GnrAriOperationavantPage},
      {title:'Pendant Engagement', component: GnrAriOperationpendantPage},
      {title:'Illustrations', component: GnrAriOperationillustrationsPage},
      {title:'Points Clés', component: GnrAriOperationpointsclesPage}
    ];
    this.list4 = [
      {title:'Equipement', component: GnrAriEquipementPage}
    ];
    this.list5 = [
      {title:'Perturbations Sensorielles', component: GnrAriContraintesPage},
      {title:'Augmentation du Travail', component: GnrAriAugmentationPage}
    ];
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrariPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}

}
