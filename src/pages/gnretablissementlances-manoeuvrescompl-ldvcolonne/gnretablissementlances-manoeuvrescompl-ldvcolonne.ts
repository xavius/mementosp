import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvcolonneChefdagresComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdagres/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvcolonneChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdequipe/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdequipe';
import { GnretablissementlancesManoeuvrescomplLdvcolonneEquipierComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-equipier/gnretablissementlances-manoeuvrescompl-ldvcolonne-equipier';
import { GnretablissementlancesManoeuvrescomplLdvcolonneBalComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-bal/gnretablissementlances-manoeuvrescompl-ldvcolonne-bal';

/**
 * Generated class for the GnretablissementlancesManoeuvrescomplLdvcolonnePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvrescompl-ldvcolonne',
  templateUrl: 'gnretablissementlances-manoeuvrescompl-ldvcolonne.html',
})
export class GnretablissementlancesManoeuvrescomplLdvcolonnePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvrescomplLdvcolonnePage');
  }
  tab61Root=GnretablissementlancesManoeuvrescomplLdvcolonneChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvrescomplLdvcolonneChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvrescomplLdvcolonneEquipierComponent;
  tab64Root=GnretablissementlancesManoeuvrescomplLdvcolonneBalComponent;  
}
