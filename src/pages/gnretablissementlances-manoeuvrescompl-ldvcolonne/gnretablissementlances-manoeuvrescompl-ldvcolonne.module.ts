import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvcolonnePage } from './gnretablissementlances-manoeuvrescompl-ldvcolonne';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvrescomplLdvcolonnePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvrescomplLdvcolonnePage),
  ],
})
export class GnretablissementlancesManoeuvrescomplLdvcolonnePageModule {}
