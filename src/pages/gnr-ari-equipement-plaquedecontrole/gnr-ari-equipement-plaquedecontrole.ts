import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriEquipementPlaquedecontrolePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-equipement-plaquedecontrole',
  templateUrl: 'gnr-ari-equipement-plaquedecontrole.html',
})
export class GnrAriEquipementPlaquedecontrolePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriEquipementPlaquedecontrolePage');
  }

}
