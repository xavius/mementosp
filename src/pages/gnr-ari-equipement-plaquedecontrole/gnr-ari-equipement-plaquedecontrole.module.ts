import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementPlaquedecontrolePage } from './gnr-ari-equipement-plaquedecontrole';

@NgModule({
  declarations: [
    GnrAriEquipementPlaquedecontrolePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementPlaquedecontrolePage),
  ],
})
export class GnrAriEquipementPlaquedecontrolePageModule {}
