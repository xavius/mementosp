import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfDihMoyensPage } from './gnrfdf-dih-moyens';

@NgModule({
  declarations: [
    GnrfdfDihMoyensPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfDihMoyensPage),
  ],
})
export class GnrfdfDihMoyensPageModule {}
