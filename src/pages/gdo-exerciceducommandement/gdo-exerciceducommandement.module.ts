import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GdoExerciceducommandementPage } from './gdo-exerciceducommandement';

@NgModule({
  declarations: [
    GdoExerciceducommandementPage,
  ],
  imports: [
    IonicPageModule.forChild(GdoExerciceducommandementPage),
  ],
})
export class GdoExerciceducommandementPageModule {}
