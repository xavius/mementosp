import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrOuverturelaterale_1Component } from '../../components/sr-ouverturelaterale/sr-ouverturelaterale-1/sr-ouverturelaterale-1';
import { SrOuverturelaterale_2Component } from '../../components/sr-ouverturelaterale/sr-ouverturelaterale-2/sr-ouverturelaterale-2';
import { SrOuverturelaterale_3Component } from '../../components/sr-ouverturelaterale/sr-ouverturelaterale-3/sr-ouverturelaterale-3';

/**
 * Generated class for the SrouverturelateralePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srouverturelaterale',
  templateUrl: 'srouverturelaterale.html',
})
export class SrouverturelateralePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrouverturelateralePage');
  }
  tab22Root=SrOuverturelaterale_1Component;
  tab23Root=SrOuverturelaterale_2Component;
  tab24Root=SrOuverturelaterale_3Component;
}
