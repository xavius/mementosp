import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrouverturelateralePage } from './srouverturelaterale';

@NgModule({
  declarations: [
    SrouverturelateralePage,
  ],
  imports: [
    IonicPageModule.forChild(SrouverturelateralePage),
  ],
})
export class SrouverturelateralePageModule {}
