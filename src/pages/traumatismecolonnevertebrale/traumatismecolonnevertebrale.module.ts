import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TraumatismecolonnevertebralePage } from './traumatismecolonnevertebrale';

@NgModule({
  declarations: [
    TraumatismecolonnevertebralePage,
  ],
  imports: [
    IonicPageModule.forChild(TraumatismecolonnevertebralePage),
  ],
})
export class TraumatismecolonnevertebralePageModule {}
