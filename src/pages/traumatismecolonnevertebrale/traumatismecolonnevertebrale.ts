import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapTraumatismecolonnevertebraleSignesComponent } from '../../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-signes/sap-traumatismecolonnevertebrale-signes';
import { SapTraumatismecolonnevertebraleBilansComponent } from '../../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-bilans/sap-traumatismecolonnevertebrale-bilans';
import { SapTraumatismecolonnevertebraleActionsComponent } from '../../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-actions/sap-traumatismecolonnevertebrale-actions';
import { SapTraumatismecolonnevertebraleImportantComponent } from '../../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-important/sap-traumatismecolonnevertebrale-important';

/**
 * Generated class for the TraumatismecolonnevertebralePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-traumatismecolonnevertebrale',
  templateUrl: 'traumatismecolonnevertebrale.html',
})
export class TraumatismecolonnevertebralePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TraumatismecolonnevertebralePage');
  }
  tab7Root=SapTraumatismecolonnevertebraleSignesComponent;
  tab8Root=SapTraumatismecolonnevertebraleBilansComponent;
  tab9Root=SapTraumatismecolonnevertebraleActionsComponent;
  tab10Root=SapTraumatismecolonnevertebraleImportantComponent;
}
