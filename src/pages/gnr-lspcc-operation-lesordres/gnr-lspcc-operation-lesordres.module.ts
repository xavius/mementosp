import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationLesordresPage } from './gnr-lspcc-operation-lesordres';

@NgModule({
  declarations: [
    GnrLspccOperationLesordresPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationLesordresPage),
  ],
})
export class GnrLspccOperationLesordresPageModule {}
