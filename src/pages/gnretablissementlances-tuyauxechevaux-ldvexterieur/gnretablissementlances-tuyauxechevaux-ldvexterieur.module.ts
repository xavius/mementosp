import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvexterieurPage } from './gnretablissementlances-tuyauxechevaux-ldvexterieur';

@NgModule({
  declarations: [
    GnretablissementlancesTuyauxechevauxLdvexterieurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesTuyauxechevauxLdvexterieurPage),
  ],
})
export class GnretablissementlancesTuyauxechevauxLdvexterieurPageModule {}
