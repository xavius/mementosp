import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvexterieurChefdagresComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdagres/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvexterieurChefdequipeComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvexterieurEquipierComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-equipier/gnretablissementlances-tuyauxechevaux-ldvexterieur-equipier';
import { GnretablissementlancesTuyauxechevauxLdvexterieurBalComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-bal/gnretablissementlances-tuyauxechevaux-ldvexterieur-bal';

/**
 * Generated class for the GnretablissementlancesTuyauxechevauxLdvexterieurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-tuyauxechevaux-ldvexterieur',
  templateUrl: 'gnretablissementlances-tuyauxechevaux-ldvexterieur.html',
})
export class GnretablissementlancesTuyauxechevauxLdvexterieurPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesTuyauxechevauxLdvexterieurPage');
  }
  tab61Root=GnretablissementlancesTuyauxechevauxLdvexterieurChefdagresComponent;
  tab62Root=GnretablissementlancesTuyauxechevauxLdvexterieurChefdequipeComponent;
  tab63Root=GnretablissementlancesTuyauxechevauxLdvexterieurEquipierComponent;
  tab64Root=GnretablissementlancesTuyauxechevauxLdvexterieurBalComponent;  
}
