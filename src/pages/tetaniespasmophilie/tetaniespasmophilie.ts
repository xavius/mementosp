import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapTetaniespasmophilieSignesComponent } from '../../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-signes/sap-tetaniespasmophilie-signes';
import { SapTetaniespasmophilieBilansComponent } from '../../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-bilans/sap-tetaniespasmophilie-bilans';
import { SapTetaniespasmophilieActionsComponent } from '../../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-actions/sap-tetaniespasmophilie-actions';
import { SapTetaniespasmophilieImportantComponent } from '../../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-important/sap-tetaniespasmophilie-important';

/**
 * Generated class for the TetaniespasmophiliePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tetaniespasmophilie',
  templateUrl: 'tetaniespasmophilie.html',
})
export class TetaniespasmophiliePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TetaniespasmophiliePage');
  }
  tab7Root=SapTetaniespasmophilieSignesComponent;
  tab8Root=SapTetaniespasmophilieBilansComponent;
  tab9Root=SapTetaniespasmophilieActionsComponent;
  tab10Root=SapTetaniespasmophilieImportantComponent;
}
