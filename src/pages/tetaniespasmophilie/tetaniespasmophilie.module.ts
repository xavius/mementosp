import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TetaniespasmophiliePage } from './tetaniespasmophilie';

@NgModule({
  declarations: [
    TetaniespasmophiliePage,
  ],
  imports: [
    IonicPageModule.forChild(TetaniespasmophiliePage),
  ],
})
export class TetaniespasmophiliePageModule {}
