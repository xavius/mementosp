import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesFindinterPage } from './gnretablissementlances-findinter';

@NgModule({
  declarations: [
    GnretablissementlancesFindinterPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesFindinterPage),
  ],
})
export class GnretablissementlancesFindinterPageModule {}
