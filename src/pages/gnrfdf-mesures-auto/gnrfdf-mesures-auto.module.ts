import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfMesuresAutoPage } from './gnrfdf-mesures-auto';

@NgModule({
  declarations: [
    GnrfdfMesuresAutoPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfMesuresAutoPage),
  ],
})
export class GnrfdfMesuresAutoPageModule {}
