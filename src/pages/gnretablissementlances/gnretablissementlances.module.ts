import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesPage } from './gnretablissementlances';

@NgModule({
  declarations: [
    GnretablissementlancesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesPage),
  ],
})
export class GnretablissementlancesPageModule {}
