import { GnretablissementlancesRetourdinterPage } from './../gnretablissementlances-retourdinter/gnretablissementlances-retourdinter';
import { GnretablissementlancesFindinterPage } from './../gnretablissementlances-findinter/gnretablissementlances-findinter';
import { GnretablissementlancesMessagesPage } from './../gnretablissementlances-messages/gnretablissementlances-messages';
import { GnretablissementlancesSmesPage } from './../gnretablissementlances-smes/gnretablissementlances-smes';
import { GnretablissementlancesSacdattaquePage } from './../gnretablissementlances-sacdattaque/gnretablissementlances-sacdattaque';
import { GnretablissementlancesTuyauxechevauxPage } from './../gnretablissementlances-tuyauxechevaux/gnretablissementlances-tuyauxechevaux';
import { GnretablissementlancesManoeuvrescomplPage } from './../gnretablissementlances-manoeuvrescompl/gnretablissementlances-manoeuvrescompl';
import { GnretablissementlancesReglesdebasePage } from './../gnretablissementlances-reglesdebase/gnretablissementlances-reglesdebase';
import { GnretablissementlancesGeneralitesPage } from './../gnretablissementlances-generalites/gnretablissementlances-generalites';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrPage } from '../gnretablissementlances-manoeuvres-gnr/gnretablissementlances-manoeuvres-gnr';

/**
 * Generated class for the GnretablissementlancesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances',
  templateUrl: 'gnretablissementlances.html',
})
export class GnretablissementlancesPage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  list3: Array<{title: string, component: any}>;
  list4: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Généralités', component: GnretablissementlancesGeneralitesPage},
    ];
    this.list2 = [
      {title:'Règles de Base', component: GnretablissementlancesReglesdebasePage},
      {title:'Manoeuvres type GNR', component: GnretablissementlancesManoeuvresGnrPage},
      {title:'Manoeuvres Complémentaires', component: GnretablissementlancesManoeuvrescomplPage},
      {title:'Tuyaux en Echeveaux', component: GnretablissementlancesTuyauxechevauxPage},
      {title:'Sac d Attaque', component: GnretablissementlancesSacdattaquePage}
    ];
    this.list3 = [
      {title:'SMES', component: GnretablissementlancesSmesPage},
      {title:'MESSAGES', component: GnretablissementlancesMessagesPage}
    ];
    this.list4 = [
      {title:'Fin D intervention', component: GnretablissementlancesFindinterPage},
      {title:'Retour D intervention', component: GnretablissementlancesRetourdinterPage}
    ];
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
