import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrariAtmospherePage } from './gnrari-atmosphere';

@NgModule({
  declarations: [
    GnrariAtmospherePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrariAtmospherePage),
  ],
})
export class GnrariAtmospherePageModule {}
