import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvattaquePage } from './gnretablissementlances-tuyauxechevaux-ldvattaque';

@NgModule({
  declarations: [
    GnretablissementlancesTuyauxechevauxLdvattaquePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesTuyauxechevauxLdvattaquePage),
  ],
})
export class GnretablissementlancesTuyauxechevauxLdvattaquePageModule {}
