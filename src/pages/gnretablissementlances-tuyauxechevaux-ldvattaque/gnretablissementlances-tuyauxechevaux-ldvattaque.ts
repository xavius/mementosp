import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvattaqueChefdagresComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdagres/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvattaqueChefdequipeComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvattaqueEquipierComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque-equipier/gnretablissementlances-tuyauxechevaux-ldvattaque-equipier';

/**
 * Generated class for the GnretablissementlancesTuyauxechevauxLdvattaquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-tuyauxechevaux-ldvattaque',
  templateUrl: 'gnretablissementlances-tuyauxechevaux-ldvattaque.html',
})
export class GnretablissementlancesTuyauxechevauxLdvattaquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesTuyauxechevauxLdvattaquePage');
  }
  tab61Root=GnretablissementlancesTuyauxechevauxLdvattaqueChefdagresComponent;
  tab62Root=GnretablissementlancesTuyauxechevauxLdvattaqueChefdequipeComponent;
  tab63Root=GnretablissementlancesTuyauxechevauxLdvattaqueEquipierComponent;
}
