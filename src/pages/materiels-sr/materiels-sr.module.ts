import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSrPage } from './materiels-sr';

@NgModule({
  declarations: [
    MaterielsSrPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSrPage),
  ],
})
export class MaterielsSrPageModule {}
