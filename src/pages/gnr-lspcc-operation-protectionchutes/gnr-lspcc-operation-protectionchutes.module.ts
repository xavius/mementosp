import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationProtectionchutesPage } from './gnr-lspcc-operation-protectionchutes';

@NgModule({
  declarations: [
    GnrLspccOperationProtectionchutesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationProtectionchutesPage),
  ],
})
export class GnrLspccOperationProtectionchutesPageModule {}
