import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccOperationProtectionchutesChefdagresComponent } from '../../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-chefdagres/gnr-lspcc-operation-protectionchutes-chefdagres';
import { GnrLspccOperationProtectionchutesChefdequipeComponent } from '../../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-chefdequipe/gnr-lspcc-operation-protectionchutes-chefdequipe';
import { GnrLspccOperationProtectionchutesEquipierComponent } from '../../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-equipier/gnr-lspcc-operation-protectionchutes-equipier';
import { GnrLspccOperationProtectionchutes_2ebinomeComponent } from '../../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-2ebinome/gnr-lspcc-operation-protectionchutes-2ebinome';

/**
 * Generated class for the GnrLspccOperationProtectionchutesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-protectionchutes',
  templateUrl: 'gnr-lspcc-operation-protectionchutes.html',
})
export class GnrLspccOperationProtectionchutesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationProtectionchutesPage');
  }
  tab61Root=GnrLspccOperationProtectionchutesChefdagresComponent;
  tab62Root=GnrLspccOperationProtectionchutesChefdequipeComponent;
  tab63Root=GnrLspccOperationProtectionchutesEquipierComponent;
  tab64Root=GnrLspccOperationProtectionchutes_2ebinomeComponent;
}
