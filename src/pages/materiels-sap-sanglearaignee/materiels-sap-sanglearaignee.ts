import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapSanglearaigneeDescriptionComponent } from '../../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-description/materiels-sap-sanglearaignee-description';
import { MaterielsSapSanglearaigneeIndicationsComponent } from '../../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-indications/materiels-sap-sanglearaignee-indications';
import { MaterielsSapSanglearaigneeRisquesComponent } from '../../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-risques/materiels-sap-sanglearaignee-risques';
import { MaterielsSapSanglearaigneeUtilisationComponent } from '../../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-utilisation/materiels-sap-sanglearaignee-utilisation';
import { MaterielsSapSanglearaigneePointsclesComponent } from '../../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-pointscles/materiels-sap-sanglearaignee-pointscles';
import { MaterielsSapSanglearaigneeCriteresdefficaciteComponent } from '../../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-criteresdefficacite/materiels-sap-sanglearaignee-criteresdefficacite';

/**
 * Generated class for the MaterielsSapSanglearaigneePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-sanglearaignee',
  templateUrl: 'materiels-sap-sanglearaignee.html',
})
export class MaterielsSapSanglearaigneePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapSanglearaigneePage');
  }
  tab65Root=MaterielsSapSanglearaigneeDescriptionComponent;
  tab66Root=MaterielsSapSanglearaigneeIndicationsComponent;
  tab67Root=MaterielsSapSanglearaigneeRisquesComponent;
  tab68Root=MaterielsSapSanglearaigneeUtilisationComponent;
  tab69Root=MaterielsSapSanglearaigneePointsclesComponent;
  tab70Root=MaterielsSapSanglearaigneeCriteresdefficaciteComponent;
}
