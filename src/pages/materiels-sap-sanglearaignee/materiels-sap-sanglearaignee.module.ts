import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapSanglearaigneePage } from './materiels-sap-sanglearaignee';

@NgModule({
  declarations: [
    MaterielsSapSanglearaigneePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapSanglearaigneePage),
  ],
})
export class MaterielsSapSanglearaigneePageModule {}
