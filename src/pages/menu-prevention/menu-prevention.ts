import { PrevssiapPage } from './../prevssiap/prevssiap';
import { PrevssiPage } from './../prevssi/prevssi';
import { PrevplansdinterventionPage } from './../prevplansdintervention/prevplansdintervention';
import { PrevmoyensdextinctionPage } from './../prevmoyensdextinction/prevmoyensdextinction';
import { PrevdesenfumagePage } from './../prevdesenfumage/prevdesenfumage';
import { PrevlocauxarisquesPage } from './../prevlocauxarisques/prevlocauxarisques';
import { PrevgrosetsecondoeuvrePage } from './../prevgrosetsecondoeuvre/prevgrosetsecondoeuvre';
import { PrevfacadesPage } from './../prevfacades/prevfacades';
import { PrevvoiesenginsetechellesPage } from './../prevvoiesenginsetechelles/prevvoiesenginsetechelles';
import { PrevclassementdesbatimentsPage } from './../prevclassementdesbatiments/prevclassementdesbatiments';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevprincipesgenerauxPage } from '../prevprincipesgeneraux/prevprincipesgeneraux';
import { PrevdegagementsaccesPage } from '../prevdegagementsacces/prevdegagementsacces';

/**
 * Generated class for the MenuPreventionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-prevention',
  templateUrl: 'menu-prevention.html',
})
export class MenuPreventionPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Principes Généraux', component: PrevprincipesgenerauxPage},
      {title:'Classement des Bâtiments', component: PrevclassementdesbatimentsPage},
      {title:'Voies Engins et Echelles', component: PrevvoiesenginsetechellesPage},
      {title:'Façades', component: PrevfacadesPage},
      {title:'Gros et Second Oeuvre', component: PrevgrosetsecondoeuvrePage},
      {title:'Dégagements - Accès', component: PrevdegagementsaccesPage},
      {title:'Locaux à Risques', component: PrevlocauxarisquesPage},
      {title:'Désenfumage', component: PrevdesenfumagePage},
      {title:'Moyens d\'Extinction', component: PrevmoyensdextinctionPage},
      {title:'Service de Sécurité Incendie et d\'Assistance à Personnes', component: PrevssiapPage},
      {title:'Système de Sécurité Incendie', component: PrevssiPage},
      {title:'Plans d\'Intervention', component: PrevplansdinterventionPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPreventionPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
  }

}
