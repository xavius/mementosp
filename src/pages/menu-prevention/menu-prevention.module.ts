import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuPreventionPage } from './menu-prevention';

@NgModule({
  declarations: [
    MenuPreventionPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuPreventionPage),
  ],
})
export class MenuPreventionPageModule {}
