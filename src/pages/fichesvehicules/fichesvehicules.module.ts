import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichesvehiculesPage } from './fichesvehicules';

@NgModule({
  declarations: [
    FichesvehiculesPage,
  ],
  imports: [
    IonicPageModule.forChild(FichesvehiculesPage),
  ],
})
export class FichesvehiculesPageModule {}
