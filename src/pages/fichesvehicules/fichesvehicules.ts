import { FeudecamionciternePage } from './../feudecamionciterne/feudecamionciterne';
import { FeudetunnelPage } from './../feudetunnel/feudetunnel';
import { FeuaeronefPage } from './../feuaeronef/feuaeronef';
import { FeudepenichePage } from './../feudepeniche/feudepeniche';
import { FeudetrainPage } from './../feudetrain/feudetrain';
import { FeudebateauPage } from './../feudebateau/feudebateau';
import { FeudevehiculesPage } from './../feudevehicules/feudevehicules';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FichesvehiculesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichesvehicules',
  templateUrl: 'fichesvehicules.html',
})
export class FichesvehiculesPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Feu de Véhicules', component: FeudevehiculesPage},
      {title:'Feu de Train sans TMD', component: FeudetrainPage},
      {title:'Feu de Bateau à 50m maxi', component: FeudebateauPage},
      {title:'Feu de Péniche', component: FeudepenichePage},
      {title:'Feu d\'Aéronef', component: FeuaeronefPage},
      {title:'Feu de Tunnel ou Galerie', component: FeudetunnelPage},
      {title:'Feu de Camion Citerne', component: FeudecamionciternePage}

    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichesvehiculesPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);

  }
}
