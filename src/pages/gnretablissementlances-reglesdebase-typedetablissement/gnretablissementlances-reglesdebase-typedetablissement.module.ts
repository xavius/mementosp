import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesReglesdebaseTypedetablissementPage } from './gnretablissementlances-reglesdebase-typedetablissement';

@NgModule({
  declarations: [
    GnretablissementlancesReglesdebaseTypedetablissementPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesReglesdebaseTypedetablissementPage),
  ],
})
export class GnretablissementlancesReglesdebaseTypedetablissementPageModule {}
