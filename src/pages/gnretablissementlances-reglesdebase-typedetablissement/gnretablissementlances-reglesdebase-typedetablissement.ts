import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesReglesdebaseTypedetablissementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-reglesdebase-typedetablissement',
  templateUrl: 'gnretablissementlances-reglesdebase-typedetablissement.html',
})
export class GnretablissementlancesReglesdebaseTypedetablissementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesReglesdebaseTypedetablissementPage');
  }

}
