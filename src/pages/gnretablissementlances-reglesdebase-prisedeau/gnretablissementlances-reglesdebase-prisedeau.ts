import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesReglesdebasePrisedeauPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-reglesdebase-prisedeau',
  templateUrl: 'gnretablissementlances-reglesdebase-prisedeau.html',
})
export class GnretablissementlancesReglesdebasePrisedeauPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesReglesdebasePrisedeauPage');
  }

}
