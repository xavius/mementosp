import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesReglesdebasePrisedeauPage } from './gnretablissementlances-reglesdebase-prisedeau';

@NgModule({
  declarations: [
    GnretablissementlancesReglesdebasePrisedeauPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesReglesdebasePrisedeauPage),
  ],
})
export class GnretablissementlancesReglesdebasePrisedeauPageModule {}
