import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesReconnaissancePage } from './gnretablissementlances-generalites-reconnaissance';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesReconnaissancePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesReconnaissancePage),
  ],
})
export class GnretablissementlancesGeneralitesReconnaissancePageModule {}
