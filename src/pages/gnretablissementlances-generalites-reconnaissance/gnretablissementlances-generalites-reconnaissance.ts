import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesGeneralitesReconnaissancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-generalites-reconnaissance',
  templateUrl: 'gnretablissementlances-generalites-reconnaissance.html',
})
export class GnretablissementlancesGeneralitesReconnaissancePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesGeneralitesReconnaissancePage');
  }

}
