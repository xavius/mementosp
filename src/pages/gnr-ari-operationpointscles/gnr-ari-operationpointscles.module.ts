import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationpointsclesPage } from './gnr-ari-operationpointscles';

@NgModule({
  declarations: [
    GnrAriOperationpointsclesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationpointsclesPage),
  ],
})
export class GnrAriOperationpointsclesPageModule {}
