import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriEquipementLiaisonpersonnellePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-equipement-liaisonpersonnelle',
  templateUrl: 'gnr-ari-equipement-liaisonpersonnelle.html',
})
export class GnrAriEquipementLiaisonpersonnellePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriEquipementLiaisonpersonnellePage');
  }

}
