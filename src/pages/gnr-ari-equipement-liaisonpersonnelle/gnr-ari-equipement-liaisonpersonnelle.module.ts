import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementLiaisonpersonnellePage } from './gnr-ari-equipement-liaisonpersonnelle';

@NgModule({
  declarations: [
    GnrAriEquipementLiaisonpersonnellePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementLiaisonpersonnellePage),
  ],
})
export class GnrAriEquipementLiaisonpersonnellePageModule {}
