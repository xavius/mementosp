import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriAugmentationThermoregulationPage } from './gnr-ari-augmentation-thermoregulation';

@NgModule({
  declarations: [
    GnrAriAugmentationThermoregulationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriAugmentationThermoregulationPage),
  ],
})
export class GnrAriAugmentationThermoregulationPageModule {}
