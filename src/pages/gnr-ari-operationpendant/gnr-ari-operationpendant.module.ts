import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationpendantPage } from './gnr-ari-operationpendant';

@NgModule({
  declarations: [
    GnrAriOperationpendantPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationpendantPage),
  ],
})
export class GnrAriOperationpendantPageModule {}
