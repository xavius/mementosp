import { GnrAriOperationpendantPorteurPage } from './../gnr-ari-operationpendant-porteur/gnr-ari-operationpendant-porteur';
import { GnrAriOperationpendantControleurPage } from './../gnr-ari-operationpendant-controleur/gnr-ari-operationpendant-controleur';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationpendantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationpendant',
  templateUrl: 'gnr-ari-operationpendant.html',
})
export class GnrAriOperationpendantPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'CONTROLEUR', component: GnrAriOperationpendantControleurPage},
      {title:'PORTEUR', component: GnrAriOperationpendantPorteurPage}

    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationpendantPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}

}
