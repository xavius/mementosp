import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxMaterielPage } from './gnretablissementlances-tuyauxechevaux-materiel';

@NgModule({
  declarations: [
    GnretablissementlancesTuyauxechevauxMaterielPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesTuyauxechevauxMaterielPage),
  ],
})
export class GnretablissementlancesTuyauxechevauxMaterielPageModule {}
