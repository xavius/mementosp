import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEnvironnementElementsPage } from './gnrege-environnement-elements';

@NgModule({
  declarations: [
    GnregeEnvironnementElementsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEnvironnementElementsPage),
  ],
})
export class GnregeEnvironnementElementsPageModule {}
