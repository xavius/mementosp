import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutresTunnelsPage } from './autres-tunnels';

@NgModule({
  declarations: [
    AutresTunnelsPage,
  ],
  imports: [
    IonicPageModule.forChild(AutresTunnelsPage),
  ],
})
export class AutresTunnelsPageModule {}
