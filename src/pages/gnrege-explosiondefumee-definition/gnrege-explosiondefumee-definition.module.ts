import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeExplosiondefumeeDefinitionPage } from './gnrege-explosiondefumee-definition';

@NgModule({
  declarations: [
    GnregeExplosiondefumeeDefinitionPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeExplosiondefumeeDefinitionPage),
  ],
})
export class GnregeExplosiondefumeeDefinitionPageModule {}
