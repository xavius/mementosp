import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesMessagesPage } from './gnretablissementlances-messages';

@NgModule({
  declarations: [
    GnretablissementlancesMessagesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesMessagesPage),
  ],
})
export class GnretablissementlancesMessagesPageModule {}
