import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfMoyensAeriensPage } from './gnrfdf-moyens-aeriens';

@NgModule({
  declarations: [
    GnrfdfMoyensAeriensPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfMoyensAeriensPage),
  ],
})
export class GnrfdfMoyensAeriensPageModule {}
