import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxMatieresplastiquesAccesComponent } from '../../components/feux-matieresplastiques/feux-matieresplastiques-acces/feux-matieresplastiques-acces';
import { FeuxMatieresplastiquesActionComponent } from '../../components/feux-matieresplastiques/feux-matieresplastiques-action/feux-matieresplastiques-action';
import { FeuxMatieresplastiquesAttentionComponent } from '../../components/feux-matieresplastiques/feux-matieresplastiques-attention/feux-matieresplastiques-attention';
import { FeuxMatieresplastiquesInterditComponent } from '../../components/feux-matieresplastiques/feux-matieresplastiques-interdit/feux-matieresplastiques-interdit';
import { FeuxMatieresplastiquesRisquesComponent } from '../../components/feux-matieresplastiques/feux-matieresplastiques-risques/feux-matieresplastiques-risques';
import { FeuxMatieresplastiquesStrategieComponent } from '../../components/feux-matieresplastiques/feux-matieresplastiques-strategie/feux-matieresplastiques-strategie';

/**
 * Generated class for the FeumatieresplastiquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feumatieresplastiques',
  templateUrl: 'feumatieresplastiques.html',
})
export class FeumatieresplastiquesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeumatieresplastiquesPage');
  }
  tab1Root=FeuxMatieresplastiquesRisquesComponent;
  tab2Root=FeuxMatieresplastiquesStrategieComponent;
  tab3Root=FeuxMatieresplastiquesAccesComponent;
  tab4Root=FeuxMatieresplastiquesActionComponent;
  tab5Root=FeuxMatieresplastiquesAttentionComponent;
  tab6Root=FeuxMatieresplastiquesInterditComponent;
}
