import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeumatieresplastiquesPage } from './feumatieresplastiques';

@NgModule({
  declarations: [
    FeumatieresplastiquesPage,
  ],
  imports: [
    IonicPageModule.forChild(FeumatieresplastiquesPage),
  ],
})
export class FeumatieresplastiquesPageModule {}
