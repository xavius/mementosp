import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BilansPage } from './bilans';

@NgModule({
  declarations: [
    BilansPage,
  ],
  imports: [
    IonicPageModule.forChild(BilansPage),
  ],
})
export class BilansPageModule {}
