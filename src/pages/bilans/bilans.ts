import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapBilansCirconstancielComponent } from '../../components/sap-bilans/sap-bilans-circonstanciel/sap-bilans-circonstanciel';
import { SapBilansVitalComponent } from '../../components/sap-bilans/sap-bilans-vital/sap-bilans-vital';
import { SapBilansLesionnelComponent } from '../../components/sap-bilans/sap-bilans-lesionnel/sap-bilans-lesionnel';
import { SapBilansFonctionnelComponent } from '../../components/sap-bilans/sap-bilans-fonctionnel/sap-bilans-fonctionnel';
import { SapBilansRadioComponent } from '../../components/sap-bilans/sap-bilans-radio/sap-bilans-radio';
import { SapBilansEvolutifComponent } from '../../components/sap-bilans/sap-bilans-evolutif/sap-bilans-evolutif';

/**
 * Generated class for the BilansPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bilans',
  templateUrl: 'bilans.html',
})
export class BilansPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BilansPage');
  }
  tab11Root=SapBilansCirconstancielComponent;
  tab12Root=SapBilansVitalComponent;
  tab13Root=SapBilansLesionnelComponent;
  tab14Root=SapBilansFonctionnelComponent;
  tab15Root=SapBilansRadioComponent;
  tab16Root=SapBilansEvolutifComponent;
}
