import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxIghAccesComponent } from '../../components/feux-igh/feux-igh-acces/feux-igh-acces';
import { FeuxIghActionComponent } from '../../components/feux-igh/feux-igh-action/feux-igh-action';
import { FeuxIghAttentionComponent } from '../../components/feux-igh/feux-igh-attention/feux-igh-attention';
import { FeuxIghInterditComponent } from '../../components/feux-igh/feux-igh-interdit/feux-igh-interdit';
import { FeuxIghRisquesComponent } from '../../components/feux-igh/feux-igh-risques/feux-igh-risques';
import { FeuxIghStrategieComponent } from '../../components/feux-igh/feux-igh-strategie/feux-igh-strategie';

/**
 * Generated class for the FeuIghPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feu-igh',
  templateUrl: 'feu-igh.html',
})
export class FeuIghPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuIghPage');
  }
  tab1Root=FeuxIghRisquesComponent;
  tab2Root=FeuxIghStrategieComponent;
  tab3Root=FeuxIghAccesComponent;
  tab4Root=FeuxIghActionComponent;
  tab5Root=FeuxIghAttentionComponent;
  tab6Root=FeuxIghInterditComponent;
}
