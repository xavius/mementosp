import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuIghPage } from './feu-igh';

@NgModule({
  declarations: [
    FeuIghPage,
  ],
  imports: [
    IonicPageModule.forChild(FeuIghPage),
  ],
})
export class FeuIghPageModule {}
