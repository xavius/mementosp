import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevdesenfumagePage } from './prevdesenfumage';

@NgModule({
  declarations: [
    PrevdesenfumagePage,
  ],
  imports: [
    IonicPageModule.forChild(PrevdesenfumagePage),
  ],
})
export class PrevdesenfumagePageModule {}
