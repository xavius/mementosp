import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevDesenfumageFonctionnementComponent } from '../../components/prev-desenfumage/prev-desenfumage-fonctionnement/prev-desenfumage-fonctionnement';
import { PrevDesenfumagePrecautionComponent } from '../../components/prev-desenfumage/prev-desenfumage-precaution/prev-desenfumage-precaution';

/**
 * Generated class for the PrevdesenfumagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevdesenfumage',
  templateUrl: 'prevdesenfumage.html',
})
export class PrevdesenfumagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevdesenfumagePage');
  }
  tab46Root=PrevDesenfumageFonctionnementComponent;
  tab47Root=PrevDesenfumagePrecautionComponent;
}
