import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapAcrSignesComponent } from '../../components/sap-acr/sap-acr-signes/sap-acr-signes';
import { SapAcrBilansComponent } from '../../components/sap-acr/sap-acr-bilans/sap-acr-bilans';
import { SapAcrActionsComponent } from '../../components/sap-acr/sap-acr-actions/sap-acr-actions';
import { SapAcrImportantComponent } from '../../components/sap-acr/sap-acr-important/sap-acr-important';

/**
 * Generated class for the AcrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-acr',
  templateUrl: 'acr.html',
})
export class AcrPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcrPage');
  }
  tab7Root=SapAcrSignesComponent;
  tab8Root=SapAcrBilansComponent;
  tab9Root=SapAcrActionsComponent;
  tab10Root=SapAcrImportantComponent;
}
