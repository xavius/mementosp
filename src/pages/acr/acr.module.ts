import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcrPage } from './acr';

@NgModule({
  declarations: [
    AcrPage,
  ],
  imports: [
    IonicPageModule.forChild(AcrPage),
  ],
})
export class AcrPageModule {}
