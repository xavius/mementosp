import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfGiffAlimentationPage } from './gnrfdf-giff-alimentation';

@NgModule({
  declarations: [
    GnrfdfGiffAlimentationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfGiffAlimentationPage),
  ],
})
export class GnrfdfGiffAlimentationPageModule {}
