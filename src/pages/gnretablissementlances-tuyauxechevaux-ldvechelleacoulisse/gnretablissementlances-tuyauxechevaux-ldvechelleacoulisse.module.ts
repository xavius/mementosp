import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage } from './gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse';

@NgModule({
  declarations: [
    GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage),
  ],
})
export class GnretablissementlancesTuyauxechevauxLdvechelleacoulissePageModule {}
