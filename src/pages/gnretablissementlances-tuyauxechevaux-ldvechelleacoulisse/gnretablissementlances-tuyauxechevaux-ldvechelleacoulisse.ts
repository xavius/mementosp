import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdagresComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdagres/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdequipeComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseEquipierComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-equipier/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-equipier';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseBalComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-bal/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-bal';

/**
 * Generated class for the GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse',
  templateUrl: 'gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse.html',
})
export class GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage');
  }
  tab61Root=GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdagresComponent;
  tab62Root=GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdequipeComponent;
  tab63Root=GnretablissementlancesTuyauxechevauxLdvechelleacoulisseEquipierComponent;
  tab64Root=GnretablissementlancesTuyauxechevauxLdvechelleacoulisseBalComponent;
}
