import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfMesuresSecuritePage } from './gnrfdf-mesures-securite';

@NgModule({
  declarations: [
    GnrfdfMesuresSecuritePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfMesuresSecuritePage),
  ],
})
export class GnrfdfMesuresSecuritePageModule {}
