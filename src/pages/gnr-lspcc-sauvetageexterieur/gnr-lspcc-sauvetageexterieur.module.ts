import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccSauvetageexterieurPage } from './gnr-lspcc-sauvetageexterieur';

@NgModule({
  declarations: [
    GnrLspccSauvetageexterieurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccSauvetageexterieurPage),
  ],
})
export class GnrLspccSauvetageexterieurPageModule {}
