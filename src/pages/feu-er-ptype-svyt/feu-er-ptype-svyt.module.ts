import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuErPtypeSvytPage } from './feu-er-ptype-svyt';

@NgModule({
  declarations: [
    FeuErPtypeSvytPage,
  ],
  imports: [
    IonicPageModule.forChild(FeuErPtypeSvytPage),
  ],
})
export class FeuErPtypeSvytPageModule {}
