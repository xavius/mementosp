import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxErpTypeSvytAccesComponent } from '../../components/feux-erp-type-svyt/feux-erp-type-svyt-acces/feux-erp-type-svyt-acces';
import { FeuxErpTypeSvytActionComponent } from '../../components/feux-erp-type-svyt/feux-erp-type-svyt-action/feux-erp-type-svyt-action';
import { FeuxErpTypeSvytAttentionComponent } from '../../components/feux-erp-type-svyt/feux-erp-type-svyt-attention/feux-erp-type-svyt-attention';
import { FeuxErpTypeSvytInterditComponent } from '../../components/feux-erp-type-svyt/feux-erp-type-svyt-interdit/feux-erp-type-svyt-interdit';
import { FeuxErpTypeSvytRisquesComponent } from '../../components/feux-erp-type-svyt/feux-erp-type-svyt-risques/feux-erp-type-svyt-risques';
import { FeuxErpTypeSvytStrategieComponent } from '../../components/feux-erp-type-svyt/feux-erp-type-svyt-strategie/feux-erp-type-svyt-strategie';

/**
 * Generated class for the FeuErPtypeSvytPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feu-er-ptype-svyt',
  templateUrl: 'feu-er-ptype-svyt.html',
})
export class FeuErPtypeSvytPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuErPtypeSvytPage');
  }
  tab1Root=FeuxErpTypeSvytRisquesComponent;
  tab2Root=FeuxErpTypeSvytStrategieComponent;
  tab3Root=FeuxErpTypeSvytAccesComponent;
  tab4Root=FeuxErpTypeSvytActionComponent;
  tab5Root=FeuxErpTypeSvytAttentionComponent;
  tab6Root=FeuxErpTypeSvytInterditComponent;
}
