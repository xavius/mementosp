import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielSapAttelledetractionDescriptionComponent } from '../../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-description/materiel-sap-attelledetraction-description';
import { MaterielSapAttelledetractionIndicationsComponent } from '../../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-indications/materiel-sap-attelledetraction-indications';
import { MaterielSapAttelledetractionRisquesComponent } from '../../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-risques/materiel-sap-attelledetraction-risques';
import { MaterielSapAttelledetractionUtilisationComponent } from '../../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-utilisation/materiel-sap-attelledetraction-utilisation';
import { MaterielSapAttelledetractionPointsclesComponent } from '../../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-pointscles/materiel-sap-attelledetraction-pointscles';
import { MaterielSapAttelledetractionEfficacitéComponent } from '../../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-efficacité/materiel-sap-attelledetraction-efficacité';


/**
 * Generated class for the MaterielsSapAttelledetractionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-attelledetraction',
  templateUrl: 'materiels-sap-attelledetraction.html',
})
export class MaterielsSapAttelledetractionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapAttelledetractionPage');
  }
  tab65Root=MaterielSapAttelledetractionDescriptionComponent;
  tab66Root=MaterielSapAttelledetractionIndicationsComponent;
  tab67Root=MaterielSapAttelledetractionRisquesComponent;
  tab68Root=MaterielSapAttelledetractionUtilisationComponent;
  tab69Root=MaterielSapAttelledetractionPointsclesComponent;
  tab70Root=MaterielSapAttelledetractionEfficacitéComponent;
}
