import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapAttelledetractionPage } from './materiels-sap-attelledetraction';

@NgModule({
  declarations: [
    MaterielsSapAttelledetractionPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapAttelledetractionPage),
  ],
})
export class MaterielsSapAttelledetractionPageModule {}
