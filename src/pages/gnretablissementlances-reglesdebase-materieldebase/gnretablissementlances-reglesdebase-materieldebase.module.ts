import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesReglesdebaseMaterieldebasePage } from './gnretablissementlances-reglesdebase-materieldebase';

@NgModule({
  declarations: [
    GnretablissementlancesReglesdebaseMaterieldebasePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesReglesdebaseMaterieldebasePage),
  ],
})
export class GnretablissementlancesReglesdebaseMaterieldebasePageModule {}
