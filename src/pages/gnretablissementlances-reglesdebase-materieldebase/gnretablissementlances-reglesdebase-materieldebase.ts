import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesReglesdebaseMaterieldebasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-reglesdebase-materieldebase',
  templateUrl: 'gnretablissementlances-reglesdebase-materieldebase.html',
})
export class GnretablissementlancesReglesdebaseMaterieldebasePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesReglesdebaseMaterieldebasePage');
  }

}
