import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutilsopO2Page } from './outilsop-o2';

@NgModule({
  declarations: [
    OutilsopO2Page,
  ],
  imports: [
    IonicPageModule.forChild(OutilsopO2Page),
  ],
})
export class OutilsopO2PageModule {}
