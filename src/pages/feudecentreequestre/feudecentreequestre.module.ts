import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudecentreequestrePage } from './feudecentreequestre';

@NgModule({
  declarations: [
    FeudecentreequestrePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudecentreequestrePage),
  ],
})
export class FeudecentreequestrePageModule {}
