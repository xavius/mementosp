import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxCentreequestreAccesComponent } from '../../components/feux-centreequestre/feux-centreequestre-acces/feux-centreequestre-acces';
import { FeuxCentreequestreActionComponent } from '../../components/feux-centreequestre/feux-centreequestre-action/feux-centreequestre-action';
import { FeuxCentreequestreAttentionComponent } from '../../components/feux-centreequestre/feux-centreequestre-attention/feux-centreequestre-attention';
import { FeuxCentreequestreInterditComponent } from '../../components/feux-centreequestre/feux-centreequestre-interdit/feux-centreequestre-interdit';
import { FeuxCentreequestreRisquesComponent } from '../../components/feux-centreequestre/feux-centreequestre-risques/feux-centreequestre-risques';
import { FeuxCentreequestreStrategieComponent } from '../../components/feux-centreequestre/feux-centreequestre-strategie/feux-centreequestre-strategie';

/**
 * Generated class for the FeudecentreequestrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudecentreequestre',
  templateUrl: 'feudecentreequestre.html',
})
export class FeudecentreequestrePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudecentreequestrePage');
  }
  tab1Root=FeuxCentreequestreRisquesComponent;
  tab2Root=FeuxCentreequestreStrategieComponent;
  tab3Root=FeuxCentreequestreAccesComponent;
  tab4Root=FeuxCentreequestreActionComponent;
  tab5Root=FeuxCentreequestreAttentionComponent;
  tab6Root=FeuxCentreequestreInterditComponent;
}
