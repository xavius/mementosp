import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevlocauxarisquesPage } from './prevlocauxarisques';

@NgModule({
  declarations: [
    PrevlocauxarisquesPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevlocauxarisquesPage),
  ],
})
export class PrevlocauxarisquesPageModule {}
