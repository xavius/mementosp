import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevLocauxarisquesFonctionnementComponent } from '../../components/prev-locauxarisques/prev-locauxarisques-fonctionnement/prev-locauxarisques-fonctionnement';
import { PrevLocauxarisquesPrecautionComponent } from '../../components/prev-locauxarisques/prev-locauxarisques-precaution/prev-locauxarisques-precaution';

/**
 * Generated class for the PrevlocauxarisquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevlocauxarisques',
  templateUrl: 'prevlocauxarisques.html',
})
export class PrevlocauxarisquesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevlocauxarisquesPage');
  }
  tab46Root=PrevLocauxarisquesFonctionnementComponent;
  tab47Root=PrevLocauxarisquesPrecautionComponent;
}
