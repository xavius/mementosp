import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NrbcRtnEtiquettesdedangerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nrbc-rtn-etiquettesdedanger',
  templateUrl: 'nrbc-rtn-etiquettesdedanger.html',
})
export class NrbcRtnEtiquettesdedangerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NrbcRtnEtiquettesdedangerPage');
  }

}
