import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NrbcRtnEtiquettesdedangerPage } from './nrbc-rtn-etiquettesdedanger';

@NgModule({
  declarations: [
    NrbcRtnEtiquettesdedangerPage,
  ],
  imports: [
    IonicPageModule.forChild(NrbcRtnEtiquettesdedangerPage),
  ],
})
export class NrbcRtnEtiquettesdedangerPageModule {}
