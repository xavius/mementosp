import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxAppartementRisquesComponent } from '../../components/feux-appartement/feux-appartement-risques/feux-appartement-risques';
import { FeuxAppartementStrategieComponent } from '../../components/feux-appartement/feux-appartement-strategie/feux-appartement-strategie';
import { FeuxAppartementAccesComponent } from '../../components/feux-appartement/feux-appartement-acces/feux-appartement-acces';
import { FeuxAppartementActionComponent } from '../../components/feux-appartement/feux-appartement-action/feux-appartement-action';
import { FeuxAppartementAttentionComponent } from '../../components/feux-appartement/feux-appartement-attention/feux-appartement-attention';
import { FeuxAppartementInterditComponent } from '../../components/feux-appartement/feux-appartement-interdit/feux-appartement-interdit';

/**
 * Generated class for the FeudappartementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudappartement',
  templateUrl: 'feudappartement.html',
})
export class FeudappartementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudappartementPage');
  }

  tab1Root= FeuxAppartementRisquesComponent;
  tab2Root= FeuxAppartementStrategieComponent;
  tab3Root= FeuxAppartementAccesComponent;
  tab4Root= FeuxAppartementActionComponent;
  tab5Root= FeuxAppartementAttentionComponent;
  tab6Root= FeuxAppartementInterditComponent;

}
