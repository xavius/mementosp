import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudappartementPage } from './feudappartement';

@NgModule({
  declarations: [
    FeudappartementPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudappartementPage),
  ],
})
export class FeudappartementPageModule {}
