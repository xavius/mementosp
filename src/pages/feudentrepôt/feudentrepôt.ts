import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxDentrepôtAccesComponent } from '../../components/feux-dentrepôt/feux-dentrepôt-acces/feux-dentrepôt-acces';
import { FeuxDentrepôtActionComponent } from '../../components/feux-dentrepôt/feux-dentrepôt-action/feux-dentrepôt-action';
import { FeuxDentrepôtAttentionComponent } from '../../components/feux-dentrepôt/feux-dentrepôt-attention/feux-dentrepôt-attention';
import { FeuxDentrepôtInterditComponent } from '../../components/feux-dentrepôt/feux-dentrepôt-interdit/feux-dentrepôt-interdit';
import { FeuxDentrepôtRisquesComponent } from '../../components/feux-dentrepôt/feux-dentrepôt-risques/feux-dentrepôt-risques';
import { FeuxDentrepôtStrategieComponent } from '../../components/feux-dentrepôt/feux-dentrepôt-strategie/feux-dentrepôt-strategie';

/**
 * Generated class for the FeudentrepôtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudentrepôt',
  templateUrl: 'feudentrepôt.html',
})
export class FeudentrepôtPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudentrepôtPage');
  }
  tab1Root=FeuxDentrepôtRisquesComponent;
  tab2Root=FeuxDentrepôtStrategieComponent;
  tab3Root=FeuxDentrepôtAccesComponent;
  tab4Root=FeuxDentrepôtActionComponent;
  tab5Root=FeuxDentrepôtAttentionComponent;
  tab6Root=FeuxDentrepôtInterditComponent;
}
