import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudentrepôtPage } from './feudentrepôt';

@NgModule({
  declarations: [
    FeudentrepôtPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudentrepôtPage),
  ],
})
export class FeudentrepôtPageModule {}
