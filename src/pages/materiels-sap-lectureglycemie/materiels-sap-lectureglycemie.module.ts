import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapLectureglycemiePage } from './materiels-sap-lectureglycemie';

@NgModule({
  declarations: [
    MaterielsSapLectureglycemiePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapLectureglycemiePage),
  ],
})
export class MaterielsSapLectureglycemiePageModule {}
