import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapLectureglycemieDescriptionComponent } from '../../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-description/materiels-sap-lectureglycemie-description';
import { MaterielsSapLectureglycemieIndicationsComponent } from '../../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-indications/materiels-sap-lectureglycemie-indications';
import { MaterielsSapLectureglycemieRisquesComponent } from '../../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-risques/materiels-sap-lectureglycemie-risques';
import { MaterielsSapLectureglycemieUtilisationComponent } from '../../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-utilisation/materiels-sap-lectureglycemie-utilisation';
import { MaterielsSapLectureglycemiePointsclesComponent } from '../../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-pointscles/materiels-sap-lectureglycemie-pointscles';
import { MaterielsSapLectureglycemieEfficaciteComponent } from '../../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-efficacite/materiels-sap-lectureglycemie-efficacite';


/**
 * Generated class for the MaterielsSapLectureglycemiePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-lectureglycemie',
  templateUrl: 'materiels-sap-lectureglycemie.html',
})
export class MaterielsSapLectureglycemiePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapLectureglycemiePage');
  }
  tab65Root=MaterielsSapLectureglycemieDescriptionComponent;
  tab66Root=MaterielsSapLectureglycemieIndicationsComponent;
  tab67Root=MaterielsSapLectureglycemieRisquesComponent;
  tab68Root=MaterielsSapLectureglycemieUtilisationComponent;
  tab69Root=MaterielsSapLectureglycemiePointsclesComponent;
  tab70Root=MaterielsSapLectureglycemieEfficaciteComponent;
}
