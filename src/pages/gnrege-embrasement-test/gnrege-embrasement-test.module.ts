import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEmbrasementTestPage } from './gnrege-embrasement-test';

@NgModule({
  declarations: [
    GnregeEmbrasementTestPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEmbrasementTestPage),
  ],
})
export class GnregeEmbrasementTestPageModule {}
