import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesRetourdinterPage } from './gnretablissementlances-retourdinter';

@NgModule({
  declarations: [
    GnretablissementlancesRetourdinterPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesRetourdinterPage),
  ],
})
export class GnretablissementlancesRetourdinterPageModule {}
