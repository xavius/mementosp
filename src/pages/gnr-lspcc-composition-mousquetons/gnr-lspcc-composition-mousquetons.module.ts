import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionMousquetonsPage } from './gnr-lspcc-composition-mousquetons';

@NgModule({
  declarations: [
    GnrLspccCompositionMousquetonsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionMousquetonsPage),
  ],
})
export class GnrLspccCompositionMousquetonsPageModule {}
