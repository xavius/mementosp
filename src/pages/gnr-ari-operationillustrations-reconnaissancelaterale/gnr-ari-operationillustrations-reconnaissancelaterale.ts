import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationillustrationsReconnaissancelateralePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationillustrations-reconnaissancelaterale',
  templateUrl: 'gnr-ari-operationillustrations-reconnaissancelaterale.html',
})
export class GnrAriOperationillustrationsReconnaissancelateralePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationillustrationsReconnaissancelateralePage');
  }

}
