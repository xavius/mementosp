import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationillustrationsReconnaissancelateralePage } from './gnr-ari-operationillustrations-reconnaissancelaterale';

@NgModule({
  declarations: [
    GnrAriOperationillustrationsReconnaissancelateralePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationillustrationsReconnaissancelateralePage),
  ],
})
export class GnrAriOperationillustrationsReconnaissancelateralePageModule {}
