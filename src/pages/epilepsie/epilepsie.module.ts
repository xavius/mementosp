import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EpilepsiePage } from './epilepsie';

@NgModule({
  declarations: [
    EpilepsiePage,
  ],
  imports: [
    IonicPageModule.forChild(EpilepsiePage),
  ],
})
export class EpilepsiePageModule {}
