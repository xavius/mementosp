import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapEpilepsieSignesComponent } from '../../components/sap-epilepsie/sap-epilepsie-signes/sap-epilepsie-signes';
import { SapEpilepsieBilansComponent } from '../../components/sap-epilepsie/sap-epilepsie-bilans/sap-epilepsie-bilans';
import { SapEpilepsieActionsComponent } from '../../components/sap-epilepsie/sap-epilepsie-actions/sap-epilepsie-actions';
import { SapEpilepsieImportantComponent } from '../../components/sap-epilepsie/sap-epilepsie-important/sap-epilepsie-important';

/**
 * Generated class for the EpilepsiePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-epilepsie',
  templateUrl: 'epilepsie.html',
})
export class EpilepsiePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EpilepsiePage');
  }
  tab7Root=SapEpilepsieSignesComponent;
  tab8Root=SapEpilepsieBilansComponent;
  tab9Root=SapEpilepsieActionsComponent;
  tab10Root=SapEpilepsieImportantComponent;
}
