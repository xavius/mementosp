import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutresTramwayPage } from './autres-tramway';

@NgModule({
  declarations: [
    AutresTramwayPage,
  ],
  imports: [
    IonicPageModule.forChild(AutresTramwayPage),
  ],
})
export class AutresTramwayPageModule {}
