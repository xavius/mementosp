import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationavantPorteursPage } from './gnr-ari-operationavant-porteurs';

@NgModule({
  declarations: [
    GnrAriOperationavantPorteursPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationavantPorteursPage),
  ],
})
export class GnrAriOperationavantPorteursPageModule {}
