import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxDengraisAccesComponent } from '../../components/feux-dengrais/feux-dengrais-acces/feux-dengrais-acces';
import { FeuxDengraisActionComponent } from '../../components/feux-dengrais/feux-dengrais-action/feux-dengrais-action';
import { FeuxDengraisAttentionComponent } from '../../components/feux-dengrais/feux-dengrais-attention/feux-dengrais-attention';
import { FeuxDengraisInterditComponent } from '../../components/feux-dengrais/feux-dengrais-interdit/feux-dengrais-interdit';
import { FeuxDengraisRisquesComponent } from '../../components/feux-dengrais/feux-dengrais-risques/feux-dengrais-risques';
import { FeuxDengraisStrategieComponent } from '../../components/feux-dengrais/feux-dengrais-strategie/feux-dengrais-strategie';

/**
 * Generated class for the FeudengraisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudengrais',
  templateUrl: 'feudengrais.html',
})
export class FeudengraisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudengraisPage');
  }
  tab1Root=FeuxDengraisRisquesComponent;
  tab2Root=FeuxDengraisStrategieComponent;
  tab3Root=FeuxDengraisAccesComponent;
  tab4Root=FeuxDengraisActionComponent;
  tab5Root=FeuxDengraisAttentionComponent;
  tab6Root=FeuxDengraisInterditComponent;
}
