import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudengraisPage } from './feudengrais';

@NgModule({
  declarations: [
    FeudengraisPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudengraisPage),
  ],
})
export class FeudengraisPageModule {}
