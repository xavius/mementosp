import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesSmesPage } from './gnretablissementlances-smes';

@NgModule({
  declarations: [
    GnretablissementlancesSmesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesSmesPage),
  ],
})
export class GnretablissementlancesSmesPageModule {}
