import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionCordePage } from './gnr-lspcc-composition-corde';

@NgModule({
  declarations: [
    GnrLspccCompositionCordePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionCordePage),
  ],
})
export class GnrLspccCompositionCordePageModule {}
