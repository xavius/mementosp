import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrLspccCompositionCordePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-composition-corde',
  templateUrl: 'gnr-lspcc-composition-corde.html',
})
export class GnrLspccCompositionCordePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccCompositionCordePage');
  }

}
