import { FeudemetauxPage } from './../feudemetaux/feudemetaux';
import { FeudeliquidesinflammablesPage } from './../feudeliquidesinflammables/feudeliquidesinflammables';
import { FeudesilosPage } from './../feudesilos/feudesilos';
import { FeudepesticidesPage } from './../feudepesticides/feudepesticides';
import { FeumatieresplastiquesPage } from './../feumatieresplastiques/feumatieresplastiques';
import { FeuchimiquePage } from './../feuchimique/feuchimique';
import { FeuradiologiquePage } from './../feuradiologique/feuradiologique';
import { FeudetransformateurPage } from './../feudetransformateur/feudetransformateur';
import { FeudechaufferieindustriellePage } from './../feudechaufferieindustrielle/feudechaufferieindustrielle';
import { FeudechaufferiePage } from './../feudechaufferie/feudechaufferie';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FichesfeuxspeciauxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichesfeuxspeciaux',
  templateUrl: 'fichesfeuxspeciaux.html',
})
export class FichesfeuxspeciauxPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Feu de Chaufferie au Fuel', component: FeudechaufferiePage},
      {title:'Feu de Chaufferie Industrielle', component: FeudechaufferieindustriellePage},
      {title:'Feu de Transformateur au PCB', component: FeudetransformateurPage},
      {title:'Feu avec Risque Radiologique', component: FeuradiologiquePage},
      {title:'Feu avec Risque Chimique', component: FeuchimiquePage},
      {title:'Feu avec Matiéres Plastiques', component: FeumatieresplastiquesPage},
      {title:'Feu de Pesticides', component: FeudepesticidesPage},
      {title:'Feu de Silos', component: FeudesilosPage},
      {title:'Feu de Liquides Inflammables', component: FeudeliquidesinflammablesPage},
      {title:'Feu de Métaux', component: FeudemetauxPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichesfeuxspeciauxPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
  }
  
}
