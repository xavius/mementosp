import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichesfeuxspeciauxPage } from './fichesfeuxspeciaux';

@NgModule({
  declarations: [
    FichesfeuxspeciauxPage,
  ],
  imports: [
    IonicPageModule.forChild(FichesfeuxspeciauxPage),
  ],
})
export class FichesfeuxspeciauxPageModule {}
