import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvenginPage } from './gnretablissementlances-manoeuvrescompl-ldvengin';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvrescomplLdvenginPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvrescomplLdvenginPage),
  ],
})
export class GnretablissementlancesManoeuvrescomplLdvenginPageModule {}
