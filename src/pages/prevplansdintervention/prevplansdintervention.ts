import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevPlansdinterventionBaseComponent } from '../../components/prev-plansdintervention/prev-plansdintervention-base/prev-plansdintervention-base';
import { PrevPlansdinterventionEauComponent } from '../../components/prev-plansdintervention/prev-plansdintervention-eau/prev-plansdintervention-eau';
import { PrevPlansdinterventionIncComponent } from '../../components/prev-plansdintervention/prev-plansdintervention-inc/prev-plansdintervention-inc';
import { PrevPlansdinterventionTechComponent } from '../../components/prev-plansdintervention/prev-plansdintervention-tech/prev-plansdintervention-tech';

/**
 * Generated class for the PrevplansdinterventionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevplansdintervention',
  templateUrl: 'prevplansdintervention.html',
})
export class PrevplansdinterventionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevplansdinterventionPage');
  }
  tab52Root=PrevPlansdinterventionBaseComponent;
  tab53Root=PrevPlansdinterventionEauComponent;
  tab54Root=PrevPlansdinterventionIncComponent;
  tab55Root=PrevPlansdinterventionTechComponent;
}
