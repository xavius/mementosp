import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevplansdinterventionPage } from './prevplansdintervention';

@NgModule({
  declarations: [
    PrevplansdinterventionPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevplansdinterventionPage),
  ],
})
export class PrevplansdinterventionPageModule {}
