import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrDemipavillonarriere_1Component } from '../../components/sr-demipavillonarriere/sr-demipavillonarriere-1/sr-demipavillonarriere-1';
import { SrDemipavillonarriere_2Component } from '../../components/sr-demipavillonarriere/sr-demipavillonarriere-2/sr-demipavillonarriere-2';
import { SrDemipavillonarriere_3Component } from '../../components/sr-demipavillonarriere/sr-demipavillonarriere-3/sr-demipavillonarriere-3';

/**
 * Generated class for the SrdemipavillonarrierePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srdemipavillonarriere',
  templateUrl: 'srdemipavillonarriere.html',
})
export class SrdemipavillonarrierePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrdemipavillonarrierePage');
  }
  tab22Root=SrDemipavillonarriere_1Component;
  tab23Root=SrDemipavillonarriere_2Component;
  tab24Root=SrDemipavillonarriere_3Component;
}
