import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrdemipavillonarrierePage } from './srdemipavillonarriere';

@NgModule({
  declarations: [
    SrdemipavillonarrierePage,
  ],
  imports: [
    IonicPageModule.forChild(SrdemipavillonarrierePage),
  ],
})
export class SrdemipavillonarrierePageModule {}
