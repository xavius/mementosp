import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GocMessageradioAmbianceComponent } from '../../components/goc-messageradio/goc-messageradio-ambiance/goc-messageradio-ambiance';
import { GocMessageradioRenseignementComponent } from '../../components/goc-messageradio/goc-messageradio-renseignement/goc-messageradio-renseignement';

/**
 * Generated class for the MessageradioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messageradio',
  templateUrl: 'messageradio.html',
})
export class MessageradioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageradioPage');
  }
  tab33Root=GocMessageradioAmbianceComponent;
  tab34Root=GocMessageradioRenseignementComponent;
}
