import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageradioPage } from './messageradio';

@NgModule({
  declarations: [
    MessageradioPage,
  ],
  imports: [
    IonicPageModule.forChild(MessageradioPage),
  ],
})
export class MessageradioPageModule {}
