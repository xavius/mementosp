import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxLiquidesinflammablesAccesComponent } from '../../components/feux-liquidesinflammables/feux-liquidesinflammables-acces/feux-liquidesinflammables-acces';
import { FeuxLiquidesinflammablesActionComponent } from '../../components/feux-liquidesinflammables/feux-liquidesinflammables-action/feux-liquidesinflammables-action';
import { FeuxLiquidesinflammablesAttentionComponent } from '../../components/feux-liquidesinflammables/feux-liquidesinflammables-attention/feux-liquidesinflammables-attention';
import { FeuxLiquidesinflammablesInterditComponent } from '../../components/feux-liquidesinflammables/feux-liquidesinflammables-interdit/feux-liquidesinflammables-interdit';
import { FeuxLiquidesinflammablesRisquesComponent } from '../../components/feux-liquidesinflammables/feux-liquidesinflammables-risques/feux-liquidesinflammables-risques';
import { FeuxLiquidesinflammablesStrategieComponent } from '../../components/feux-liquidesinflammables/feux-liquidesinflammables-strategie/feux-liquidesinflammables-strategie';

/**
 * Generated class for the FeudeliquidesinflammablesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudeliquidesinflammables',
  templateUrl: 'feudeliquidesinflammables.html',
})
export class FeudeliquidesinflammablesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudeliquidesinflammablesPage');
  }
  tab1Root=FeuxLiquidesinflammablesRisquesComponent;
  tab2Root=FeuxLiquidesinflammablesStrategieComponent;
  tab3Root=FeuxLiquidesinflammablesAccesComponent;
  tab4Root=FeuxLiquidesinflammablesActionComponent;
  tab5Root=FeuxLiquidesinflammablesAttentionComponent;
  tab6Root=FeuxLiquidesinflammablesInterditComponent;

}
