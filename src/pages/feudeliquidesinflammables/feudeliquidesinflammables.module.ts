import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudeliquidesinflammablesPage } from './feudeliquidesinflammables';

@NgModule({
  declarations: [
    FeudeliquidesinflammablesPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudeliquidesinflammablesPage),
  ],
})
export class FeudeliquidesinflammablesPageModule {}
