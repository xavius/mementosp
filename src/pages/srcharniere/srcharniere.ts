import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrCharniere_1Component } from '../../components/sr-charniere/sr-charniere-1/sr-charniere-1';
import { SrCharniere_2Component } from '../../components/sr-charniere/sr-charniere-2/sr-charniere-2';
import { SrCharniere_3Component } from '../../components/sr-charniere/sr-charniere-3/sr-charniere-3';

/**
 * Generated class for the SrcharnierePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srcharniere',
  templateUrl: 'srcharniere.html',
})
export class SrcharnierePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrcharnierePage');
  }
  tab22Root=SrCharniere_1Component;
  tab23Root=SrCharniere_2Component;
  tab24Root=SrCharniere_3Component;
}
