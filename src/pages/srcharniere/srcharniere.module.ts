import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrcharnierePage } from './srcharniere';

@NgModule({
  declarations: [
    SrcharnierePage,
  ],
  imports: [
    IonicPageModule.forChild(SrcharnierePage),
  ],
})
export class SrcharnierePageModule {}
