import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfCcfAlimentationPage } from './gnrfdf-ccf-alimentation';

@NgModule({
  declarations: [
    GnrfdfCcfAlimentationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfCcfAlimentationPage),
  ],
})
export class GnrfdfCcfAlimentationPageModule {}
