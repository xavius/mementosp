import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrfdfCcfAlimentationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrfdf-ccf-alimentation',
  templateUrl: 'gnrfdf-ccf-alimentation.html',
})
export class GnrfdfCcfAlimentationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrfdfCcfAlimentationPage');
  }

}
