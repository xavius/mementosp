import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationExplorationPage } from './gnr-lspcc-operation-sauvetageexcavation-exploration';

@NgModule({
  declarations: [
    GnrLspccOperationSauvetageexcavationExplorationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationSauvetageexcavationExplorationPage),
  ],
})
export class GnrLspccOperationSauvetageexcavationExplorationPageModule {}
