import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent } from '../../components/gnr-lspcc-operation-sauvetageexcavation-exploration/gnr-lspcc-operation-sauvetageexcavation-exploration-chefdequipe/gnr-lspcc-operation-sauvetageexcavation-exploration-chefdequipe';
import { GnrLspccOperationSauvetageexcavationExplorationEquipierComponent } from '../../components/gnr-lspcc-operation-sauvetageexcavation-exploration/gnr-lspcc-operation-sauvetageexcavation-exploration-equipier/gnr-lspcc-operation-sauvetageexcavation-exploration-equipier';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationExplorationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-sauvetageexcavation-exploration',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-exploration.html',
})
export class GnrLspccOperationSauvetageexcavationExplorationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationSauvetageexcavationExplorationPage');
  }
  tab62Root=GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent;
  tab63Root=GnrLspccOperationSauvetageexcavationExplorationEquipierComponent;
}
