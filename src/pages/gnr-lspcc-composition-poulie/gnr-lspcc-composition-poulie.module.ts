import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionPouliePage } from './gnr-lspcc-composition-poulie';

@NgModule({
  declarations: [
    GnrLspccCompositionPouliePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionPouliePage),
  ],
})
export class GnrLspccCompositionPouliePageModule {}
