import { GnregeConduiteatenirTechniquePage } from './../gnrege-conduiteatenir-technique/gnrege-conduiteatenir-technique';
import { GnregeSyntheseTypologiePage } from './../gnrege-synthese-typologie/gnrege-synthese-typologie';
import { GnregeEmbrasementTestPage } from './../gnrege-embrasement-test/gnrege-embrasement-test';
import { GnregeEmbrasementSignesPage } from './../gnrege-embrasement-signes/gnrege-embrasement-signes';
import { GnregeEmbrasementScenarioPage } from './../gnrege-embrasement-scenario/gnrege-embrasement-scenario';
import { GnregeEmbrasementParametrePage } from './../gnrege-embrasement-parametre/gnrege-embrasement-parametre';
import { GnregeEmbrasementDefinitionPage } from './../gnrege-embrasement-definition/gnrege-embrasement-definition';
import { GnregeExplosiondefumeeSignesPage } from './../gnrege-explosiondefumee-signes/gnrege-explosiondefumee-signes';
import { GnregeExplosiondefumeeParametresPage } from './../gnrege-explosiondefumee-parametres/gnrege-explosiondefumee-parametres';
import { GnregeExplosiondefumeeDefinitionPage } from './../gnrege-explosiondefumee-definition/gnrege-explosiondefumee-definition';
import { GnregeEnvironnementScenarioPage } from './../gnrege-environnement-scenario/gnrege-environnement-scenario';
import { GnregeEnvironnementElementsPage } from './../gnrege-environnement-elements/gnrege-environnement-elements';
import { GnregeEnvironnementEnveloppePage } from './../gnrege-environnement-enveloppe/gnrege-environnement-enveloppe';
import { GnregePreambulePage } from './../gnrege-preambule/gnrege-preambule';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnregeExplosiondefumeeScenarioPage } from '../gnrege-explosiondefumee-scenario/gnrege-explosiondefumee-scenario';
import { GnregeSyntheseComparaisonPage } from '../gnrege-synthese-comparaison/gnrege-synthese-comparaison';
import { GnregeSyntheseApparitionPage } from '../gnrege-synthese-apparition/gnrege-synthese-apparition';
import { GnregeSyntheseConjugaisonPage } from '../gnrege-synthese-conjugaison/gnrege-synthese-conjugaison';
import { GnregeConduiteatenirLecturePage } from '../gnrege-conduiteatenir-lecture/gnrege-conduiteatenir-lecture';
import { GnregeConduiteatenirActionsPage } from '../gnrege-conduiteatenir-actions/gnrege-conduiteatenir-actions';
import { GnregeConduiteatenirSynoptiquePage } from '../gnrege-conduiteatenir-synoptique/gnrege-conduiteatenir-synoptique';
import { GnregeConduiteatenirConsignesPage } from '../gnrege-conduiteatenir-consignes/gnrege-conduiteatenir-consignes';

/**
 * Generated class for the GnregePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege',
  templateUrl: 'gnrege.html',
})
export class GnregePage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  list3: Array<{title: string, component: any}>;
  list4: Array<{title: string, component: any}>;
  list5: Array<{title: string, component: any}>;
  list6: Array<{title: string, component: any}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'PREAMBULE', component: GnregePreambulePage}
    ];
    this.list2 = [
      {title:'Enveloppe bâtimentaire', component: GnregeEnvironnementEnveloppePage},
      {title:'Eléments nécessaires au développement du feu', component: GnregeEnvironnementElementsPage},
      {title:'Scénario type d un incendie dans un volume' , component: GnregeEnvironnementScenarioPage}
    ];
    this.list3 = [
      {title:'Définition', component: GnregeExplosiondefumeeDefinitionPage},
      {title:'Paramètre d apparition du phénomène', component: GnregeExplosiondefumeeParametresPage},
      {title:'Scénario type et description du phénomène', component: GnregeExplosiondefumeeScenarioPage},
      {title:'Signes d alarme' , component: GnregeExplosiondefumeeSignesPage}
    ];
    this.list4 = [
      {title:'Définition', component: GnregeEmbrasementDefinitionPage},
      {title:'Paramètre d apparition du phénomène', component: GnregeEmbrasementParametrePage},
      {title:'Scénario type et description du phénomène', component: GnregeEmbrasementScenarioPage},
      {title:'Signes d alarme' , component: GnregeEmbrasementSignesPage},
      {title:'Test du plafond', component: GnregeEmbrasementTestPage}
    ];
    this.list5 = [
      {title:'Comparaison des deux phénomènes', component: GnregeSyntheseComparaisonPage},
      {title:'Apparition des phénomènes dans le temps', component: GnregeSyntheseApparitionPage},
      {title:'Typologie incertaine', component: GnregeSyntheseTypologiePage},
      {title:'Conjugaison des phénomènes d’embrasement généralisé éclair et d’explosion de fumées' , component: GnregeSyntheseConjugaisonPage}
    ];
    this.list6 = [
      {title:'Lecture précise du feu', component: GnregeConduiteatenirLecturePage},
      {title:'Technique de progression (T.O.O.T.EM)', component: GnregeConduiteatenirTechniquePage},
      {title:'Actions tactiques', component: GnregeConduiteatenirActionsPage},
      {title:'Synoptique de la conduite a tenir' , component: GnregeConduiteatenirSynoptiquePage},
      {title:'Consignes de sécurité', component: GnregeConduiteatenirConsignesPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregePage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
