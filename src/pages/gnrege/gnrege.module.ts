import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregePage } from './gnrege';

@NgModule({
  declarations: [
    GnregePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregePage),
  ],
})
export class GnregePageModule {}
