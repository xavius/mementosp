import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfCcfPossibilitePage } from './gnrfdf-ccf-possibilite';

@NgModule({
  declarations: [
    GnrfdfCcfPossibilitePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfCcfPossibilitePage),
  ],
})
export class GnrfdfCcfPossibilitePageModule {}
