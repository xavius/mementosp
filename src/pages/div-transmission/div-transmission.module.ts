import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivTransmissionPage } from './div-transmission';

@NgModule({
  declarations: [
    DivTransmissionPage,
  ],
  imports: [
    IonicPageModule.forChild(DivTransmissionPage),
  ],
})
export class DivTransmissionPageModule {}
