import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfPage } from './gnrfdf';

@NgModule({
  declarations: [
    GnrfdfPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfPage),
  ],
})
export class GnrfdfPageModule {}
