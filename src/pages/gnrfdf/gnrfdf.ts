import { GnrfdfDihPrincipePage } from './../gnrfdf-dih-principe/gnrfdf-dih-principe';
import { GnrfdfDihCompositionPage } from './../gnrfdf-dih-composition/gnrfdf-dih-composition';
import { GnrfdfUiffDefensivesPage } from './../gnrfdf-uiff-defensives/gnrfdf-uiff-defensives';
import { GnrfdfUiffEtablissementsPage } from './../gnrfdf-uiff-etablissements/gnrfdf-uiff-etablissements';
import { GnrfdfUiffOffensivesPage } from './../gnrfdf-uiff-offensives/gnrfdf-uiff-offensives';
import { GnrfdfUiffDeplacementsPage } from './../gnrfdf-uiff-deplacements/gnrfdf-uiff-deplacements';
import { GnrfdfGiffAlimentationPage } from './../gnrfdf-giff-alimentation/gnrfdf-giff-alimentation';
import { GnrfdfGiffDeplacementPage } from './../gnrfdf-giff-deplacement/gnrfdf-giff-deplacement';
import { GnrfdfGiffOffensivesPage } from './../gnrfdf-giff-offensives/gnrfdf-giff-offensives';
import { GnrfdfGiffDefensivesPage } from './../gnrfdf-giff-defensives/gnrfdf-giff-defensives';
import { GnrfdfCcfAlimentationPage } from './../gnrfdf-ccf-alimentation/gnrfdf-ccf-alimentation';
import { GnrfdfCcfBasePage } from './../gnrfdf-ccf-base/gnrfdf-ccf-base';
import { GnrfdfCcfPossibilitePage } from './../gnrfdf-ccf-possibilite/gnrfdf-ccf-possibilite';
import { GnrfdfMesuresAutoPage } from './../gnrfdf-mesures-auto/gnrfdf-mesures-auto';
import { GnrfdfMesuresSecuritePage } from './../gnrfdf-mesures-securite/gnrfdf-mesures-securite';
import { GnrfdfPreambulePage } from './../gnrfdf-preambule/gnrfdf-preambule';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrfdfMoyensTerrestresPage } from '../gnrfdf-moyens-terrestres/gnrfdf-moyens-terrestres';
import { GnrfdfMoyensAeriensPage } from '../gnrfdf-moyens-aeriens/gnrfdf-moyens-aeriens';
import { GnrfdfDihMissionsPage } from '../gnrfdf-dih-missions/gnrfdf-dih-missions';
import { GnrfdfDihLimitesPage } from '../gnrfdf-dih-limites/gnrfdf-dih-limites';
import { GnrfdfDihMoyensPage } from '../gnrfdf-dih-moyens/gnrfdf-dih-moyens';
import { GnrfdfDihDeroulementPage } from '../gnrfdf-dih-deroulement/gnrfdf-dih-deroulement';

/**
 * Generated class for the GnrfdfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrfdf',
  templateUrl: 'gnrfdf.html',
})
export class GnrfdfPage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  list3: Array<{title: string, component: any}>;
  list4: Array<{title: string, component: any}>;
  list5: Array<{title: string, component: any}>;
  list6: Array<{title: string, component: any}>;
  list7: Array<{title: string, component: any}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'PREAMBULE', component: GnrfdfPreambulePage}
    ];
    this.list2 = [
      {title:'MOYENS OPÉRATIONNELS TERRESTRES', component: GnrfdfMoyensTerrestresPage},
      {title:'MOYENS AÉRIENS' , component: GnrfdfMoyensAeriensPage}
    ];
    this.list3 = [
      {title:'MESURES DE SÉCURITÉ', component: GnrfdfMesuresSecuritePage},
      {title:'MESURES D’AUTOPROTECTION ET D’AUTODÉFENSE' , component: GnrfdfMesuresAutoPage}
    ];
    this.list4 = [
      {title:'POSSIBILITÉS D’ÉTABLISSEMENT', component: GnrfdfCcfPossibilitePage},
      {title:'MANOEUVRES DE BASE', component: GnrfdfCcfBasePage},
      {title:'MANOEUVRES D’ALIMENTATION' , component: GnrfdfCcfAlimentationPage}
    ];
    this.list5 = [
      {title:'DEPLACEMENTS', component: GnrfdfGiffDeplacementPage},
      {title:'MANOEUVRES OFFENSIVES', component: GnrfdfGiffOffensivesPage},
      {title:'MANOEUVRES DEFENSIVES', component: GnrfdfGiffDefensivesPage},
      {title:'MANOEUVRES D’ALIMENTATION DES CITERNES DES CCF ET DU GIFF' , component: GnrfdfGiffAlimentationPage}
    ];
    this.list6 = [
      {title:'DEPLACEMENTS', component: GnrfdfUiffDeplacementsPage},
      {title:'MANOEUVRES OFFENSIVES', component: GnrfdfUiffOffensivesPage},
      {title:'ÉTABLISSEMENTS' , component: GnrfdfUiffEtablissementsPage},
      {title:'MANOEUVRES DEFENSIVES', component: GnrfdfUiffDefensivesPage}
    ];
    this.list7 = [
      {title:'MISSIONS', component: GnrfdfDihMissionsPage},
      {title:'LIMITES D’EMPLOI AÉRONAUTIQUES', component: GnrfdfDihLimitesPage},
      {title:'COMPOSITION' , component: GnrfdfDihCompositionPage},
      {title:'MOYENS' , component: GnrfdfDihMoyensPage},
      {title:'DÉROULEMENT CHRONOLOGIQUE D’UNE OPÉRATION' , component: GnrfdfDihDeroulementPage},
      {title:'PRINCIPE D’UTILISATION', component: GnrfdfDihPrincipePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrfdfPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
