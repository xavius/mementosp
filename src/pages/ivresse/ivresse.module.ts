import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IvressePage } from './ivresse';

@NgModule({
  declarations: [
    IvressePage,
  ],
  imports: [
    IonicPageModule.forChild(IvressePage),
  ],
})
export class IvressePageModule {}
