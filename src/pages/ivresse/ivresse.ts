import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapIvresseSignesComponent } from '../../components/sap-ivresse/sap-ivresse-signes/sap-ivresse-signes';
import { SapIvresseBilansComponent } from '../../components/sap-ivresse/sap-ivresse-bilans/sap-ivresse-bilans';
import { SapIvresseActionsComponent } from '../../components/sap-ivresse/sap-ivresse-actions/sap-ivresse-actions';
import { SapIvresseImportantComponent } from '../../components/sap-ivresse/sap-ivresse-important/sap-ivresse-important';

/**
 * Generated class for the IvressePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ivresse',
  templateUrl: 'ivresse.html',
})
export class IvressePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IvressePage');
  }
  tab7Root=SapIvresseSignesComponent;
  tab8Root=SapIvresseBilansComponent;
  tab9Root=SapIvresseActionsComponent;
  tab10Root=SapIvresseImportantComponent;
}
