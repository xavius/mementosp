import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapPendaisonSignesComponent } from '../../components/sap-pendaison/sap-pendaison-signes/sap-pendaison-signes';
import { SapPendaisonBilansComponent } from '../../components/sap-pendaison/sap-pendaison-bilans/sap-pendaison-bilans';
import { SapPendaisonActionsComponent } from '../../components/sap-pendaison/sap-pendaison-actions/sap-pendaison-actions';
import { SapPendaisonImportantComponent } from '../../components/sap-pendaison/sap-pendaison-important/sap-pendaison-important';

/**
 * Generated class for the PendaisonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pendaison',
  templateUrl: 'pendaison.html',
})
export class PendaisonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendaisonPage');
  }
  tab7Root=SapPendaisonSignesComponent;
  tab8Root=SapPendaisonBilansComponent;
  tab9Root=SapPendaisonActionsComponent;
  tab10Root=SapPendaisonImportantComponent;
}
