import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendaisonPage } from './pendaison';

@NgModule({
  declarations: [
    PendaisonPage,
  ],
  imports: [
    IonicPageModule.forChild(PendaisonPage),
  ],
})
export class PendaisonPageModule {}
