import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DesincarcerationPage } from './desincarceration';

@NgModule({
  declarations: [
    DesincarcerationPage,
  ],
  imports: [
    IonicPageModule.forChild(DesincarcerationPage),
  ],
})
export class DesincarcerationPageModule {}
