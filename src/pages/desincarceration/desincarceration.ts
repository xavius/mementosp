import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapDesincarserationSignesComponent } from '../../components/sap-desincarseration/sap-desincarseration-signes/sap-desincarseration-signes';
import { SapDesincarserationBilansComponent } from '../../components/sap-desincarseration/sap-desincarseration-bilans/sap-desincarseration-bilans';
import { SapDesincarserationActionsComponent } from '../../components/sap-desincarseration/sap-desincarseration-actions/sap-desincarseration-actions';
import { SapDesincarserationImportantComponent } from '../../components/sap-desincarseration/sap-desincarseration-important/sap-desincarseration-important';

/**
 * Generated class for the DesincarcerationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-desincarceration',
  templateUrl: 'desincarceration.html',
})
export class DesincarcerationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DesincarcerationPage');
  }
  tab7Root=SapDesincarserationSignesComponent;
  tab8Root=SapDesincarserationBilansComponent;
  tab9Root=SapDesincarserationActionsComponent;
  tab10Root=SapDesincarserationImportantComponent;
}
