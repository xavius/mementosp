import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesSacdattaquePage } from './gnretablissementlances-sacdattaque';

@NgModule({
  declarations: [
    GnretablissementlancesSacdattaquePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesSacdattaquePage),
  ],
})
export class GnretablissementlancesSacdattaquePageModule {}
