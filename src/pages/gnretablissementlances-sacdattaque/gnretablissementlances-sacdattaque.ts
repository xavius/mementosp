import { GnretablissementlancesSacdattaqueLdvexterieurPage } from './../gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur';
import { GnretablissementlancesSacdattaqueLdvcommunicationPage } from './../gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication';
import { GnretablissementlancesSacdattaqueMaterielPage } from './../gnretablissementlances-sacdattaque-materiel/gnretablissementlances-sacdattaque-materiel';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesSacdattaquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-sacdattaque',
  templateUrl: 'gnretablissementlances-sacdattaque.html',
})
export class GnretablissementlancesSacdattaquePage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Matériel' , component: GnretablissementlancesSacdattaqueMaterielPage},
      {title:'LDV - Communication Existante' , component: GnretablissementlancesSacdattaqueLdvcommunicationPage},
      {title:'LDV par l Exterieur' , component: GnretablissementlancesSacdattaqueLdvexterieurPage}

    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesSacdattaquePage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
