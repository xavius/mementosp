import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvpoteauPage } from './gnretablissementlances-manoeuvrescompl-ldvpoteau';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvrescomplLdvpoteauPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvrescomplLdvpoteauPage),
  ],
})
export class GnretablissementlancesManoeuvrescomplLdvpoteauPageModule {}
