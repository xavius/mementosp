import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvpoteauChefdagresComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau-chefdagres/gnretablissementlances-manoeuvrescompl-ldvpoteau-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvpoteauBatComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau-bat/gnretablissementlances-manoeuvrescompl-ldvpoteau-bat';
import { GnretablissementlancesManoeuvrescomplLdvpoteauBalComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau-bal/gnretablissementlances-manoeuvrescompl-ldvpoteau-bal';

/**
 * Generated class for the GnretablissementlancesManoeuvrescomplLdvpoteauPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvrescompl-ldvpoteau',
  templateUrl: 'gnretablissementlances-manoeuvrescompl-ldvpoteau.html',
})
export class GnretablissementlancesManoeuvrescomplLdvpoteauPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvrescomplLdvpoteauPage');
  }
  tab61Root=GnretablissementlancesManoeuvrescomplLdvpoteauChefdagresComponent;
  tab46Root=GnretablissementlancesManoeuvrescomplLdvpoteauBatComponent;
  tab64Root=GnretablissementlancesManoeuvrescomplLdvpoteauBalComponent;  
}
