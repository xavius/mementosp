import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEmbrasementParametrePage } from './gnrege-embrasement-parametre';

@NgModule({
  declarations: [
    GnregeEmbrasementParametrePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEmbrasementParametrePage),
  ],
})
export class GnregeEmbrasementParametrePageModule {}
