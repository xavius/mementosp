import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrPage } from './gnretablissementlances-manoeuvres-gnr';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrPage),
  ],
})
export class GnretablissementlancesManoeuvresGnrPageModule {}
