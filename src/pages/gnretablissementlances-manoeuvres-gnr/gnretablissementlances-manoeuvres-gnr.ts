import { GnretablissementlancesManoeuvresGnrM6Page } from './../gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6';
import { GnretablissementlancesManoeuvresGnrM3Page } from './../gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3';
import { GnretablissementlancesManoeuvresGnrM2Page } from './../gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM1Page } from '../gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1';
import { GnretablissementlancesManoeuvresGnrM4Page } from '../gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4';
import { GnretablissementlancesManoeuvresGnrM5Page } from '../gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5';
import { GnretablissementlancesManoeuvresGnrCombinaisonsPage } from '../gnretablissementlances-manoeuvres-gnr-combinaisons/gnretablissementlances-manoeuvres-gnr-combinaisons';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr.html',
})
export class GnretablissementlancesManoeuvresGnrPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'M1: LDT' , component: GnretablissementlancesManoeuvresGnrM1Page},
      {title:'M2: Alimentation prise d eau' , component: GnretablissementlancesManoeuvresGnrM2Page},
      {title:'M3: Lance sur prise d eau' , component: GnretablissementlancesManoeuvresGnrM3Page},
      {title:'M4: Alimentation de l engin' , component: GnretablissementlancesManoeuvresGnrM4Page},
      {title:'M5: Lance à Mousse', component: GnretablissementlancesManoeuvresGnrM5Page},
      {title:'M6: Prolongement/Changement', component: GnretablissementlancesManoeuvresGnrM6Page},
      {title:'Combinaisons Possibles', component: GnretablissementlancesManoeuvresGnrCombinaisonsPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
