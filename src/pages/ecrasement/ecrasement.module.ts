import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EcrasementPage } from './ecrasement';

@NgModule({
  declarations: [
    EcrasementPage,
  ],
  imports: [
    IonicPageModule.forChild(EcrasementPage),
  ],
})
export class EcrasementPageModule {}
