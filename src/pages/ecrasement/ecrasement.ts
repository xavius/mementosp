import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapEcrasementSignesComponent } from '../../components/sap-ecrasement/sap-ecrasement-signes/sap-ecrasement-signes';
import { SapEcrasementBilansComponent } from '../../components/sap-ecrasement/sap-ecrasement-bilans/sap-ecrasement-bilans';
import { SapEcrasementActionsComponent } from '../../components/sap-ecrasement/sap-ecrasement-actions/sap-ecrasement-actions';
import { SapEcrasementImportantComponent } from '../../components/sap-ecrasement/sap-ecrasement-important/sap-ecrasement-important';

/**
 * Generated class for the EcrasementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ecrasement',
  templateUrl: 'ecrasement.html',
})
export class EcrasementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EcrasementPage');
  }
  tab7Root=SapEcrasementSignesComponent;
  tab8Root=SapEcrasementBilansComponent;
  tab9Root=SapEcrasementActionsComponent;
  tab10Root=SapEcrasementImportantComponent;
}
