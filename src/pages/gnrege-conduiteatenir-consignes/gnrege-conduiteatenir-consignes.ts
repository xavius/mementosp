import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeConduiteatenirConsignesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-conduiteatenir-consignes',
  templateUrl: 'gnrege-conduiteatenir-consignes.html',
})
export class GnregeConduiteatenirConsignesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeConduiteatenirConsignesPage');
  }

}
