import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeConduiteatenirConsignesPage } from './gnrege-conduiteatenir-consignes';

@NgModule({
  declarations: [
    GnregeConduiteatenirConsignesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeConduiteatenirConsignesPage),
  ],
})
export class GnregeConduiteatenirConsignesPageModule {}
