import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationSauvetageexterieurPage } from './gnr-lspcc-operation-sauvetageexterieur';

@NgModule({
  declarations: [
    GnrLspccOperationSauvetageexterieurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationSauvetageexterieurPage),
  ],
})
export class GnrLspccOperationSauvetageexterieurPageModule {}
