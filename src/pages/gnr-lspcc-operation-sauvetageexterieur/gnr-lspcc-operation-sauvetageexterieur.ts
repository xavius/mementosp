import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccOperationSauvetageexterieurChefdagresComponent } from '../../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-chefdagres/gnr-lspcc-operation-sauvetageexterieur-chefdagres';
import { GnrLspccOperationSauvetageexterieurChefdequipeComponent } from '../../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-chefdequipe/gnr-lspcc-operation-sauvetageexterieur-chefdequipe';
import { GnrLspccOperationSauvetageexterieurEquipierComponent } from '../../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-equipier/gnr-lspcc-operation-sauvetageexterieur-equipier';
import { GnrLspccOperationSauvetageexterieur_2ebinomeComponent } from '../../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-2ebinome/gnr-lspcc-operation-sauvetageexterieur-2ebinome';

/**
 * Generated class for the GnrLspccOperationSauvetageexterieurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-sauvetageexterieur',
  templateUrl: 'gnr-lspcc-operation-sauvetageexterieur.html',
})
export class GnrLspccOperationSauvetageexterieurPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationSauvetageexterieurPage');
  }
  tab61Root=GnrLspccOperationSauvetageexterieurChefdagresComponent;
  tab62Root=GnrLspccOperationSauvetageexterieurChefdequipeComponent;
  tab63Root=GnrLspccOperationSauvetageexterieurEquipierComponent;
  tab64Root=GnrLspccOperationSauvetageexterieur_2ebinomeComponent;
  }
