import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutilsopHydroliquePage } from './outilsop-hydrolique';

@NgModule({
  declarations: [
    OutilsopHydroliquePage,
  ],
  imports: [
    IonicPageModule.forChild(OutilsopHydroliquePage),
  ],
})
export class OutilsopHydroliquePageModule {}
