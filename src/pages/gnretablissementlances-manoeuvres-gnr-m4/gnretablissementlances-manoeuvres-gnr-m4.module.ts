import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM4Page } from './gnretablissementlances-manoeuvres-gnr-m4';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrM4Page,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrM4Page),
  ],
})
export class GnretablissementlancesManoeuvresGnrM4PageModule {}
