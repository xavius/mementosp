import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM4ChefdagresComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4-chefdagres/gnretablissementlances-manoeuvres-gnr-m4-chefdagres';
import { GnretablissementlancesManoeuvresGnrM4ChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4-chefdequipe/gnretablissementlances-manoeuvres-gnr-m4-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM4EquipierComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4-equipier/gnretablissementlances-manoeuvres-gnr-m4-equipier';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrM4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-m4',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-m4.html',
})
export class GnretablissementlancesManoeuvresGnrM4Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrM4Page');
  }
  tab61Root=GnretablissementlancesManoeuvresGnrM4ChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvresGnrM4ChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvresGnrM4EquipierComponent;
}
