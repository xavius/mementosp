import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiabetiquePage } from './diabetique';

@NgModule({
  declarations: [
    DiabetiquePage,
  ],
  imports: [
    IonicPageModule.forChild(DiabetiquePage),
  ],
})
export class DiabetiquePageModule {}
