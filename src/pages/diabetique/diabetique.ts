import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapDiabetiqueSignesComponent } from '../../components/sap-diabetique/sap-diabetique-signes/sap-diabetique-signes';
import { SapDiabetiqueBilansComponent } from '../../components/sap-diabetique/sap-diabetique-bilans/sap-diabetique-bilans';
import { SapDiabetiqueActionsComponent } from '../../components/sap-diabetique/sap-diabetique-actions/sap-diabetique-actions';
import { SapDiabetiqueImportantComponent } from '../../components/sap-diabetique/sap-diabetique-important/sap-diabetique-important';

/**
 * Generated class for the DiabetiquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-diabetique',
  templateUrl: 'diabetique.html',
})
export class DiabetiquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiabetiquePage');
  }
  tab7Root=SapDiabetiqueSignesComponent;
  tab8Root=SapDiabetiqueBilansComponent;
  tab9Root=SapDiabetiqueActionsComponent;
  tab10Root=SapDiabetiqueImportantComponent;
}
