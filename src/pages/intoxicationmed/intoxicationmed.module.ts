import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntoxicationmedPage } from './intoxicationmed';

@NgModule({
  declarations: [
    IntoxicationmedPage,
  ],
  imports: [
    IonicPageModule.forChild(IntoxicationmedPage),
  ],
})
export class IntoxicationmedPageModule {}
