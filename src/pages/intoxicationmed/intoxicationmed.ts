import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapIntoxicationmedSignesComponent } from '../../components/sap-intoxicationmed/sap-intoxicationmed-signes/sap-intoxicationmed-signes';
import { SapIntoxicationmedBilansComponent } from '../../components/sap-intoxicationmed/sap-intoxicationmed-bilans/sap-intoxicationmed-bilans';
import { SapIntoxicationmedActionsComponent } from '../../components/sap-intoxicationmed/sap-intoxicationmed-actions/sap-intoxicationmed-actions';
import { SapIntoxicationmedImportantComponent } from '../../components/sap-intoxicationmed/sap-intoxicationmed-important/sap-intoxicationmed-important';

/**
 * Generated class for the IntoxicationmedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intoxicationmed',
  templateUrl: 'intoxicationmed.html',
})
export class IntoxicationmedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntoxicationmedPage');
  }
  tab7Root=SapIntoxicationmedSignesComponent;
  tab8Root=SapIntoxicationmedBilansComponent;
  tab9Root=SapIntoxicationmedActionsComponent;
  tab10Root=SapIntoxicationmedImportantComponent;
}
