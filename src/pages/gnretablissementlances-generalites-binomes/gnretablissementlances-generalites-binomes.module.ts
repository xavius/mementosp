import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesBinomesPage } from './gnretablissementlances-generalites-binomes';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesBinomesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesBinomesPage),
  ],
})
export class GnretablissementlancesGeneralitesBinomesPageModule {}
