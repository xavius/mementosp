import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrchartegraphiquePage } from './srchartegraphique';

@NgModule({
  declarations: [
    SrchartegraphiquePage,
  ],
  imports: [
    IonicPageModule.forChild(SrchartegraphiquePage),
  ],
})
export class SrchartegraphiquePageModule {}
