import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudechemineePage } from './feudecheminee';

@NgModule({
  declarations: [
    FeudechemineePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudechemineePage),
  ],
})
export class FeudechemineePageModule {}
