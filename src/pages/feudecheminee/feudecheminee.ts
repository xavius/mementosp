import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxChemineeAccesComponent } from '../../components/feux-cheminee/feux-cheminee-acces/feux-cheminee-acces';
import { FeuxChemineeActionComponent } from '../../components/feux-cheminee/feux-cheminee-action/feux-cheminee-action';
import { FeuxChemineeAttentionComponent } from '../../components/feux-cheminee/feux-cheminee-attention/feux-cheminee-attention';
import { FeuxChemineeInterditComponent } from '../../components/feux-cheminee/feux-cheminee-interdit/feux-cheminee-interdit';
import { FeuxChemineeRisquesComponent } from '../../components/feux-cheminee/feux-cheminee-risques/feux-cheminee-risques';
import { FeuxChemineeStrategieComponent } from '../../components/feux-cheminee/feux-cheminee-strategie/feux-cheminee-strategie';

/**
 * Generated class for the FeudechemineePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudecheminee',
  templateUrl: 'feudecheminee.html',
})
export class FeudechemineePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudechemineePage');
  }

tab1Root=FeuxChemineeRisquesComponent;
tab2Root=FeuxChemineeStrategieComponent;
tab3Root=FeuxChemineeAccesComponent;
tab4Root=FeuxChemineeActionComponent;
tab5Root=FeuxChemineeAttentionComponent;
tab6Root=FeuxChemineeInterditComponent;
 }
