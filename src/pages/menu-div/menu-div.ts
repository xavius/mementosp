import { DivOuvertureportePage } from './../div-ouvertureporte/div-ouvertureporte';
import { DivAnimalierPage } from './../div-animalier/div-animalier';
import { DivTopographiePage } from './../div-topographie/div-topographie';
import { DivAscenseursPage } from './../div-ascenseurs/div-ascenseurs';
import { DivTronconnagePage } from './../div-tronconnage/div-tronconnage';
import { DivBachagePage } from './../div-bachage/div-bachage';
import { DivCalagePage } from './../div-calage/div-calage';
import { DivEpuisementPage } from './../div-epuisement/div-epuisement';
import { DivTransmissionPage } from './../div-transmission/div-transmission';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MenuDivPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-div',
  templateUrl: 'menu-div.html',
})
export class MenuDivPage {

  list: Array<{image: string ,title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {image: 'assets/icon/trans.png', title:'TRANSMISSION',component: DivTransmissionPage},
      {image: 'assets/icon/aspirateur.png' ,title:'EPUISEMENT',component: DivEpuisementPage},
      {image: 'assets/icon/etaiselementaires.png' ,title:'CALAGE TEMPORAIRE',component: DivCalagePage},
      {image: 'assets/icon/bachage.png',title:'BACHAGE',component: DivBachagePage},
      {image: 'assets/icon/tronconeuse.jpg',title:'TRONCONNAGE',component: DivTronconnagePage},
      {image: 'assets/icon/ascenseur.png',title:'ASCENSEURS',component: DivAscenseursPage},
      {image: 'assets/icon/topo.jpg',title:'TOPOGRAPHIE',component: DivTopographiePage},
      {image: 'assets/icon/frelon.jpg',title:'RISQUE ANIMALIER',component: DivAnimalierPage},
      {image: 'assets/icon/ouvertureporte.png',title:'OUVERTURE DE PORTE',component: DivOuvertureportePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuDivPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
