import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuDivPage } from './menu-div';

@NgModule({
  declarations: [
    MenuDivPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuDivPage),
  ],
})
export class MenuDivPageModule {}
