import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuderecoltePage } from './feuderecolte';

@NgModule({
  declarations: [
    FeuderecoltePage,
  ],
  imports: [
    IonicPageModule.forChild(FeuderecoltePage),
  ],
})
export class FeuderecoltePageModule {}
