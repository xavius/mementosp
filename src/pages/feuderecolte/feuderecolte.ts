import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxRecolteAccesComponent } from '../../components/feux-recolte/feux-recolte-acces/feux-recolte-acces';
import { FeuxRecolteActionComponent } from '../../components/feux-recolte/feux-recolte-action/feux-recolte-action';
import { FeuxRecolteAttentionComponent } from '../../components/feux-recolte/feux-recolte-attention/feux-recolte-attention';
import { FeuxRecolteInterditComponent } from '../../components/feux-recolte/feux-recolte-interdit/feux-recolte-interdit';
import { FeuxRecolteRisquesComponent } from '../../components/feux-recolte/feux-recolte-risques/feux-recolte-risques';
import { FeuxRecolteStrategieComponent } from '../../components/feux-recolte/feux-recolte-strategie/feux-recolte-strategie';

/**
 * Generated class for the FeuderecoltePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feuderecolte',
  templateUrl: 'feuderecolte.html',
})
export class FeuderecoltePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuderecoltePage');
  }
  tab1Root=FeuxRecolteRisquesComponent;
  tab2Root=FeuxRecolteStrategieComponent;
  tab3Root=FeuxRecolteAccesComponent;
  tab4Root=FeuxRecolteActionComponent;
  tab5Root=FeuxRecolteAttentionComponent;
  tab6Root=FeuxRecolteInterditComponent;
}
