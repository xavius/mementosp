import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudechaufferiePage } from './feudechaufferie';

@NgModule({
  declarations: [
    FeudechaufferiePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudechaufferiePage),
  ],
})
export class FeudechaufferiePageModule {}
