import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxChaufferieAccesComponent } from '../../components/feux-chaufferie/feux-chaufferie-acces/feux-chaufferie-acces';
import { FeuxChaufferieActionComponent } from '../../components/feux-chaufferie/feux-chaufferie-action/feux-chaufferie-action';
import { FeuxChaufferieAttentionComponent } from '../../components/feux-chaufferie/feux-chaufferie-attention/feux-chaufferie-attention';
import { FeuxChaufferieInterditComponent } from '../../components/feux-chaufferie/feux-chaufferie-interdit/feux-chaufferie-interdit';
import { FeuxChaufferieRisquesComponent } from '../../components/feux-chaufferie/feux-chaufferie-risques/feux-chaufferie-risques';
import { FeuxChaufferieStrategieComponent } from '../../components/feux-chaufferie/feux-chaufferie-strategie/feux-chaufferie-strategie';

/**
 * Generated class for the FeudechaufferiePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudechaufferie',
  templateUrl: 'feudechaufferie.html',
})
export class FeudechaufferiePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudechaufferiePage');
  }
  tab1Root=FeuxChaufferieRisquesComponent;
  tab2Root=FeuxChaufferieStrategieComponent;
  tab3Root=FeuxChaufferieAccesComponent;
  tab4Root=FeuxChaufferieActionComponent;
  tab5Root=FeuxChaufferieAttentionComponent;
  tab6Root=FeuxChaufferieInterditComponent;
}
