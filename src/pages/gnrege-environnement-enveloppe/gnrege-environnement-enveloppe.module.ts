import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEnvironnementEnveloppePage } from './gnrege-environnement-enveloppe';

@NgModule({
  declarations: [
    GnregeEnvironnementEnveloppePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEnvironnementEnveloppePage),
  ],
})
export class GnregeEnvironnementEnveloppePageModule {}
