import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeEnvironnementEnveloppePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-environnement-enveloppe',
  templateUrl: 'gnrege-environnement-enveloppe.html',
})
export class GnregeEnvironnementEnveloppePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeEnvironnementEnveloppePage');
  }

}
