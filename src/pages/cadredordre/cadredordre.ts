import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GocCadredordrePatracdrComponent } from '../../components/goc-cadredordre/goc-cadredordre-patracdr/goc-cadredordre-patracdr';
import { GocCadredordreDpifComponent } from '../../components/goc-cadredordre/goc-cadredordre-dpif/goc-cadredordre-dpif';
import { GocCadredordreSmesComponent } from '../../components/goc-cadredordre/goc-cadredordre-smes/goc-cadredordre-smes';
import { GocCadredordreSoiecComponent } from '../../components/goc-cadredordre/goc-cadredordre-soiec/goc-cadredordre-soiec';

/**
 * Generated class for the CadredordrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadredordre',
  templateUrl: 'cadredordre.html',
})
export class CadredordrePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadredordrePage');
  }
  tab29Root=GocCadredordrePatracdrComponent;
  tab30Root=GocCadredordreDpifComponent;
  tab31Root=GocCadredordreSmesComponent;
  tab32Root=GocCadredordreSoiecComponent;
}
