import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadredordrePage } from './cadredordre';

@NgModule({
  declarations: [
    CadredordrePage,
  ],
  imports: [
    IonicPageModule.forChild(CadredordrePage),
  ],
})
export class CadredordrePageModule {}
