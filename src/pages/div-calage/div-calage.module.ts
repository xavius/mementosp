import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivCalagePage } from './div-calage';

@NgModule({
  declarations: [
    DivCalagePage,
  ],
  imports: [
    IonicPageModule.forChild(DivCalagePage),
  ],
})
export class DivCalagePageModule {}
