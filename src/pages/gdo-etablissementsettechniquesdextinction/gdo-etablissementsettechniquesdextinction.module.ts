import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GdoEtablissementsettechniquesdextinctionPage } from './gdo-etablissementsettechniquesdextinction';

@NgModule({
  declarations: [
    GdoEtablissementsettechniquesdextinctionPage,
  ],
  imports: [
    IonicPageModule.forChild(GdoEtablissementsettechniquesdextinctionPage),
  ],
})
export class GdoEtablissementsettechniquesdextinctionPageModule {}
