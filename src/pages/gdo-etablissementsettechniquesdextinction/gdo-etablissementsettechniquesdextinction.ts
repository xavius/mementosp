import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GdoEtablissementsettechniquesdextinctionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gdo-etablissementsettechniquesdextinction',
  templateUrl: 'gdo-etablissementsettechniquesdextinction.html',
})
export class GdoEtablissementsettechniquesdextinctionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GdoEtablissementsettechniquesdextinctionPage');
  }

}
