import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudepenichePage } from './feudepeniche';

@NgModule({
  declarations: [
    FeudepenichePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudepenichePage),
  ],
})
export class FeudepenichePageModule {}
