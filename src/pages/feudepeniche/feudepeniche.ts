import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxPenicheAccesComponent } from '../../components/feux-peniche/feux-peniche-acces/feux-peniche-acces';
import { FeuxPenicheActionComponent } from '../../components/feux-peniche/feux-peniche-action/feux-peniche-action';
import { FeuxPenicheAttentionComponent } from '../../components/feux-peniche/feux-peniche-attention/feux-peniche-attention';
import { FeuxPenicheInterditComponent } from '../../components/feux-peniche/feux-peniche-interdit/feux-peniche-interdit';
import { FeuxPenicheRisquesComponent } from '../../components/feux-peniche/feux-peniche-risques/feux-peniche-risques';
import { FeuxPenicheStrategieComponent } from '../../components/feux-peniche/feux-peniche-strategie/feux-peniche-strategie';

/**
 * Generated class for the FeudepenichePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudepeniche',
  templateUrl: 'feudepeniche.html',
})
export class FeudepenichePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudepenichePage');
  }
  tab1Root=FeuxPenicheRisquesComponent;
  tab2Root=FeuxPenicheStrategieComponent;
  tab3Root=FeuxPenicheAccesComponent;
  tab4Root=FeuxPenicheActionComponent;
  tab5Root=FeuxPenicheAttentionComponent;
  tab6Root=FeuxPenicheInterditComponent;
}
