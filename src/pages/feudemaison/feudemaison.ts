import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxMaisonRisquesComponent } from '../../components/feux-maison/feux-maison-risques/feux-maison-risques';
import { FeuxMaisonStrategieComponent } from '../../components/feux-maison/feux-maison-strategie/feux-maison-strategie';
import { FeuxMaisonAccesComponent } from '../../components/feux-maison/feux-maison-acces/feux-maison-acces';
import { FeuxMaisonActionComponent } from '../../components/feux-maison/feux-maison-action/feux-maison-action';
import { FeuxMaisonAttentionComponent } from '../../components/feux-maison/feux-maison-attention/feux-maison-attention';
import { FeuxMaisonInterditComponent } from '../../components/feux-maison/feux-maison-interdit/feux-maison-interdit';

/**
 * Generated class for the FeudemaisonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudemaison',
  templateUrl: 'feudemaison.html',
})
export class FeudemaisonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudemaisonPage');
  }

  tab1Root= FeuxMaisonRisquesComponent;
  tab2Root= FeuxMaisonStrategieComponent;
  tab3Root= FeuxMaisonAccesComponent;
  tab4Root= FeuxMaisonActionComponent;
  tab5Root= FeuxMaisonAttentionComponent;
  tab6Root= FeuxMaisonInterditComponent;

}
