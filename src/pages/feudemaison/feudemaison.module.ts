import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudemaisonPage } from './feudemaison';

@NgModule({
  declarations: [
    FeudemaisonPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudemaisonPage),
  ],
})
export class FeudemaisonPageModule {}
