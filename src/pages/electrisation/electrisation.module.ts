import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElectrisationPage } from './electrisation';

@NgModule({
  declarations: [
    ElectrisationPage,
  ],
  imports: [
    IonicPageModule.forChild(ElectrisationPage),
  ],
})
export class ElectrisationPageModule {}
