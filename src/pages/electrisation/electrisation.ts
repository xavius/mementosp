import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapElectrisationSignesComponent } from '../../components/sap-electrisation/sap-electrisation-signes/sap-electrisation-signes';
import { SapElectrisationBilansComponent } from '../../components/sap-electrisation/sap-electrisation-bilans/sap-electrisation-bilans';
import { SapElectrisationActionsComponent } from '../../components/sap-electrisation/sap-electrisation-actions/sap-electrisation-actions';
import { SapElectrisationImportantComponent } from '../../components/sap-electrisation/sap-electrisation-important/sap-electrisation-important';

/**
 * Generated class for the ElectrisationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-electrisation',
  templateUrl: 'electrisation.html',
})
export class ElectrisationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ElectrisationPage');
  }
  tab7Root=SapElectrisationSignesComponent;
  tab8Root=SapElectrisationBilansComponent;
  tab9Root=SapElectrisationActionsComponent;
  tab10Root=SapElectrisationImportantComponent;
}
