import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionTriangledevacuationPage } from './gnr-lspcc-composition-triangledevacuation';

@NgModule({
  declarations: [
    GnrLspccCompositionTriangledevacuationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionTriangledevacuationPage),
  ],
})
export class GnrLspccCompositionTriangledevacuationPageModule {}
