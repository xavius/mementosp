import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccProtectionindPage } from './gnr-lspcc-protectionind';

@NgModule({
  declarations: [
    GnrLspccProtectionindPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccProtectionindPage),
  ],
})
export class GnrLspccProtectionindPageModule {}
