import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeConduiteatenirLecturePage } from './gnrege-conduiteatenir-lecture';

@NgModule({
  declarations: [
    GnregeConduiteatenirLecturePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeConduiteatenirLecturePage),
  ],
})
export class GnregeConduiteatenirLecturePageModule {}
