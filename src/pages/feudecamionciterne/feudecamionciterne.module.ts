import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudecamionciternePage } from './feudecamionciterne';

@NgModule({
  declarations: [
    FeudecamionciternePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudecamionciternePage),
  ],
})
export class FeudecamionciternePageModule {}
