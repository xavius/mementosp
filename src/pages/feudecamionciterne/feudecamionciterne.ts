import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxCamionciterneAccesComponent } from '../../components/feux-camionciterne/feux-camionciterne-acces/feux-camionciterne-acces';
import { FeuxCamionciterneActionComponent } from '../../components/feux-camionciterne/feux-camionciterne-action/feux-camionciterne-action';
import { FeuxCamionciterneAttentionComponent } from '../../components/feux-camionciterne/feux-camionciterne-attention/feux-camionciterne-attention';
import { FeuxCamionciterneInterditComponent } from '../../components/feux-camionciterne/feux-camionciterne-interdit/feux-camionciterne-interdit';
import { FeuxCamionciterneRisquesComponent } from '../../components/feux-camionciterne/feux-camionciterne-risques/feux-camionciterne-risques';
import { FeuxCamionciterneStrategieComponent } from '../../components/feux-camionciterne/feux-camionciterne-strategie/feux-camionciterne-strategie';

/**
 * Generated class for the FeudecamionciternePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudecamionciterne',
  templateUrl: 'feudecamionciterne.html',
})
export class FeudecamionciternePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudecamionciternePage');
  }
  tab1Root=FeuxCamionciterneRisquesComponent;
  tab2Root=FeuxCamionciterneStrategieComponent;
  tab3Root=FeuxCamionciterneAccesComponent;
  tab4Root=FeuxCamionciterneActionComponent;
  tab5Root=FeuxCamionciterneAttentionComponent;
  tab6Root=FeuxCamionciterneInterditComponent;

}
