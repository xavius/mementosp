import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapTraumatismedesmembresSignesComponent } from '../../components/sap-traumatismedesmembres/sap-traumatismedesmembres-signes/sap-traumatismedesmembres-signes';
import { SapTraumatismedesmembresBilansComponent } from '../../components/sap-traumatismedesmembres/sap-traumatismedesmembres-bilans/sap-traumatismedesmembres-bilans';
import { SapTraumatismedesmembresActionsComponent } from '../../components/sap-traumatismedesmembres/sap-traumatismedesmembres-actions/sap-traumatismedesmembres-actions';
import { SapTraumatismedesmembresImportantComponent } from '../../components/sap-traumatismedesmembres/sap-traumatismedesmembres-important/sap-traumatismedesmembres-important';

/**
 * Generated class for the TraumatismedesmembresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-traumatismedesmembres',
  templateUrl: 'traumatismedesmembres.html',
})
export class TraumatismedesmembresPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TraumatismedesmembresPage');
  }
  tab7Root=SapTraumatismedesmembresSignesComponent;
  tab8Root=SapTraumatismedesmembresBilansComponent;
  tab9Root=SapTraumatismedesmembresActionsComponent;
  tab10Root=SapTraumatismedesmembresImportantComponent;
}
