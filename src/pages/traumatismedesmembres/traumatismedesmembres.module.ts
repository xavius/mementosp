import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TraumatismedesmembresPage } from './traumatismedesmembres';

@NgModule({
  declarations: [
    TraumatismedesmembresPage,
  ],
  imports: [
    IonicPageModule.forChild(TraumatismedesmembresPage),
  ],
})
export class TraumatismedesmembresPageModule {}
