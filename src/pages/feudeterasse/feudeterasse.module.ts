import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudeterassePage } from './feudeterasse';

@NgModule({
  declarations: [
    FeudeterassePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudeterassePage),
  ],
})
export class FeudeterassePageModule {}
