import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxTerrasseAccesComponent } from '../../components/feux-terrasse/feux-terrasse-acces/feux-terrasse-acces';
import { FeuxTerrasseActionComponent } from '../..//components/feux-terrasse/feux-terrasse-action/feux-terrasse-action';
import { FeuxTerrasseAttentionComponent } from '../../components/feux-terrasse/feux-terrasse-attention/feux-terrasse-attention';
import { FeuxTerrasseInterditComponent } from '../../components/feux-terrasse/feux-terrasse-interdit/feux-terrasse-interdit';
import { FeuxTerrasseRisquesComponent } from '../../components/feux-terrasse/feux-terrasse-risques/feux-terrasse-risques';
import { FeuxTerrasseStrategieComponent } from '../../components/feux-terrasse/feux-terrasse-strategie/feux-terrasse-strategie';

/**
 * Generated class for the FeudeterassePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudeterasse',
  templateUrl: 'feudeterasse.html',
})
export class FeudeterassePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudeterassePage');
  }
  tab1Root=FeuxTerrasseRisquesComponent;
  tab2Root=FeuxTerrasseStrategieComponent;
  tab3Root=FeuxTerrasseAccesComponent;
  tab4Root=FeuxTerrasseActionComponent;
  tab5Root=FeuxTerrasseAttentionComponent;
  tab6Root=FeuxTerrasseInterditComponent;
}
