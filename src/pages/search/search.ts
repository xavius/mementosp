import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MgoPage } from '../mgo/mgo';
import { FeudappartementPage } from '../feudappartement/feudappartement';
import { FeudemaisonPage } from '../feudemaison/feudemaison';


/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  declareCat: any;
  cat: any;

  public isSearchbarOpened = false;
  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {


    this.list = [
      {title:'M.G.O.',component:MgoPage},
      {title:'Feu d\'Appartement', component: FeudappartementPage},
      {title:'Feu de Maison', component: FeudemaisonPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
  onSearch(event){
    this.declareCat();
    console.log(event.value);
    let val : string = event.value;
      if (val.trim() !=='')
      {
        this.cat = this.cat.filter((item) =>
        {
          return item.title.toLowerCase().indexOf(val.toLowerCase()) > -1;
        })
      }
  }
}

