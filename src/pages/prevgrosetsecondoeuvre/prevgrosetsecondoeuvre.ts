import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevGrosetsecondoeuvreFonctionnementComponent } from '../../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-fonctionnement/prev-grosetsecondoeuvre-fonctionnement';
import { PrevGrosetsecondoeuvrePrecautionComponent } from '../../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-precaution/prev-grosetsecondoeuvre-precaution';
import { PrevGrosetsecondoeuvreDistributionComponent } from '../../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-distribution/prev-grosetsecondoeuvre-distribution';
import { PrevGrosetsecondoeuvreOuvertureComponent } from '../../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-ouverture/prev-grosetsecondoeuvre-ouverture';

/**
 * Generated class for the PrevgrosetsecondoeuvrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevgrosetsecondoeuvre',
  templateUrl: 'prevgrosetsecondoeuvre.html',
})
export class PrevgrosetsecondoeuvrePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevgrosetsecondoeuvrePage');
  }
  tab48Root=PrevGrosetsecondoeuvreFonctionnementComponent;
  tab49Root=PrevGrosetsecondoeuvrePrecautionComponent;
  tab50Root=PrevGrosetsecondoeuvreDistributionComponent;
  tab51Root=PrevGrosetsecondoeuvreOuvertureComponent;
}
