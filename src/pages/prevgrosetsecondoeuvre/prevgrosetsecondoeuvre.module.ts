import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevgrosetsecondoeuvrePage } from './prevgrosetsecondoeuvre';

@NgModule({
  declarations: [
    PrevgrosetsecondoeuvrePage,
  ],
  imports: [
    IonicPageModule.forChild(PrevgrosetsecondoeuvrePage),
  ],
})
export class PrevgrosetsecondoeuvrePageModule {}
