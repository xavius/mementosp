import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrdepavillonnagePage } from './srdepavillonnage';

@NgModule({
  declarations: [
    SrdepavillonnagePage,
  ],
  imports: [
    IonicPageModule.forChild(SrdepavillonnagePage),
  ],
})
export class SrdepavillonnagePageModule {}
