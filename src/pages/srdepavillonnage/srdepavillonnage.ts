import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrDepavillonnage_1Component } from '../../components/sr-depavillonnage/sr-depavillonnage-1/sr-depavillonnage-1';
import { SrDepavillonnage_2Component } from '../../components/sr-depavillonnage/sr-depavillonnage-2/sr-depavillonnage-2';
import { SrDepavillonnage_3Component } from '../../components/sr-depavillonnage/sr-depavillonnage-3/sr-depavillonnage-3';

/**
 * Generated class for the SrdepavillonnagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srdepavillonnage',
  templateUrl: 'srdepavillonnage.html',
})
export class SrdepavillonnagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrdepavillonnagePage');
  }
  tab22Root=SrDepavillonnage_1Component;
  tab23Root=SrDepavillonnage_2Component;
  tab24Root=SrDepavillonnage_3Component;
}
