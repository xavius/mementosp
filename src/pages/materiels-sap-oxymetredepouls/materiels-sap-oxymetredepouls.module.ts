import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapOxymetredepoulsPage } from './materiels-sap-oxymetredepouls';

@NgModule({
  declarations: [
    MaterielsSapOxymetredepoulsPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapOxymetredepoulsPage),
  ],
})
export class MaterielsSapOxymetredepoulsPageModule {}
