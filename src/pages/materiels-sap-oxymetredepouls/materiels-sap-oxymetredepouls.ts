import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapOxymetredepoulsDescriptionComponent } from '../../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-description/materiels-sap-oxymetredepouls-description';
import { MaterielsSapOxymetredepoulsIndicationsComponent } from '../../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-indications/materiels-sap-oxymetredepouls-indications';
import { MaterielsSapOxymetredepoulsRisquesComponent } from '../../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-risques/materiels-sap-oxymetredepouls-risques';
import { MaterielsSapOxymetredepoulsUtilisationComponent } from '../../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-utilisation/materiels-sap-oxymetredepouls-utilisation';
import { MaterielsSapOxymetredepoulsPointsclesComponent } from '../../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-pointscles/materiels-sap-oxymetredepouls-pointscles';
import { MaterielsSapOxymetredepoulsEfficaciteComponent } from '../../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-efficacite/materiels-sap-oxymetredepouls-efficacite';


/**
 * Generated class for the MaterielsSapOxymetredepoulsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-oxymetredepouls',
  templateUrl: 'materiels-sap-oxymetredepouls.html',
})
export class MaterielsSapOxymetredepoulsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapOxymetredepoulsPage');
  }
  tab65Root=MaterielsSapOxymetredepoulsDescriptionComponent;
  tab66Root=MaterielsSapOxymetredepoulsIndicationsComponent;
  tab67Root=MaterielsSapOxymetredepoulsRisquesComponent;
  tab68Root=MaterielsSapOxymetredepoulsUtilisationComponent;
  tab69Root=MaterielsSapOxymetredepoulsPointsclesComponent;
  tab70Root=MaterielsSapOxymetredepoulsEfficaciteComponent;
}
