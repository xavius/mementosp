import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeExplosiondefumeeSignesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-explosiondefumee-signes',
  templateUrl: 'gnrege-explosiondefumee-signes.html',
})
export class GnregeExplosiondefumeeSignesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeExplosiondefumeeSignesPage');
  }

}
