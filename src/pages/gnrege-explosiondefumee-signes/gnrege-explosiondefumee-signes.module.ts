import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeExplosiondefumeeSignesPage } from './gnrege-explosiondefumee-signes';

@NgModule({
  declarations: [
    GnregeExplosiondefumeeSignesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeExplosiondefumeeSignesPage),
  ],
})
export class GnregeExplosiondefumeeSignesPageModule {}
