import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichessapPage } from './fichessap';

@NgModule({
  declarations: [
    FichessapPage,
  ],
  imports: [
    IonicPageModule.forChild(FichessapPage),
  ],
})
export class FichessapPageModule {}
