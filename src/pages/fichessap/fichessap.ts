import { SanglearaigneePage } from './../sanglearaignee/sanglearaignee';
import { TraumatismedesmembresPage } from './../traumatismedesmembres/traumatismedesmembres';
import { TraumatismecolonnevertebralePage } from './../traumatismecolonnevertebrale/traumatismecolonnevertebrale';
import { TraumatismecranienPage } from './../traumatismecranien/traumatismecranien';
import { TetaniespasmophiliePage } from './../tetaniespasmophilie/tetaniespasmophilie';
import { PolytraumatisePage } from './../polytraumatise/polytraumatise';
import { PlaiesPage } from './../plaies/plaies';
import { PiqureshymenopteresPage } from './../piqureshymenopteres/piqureshymenopteres';
import { PendaisonPage } from './../pendaison/pendaison';
import { OedemedequinckePage } from './../oedemedequincke/oedemedequincke';
import { OedemepoumonPage } from './../oedemepoumon/oedemepoumon';
import { NoyadePage } from './../noyade/noyade';
import { MalaisesPage } from './../malaises/malaises';
import { IvressePage } from './../ivresse/ivresse';
import { IntoxicationmedPage } from './../intoxicationmed/intoxicationmed';
import { IntoxicationcoPage } from './../intoxicationco/intoxicationco';
import { HemorragiePage } from './../hemorragie/hemorragie';
import { EpilepsiePage } from './../epilepsie/epilepsie';
import { ElectrisationPage } from './../electrisation/electrisation';
import { DiabetiquePage } from './../diabetique/diabetique';
import { CrisedasthmesPage } from './../crisedasthmes/crisedasthmes';
import { BruluresPage } from './../brulures/brulures';
import { AcrPage } from './../acr/acr';
import { AccouchementPage } from './../accouchement/accouchement';
import { AccidentvasculairePage } from './../accidentvasculaire/accidentvasculaire';
import { BilansPage } from './../bilans/bilans';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConvulsionsPage } from '../convulsions/convulsions';
import { DesincarcerationPage } from '../desincarceration/desincarceration';
import { EcrasementPage } from '../ecrasement/ecrasement';
import { InfarctusPage } from '../infarctus/infarctus';

/**
 * Generated class for the FichessapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichessap',
  templateUrl: 'fichessap.html',
})
export class FichessapPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Bilans', component: BilansPage},
      {title:'A.V.C.', component: AccidentvasculairePage},
      {title:'Accouchement', component: AccouchementPage},
      {title:'A.C.R.', component: AcrPage},
      {title:'Brulures', component: BruluresPage},
      {title:'Convulsions du nouveau-né', component: ConvulsionsPage},
      {title:'Crise d Asthmes', component: CrisedasthmesPage},
      {title:'Désincarceration', component: DesincarcerationPage},
      {title:'Diabétique', component: DiabetiquePage},
      {title:'Ecrasement des Membres', component: EcrasementPage},
      {title:'Electrisation - Electrocution', component: ElectrisationPage},
      {title:'Epilepsie', component: EpilepsiePage},
      {title:'Hémorragie Externes', component: HemorragiePage},
      {title:'Infarctus', component: InfarctusPage},
      {title:'Intoxication au CO', component: IntoxicationcoPage},
      {title:'Intoxication Méd, Alimentaire', component: IntoxicationmedPage},
      {title:'Ivresse', component: IvressePage},
      {title:'La Sangle Araignée', component: SanglearaigneePage},
      {title:'Malaises', component: MalaisesPage},
      {title:'Noyade', component: NoyadePage},
      {title:'Oedème Aigu du Poumon', component: OedemepoumonPage},
      {title:'Oedème de Quincke', component: OedemedequinckePage},
      {title:'Pendaison', component: PendaisonPage},
      {title:'Piqûres d Hyménoptères', component: PiqureshymenopteresPage},
      {title:'Plaies', component: PlaiesPage},
      {title:'Polytraumatisé', component: PolytraumatisePage},
      {title:'Tétanie - Spasmophilie', component: TetaniespasmophiliePage},
      {title:'Traumatisme Crânien', component: TraumatismecranienPage},
      {title:'Traumatisme Colonne Vertébrale', component: TraumatismecolonnevertebralePage},
      {title:'Traumatisme des Membres', component: TraumatismedesmembresPage}
      

    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichessapPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
