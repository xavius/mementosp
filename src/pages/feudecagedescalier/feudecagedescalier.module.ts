import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudecagedescalierPage } from './feudecagedescalier';

@NgModule({
  declarations: [
    FeudecagedescalierPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudecagedescalierPage),
  ],
})
export class FeudecagedescalierPageModule {}
