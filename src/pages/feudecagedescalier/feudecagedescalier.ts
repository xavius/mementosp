import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxCagedescalierAccesComponent } from '../../components/feux-cagedescalier/feux-cagedescalier-acces/feux-cagedescalier-acces';
import { FeuxCagedescalierActionComponent } from '../../components/feux-cagedescalier/feux-cagedescalier-action/feux-cagedescalier-action';
import { FeuxCagedescalierAttentionComponent } from '../../components/feux-cagedescalier/feux-cagedescalier-attention/feux-cagedescalier-attention';
import { FeuxCagedescalierInterditComponent } from '../../components/feux-cagedescalier/feux-cagedescalier-interdit/feux-cagedescalier-interdit';
import { FeuxCagedescalierRisquesComponent } from '../../components/feux-cagedescalier/feux-cagedescalier-risques/feux-cagedescalier-risques';
import { FeuxCagedescalierStrategieComponent } from '../../components/feux-cagedescalier/feux-cagedescalier-strategie/feux-cagedescalier-strategie';

/**
 * Generated class for the FeudecagedescalierPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudecagedescalier',
  templateUrl: 'feudecagedescalier.html',
})
export class FeudecagedescalierPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudecagedescalierPage');
  }
  tab1Root=FeuxCagedescalierRisquesComponent;
  tab2Root=FeuxCagedescalierStrategieComponent;
  tab3Root=FeuxCagedescalierAccesComponent;
  tab4Root=FeuxCagedescalierActionComponent;
  tab5Root=FeuxCagedescalierAttentionComponent;
  tab6Root=FeuxCagedescalierInterditComponent;
}
