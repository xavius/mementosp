import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrDemipavillonavant_1Component } from '../../components/sr-demipavillonavant/sr-demipavillonavant-1/sr-demipavillonavant-1';
import { SrDemipavillonavant_2Component } from '../../components/sr-demipavillonavant/sr-demipavillonavant-2/sr-demipavillonavant-2';
import { SrDemipavillonavant_3Component } from '../../components/sr-demipavillonavant/sr-demipavillonavant-3/sr-demipavillonavant-3';

/**
 * Generated class for the SrdemipavillonavantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srdemipavillonavant',
  templateUrl: 'srdemipavillonavant.html',
})
export class SrdemipavillonavantPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrdemipavillonavantPage');
  }
  tab22Root=SrDemipavillonavant_1Component;
  tab23Root=SrDemipavillonavant_2Component;
  tab24Root=SrDemipavillonavant_3Component;
}
