import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrdemipavillonavantPage } from './srdemipavillonavant';

@NgModule({
  declarations: [
    SrdemipavillonavantPage,
  ],
  imports: [
    IonicPageModule.forChild(SrdemipavillonavantPage),
  ],
})
export class SrdemipavillonavantPageModule {}
