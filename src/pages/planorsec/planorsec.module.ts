import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanorsecPage } from './planorsec';

@NgModule({
  declarations: [
    PlanorsecPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanorsecPage),
  ],
})
export class PlanorsecPageModule {}
