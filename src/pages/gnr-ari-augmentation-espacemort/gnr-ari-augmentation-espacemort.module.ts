import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriAugmentationEspacemortPage } from './gnr-ari-augmentation-espacemort';

@NgModule({
  declarations: [
    GnrAriAugmentationEspacemortPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriAugmentationEspacemortPage),
  ],
})
export class GnrAriAugmentationEspacemortPageModule {}
