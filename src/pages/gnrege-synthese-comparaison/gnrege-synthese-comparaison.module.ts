import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeSyntheseComparaisonPage } from './gnrege-synthese-comparaison';

@NgModule({
  declarations: [
    GnregeSyntheseComparaisonPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeSyntheseComparaisonPage),
  ],
})
export class GnregeSyntheseComparaisonPageModule {}
