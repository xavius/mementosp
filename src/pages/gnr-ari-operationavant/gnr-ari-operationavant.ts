import { GnrAriOperationavantControleurPage } from './../gnr-ari-operationavant-controleur/gnr-ari-operationavant-controleur';
import { GnrAriOperationavantChefdagresPage } from './../gnr-ari-operationavant-chefdagres/gnr-ari-operationavant-chefdagres';
import { GnrAriOperationavantPorteursPage } from './../gnr-ari-operationavant-porteurs/gnr-ari-operationavant-porteurs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationavantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationavant',
  templateUrl: 'gnr-ari-operationavant.html',
})
export class GnrAriOperationavantPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'CHEF D AGRES', component: GnrAriOperationavantChefdagresPage},
      {title:'CONTROLEUR', component: GnrAriOperationavantControleurPage},
      {title:'PORTEURS', component: GnrAriOperationavantPorteursPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationavantPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
