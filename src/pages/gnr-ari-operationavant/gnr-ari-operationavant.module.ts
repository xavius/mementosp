import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationavantPage } from './gnr-ari-operationavant';

@NgModule({
  declarations: [
    GnrAriOperationavantPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationavantPage),
  ],
})
export class GnrAriOperationavantPageModule {}
