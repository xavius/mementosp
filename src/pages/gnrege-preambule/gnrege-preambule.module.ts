import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregePreambulePage } from './gnrege-preambule';

@NgModule({
  declarations: [
    GnregePreambulePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregePreambulePage),
  ],
})
export class GnregePreambulePageModule {}
