import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TraumatismecranienPage } from './traumatismecranien';

@NgModule({
  declarations: [
    TraumatismecranienPage,
  ],
  imports: [
    IonicPageModule.forChild(TraumatismecranienPage),
  ],
})
export class TraumatismecranienPageModule {}
