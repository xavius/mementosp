import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapTraumatismecranienSignesComponent } from '../../components/sap-traumatismecranien/sap-traumatismecranien-signes/sap-traumatismecranien-signes';
import { SapTraumatismecranienBilansComponent } from '../../components/sap-traumatismecranien/sap-traumatismecranien-bilans/sap-traumatismecranien-bilans';
import { SapTraumatismecranienActionsComponent } from '../../components/sap-traumatismecranien/sap-traumatismecranien-actions/sap-traumatismecranien-actions';
import { SapTraumatismecranienImportantComponent } from '../../components/sap-traumatismecranien/sap-traumatismecranien-important/sap-traumatismecranien-important';

/**
 * Generated class for the TraumatismecranienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-traumatismecranien',
  templateUrl: 'traumatismecranien.html',
})
export class TraumatismecranienPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TraumatismecranienPage');
  }
  tab7Root=SapTraumatismecranienSignesComponent;
  tab8Root=SapTraumatismecranienBilansComponent;
  tab9Root=SapTraumatismecranienActionsComponent;
  tab10Root=SapTraumatismecranienImportantComponent;
  }
