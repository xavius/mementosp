import { ContactPage } from './../contact/contact';
import { SearchPage } from './../search/search';
import { MenuDivPage } from './../menu-div/menu-div';
import { MenuCommandementPage } from './../menu-commandement/menu-commandement';
import { MenuSapPage } from './../menu-sap/menu-sap';
import { MenuIncPage } from './../menu-inc/menu-inc';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuGnrPage } from '../menu-gnr/menu-gnr';
import { MenuPreventionPage } from '../menu-prevention/menu-prevention';
import { MenuNrbcRtnPage } from '../menu-nrbc-rtn/menu-nrbc-rtn';
import { MenuOutilsopPage } from '../menu-outilsop/menu-outilsop';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  searchPage:SearchPage;

  public isSearchbarOpened = false;
  cat: Array<{title: string, img: string, component: any}>;
  constructor(public navCtrl: NavController) {

    this.cat = [
      {title:'SAP',img:'/assets/imgs/M-SAP.png',component:MenuSapPage},
      {title:'INC',img:'/assets/imgs/M-INC.png',component:MenuIncPage},
      {title:'DIV',img:'/assets/imgs/M-DIV.png',component:MenuDivPage},
      {title:'NRBC-RTN',img:'/assets/imgs/M-NRBC.png',component:MenuNrbcRtnPage},
      {title:'Commandement',img:'/assets/imgs/M-COMMANDEMENT.png',component:MenuCommandementPage},
      {title:'Prevention',img:'/assets/imgs/M-PREVENTION.png',component:MenuPreventionPage},
      {title:'GNR',img:'/assets/imgs/M-GNR.png',component:MenuGnrPage},
      {title:'OUTILS OP',img:'/assets/imgs/M-OUTILS OP.png',component:MenuOutilsopPage}
    ];
  }

  openPage(page) {
    this.navCtrl.push(page.component);
  }
  openSearch() {
    this.navCtrl.push(SearchPage);
  }
  openEmail() {
    this.navCtrl.push(ContactPage);
  }
}
