import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxErpTypeMAccesComponent } from '../../components/feux-erp-type-m/feux-erp-type-m-acces/feux-erp-type-m-acces';
import { FeuxErpTypeMActionComponent } from '../../components/feux-erp-type-m/feux-erp-type-m-action/feux-erp-type-m-action';
import { FeuxErpTypeMAttentionComponent } from '../../components/feux-erp-type-m/feux-erp-type-m-attention/feux-erp-type-m-attention';
import { FeuxErpTypeMInterditComponent } from '../../components/feux-erp-type-m/feux-erp-type-m-interdit/feux-erp-type-m-interdit';
import { FeuxErpTypeMRisquesComponent } from '../../components/feux-erp-type-m/feux-erp-type-m-risques/feux-erp-type-m-risques';
import { FeuxErpTypeMStrategieComponent } from '../../components/feux-erp-type-m/feux-erp-type-m-strategie/feux-erp-type-m-strategie';

/**
 * Generated class for the FeuErPtypeMPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feu-er-ptype-m',
  templateUrl: 'feu-er-ptype-m.html',
})
export class FeuErPtypeMPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuErPtypeMPage');
  }
  tab1Root=FeuxErpTypeMRisquesComponent;
  tab2Root=FeuxErpTypeMStrategieComponent;
  tab3Root=FeuxErpTypeMAccesComponent;
  tab4Root=FeuxErpTypeMActionComponent;
  tab5Root=FeuxErpTypeMAttentionComponent;
  tab6Root=FeuxErpTypeMInterditComponent;
}
