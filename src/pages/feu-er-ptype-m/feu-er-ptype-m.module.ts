import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuErPtypeMPage } from './feu-er-ptype-m';

@NgModule({
  declarations: [
    FeuErPtypeMPage,
  ],
  imports: [
    IonicPageModule.forChild(FeuErPtypeMPage),
  ],
})
export class FeuErPtypeMPageModule {}
