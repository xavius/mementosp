import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxTunnelAccesComponent } from '../../components/feux-tunnel/feux-tunnel-acces/feux-tunnel-acces';
import { FeuxTunnelActionComponent } from '../../components/feux-tunnel/feux-tunnel-action/feux-tunnel-action';
import { FeuxTunnelAttentionComponent } from '../../components/feux-tunnel/feux-tunnel-attention/feux-tunnel-attention';
import { FeuxTunnelInterditComponent } from '../../components/feux-tunnel/feux-tunnel-interdit/feux-tunnel-interdit';
import { FeuxTunnelRisquesComponent } from '../../components/feux-tunnel/feux-tunnel-risques/feux-tunnel-risques';
import { FeuxTunnelStrategieComponent } from '../../components/feux-tunnel/feux-tunnel-strategie/feux-tunnel-strategie';

/**
 * Generated class for the FeudetunnelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudetunnel',
  templateUrl: 'feudetunnel.html',
})
export class FeudetunnelPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudetunnelPage');
  }
  tab1Root=FeuxTunnelRisquesComponent;
  tab2Root=FeuxTunnelStrategieComponent;
  tab3Root=FeuxTunnelAccesComponent;
  tab4Root=FeuxTunnelActionComponent;
  tab5Root=FeuxTunnelAttentionComponent;
  tab6Root=FeuxTunnelInterditComponent;
}
