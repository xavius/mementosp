import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudetunnelPage } from './feudetunnel';

@NgModule({
  declarations: [
    FeudetunnelPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudetunnelPage),
  ],
})
export class FeudetunnelPageModule {}
