import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudefermePage } from './feudeferme';

@NgModule({
  declarations: [
    FeudefermePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudefermePage),
  ],
})
export class FeudefermePageModule {}
