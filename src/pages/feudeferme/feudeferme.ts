import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxFermeAccesComponent } from '../../components/feux-ferme/feux-ferme-acces/feux-ferme-acces';
import { FeuxFermeActionComponent } from '../../components/feux-ferme/feux-ferme-action/feux-ferme-action';
import { FeuxFermeAttentionComponent } from '../../components/feux-ferme/feux-ferme-attention/feux-ferme-attention';
import { FeuxFermeInterditComponent } from '../../components/feux-ferme/feux-ferme-interdit/feux-ferme-interdit';
import { FeuxFermeRisquesComponent } from '../../components/feux-ferme/feux-ferme-risques/feux-ferme-risques';
import { FeuxFermeStrategieComponent } from '../../components/feux-ferme/feux-ferme-strategie/feux-ferme-strategie';

/**
 * Generated class for the FeudefermePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudeferme',
  templateUrl: 'feudeferme.html',
})
export class FeudefermePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudefermePage');
  }
  tab1Root=FeuxFermeRisquesComponent;
  tab2Root=FeuxFermeStrategieComponent;
  tab3Root=FeuxFermeAccesComponent;
  tab4Root=FeuxFermeActionComponent;
  tab5Root=FeuxFermeAttentionComponent;
  tab6Root=FeuxFermeInterditComponent;
}
