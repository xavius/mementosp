import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfUiffEtablissementsPage } from './gnrfdf-uiff-etablissements';

@NgModule({
  declarations: [
    GnrfdfUiffEtablissementsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfUiffEtablissementsPage),
  ],
})
export class GnrfdfUiffEtablissementsPageModule {}
