import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEmbrasementDefinitionPage } from './gnrege-embrasement-definition';

@NgModule({
  declarations: [
    GnregeEmbrasementDefinitionPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEmbrasementDefinitionPage),
  ],
})
export class GnregeEmbrasementDefinitionPageModule {}
