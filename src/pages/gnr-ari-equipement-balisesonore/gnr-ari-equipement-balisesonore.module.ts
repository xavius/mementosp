import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementBalisesonorePage } from './gnr-ari-equipement-balisesonore';

@NgModule({
  declarations: [
    GnrAriEquipementBalisesonorePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementBalisesonorePage),
  ],
})
export class GnrAriEquipementBalisesonorePageModule {}
