import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfUiffOffensivesPage } from './gnrfdf-uiff-offensives';

@NgModule({
  declarations: [
    GnrfdfUiffOffensivesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfUiffOffensivesPage),
  ],
})
export class GnrfdfUiffOffensivesPageModule {}
