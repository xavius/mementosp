import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementDetendeurhpPage } from './gnr-ari-equipement-detendeurhp';

@NgModule({
  declarations: [
    GnrAriEquipementDetendeurhpPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementDetendeurhpPage),
  ],
})
export class GnrAriEquipementDetendeurhpPageModule {}
