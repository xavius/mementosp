import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfGiffOffensivesPage } from './gnrfdf-giff-offensives';

@NgModule({
  declarations: [
    GnrfdfGiffOffensivesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfGiffOffensivesPage),
  ],
})
export class GnrfdfGiffOffensivesPageModule {}
