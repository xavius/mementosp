import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent } from '../../components/gnr-lspcc-operation-sauvetageexcavation-remonte/gnr-lspcc-operation-sauvetageexcavation-remonte-chefdagres/gnr-lspcc-operation-sauvetageexcavation-remonte-chefdagres';
import { GnrLspccOperationSauvetageexcavationRemonteEquipierComponent } from '../../components/gnr-lspcc-operation-sauvetageexcavation-remonte/gnr-lspcc-operation-sauvetageexcavation-remonte-equipier/gnr-lspcc-operation-sauvetageexcavation-remonte-equipier';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationRemontePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-sauvetageexcavation-remonte',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-remonte.html',
})
export class GnrLspccOperationSauvetageexcavationRemontePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationSauvetageexcavationRemontePage');
  }
  tab62Root=GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent;
  tab63Root=GnrLspccOperationSauvetageexcavationRemonteEquipierComponent;
}
