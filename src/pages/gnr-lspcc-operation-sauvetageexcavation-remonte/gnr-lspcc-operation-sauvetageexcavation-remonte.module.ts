import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationRemontePage } from './gnr-lspcc-operation-sauvetageexcavation-remonte';

@NgModule({
  declarations: [
    GnrLspccOperationSauvetageexcavationRemontePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationSauvetageexcavationRemontePage),
  ],
})
export class GnrLspccOperationSauvetageexcavationRemontePageModule {}
