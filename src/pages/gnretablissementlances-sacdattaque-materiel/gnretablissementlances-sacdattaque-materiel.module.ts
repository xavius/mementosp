import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesSacdattaqueMaterielPage } from './gnretablissementlances-sacdattaque-materiel';

@NgModule({
  declarations: [
    GnretablissementlancesSacdattaqueMaterielPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesSacdattaqueMaterielPage),
  ],
})
export class GnretablissementlancesSacdattaqueMaterielPageModule {}
