import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesSacdattaqueMaterielPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-sacdattaque-materiel',
  templateUrl: 'gnretablissementlances-sacdattaque-materiel.html',
})
export class GnretablissementlancesSacdattaqueMaterielPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesSacdattaqueMaterielPage');
  }

}
