import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesGeneralitesSauvetagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-generalites-sauvetages',
  templateUrl: 'gnretablissementlances-generalites-sauvetages.html',
})
export class GnretablissementlancesGeneralitesSauvetagesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesGeneralitesSauvetagesPage');
  }

}
