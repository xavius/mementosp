import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesSauvetagesPage } from './gnretablissementlances-generalites-sauvetages';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesSauvetagesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesSauvetagesPage),
  ],
})
export class GnretablissementlancesGeneralitesSauvetagesPageModule {}
