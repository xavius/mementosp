import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevVoiesenginsetechellesFonctionnementComponent } from '../../components/prev-voiesenginsetechelles/prev-voiesenginsetechelles-fonctionnement/prev-voiesenginsetechelles-fonctionnement';
import { PrevVoiesenginsetechellesPrecautionComponent } from '../../components/prev-voiesenginsetechelles/prev-voiesenginsetechelles-precaution/prev-voiesenginsetechelles-precaution';

/**
 * Generated class for the PrevvoiesenginsetechellesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevvoiesenginsetechelles',
  templateUrl: 'prevvoiesenginsetechelles.html',
})
export class PrevvoiesenginsetechellesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevvoiesenginsetechellesPage');
  }
  tab46Root=PrevVoiesenginsetechellesFonctionnementComponent;
  tab47Root=PrevVoiesenginsetechellesPrecautionComponent;
}
