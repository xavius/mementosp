import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevvoiesenginsetechellesPage } from './prevvoiesenginsetechelles';

@NgModule({
  declarations: [
    PrevvoiesenginsetechellesPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevvoiesenginsetechellesPage),
  ],
})
export class PrevvoiesenginsetechellesPageModule {}
