import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivAnimalierPage } from './div-animalier';

@NgModule({
  declarations: [
    DivAnimalierPage,
  ],
  imports: [
    IonicPageModule.forChild(DivAnimalierPage),
  ],
})
export class DivAnimalierPageModule {}
