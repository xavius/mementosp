import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutilsopLifPage } from './outilsop-lif';

@NgModule({
  declarations: [
    OutilsopLifPage,
  ],
  imports: [
    IonicPageModule.forChild(OutilsopLifPage),
  ],
})
export class OutilsopLifPageModule {}
