import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapIntoxicationcoSignesComponent } from '../../components/sap-intoxicationco/sap-intoxicationco-signes/sap-intoxicationco-signes';
import { SapIntoxicationcoBilansComponent } from '../../components/sap-intoxicationco/sap-intoxicationco-bilans/sap-intoxicationco-bilans';
import { SapIntoxicationcoActionsComponent } from '../../components/sap-intoxicationco/sap-intoxicationco-actions/sap-intoxicationco-actions';
import { SapIntoxicationcoImportantComponent } from '../../components/sap-intoxicationco/sap-intoxicationco-important/sap-intoxicationco-important';

/**
 * Generated class for the IntoxicationcoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intoxicationco',
  templateUrl: 'intoxicationco.html',
})
export class IntoxicationcoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntoxicationcoPage');
  }
  tab7Root=SapIntoxicationcoSignesComponent;
  tab8Root=SapIntoxicationcoBilansComponent;
  tab9Root=SapIntoxicationcoActionsComponent;
  tab10Root=SapIntoxicationcoImportantComponent;
}
