import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntoxicationcoPage } from './intoxicationco';

@NgModule({
  declarations: [
    IntoxicationcoPage,
  ],
  imports: [
    IonicPageModule.forChild(IntoxicationcoPage),
  ],
})
export class IntoxicationcoPageModule {}
