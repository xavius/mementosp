import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapAccouchementSignesComponent } from '../../components/sap-accouchement/sap-accouchement-signes/sap-accouchement-signes';
import { SapAccouchementBilansComponent } from '../../components/sap-accouchement/sap-accouchement-bilans/sap-accouchement-bilans';
import { SapAccouchementActionsComponent } from '../../components/sap-accouchement/sap-accouchement-actions/sap-accouchement-actions';
import { SapAccouchementImportantComponent } from '../../components/sap-accouchement/sap-accouchement-important/sap-accouchement-important';

/**
 * Generated class for the AccouchementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accouchement',
  templateUrl: 'accouchement.html',
})
export class AccouchementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccouchementPage');
  }
  tab7Root=SapAccouchementSignesComponent;
  tab8Root=SapAccouchementBilansComponent;
  tab9Root=SapAccouchementActionsComponent;
  tab10Root=SapAccouchementImportantComponent;
}
