import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccouchementPage } from './accouchement';

@NgModule({
  declarations: [
    AccouchementPage,
  ],
  imports: [
    IonicPageModule.forChild(AccouchementPage),
  ],
})
export class AccouchementPageModule {}
