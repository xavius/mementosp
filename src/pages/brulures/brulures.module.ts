import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BruluresPage } from './brulures';

@NgModule({
  declarations: [
    BruluresPage,
  ],
  imports: [
    IonicPageModule.forChild(BruluresPage),
  ],
})
export class BruluresPageModule {}
