import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapBruluresSignesComponent } from '../../components/sap-brulure/sap-brulures-signes/sap-brulures-signes';
import { SapBruluresBilansComponent } from '../../components/sap-brulure/sap-brulures-bilans/sap-brulures-bilans';
import { SapBruluresActionsComponent } from '../../components/sap-brulure/sap-brulures-actions/sap-brulures-actions';
import { SapBruluresImportantComponent } from '../../components/sap-brulure/sap-brulures-important/sap-brulures-important';

/**
 * Generated class for the BruluresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-brulures',
  templateUrl: 'brulures.html',
})
export class BruluresPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BruluresPage');
  }
  tab7Root=SapBruluresSignesComponent;
  tab8Root=SapBruluresBilansComponent;
  tab9Root=SapBruluresActionsComponent;
  tab10Root=SapBruluresImportantComponent;
}
