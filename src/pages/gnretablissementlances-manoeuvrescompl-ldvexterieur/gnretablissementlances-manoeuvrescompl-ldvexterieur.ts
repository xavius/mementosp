import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvexterieurChefdagresComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdagres/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvexterieurChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdequipe/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdequipe';
import { GnretablissementlancesManoeuvrescomplLdvexterieurEquipierComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-equipier/gnretablissementlances-manoeuvrescompl-ldvexterieur-equipier';
import { GnretablissementlancesManoeuvrescomplLdvexterieurBalComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-bal/gnretablissementlances-manoeuvrescompl-ldvexterieur-bal';

/**
 * Generated class for the GnretablissementlancesManoeuvrescomplLdvexterieurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvrescompl-ldvexterieur',
  templateUrl: 'gnretablissementlances-manoeuvrescompl-ldvexterieur.html',
})
export class GnretablissementlancesManoeuvrescomplLdvexterieurPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvrescomplLdvexterieurPage');
  }
  tab61Root=GnretablissementlancesManoeuvrescomplLdvexterieurChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvrescomplLdvexterieurChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvrescomplLdvexterieurEquipierComponent;
  tab64Root=GnretablissementlancesManoeuvrescomplLdvexterieurBalComponent;
}
