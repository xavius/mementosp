import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvexterieurPage } from './gnretablissementlances-manoeuvrescompl-ldvexterieur';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvrescomplLdvexterieurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvrescomplLdvexterieurPage),
  ],
})
export class GnretablissementlancesManoeuvrescomplLdvexterieurPageModule {}
