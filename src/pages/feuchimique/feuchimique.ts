import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxChimiqueAccesComponent } from '../../components/feux-chimique/feux-chimique-acces/feux-chimique-acces';
import { FeuxChimiqueActionComponent } from '../../components/feux-chimique/feux-chimique-action/feux-chimique-action';
import { FeuxChimiqueAttentionComponent } from '../../components/feux-chimique/feux-chimique-attention/feux-chimique-attention';
import { FeuxChimiqueInterditComponent } from '../../components/feux-chimique/feux-chimique-interdit/feux-chimique-interdit';
import { FeuxChimiqueRisquesComponent } from '../../components/feux-chimique/feux-chimique-risques/feux-chimique-risques';
import { FeuxChimiqueStrategieComponent } from '../../components/feux-chimique/feux-chimique-strategie/feux-chimique-strategie';

/**
 * Generated class for the FeuchimiquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feuchimique',
  templateUrl: 'feuchimique.html',
})
export class FeuchimiquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuchimiquePage');
  }
  tab1Root=FeuxChimiqueRisquesComponent;
  tab2Root=FeuxChimiqueStrategieComponent;
  tab3Root=FeuxChimiqueAccesComponent;
  tab4Root=FeuxChimiqueActionComponent;
  tab5Root=FeuxChimiqueAttentionComponent;
  tab6Root=FeuxChimiqueInterditComponent;
}
