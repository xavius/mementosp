import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuchimiquePage } from './feuchimique';

@NgModule({
  declarations: [
    FeuchimiquePage,
  ],
  imports: [
    IonicPageModule.forChild(FeuchimiquePage),
  ],
})
export class FeuchimiquePageModule {}
