import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfarctusPage } from './infarctus';

@NgModule({
  declarations: [
    InfarctusPage,
  ],
  imports: [
    IonicPageModule.forChild(InfarctusPage),
  ],
})
export class InfarctusPageModule {}
