import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapInfarctusSignesComponent } from '../../components/sap-infarctus/sap-infarctus-signes/sap-infarctus-signes';
import { SapInfarctusBilansComponent } from '../../components/sap-infarctus/sap-infarctus-bilans/sap-infarctus-bilans';
import { SapInfarctusActionsComponent } from '../../components/sap-infarctus/sap-infarctus-actions/sap-infarctus-actions';
import { SapInfarctusImportantComponent } from '../../components/sap-infarctus/sap-infarctus-important/sap-infarctus-important';

/**
 * Generated class for the InfarctusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-infarctus',
  templateUrl: 'infarctus.html',
})
export class InfarctusPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfarctusPage');
  }
  tab7Root=SapInfarctusSignesComponent;
  tab8Root=SapInfarctusBilansComponent;
  tab9Root=SapInfarctusActionsComponent;
  tab10Root=SapInfarctusImportantComponent;
}
