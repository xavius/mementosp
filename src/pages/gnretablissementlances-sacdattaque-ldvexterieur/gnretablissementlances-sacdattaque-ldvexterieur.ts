import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesSacdattaqueLdvexterieurChefdadresComponent } from '../../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-chefdadres/gnretablissementlances-sacdattaque-ldvexterieur-chefdadres';
import { GnretablissementlancesSacdattaqueLdvexterieurChefdequipeComponent } from '../../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-chefdequipe/gnretablissementlances-sacdattaque-ldvexterieur-chefdequipe';
import { GnretablissementlancesSacdattaqueLdvexterieurEquipierComponent } from '../../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-equipier/gnretablissementlances-sacdattaque-ldvexterieur-equipier';
import { GnretablissementlancesSacdattaqueLdvexterieurBalComponent } from '../../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-bal/gnretablissementlances-sacdattaque-ldvexterieur-bal';

/**
 * Generated class for the GnretablissementlancesSacdattaqueLdvexterieurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-sacdattaque-ldvexterieur',
  templateUrl: 'gnretablissementlances-sacdattaque-ldvexterieur.html',
})
export class GnretablissementlancesSacdattaqueLdvexterieurPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesSacdattaqueLdvexterieurPage');
  }
  tab61Root=GnretablissementlancesSacdattaqueLdvexterieurChefdadresComponent;
  tab62Root=GnretablissementlancesSacdattaqueLdvexterieurChefdequipeComponent;
  tab63Root=GnretablissementlancesSacdattaqueLdvexterieurEquipierComponent;
  tab64Root=GnretablissementlancesSacdattaqueLdvexterieurBalComponent;
}
