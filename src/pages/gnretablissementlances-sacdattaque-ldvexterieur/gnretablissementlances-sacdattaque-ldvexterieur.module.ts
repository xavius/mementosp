import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesSacdattaqueLdvexterieurPage } from './gnretablissementlances-sacdattaque-ldvexterieur';

@NgModule({
  declarations: [
    GnretablissementlancesSacdattaqueLdvexterieurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesSacdattaqueLdvexterieurPage),
  ],
})
export class GnretablissementlancesSacdattaqueLdvexterieurPageModule {}
