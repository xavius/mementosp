import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevMoyensdextinctionFonctionnementComponent } from '../../components/prev-moyensdextinction/prev-moyensdextinction-fonctionnement/prev-moyensdextinction-fonctionnement';
import { PrevMoyensdextinctionPrecautionComponent } from '../../components/prev-moyensdextinction/prev-moyensdextinction-precaution/prev-moyensdextinction-precaution';

/**
 * Generated class for the PrevmoyensdextinctionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevmoyensdextinction',
  templateUrl: 'prevmoyensdextinction.html',
})
export class PrevmoyensdextinctionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevmoyensdextinctionPage');
  }
  tab46Root=PrevMoyensdextinctionFonctionnementComponent;
  tab47Root=PrevMoyensdextinctionPrecautionComponent;
}
