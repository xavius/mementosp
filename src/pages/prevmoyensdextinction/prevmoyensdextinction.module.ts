import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevmoyensdextinctionPage } from './prevmoyensdextinction';

@NgModule({
  declarations: [
    PrevmoyensdextinctionPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevmoyensdextinctionPage),
  ],
})
export class PrevmoyensdextinctionPageModule {}
