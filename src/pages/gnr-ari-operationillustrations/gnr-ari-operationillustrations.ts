import { GnrAriOperationillustrationsExplorationdunepiecePage } from './../gnr-ari-operationillustrations-explorationdunepiece/gnr-ari-operationillustrations-explorationdunepiece';
import { GnrAriOperationillustrationsReconnaissancelateralePage } from './../gnr-ari-operationillustrations-reconnaissancelaterale/gnr-ari-operationillustrations-reconnaissancelaterale';
import { GnrAriOperationillustrationsTravauxsurplacePage } from './../gnr-ari-operationillustrations-travauxsurplace/gnr-ari-operationillustrations-travauxsurplace';
import { GnrAriOperationillustrationsReconnaissancePage } from './../gnr-ari-operationillustrations-reconnaissance/gnr-ari-operationillustrations-reconnaissance';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrAriOperationillustrationsOperationcomplexePage } from '../gnr-ari-operationillustrations-operationcomplexe/gnr-ari-operationillustrations-operationcomplexe';

/**
 * Generated class for the GnrAriOperationillustrationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationillustrations',
  templateUrl: 'gnr-ari-operationillustrations.html',
})
export class GnrAriOperationillustrationsPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'RECONNAISSANCE', component: GnrAriOperationillustrationsReconnaissancePage},
      {title:'TRAVAUX SUR PLACE', component: GnrAriOperationillustrationsTravauxsurplacePage},
      {title:'RECONNAISSANCE LATERALE', component: GnrAriOperationillustrationsReconnaissancelateralePage},
      {title:'OPERATION COMPLEXE', component: GnrAriOperationillustrationsOperationcomplexePage},
      {title:'EXPLORATION D UNE PIECE', component: GnrAriOperationillustrationsExplorationdunepiecePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationillustrationsPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
