import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationillustrationsPage } from './gnr-ari-operationillustrations';

@NgModule({
  declarations: [
    GnrAriOperationillustrationsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationillustrationsPage),
  ],
})
export class GnrAriOperationillustrationsPageModule {}
