import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEmbrasementSignesPage } from './gnrege-embrasement-signes';

@NgModule({
  declarations: [
    GnregeEmbrasementSignesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEmbrasementSignesPage),
  ],
})
export class GnregeEmbrasementSignesPageModule {}
