import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudepesticidesPage } from './feudepesticides';

@NgModule({
  declarations: [
    FeudepesticidesPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudepesticidesPage),
  ],
})
export class FeudepesticidesPageModule {}
