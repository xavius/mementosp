import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxPesticidesAccesComponent } from '../../components/feux-pesticides/feux-pesticides-acces/feux-pesticides-acces';
import { FeuxPesticidesActionComponent } from '../../components/feux-pesticides/feux-pesticides-action/feux-pesticides-action';
import { FeuxPesticidesAttentionComponent } from '../../components/feux-pesticides/feux-pesticides-attention/feux-pesticides-attention';
import { FeuxPesticidesInterditComponent } from '../../components/feux-pesticides/feux-pesticides-interdit/feux-pesticides-interdit';
import { FeuxPesticidesRisquesComponent } from '../../components/feux-pesticides/feux-pesticides-risques/feux-pesticides-risques';
import { FeuxPesticidesStrategieComponent } from '../../components/feux-pesticides/feux-pesticides-strategie/feux-pesticides-strategie';

/**
 * Generated class for the FeudepesticidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudepesticides',
  templateUrl: 'feudepesticides.html',
})
export class FeudepesticidesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudepesticidesPage');
  }
  tab1Root=FeuxPesticidesRisquesComponent;
  tab2Root=FeuxPesticidesStrategieComponent;
  tab3Root=FeuxPesticidesAccesComponent;
  tab4Root=FeuxPesticidesActionComponent;
  tab5Root=FeuxPesticidesAttentionComponent;
  tab6Root=FeuxPesticidesInterditComponent;
}
