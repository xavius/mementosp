import { GnretablissementlancesGeneralitesMgoPage } from './../gnretablissementlances-generalites-mgo/gnretablissementlances-generalites-mgo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesGeneralitesReconnaissancePage } from '../gnretablissementlances-generalites-reconnaissance/gnretablissementlances-generalites-reconnaissance';
import { GnretablissementlancesGeneralitesSauvetagesPage } from '../gnretablissementlances-generalites-sauvetages/gnretablissementlances-generalites-sauvetages';
import { GnretablissementlancesGeneralitesProtectionindPage } from '../gnretablissementlances-generalites-protectionind/gnretablissementlances-generalites-protectionind';
import { GnretablissementlancesGeneralitesProtectioncollPage } from '../gnretablissementlances-generalites-protectioncoll/gnretablissementlances-generalites-protectioncoll';
import { GnretablissementlancesGeneralitesBinomesPage } from '../gnretablissementlances-generalites-binomes/gnretablissementlances-generalites-binomes';

/**
 * Generated class for the GnretablissementlancesGeneralitesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-generalites',
  templateUrl: 'gnretablissementlances-generalites.html',
})
export class GnretablissementlancesGeneralitesPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'MGO', component: GnretablissementlancesGeneralitesMgoPage},
      {title:'RECONNAISSANCE', component: GnretablissementlancesGeneralitesReconnaissancePage},
      {title:'SAUVETAGES', component: GnretablissementlancesGeneralitesSauvetagesPage},
      {title:'PROTECTION INDIVIDUELLE', component: GnretablissementlancesGeneralitesProtectionindPage},
      {title:'PROTECTION COLLECTIVE', component: GnretablissementlancesGeneralitesProtectioncollPage},
      {title:'LES BINOMES', component: GnretablissementlancesGeneralitesBinomesPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesGeneralitesPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
