import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesPage } from './gnretablissementlances-generalites';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesPage),
  ],
})
export class GnretablissementlancesGeneralitesPageModule {}
