import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapAttellecervicothoraciquePage } from './materiels-sap-attellecervicothoracique';

@NgModule({
  declarations: [
    MaterielsSapAttellecervicothoraciquePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapAttellecervicothoraciquePage),
  ],
})
export class MaterielsSapAttellecervicothoraciquePageModule {}
