import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapAttellecervicothoraciqueDescriptionComponent } from '../../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-description/materiels-sap-attellecervicothoracique-description';
import { MaterielsSapAttellecervicothoraciqueIndicationsComponent } from '../../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-indications/materiels-sap-attellecervicothoracique-indications';
import { MaterielsSapAttellecervicothoraciqueRisquesComponent } from '../../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-risques/materiels-sap-attellecervicothoracique-risques';
import { MaterielsSapAttellecervicothoraciqueUtilisationComponent } from '../../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-utilisation/materiels-sap-attellecervicothoracique-utilisation';
import { MaterielsSapAttellecervicothoraciquePointsclesComponent } from '../../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-pointscles/materiels-sap-attellecervicothoracique-pointscles';
import { MaterielsSapAttellecervicothoraciqueEfficaciteComponent } from '../../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-efficacite/materiels-sap-attellecervicothoracique-efficacite';


/**
 * Generated class for the MaterielsSapAttellecervicothoraciquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-attellecervicothoracique',
  templateUrl: 'materiels-sap-attellecervicothoracique.html',
})
export class MaterielsSapAttellecervicothoraciquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapAttellecervicothoraciquePage');
  }
  tab65Root=MaterielsSapAttellecervicothoraciqueDescriptionComponent;
  tab66Root=MaterielsSapAttellecervicothoraciqueIndicationsComponent;
  tab67Root=MaterielsSapAttellecervicothoraciqueRisquesComponent;
  tab68Root=MaterielsSapAttellecervicothoraciqueUtilisationComponent;
  tab69Root=MaterielsSapAttellecervicothoraciquePointsclesComponent;
  tab70Root=MaterielsSapAttellecervicothoraciqueEfficaciteComponent;
}
