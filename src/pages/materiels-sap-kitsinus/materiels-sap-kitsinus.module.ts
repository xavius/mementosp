import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapKitsinusPage } from './materiels-sap-kitsinus';

@NgModule({
  declarations: [
    MaterielsSapKitsinusPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapKitsinusPage),
  ],
})
export class MaterielsSapKitsinusPageModule {}
