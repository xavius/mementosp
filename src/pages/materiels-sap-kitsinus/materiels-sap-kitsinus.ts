import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapKitsinusDescriptionComponent } from '../../components/materiels-sap-kitsinus/materiels-sap-kitsinus-description/materiels-sap-kitsinus-description';
import { MaterielsSapKitsinusIndicationsComponent } from '../../components/materiels-sap-kitsinus/materiels-sap-kitsinus-indications/materiels-sap-kitsinus-indications';
import { MaterielsSapKitsinusRisquesComponent } from '../../components/materiels-sap-kitsinus/materiels-sap-kitsinus-risques/materiels-sap-kitsinus-risques';
import { MaterielsSapKitsinusUtilisationComponent } from '../../components/materiels-sap-kitsinus/materiels-sap-kitsinus-utilisation/materiels-sap-kitsinus-utilisation';


/**
 * Generated class for the MaterielsSapKitsinusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-kitsinus',
  templateUrl: 'materiels-sap-kitsinus.html',
})
export class MaterielsSapKitsinusPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapKitsinusPage');
  }
  tab65Root=MaterielsSapKitsinusDescriptionComponent;
  tab66Root=MaterielsSapKitsinusIndicationsComponent;
  tab67Root=MaterielsSapKitsinusRisquesComponent;
  tab68Root=MaterielsSapKitsinusUtilisationComponent;
}
