import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccidentvasculairePage } from './accidentvasculaire';

@NgModule({
  declarations: [
    AccidentvasculairePage,
  ],
  imports: [
    IonicPageModule.forChild(AccidentvasculairePage),
  ],
})
export class AccidentvasculairePageModule {}
