import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapAvcSignesComponent } from '../../components/sap-avc/sap-avc-signes/sap-avc-signes';
import { SapAvcBilansComponent } from '../../components/sap-avc/sap-avc-bilans/sap-avc-bilans';
import { SapAvcActionsComponent } from '../../components/sap-avc/sap-avc-actions/sap-avc-actions';
import { SapAvcImportantComponent } from '../../components/sap-avc/sap-avc-important/sap-avc-important';

/**
 * Generated class for the AccidentvasculairePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accidentvasculaire',
  templateUrl: 'accidentvasculaire.html',
})
export class AccidentvasculairePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccidentvasculairePage');
  }
  tab7Root=SapAvcSignesComponent;
  tab8Root=SapAvcBilansComponent;
  tab9Root=SapAvcActionsComponent;
  tab10Root=SapAvcImportantComponent;
}
