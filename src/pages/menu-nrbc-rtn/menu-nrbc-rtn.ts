import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NrbcRtnCodedangerPage } from '../nrbc-rtn-codedanger/nrbc-rtn-codedanger';
import { NrbcRtnEtiquettesdedangerPage } from '../nrbc-rtn-etiquettesdedanger/nrbc-rtn-etiquettesdedanger';
import { NrbcRtnEtiquettesdedanger2Page } from '../nrbc-rtn-etiquettesdedanger2/nrbc-rtn-etiquettesdedanger2';

/**
 * Generated class for the MenuNrbcRtnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-nrbc-rtn',
  templateUrl: 'menu-nrbc-rtn.html',
})
export class MenuNrbcRtnPage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'CODE DANGER', component: NrbcRtnCodedangerPage},
      {title:'ETIQUETTES DE DANGER', component: NrbcRtnEtiquettesdedangerPage}
    ];
    this.list2 = [
      {title:'ETIQUETTES DE DANGER', component: NrbcRtnEtiquettesdedanger2Page},
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuNrbcRtnPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
