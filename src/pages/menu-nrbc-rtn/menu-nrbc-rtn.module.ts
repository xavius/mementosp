import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuNrbcRtnPage } from './menu-nrbc-rtn';

@NgModule({
  declarations: [
    MenuNrbcRtnPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuNrbcRtnPage),
  ],
})
export class MenuNrbcRtnPageModule {}
