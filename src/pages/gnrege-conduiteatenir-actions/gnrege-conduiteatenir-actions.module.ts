import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeConduiteatenirActionsPage } from './gnrege-conduiteatenir-actions';

@NgModule({
  declarations: [
    GnregeConduiteatenirActionsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeConduiteatenirActionsPage),
  ],
})
export class GnregeConduiteatenirActionsPageModule {}
