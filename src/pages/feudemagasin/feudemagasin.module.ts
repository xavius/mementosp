import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudemagasinPage } from './feudemagasin';

@NgModule({
  declarations: [
    FeudemagasinPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudemagasinPage),
  ],
})
export class FeudemagasinPageModule {}
