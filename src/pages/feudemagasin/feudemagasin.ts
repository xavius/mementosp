import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxMagasinAccesComponent } from '../../components/feux-magasin/feux-magasin-acces/feux-magasin-acces';
import { FeuxMagasinActionComponent } from '../../components/feux-magasin/feux-magasin-action/feux-magasin-action';
import { FeuxMagasinAttentionComponent } from '../../components/feux-magasin/feux-magasin-attention/feux-magasin-attention';
import { FeuxMagasinInterditComponent } from '../../components/feux-magasin/feux-magasin-interdit/feux-magasin-interdit';
import { FeuxMagasinRisquesComponent } from '../../components/feux-magasin/feux-magasin-risques/feux-magasin-risques';
import { FeuxMagasinStrategieComponent } from '../../components/feux-magasin/feux-magasin-strategie/feux-magasin-strategie';

/**
 * Generated class for the FeudemagasinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudemagasin',
  templateUrl: 'feudemagasin.html',
})
export class FeudemagasinPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudemagasinPage');
  }
  tab1Root=FeuxMagasinRisquesComponent;
  tab2Root=FeuxMagasinStrategieComponent;
  tab3Root=FeuxMagasinAccesComponent;
  tab4Root=FeuxMagasinActionComponent;
  tab5Root=FeuxMagasinAttentionComponent;
  tab6Root=FeuxMagasinInterditComponent;
}
