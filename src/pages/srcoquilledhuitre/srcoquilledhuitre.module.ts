import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrcoquilledhuitrePage } from './srcoquilledhuitre';

@NgModule({
  declarations: [
    SrcoquilledhuitrePage,
  ],
  imports: [
    IonicPageModule.forChild(SrcoquilledhuitrePage),
  ],
})
export class SrcoquilledhuitrePageModule {}
