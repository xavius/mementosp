import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrCoquilledhuitre_1Component } from '../../components/sr-coquilledhuitre/sr-coquilledhuitre-1/sr-coquilledhuitre-1';
import { SrCoquilledhuitre_2Component } from '../../components/sr-coquilledhuitre/sr-coquilledhuitre-2/sr-coquilledhuitre-2';
import { SrCoquilledhuitre_3Component } from '../../components/sr-coquilledhuitre/sr-coquilledhuitre-3/sr-coquilledhuitre-3';
import { SrCoquilledhuitre_4Component } from '../../components/sr-coquilledhuitre/sr-coquilledhuitre-4/sr-coquilledhuitre-4';

/**
 * Generated class for the SrcoquilledhuitrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srcoquilledhuitre',
  templateUrl: 'srcoquilledhuitre.html',
})
export class SrcoquilledhuitrePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrcoquilledhuitrePage');
  }
  tab22Root=SrCoquilledhuitre_1Component;
  tab23Root=SrCoquilledhuitre_2Component;
  tab24Root=SrCoquilledhuitre_3Component;
  tab25Root=SrCoquilledhuitre_4Component;
}
