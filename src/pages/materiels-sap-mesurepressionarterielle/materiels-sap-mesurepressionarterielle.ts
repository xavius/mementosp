import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapMesurepressionarterielleDescriptionComponent } from '../../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-description/materiels-sap-mesurepressionarterielle-description';
import { MaterielsSapMesurepressionarterielleIndicationsComponent } from '../../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-indications/materiels-sap-mesurepressionarterielle-indications';
import { MaterielsSapMesurepressionarterielleRisquesComponent } from '../../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-risques/materiels-sap-mesurepressionarterielle-risques';
import { MaterielsSapMesurepressionarterielleUtilisationComponent } from '../../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-utilisation/materiels-sap-mesurepressionarterielle-utilisation';
import { MaterielsSapMesurepressionarteriellePointsclesComponent } from '../../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-pointscles/materiels-sap-mesurepressionarterielle-pointscles';
import { MaterielsSapMesurepressionarterielleEfficaciteComponent } from '../../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-efficacite/materiels-sap-mesurepressionarterielle-efficacite';


/**
 * Generated class for the MaterielsSapMesurepressionarteriellePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-mesurepressionarterielle',
  templateUrl: 'materiels-sap-mesurepressionarterielle.html',
})
export class MaterielsSapMesurepressionarteriellePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapMesurepressionarteriellePage');
  }
  tab65Root=MaterielsSapMesurepressionarterielleDescriptionComponent;
  tab66Root=MaterielsSapMesurepressionarterielleIndicationsComponent;
  tab67Root=MaterielsSapMesurepressionarterielleRisquesComponent;
  tab68Root=MaterielsSapMesurepressionarterielleUtilisationComponent;
  tab69Root=MaterielsSapMesurepressionarteriellePointsclesComponent;
  tab70Root=MaterielsSapMesurepressionarterielleEfficaciteComponent;
}
