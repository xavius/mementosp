import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapMesurepressionarteriellePage } from './materiels-sap-mesurepressionarterielle';

@NgModule({
  declarations: [
    MaterielsSapMesurepressionarteriellePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapMesurepressionarteriellePage),
  ],
})
export class MaterielsSapMesurepressionarteriellePageModule {}
