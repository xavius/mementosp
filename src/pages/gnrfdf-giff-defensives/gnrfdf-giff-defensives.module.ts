import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfGiffDefensivesPage } from './gnrfdf-giff-defensives';

@NgModule({
  declarations: [
    GnrfdfGiffDefensivesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfGiffDefensivesPage),
  ],
})
export class GnrfdfGiffDefensivesPageModule {}
