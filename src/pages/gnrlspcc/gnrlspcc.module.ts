import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrlspccPage } from './gnrlspcc';

@NgModule({
  declarations: [
    GnrlspccPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrlspccPage),
  ],
})
export class GnrlspccPageModule {}
