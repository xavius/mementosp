import { GnrLspccOperationLesordresPage } from './../gnr-lspcc-operation-lesordres/gnr-lspcc-operation-lesordres';
import { GnrLspccOperationProtectionfixehumainPage } from './../gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain';
import { GnrLspccOperationProtectionchutesPage } from './../gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes';
import { GnrLspccOperationSauvetageexterieurPage } from './../gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur';
import { GnrLspccOperationSauvetageexcavationPage } from './../gnr-lspcc-operation-sauvetageexcavation/gnr-lspcc-operation-sauvetageexcavation';
import { GnrLspccOperationReglesdebasesPage } from './../gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases';
import { GnrLspccAmarrageetnoeudsPage } from './../gnr-lspcc-amarrageetnoeuds/gnr-lspcc-amarrageetnoeuds';
import { GnrLspccCompositionPage } from './../gnr-lspcc-composition/gnr-lspcc-composition';
import { GnrLspccReconnaissancePage } from './../gnr-lspcc-reconnaissance/gnr-lspcc-reconnaissance';
import { GnrLspccProtectionindPage } from './../gnr-lspcc-protectionind/gnr-lspcc-protectionind';
import { GnrLspccSauvetageexcavationPage } from './../gnr-lspcc-sauvetageexcavation/gnr-lspcc-sauvetageexcavation';
import { GnrLspccSauvetageexterieurPage } from './../gnr-lspcc-sauvetageexterieur/gnr-lspcc-sauvetageexterieur';
import { GnrLspccDestinationPage } from './../gnr-lspcc-destination/gnr-lspcc-destination';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrlspccPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrlspcc',
  templateUrl: 'gnrlspcc.html',
})
export class GnrlspccPage {

  list: Array<{title: string, component: any}>;
  list2: Array<{title: string, component: any}>;
  list3: Array<{title: string, component: any}>;
  list4: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Destination', component: GnrLspccDestinationPage},
      {title:'Sauvetage par l Exterieur', component: GnrLspccSauvetageexterieurPage},
      {title:'Sauvetage en Excavation', component: GnrLspccSauvetageexcavationPage},
      {title:'Protection Individuelle', component: GnrLspccProtectionindPage},
      {title:'Reconnaissance', component: GnrLspccReconnaissancePage}
    ];
    this.list2 = [
      {title:'Composition du LSPCC', component: GnrLspccCompositionPage},
    ];
    this.list3 = [
      {title:'Amarrages et Noeuds', component: GnrLspccAmarrageetnoeudsPage},
    ];
    this.list4 = [
      {title:'Règles de Bases', component: GnrLspccOperationReglesdebasesPage},
      {title:'Sauvetage par L Exterieur', component: GnrLspccOperationSauvetageexterieurPage},
      {title:'Sauvetage en Excavation', component: GnrLspccOperationSauvetageexcavationPage},
      {title:'Protection Contre les Chutes', component: GnrLspccOperationProtectionchutesPage},
      {title:'Protection Fixe Humain', component: GnrLspccOperationProtectionfixehumainPage},
      {title:'Les Ordres', component: GnrLspccOperationLesordresPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrlspccPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
