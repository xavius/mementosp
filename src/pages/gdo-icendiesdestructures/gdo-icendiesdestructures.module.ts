import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GdoIcendiesdestructuresPage } from './gdo-icendiesdestructures';

@NgModule({
  declarations: [
    GdoIcendiesdestructuresPage,
  ],
  imports: [
    IonicPageModule.forChild(GdoIcendiesdestructuresPage),
  ],
})
export class GdoIcendiesdestructuresPageModule {}
