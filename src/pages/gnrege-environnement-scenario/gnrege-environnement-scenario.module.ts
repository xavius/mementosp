import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeEnvironnementScenarioPage } from './gnrege-environnement-scenario';

@NgModule({
  declarations: [
    GnregeEnvironnementScenarioPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeEnvironnementScenarioPage),
  ],
})
export class GnregeEnvironnementScenarioPageModule {}
