import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FonctiondupcPage } from './fonctiondupc';

@NgModule({
  declarations: [
    FonctiondupcPage,
  ],
  imports: [
    IonicPageModule.forChild(FonctiondupcPage),
  ],
})
export class FonctiondupcPageModule {}
