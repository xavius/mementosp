import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GocFonctiondupcCrmComponent } from '../../components/goc-fonctiondupc/goc-fonctiondupc-crm/goc-fonctiondupc-crm';
import { GocFonctiondupcTransComponent } from '../../components/goc-fonctiondupc/goc-fonctiondupc-trans/goc-fonctiondupc-trans';
import { GocFonctiondupcRensComponent } from '../../components/goc-fonctiondupc/goc-fonctiondupc-rens/goc-fonctiondupc-rens';

/**
 * Generated class for the FonctiondupcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fonctiondupc',
  templateUrl: 'fonctiondupc.html',
})
export class FonctiondupcPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FonctiondupcPage');
  }
  tab35Root=GocFonctiondupcCrmComponent;
  tab36Root=GocFonctiondupcTransComponent;
  tab37Root=GocFonctiondupcRensComponent;
}
