import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccDestinationPage } from './gnr-lspcc-destination';

@NgModule({
  declarations: [
    GnrLspccDestinationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccDestinationPage),
  ],
})
export class GnrLspccDestinationPageModule {}
