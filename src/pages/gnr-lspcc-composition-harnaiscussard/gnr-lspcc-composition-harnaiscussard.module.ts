import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionHarnaiscussardPage } from './gnr-lspcc-composition-harnaiscussard';

@NgModule({
  declarations: [
    GnrLspccCompositionHarnaiscussardPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionHarnaiscussardPage),
  ],
})
export class GnrLspccCompositionHarnaiscussardPageModule {}
