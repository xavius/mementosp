import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapPiqureshymenopteresSignesComponent } from '../../components/sap-piqureshymenopteres/sap-piqureshymenopteres-signes/sap-piqureshymenopteres-signes';
import { SapPiqureshymenopteresBilansComponent } from '../../components/sap-piqureshymenopteres/sap-piqureshymenopteres-bilans/sap-piqureshymenopteres-bilans';
import { SapPiqureshymenopteresActionsComponent } from '../../components/sap-piqureshymenopteres/sap-piqureshymenopteres-actions/sap-piqureshymenopteres-actions';
import { SapPiqureshymenopteresImportantComponent } from '../../components/sap-piqureshymenopteres/sap-piqureshymenopteres-important/sap-piqureshymenopteres-important';

/**
 * Generated class for the PiqureshymenopteresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-piqureshymenopteres',
  templateUrl: 'piqureshymenopteres.html',
})
export class PiqureshymenopteresPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PiqureshymenopteresPage');
  }
  tab7Root=SapPiqureshymenopteresSignesComponent;
  tab8Root=SapPiqureshymenopteresBilansComponent;
  tab9Root=SapPiqureshymenopteresActionsComponent;
  tab10Root=SapPiqureshymenopteresImportantComponent;
}
