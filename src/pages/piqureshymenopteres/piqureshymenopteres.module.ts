import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PiqureshymenopteresPage } from './piqureshymenopteres';

@NgModule({
  declarations: [
    PiqureshymenopteresPage,
  ],
  imports: [
    IonicPageModule.forChild(PiqureshymenopteresPage),
  ],
})
export class PiqureshymenopteresPageModule {}
