import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrCombinaisonsPage } from './gnretablissementlances-manoeuvres-gnr-combinaisons';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrCombinaisonsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrCombinaisonsPage),
  ],
})
export class GnretablissementlancesManoeuvresGnrCombinaisonsPageModule {}
