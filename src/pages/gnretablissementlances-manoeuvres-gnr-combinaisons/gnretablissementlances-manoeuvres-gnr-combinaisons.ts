import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrCombinaisonsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-combinaisons',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-combinaisons.html',
})
export class GnretablissementlancesManoeuvresGnrCombinaisonsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrCombinaisonsPage');
  }

}
