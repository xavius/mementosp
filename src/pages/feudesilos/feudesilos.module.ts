import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudesilosPage } from './feudesilos';

@NgModule({
  declarations: [
    FeudesilosPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudesilosPage),
  ],
})
export class FeudesilosPageModule {}
