import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxSilosAccesComponent } from '../../components/feux-silos/feux-silos-acces/feux-silos-acces';
import { FeuxSilosActionComponent } from '../../components/feux-silos/feux-silos-action/feux-silos-action';
import { FeuxSilosAttentionComponent } from '../../components/feux-silos/feux-silos-attention/feux-silos-attention';
import { FeuxSilosInterditComponent } from '../../components/feux-silos/feux-silos-interdit/feux-silos-interdit';
import { FeuxSilosRisquesComponent } from '../../components/feux-silos/feux-silos-risques/feux-silos-risques';
import { FeuxSilosStrategieComponent } from '../../components/feux-silos/feux-silos-strategie/feux-silos-strategie';

/**
 * Generated class for the FeudesilosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudesilos',
  templateUrl: 'feudesilos.html',
})
export class FeudesilosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudesilosPage');
  }
  tab1Root=FeuxSilosRisquesComponent;
  tab2Root=FeuxSilosStrategieComponent;
  tab3Root=FeuxSilosAccesComponent;
  tab4Root=FeuxSilosActionComponent;
  tab5Root=FeuxSilosAttentionComponent;
  tab6Root=FeuxSilosInterditComponent;
}
