import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivAscenseursPage } from './div-ascenseurs';

@NgModule({
  declarations: [
    DivAscenseursPage,
  ],
  imports: [
    IonicPageModule.forChild(DivAscenseursPage),
  ],
})
export class DivAscenseursPageModule {}
