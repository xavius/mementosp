import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OedemedequinckePage } from './oedemedequincke';

@NgModule({
  declarations: [
    OedemedequinckePage,
  ],
  imports: [
    IonicPageModule.forChild(OedemedequinckePage),
  ],
})
export class OedemedequinckePageModule {}
