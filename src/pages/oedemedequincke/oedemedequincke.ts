import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapOedemedequinckeSignesComponent } from '../../components/sap-oedemedequincke/sap-oedemedequincke-signes/sap-oedemedequincke-signes';
import { SapOedemedequinckeBilansComponent } from '../../components/sap-oedemedequincke/sap-oedemedequincke-bilans/sap-oedemedequincke-bilans';
import { SapOedemedequinckeActionsComponent } from '../../components/sap-oedemedequincke/sap-oedemedequincke-actions/sap-oedemedequincke-actions';
import { SapOedemedequinckeImportantComponent } from '../../components/sap-oedemedequincke/sap-oedemedequincke-important/sap-oedemedequincke-important';

/**
 * Generated class for the OedemedequinckePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-oedemedequincke',
  templateUrl: 'oedemedequincke.html',
})
export class OedemedequinckePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OedemedequinckePage');
  }
  tab7Root=SapOedemedequinckeSignesComponent;
  tab8Root=SapOedemedequinckeBilansComponent;
  tab9Root=SapOedemedequinckeActionsComponent;
  tab10Root=SapOedemedequinckeImportantComponent;
}
