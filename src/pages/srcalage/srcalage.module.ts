import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrcalagePage } from './srcalage';

@NgModule({
  declarations: [
    SrcalagePage,
  ],
  imports: [
    IonicPageModule.forChild(SrcalagePage),
  ],
})
export class SrcalagePageModule {}
