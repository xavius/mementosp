import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrCalage_1Component } from '../../components/sr-calage/sr-calage-1/sr-calage-1';
import { SrCalage_2Component } from '../../components/sr-calage/sr-calage-2/sr-calage-2';
import { SrCalage_3Component } from '../../components/sr-calage/sr-calage-3/sr-calage-3';
import { SrCalage_4Component } from '../../components/sr-calage/sr-calage-4/sr-calage-4';
import { SrCalage_5Component } from '../../components/sr-calage/sr-calage-5/sr-calage-5';

/**
 * Generated class for the SrcalagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srcalage',
  templateUrl: 'srcalage.html',
})
export class SrcalagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrcalagePage');
  }
  tab22Root=SrCalage_1Component;
  tab23Root=SrCalage_2Component;
  tab24Root=SrCalage_3Component;
  tab25Root=SrCalage_4Component;
  tab26Root=SrCalage_5Component;
}
