import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxPlanchersAccesComponent } from '../../components/feux-planchers/feux-planchers-acces/feux-planchers-acces';
import { FeuxPlanchersActionComponent } from '../../components/feux-planchers/feux-planchers-action/feux-planchers-action';
import { FeuxPlanchersAttentionComponent } from '../../components/feux-planchers/feux-planchers-attention/feux-planchers-attention';
import { FeuxPlanchersInterditComponent } from '../../components/feux-planchers/feux-planchers-interdit/feux-planchers-interdit';
import { FeuxPlanchersRisquesComponent } from '../../components/feux-planchers/feux-planchers-risques/feux-planchers-risques';
import { FeuxPlanchersStrategieComponent } from '../../components/feux-planchers/feux-planchers-strategie/feux-planchers-strategie';

/**
 * Generated class for the FeudeplanchersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudeplanchers',
  templateUrl: 'feudeplanchers.html',
})
export class FeudeplanchersPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudeplanchersPage');
  }
  tab1Root=FeuxPlanchersRisquesComponent;
  tab2Root=FeuxPlanchersStrategieComponent;
  tab3Root=FeuxPlanchersAccesComponent;
  tab4Root=FeuxPlanchersActionComponent;
  tab5Root=FeuxPlanchersAttentionComponent;
  tab6Root=FeuxPlanchersInterditComponent;

}
