import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudeplanchersPage } from './feudeplanchers';

@NgModule({
  declarations: [
    FeudeplanchersPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudeplanchersPage),
  ],
})
export class FeudeplanchersPageModule {}
