import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxErpTypeJuAccesComponent } from '../../components/feux-erp-type-ju/feux-erp-type-ju-acces/feux-erp-type-ju-acces';
import { FeuxErpTypeJuActionComponent } from '../../components/feux-erp-type-ju/feux-erp-type-ju-action/feux-erp-type-ju-action';
import { FeuxErpTypeJuAttentionComponent } from '../../components/feux-erp-type-ju/feux-erp-type-ju-attention/feux-erp-type-ju-attention';
import { FeuxErpTypeJuInterditComponent } from '../../components/feux-erp-type-ju/feux-erp-type-ju-interdit/feux-erp-type-ju-interdit';
import { FeuxErpTypeJuRisquesComponent } from '../../components/feux-erp-type-ju/feux-erp-type-ju-risques/feux-erp-type-ju-risques';
import { FeuxErpTypeJuStrategieComponent } from '../../components/feux-erp-type-ju/feux-erp-type-ju-strategie/feux-erp-type-ju-strategie';

/**
 * Generated class for the FeuErPtypeJuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feu-er-ptype-ju',
  templateUrl: 'feu-er-ptype-ju.html',
})
export class FeuErPtypeJuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuErPtypeJuPage');
  }
  tab1Root=FeuxErpTypeJuRisquesComponent;
  tab2Root=FeuxErpTypeJuStrategieComponent;
  tab3Root=FeuxErpTypeJuAccesComponent;
  tab4Root=FeuxErpTypeJuActionComponent;
  tab5Root=FeuxErpTypeJuAttentionComponent;
  tab6Root=FeuxErpTypeJuInterditComponent;
}
