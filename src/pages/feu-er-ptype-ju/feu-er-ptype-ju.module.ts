import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuErPtypeJuPage } from './feu-er-ptype-ju';

@NgModule({
  declarations: [
    FeuErPtypeJuPage,
  ],
  imports: [
    IonicPageModule.forChild(FeuErPtypeJuPage),
  ],
})
export class FeuErPtypeJuPageModule {}
