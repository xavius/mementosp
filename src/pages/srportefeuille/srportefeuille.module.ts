import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrportefeuillePage } from './srportefeuille';

@NgModule({
  declarations: [
    SrportefeuillePage,
  ],
  imports: [
    IonicPageModule.forChild(SrportefeuillePage),
  ],
})
export class SrportefeuillePageModule {}
