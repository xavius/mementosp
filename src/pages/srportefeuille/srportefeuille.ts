import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrPortefeuille_1Component } from '../../components/sr-portefeuille/sr-portefeuille-1/sr-portefeuille-1';
import { SrPortefeuille_2Component } from '../../components/sr-portefeuille/sr-portefeuille-2/sr-portefeuille-2';
import { SrPortefeuille_3Component } from '../../components/sr-portefeuille/sr-portefeuille-3/sr-portefeuille-3';

/**
 * Generated class for the SrportefeuillePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srportefeuille',
  templateUrl: 'srportefeuille.html',
})
export class SrportefeuillePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrportefeuillePage');
  }
  tab22Root=SrPortefeuille_1Component;
  tab23Root=SrPortefeuille_2Component;
  tab24Root=SrPortefeuille_3Component;
}
