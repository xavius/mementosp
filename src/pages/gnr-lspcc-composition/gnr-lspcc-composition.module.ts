import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionPage } from './gnr-lspcc-composition';

@NgModule({
  declarations: [
    GnrLspccCompositionPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionPage),
  ],
})
export class GnrLspccCompositionPageModule {}
