import { GnrLspccCompositionEntretienPage } from './../gnr-lspcc-composition-entretien/gnr-lspcc-composition-entretien';
import { GnrLspccCompositionRangementPage } from './../gnr-lspcc-composition-rangement/gnr-lspcc-composition-rangement';
import { GnrLspccCompositionCordelettePage } from './../gnr-lspcc-composition-cordelette/gnr-lspcc-composition-cordelette';
import { GnrLspccCompositionProtectionPage } from './../gnr-lspcc-composition-protection/gnr-lspcc-composition-protection';
import { GnrLspccCompositionTriangledevacuationPage } from './../gnr-lspcc-composition-triangledevacuation/gnr-lspcc-composition-triangledevacuation';
import { GnrLspccCompositionHarnaiscussardPage } from './../gnr-lspcc-composition-harnaiscussard/gnr-lspcc-composition-harnaiscussard';
import { GnrLspccCompositionAnneauxcoususPage } from './../gnr-lspcc-composition-anneauxcousus/gnr-lspcc-composition-anneauxcousus';
import { GnrLspccCompositionPouliePage } from './../gnr-lspcc-composition-poulie/gnr-lspcc-composition-poulie';
import { GnrLspccCompositionMousquetonsPage } from './../gnr-lspcc-composition-mousquetons/gnr-lspcc-composition-mousquetons';
import { GnrLspccCompositionDescendeurPage } from './../gnr-lspcc-composition-descendeur/gnr-lspcc-composition-descendeur';
import { GnrLspccCompositionCordePage } from './../gnr-lspcc-composition-corde/gnr-lspcc-composition-corde';
import { GnrLspccCompositionSacdetransportPage } from './../gnr-lspcc-composition-sacdetransport/gnr-lspcc-composition-sacdetransport';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrLspccCompositionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-composition',
  templateUrl: 'gnr-lspcc-composition.html',
})
export class GnrLspccCompositionPage {

  list: Array<{image: string ,title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {image: 'assets/icon/Sac_de_transport1.png', title:'SAC DE TRANSPORT',component: GnrLspccCompositionSacdetransportPage},
      {image: 'assets/icon/corde1.png' ,title:'CORDE',component: GnrLspccCompositionCordePage},
      {image: 'assets/icon/huit.jpg' ,title:'DESCENDEUR',component: GnrLspccCompositionDescendeurPage},
      {image: 'assets/icon/mousquetons.jpg',title:'MOUSQUETONS',component: GnrLspccCompositionMousquetonsPage},
      {image: 'assets/icon/poulie.jpg',title:'POULIE',component: GnrLspccCompositionPouliePage},
      {image: 'assets/icon/anneaux.jpg',title:'ANNEAUX COUSUS',component: GnrLspccCompositionAnneauxcoususPage},
      {image: 'assets/icon/harnais.png',title:'HARNAIS CUISSARD',component: GnrLspccCompositionHarnaiscussardPage},
      {image: 'assets/icon/triangle.gif',title:'TRIANGLE D EVACUATION',component: GnrLspccCompositionTriangledevacuationPage},
      {image: 'assets/icon/protection_corde.jpg',title:'PROTECTION (option)',component: GnrLspccCompositionProtectionPage},
      {image: 'assets/icon/anneaux_2.jpg',title:'CORDELETTE (option)',component: GnrLspccCompositionCordelettePage},
      {image: 'assets/icon/lot courant.jpg',title:'RANGEMENT DANS LE SAC',component: GnrLspccCompositionRangementPage},
      {image: 'assets/icon/Lot_de_sauvetage.jpg',title:'ENTRETIEN ET CONTROLE',component: GnrLspccCompositionEntretienPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccCompositionPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
