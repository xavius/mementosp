import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapKitsectiondemembresPage } from './materiels-sap-kitsectiondemembres';

@NgModule({
  declarations: [
    MaterielsSapKitsectiondemembresPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapKitsectiondemembresPage),
  ],
})
export class MaterielsSapKitsectiondemembresPageModule {}
