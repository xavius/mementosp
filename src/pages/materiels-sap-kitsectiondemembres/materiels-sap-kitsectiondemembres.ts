import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapKitsectiondemembresDescriptionComponent } from '../../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-description/materiels-sap-kitsectiondemembres-description';
import { MaterielsSapKitsectiondemembresIndicationsComponent } from '../../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-indications/materiels-sap-kitsectiondemembres-indications';
import { MaterielsSapKitsectiondemembresRisquesComponent } from '../../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-risques/materiels-sap-kitsectiondemembres-risques';
import { MaterielsSapKitsectiondemembresUtilisationComponent } from '../../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-utilisation/materiels-sap-kitsectiondemembres-utilisation';
import { MaterielsSapKitsectiondemembresPointsclesComponent } from '../../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-pointscles/materiels-sap-kitsectiondemembres-pointscles';
import { MaterielsSapKitsectiondemembresEfficaciteComponent } from '../../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-efficacite/materiels-sap-kitsectiondemembres-efficacite';


/**
 * Generated class for the MaterielsSapKitsectiondemembresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-kitsectiondemembres',
  templateUrl: 'materiels-sap-kitsectiondemembres.html',
})
export class MaterielsSapKitsectiondemembresPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapKitsectiondemembresPage');
  }
  tab65Root=MaterielsSapKitsectiondemembresDescriptionComponent;
  tab66Root=MaterielsSapKitsectiondemembresIndicationsComponent;
  tab67Root=MaterielsSapKitsectiondemembresRisquesComponent;
  tab68Root=MaterielsSapKitsectiondemembresUtilisationComponent;
  tab69Root=MaterielsSapKitsectiondemembresPointsclesComponent;
  tab70Root=MaterielsSapKitsectiondemembresEfficaciteComponent;
}
