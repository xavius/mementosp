import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccSauvetageexcavationPage } from './gnr-lspcc-sauvetageexcavation';

@NgModule({
  declarations: [
    GnrLspccSauvetageexcavationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccSauvetageexcavationPage),
  ],
})
export class GnrLspccSauvetageexcavationPageModule {}
