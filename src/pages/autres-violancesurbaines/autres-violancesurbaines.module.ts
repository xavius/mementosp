import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutresViolancesurbainesPage } from './autres-violancesurbaines';

@NgModule({
  declarations: [
    AutresViolancesurbainesPage,
  ],
  imports: [
    IonicPageModule.forChild(AutresViolancesurbainesPage),
  ],
})
export class AutresViolancesurbainesPageModule {}
