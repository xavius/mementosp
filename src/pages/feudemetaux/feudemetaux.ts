import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxMetauxAccesComponent } from '../../components/feux-metaux/feux-metaux-acces/feux-metaux-acces';
import { FeuxMetauxActionComponent } from '../../components/feux-metaux/feux-metaux-action/feux-metaux-action';
import { FeuxMetauxAttentionComponent } from '../../components/feux-metaux/feux-metaux-attention/feux-metaux-attention';
import { FeuxMetauxInterditComponent } from '../../components/feux-metaux/feux-metaux-interdit/feux-metaux-interdit';
import { FeuxMetauxRisquesComponent } from '../../components/feux-metaux/feux-metaux-risques/feux-metaux-risques';
import { FeuxMetauxStrategieComponent } from '../../components/feux-metaux/feux-metaux-strategie/feux-metaux-strategie';

/**
 * Generated class for the FeudemetauxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudemetaux',
  templateUrl: 'feudemetaux.html',
})
export class FeudemetauxPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudemetauxPage');
  }
  tab1Root=FeuxMetauxRisquesComponent;
  tab2Root=FeuxMetauxStrategieComponent;
  tab3Root=FeuxMetauxAccesComponent;
  tab4Root=FeuxMetauxActionComponent;
  tab5Root=FeuxMetauxAttentionComponent;
  tab6Root=FeuxMetauxInterditComponent;
}
