import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudemetauxPage } from './feudemetaux';

@NgModule({
  declarations: [
    FeudemetauxPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudemetauxPage),
  ],
})
export class FeudemetauxPageModule {}
