import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutilsopAricoPage } from './outilsop-arico';

@NgModule({
  declarations: [
    OutilsopAricoPage,
  ],
  imports: [
    IonicPageModule.forChild(OutilsopAricoPage),
  ],
})
export class OutilsopAricoPageModule {}
