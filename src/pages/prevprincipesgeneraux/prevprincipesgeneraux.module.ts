import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevprincipesgenerauxPage } from './prevprincipesgeneraux';

@NgModule({
  declarations: [
    PrevprincipesgenerauxPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevprincipesgenerauxPage),
  ],
})
export class PrevprincipesgenerauxPageModule {}
