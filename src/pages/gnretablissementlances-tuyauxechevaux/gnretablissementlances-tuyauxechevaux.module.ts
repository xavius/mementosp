import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxPage } from './gnretablissementlances-tuyauxechevaux';

@NgModule({
  declarations: [
    GnretablissementlancesTuyauxechevauxPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesTuyauxechevauxPage),
  ],
})
export class GnretablissementlancesTuyauxechevauxPageModule {}
