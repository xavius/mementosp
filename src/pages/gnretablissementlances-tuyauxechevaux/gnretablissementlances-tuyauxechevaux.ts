import { GnretablissementlancesTuyauxechevauxLdvprisedeauPage } from './../gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau';
import { GnretablissementlancesTuyauxechevauxLdvexterieurPage } from './../gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage } from './../gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse';
import { GnretablissementlancesTuyauxechevauxLdvattaquePage } from './../gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque';
import { GnretablissementlancesTuyauxechevauxMaterielPage } from './../gnretablissementlances-tuyauxechevaux-materiel/gnretablissementlances-tuyauxechevaux-materiel';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesTuyauxechevauxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-tuyauxechevaux',
  templateUrl: 'gnretablissementlances-tuyauxechevaux.html',
})
export class GnretablissementlancesTuyauxechevauxPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Matériel' , component: GnretablissementlancesTuyauxechevauxMaterielPage},
      {title:'LDV - Prise d eau vers attaque' , component: GnretablissementlancesTuyauxechevauxLdvprisedeauPage},
      {title:'LDV - Attaque vers prise d eau' , component: GnretablissementlancesTuyauxechevauxLdvattaquePage},
      {title:'LDV par l extérieur' , component: GnretablissementlancesTuyauxechevauxLdvexterieurPage},
      {title:'LDV par l échelle a coulisses', component: GnretablissementlancesTuyauxechevauxLdvechelleacoulissePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesTuyauxechevauxPage');
  }

itemSelected(item){
  this.navCtrl.push(item.component);
}
}
