import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationillustrationsOperationcomplexePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationillustrations-operationcomplexe',
  templateUrl: 'gnr-ari-operationillustrations-operationcomplexe.html',
})
export class GnrAriOperationillustrationsOperationcomplexePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationillustrationsOperationcomplexePage');
  }

}
