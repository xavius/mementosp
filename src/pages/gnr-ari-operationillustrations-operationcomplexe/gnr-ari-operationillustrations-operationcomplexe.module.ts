import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationillustrationsOperationcomplexePage } from './gnr-ari-operationillustrations-operationcomplexe';

@NgModule({
  declarations: [
    GnrAriOperationillustrationsOperationcomplexePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationillustrationsOperationcomplexePage),
  ],
})
export class GnrAriOperationillustrationsOperationcomplexePageModule {}
