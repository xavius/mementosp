import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapKitaesDescriptionComponent } from '../../components/materiels-sap-kitaes/materiels-sap-kitaes-description/materiels-sap-kitaes-description';
import { MaterielsSapKitaesIndicationsComponent } from '../../components/materiels-sap-kitaes/materiels-sap-kitaes-indications/materiels-sap-kitaes-indications';
import { MaterielsSapKitaesRisquesComponent } from '../../components/materiels-sap-kitaes/materiels-sap-kitaes-risques/materiels-sap-kitaes-risques';
import { MaterielsSapKitaesUtilisationComponent } from '../../components/materiels-sap-kitaes/materiels-sap-kitaes-utilisation/materiels-sap-kitaes-utilisation';


/**
 * Generated class for the MaterielsSapKitaesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-kitaes',
  templateUrl: 'materiels-sap-kitaes.html',
})
export class MaterielsSapKitaesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapKitaesPage');
  }
  tab65Root=MaterielsSapKitaesDescriptionComponent;
  tab66Root=MaterielsSapKitaesIndicationsComponent;
  tab67Root=MaterielsSapKitaesRisquesComponent;
  tab68Root=MaterielsSapKitaesUtilisationComponent;
}
