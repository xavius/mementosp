import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapKitaesPage } from './materiels-sap-kitaes';

@NgModule({
  declarations: [
    MaterielsSapKitaesPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapKitaesPage),
  ],
})
export class MaterielsSapKitaesPageModule {}
