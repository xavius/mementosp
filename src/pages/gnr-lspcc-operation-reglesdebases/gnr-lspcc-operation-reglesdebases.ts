import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnrLspccOperationReglesdebasesAvantComponent } from '../../components/gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases-avant/gnr-lspcc-operation-reglesdebases-avant';
import { GnrLspccOperationReglesdebasesPendantComponent } from '../../components/gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases-pendant/gnr-lspcc-operation-reglesdebases-pendant';
import { GnrLspccOperationReglesdebasesApresComponent } from '../../components/gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases-apres/gnr-lspcc-operation-reglesdebases-apres';

/**
 * Generated class for the GnrLspccOperationReglesdebasesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-operation-reglesdebases',
  templateUrl: 'gnr-lspcc-operation-reglesdebases.html',
})
export class GnrLspccOperationReglesdebasesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccOperationReglesdebasesPage');
  }
  tab22Root=GnrLspccOperationReglesdebasesAvantComponent;
  tab23Root=GnrLspccOperationReglesdebasesPendantComponent;
  tab24Root=GnrLspccOperationReglesdebasesApresComponent;
}
