import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationReglesdebasesPage } from './gnr-lspcc-operation-reglesdebases';

@NgModule({
  declarations: [
    GnrLspccOperationReglesdebasesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationReglesdebasesPage),
  ],
})
export class GnrLspccOperationReglesdebasesPageModule {}
