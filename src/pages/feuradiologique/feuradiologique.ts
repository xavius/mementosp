import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxRadiologiqueAccesComponent } from '../../components/feux-radiologique/feux-radiologique-acces/feux-radiologique-acces';
import { FeuxRadiologiqueActionComponent } from '../../components/feux-radiologique/feux-radiologique-action/feux-radiologique-action';
import { FeuxRadiologiqueAttentionComponent } from '../../components/feux-radiologique/feux-radiologique-attention/feux-radiologique-attention';
import { FeuxRadiologiqueInterditComponent } from '../../components/feux-radiologique/feux-radiologique-interdit/feux-radiologique-interdit';
import { FeuxRadiologiqueRisquesComponent } from '../../components/feux-radiologique/feux-radiologique-risques/feux-radiologique-risques';
import { FeuxRadiologiqueStrategieComponent } from '../../components/feux-radiologique/feux-radiologique-strategie/feux-radiologique-strategie';

/**
 * Generated class for the FeuradiologiquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feuradiologique',
  templateUrl: 'feuradiologique.html',
})
export class FeuradiologiquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeuradiologiquePage');
  }
  tab1Root=FeuxRadiologiqueRisquesComponent;
  tab2Root=FeuxRadiologiqueStrategieComponent;
  tab3Root=FeuxRadiologiqueAccesComponent;
  tab4Root=FeuxRadiologiqueActionComponent;
  tab5Root=FeuxRadiologiqueAttentionComponent;
  tab6Root=FeuxRadiologiqueInterditComponent;
}
