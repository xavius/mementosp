import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeuradiologiquePage } from './feuradiologique';

@NgModule({
  declarations: [
    FeuradiologiquePage,
  ],
  imports: [
    IonicPageModule.forChild(FeuradiologiquePage),
  ],
})
export class FeuradiologiquePageModule {}
