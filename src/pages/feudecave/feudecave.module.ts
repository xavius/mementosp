import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudecavePage } from './feudecave';

@NgModule({
  declarations: [
    FeudecavePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudecavePage),
  ],
})
export class FeudecavePageModule {}
