import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxCaveRisquesComponent } from '../../components/feux-cave/feux-cave-risques/feux-cave-risques';
import { FeuxCaveStrategieComponent } from '../../components/feux-cave/feux-cave-strategie/feux-cave-strategie';
import { FeuxCaveAccesComponent } from '../../components/feux-cave/feux-cave-acces/feux-cave-acces';
import { FeuxCaveActionComponent } from '../../components/feux-cave/feux-cave-action/feux-cave-action';
import { FeuxCaveAttentionComponent } from '../../components/feux-cave/feux-cave-attention/feux-cave-attention';
import { FeuxCaveInterditComponent } from '../../components/feux-cave/feux-cave-interdit/feux-cave-interdit';
/**
 * Generated class for the FeudecavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudecave',
  templateUrl: 'feudecave.html',
})
export class FeudecavePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudecavePage');
  }

  tab1Root= FeuxCaveRisquesComponent;
  tab2Root= FeuxCaveStrategieComponent;
  tab3Root= FeuxCaveAccesComponent;
  tab4Root= FeuxCaveActionComponent;
  tab5Root= FeuxCaveAttentionComponent;
  tab6Root= FeuxCaveInterditComponent;

}
