import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfDihCompositionPage } from './gnrfdf-dih-composition';

@NgModule({
  declarations: [
    GnrfdfDihCompositionPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfDihCompositionPage),
  ],
})
export class GnrfdfDihCompositionPageModule {}
