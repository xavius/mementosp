import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeExplosiondefumeeScenarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-explosiondefumee-scenario',
  templateUrl: 'gnrege-explosiondefumee-scenario.html',
})
export class GnregeExplosiondefumeeScenarioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeExplosiondefumeeScenarioPage');
  }

}
