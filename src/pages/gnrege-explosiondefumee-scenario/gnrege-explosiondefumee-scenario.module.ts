import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeExplosiondefumeeScenarioPage } from './gnrege-explosiondefumee-scenario';

@NgModule({
  declarations: [
    GnregeExplosiondefumeeScenarioPage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeExplosiondefumeeScenarioPage),
  ],
})
export class GnregeExplosiondefumeeScenarioPageModule {}
