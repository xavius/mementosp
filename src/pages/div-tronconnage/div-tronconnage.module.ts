import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivTronconnagePage } from './div-tronconnage';

@NgModule({
  declarations: [
    DivTronconnagePage,
  ],
  imports: [
    IonicPageModule.forChild(DivTronconnagePage),
  ],
})
export class DivTronconnagePageModule {}
