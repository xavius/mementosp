import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivBachagePage } from './div-bachage';

@NgModule({
  declarations: [
    DivBachagePage,
  ],
  imports: [
    IonicPageModule.forChild(DivBachagePage),
  ],
})
export class DivBachagePageModule {}
