import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriAugmentationResistancePage } from './gnr-ari-augmentation-resistance';

@NgModule({
  declarations: [
    GnrAriAugmentationResistancePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriAugmentationResistancePage),
  ],
})
export class GnrAriAugmentationResistancePageModule {}
