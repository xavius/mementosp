import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionDescendeurPage } from './gnr-lspcc-composition-descendeur';

@NgModule({
  declarations: [
    GnrLspccCompositionDescendeurPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionDescendeurPage),
  ],
})
export class GnrLspccCompositionDescendeurPageModule {}
