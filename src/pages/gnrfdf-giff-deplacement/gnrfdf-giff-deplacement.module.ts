import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfGiffDeplacementPage } from './gnrfdf-giff-deplacement';

@NgModule({
  declarations: [
    GnrfdfGiffDeplacementPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfGiffDeplacementPage),
  ],
})
export class GnrfdfGiffDeplacementPageModule {}
