import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrefegePage } from './gnrefege';

@NgModule({
  declarations: [
    GnrefegePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrefegePage),
  ],
})
export class GnrefegePageModule {}
