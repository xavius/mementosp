import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementTableaudecontrolePage } from './gnr-ari-equipement-tableaudecontrole';

@NgModule({
  declarations: [
    GnrAriEquipementTableaudecontrolePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementTableaudecontrolePage),
  ],
})
export class GnrAriEquipementTableaudecontrolePageModule {}
