import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementSoupapealademandePage } from './gnr-ari-equipement-soupapealademande';

@NgModule({
  declarations: [
    GnrAriEquipementSoupapealademandePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementSoupapealademandePage),
  ],
})
export class GnrAriEquipementSoupapealademandePageModule {}
