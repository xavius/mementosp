import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionRangementPage } from './gnr-lspcc-composition-rangement';

@NgModule({
  declarations: [
    GnrLspccCompositionRangementPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionRangementPage),
  ],
})
export class GnrLspccCompositionRangementPageModule {}
