import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivOuvertureportePage } from './div-ouvertureporte';

@NgModule({
  declarations: [
    DivOuvertureportePage,
  ],
  imports: [
    IonicPageModule.forChild(DivOuvertureportePage),
  ],
})
export class DivOuvertureportePageModule {}
