import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeConduiteatenirSynoptiquePage } from './gnrege-conduiteatenir-synoptique';

@NgModule({
  declarations: [
    GnregeConduiteatenirSynoptiquePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeConduiteatenirSynoptiquePage),
  ],
})
export class GnregeConduiteatenirSynoptiquePageModule {}
