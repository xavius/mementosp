import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnregeConduiteatenirSynoptiquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnrege-conduiteatenir-synoptique',
  templateUrl: 'gnrege-conduiteatenir-synoptique.html',
})
export class GnregeConduiteatenirSynoptiquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnregeConduiteatenirSynoptiquePage');
  }

}
