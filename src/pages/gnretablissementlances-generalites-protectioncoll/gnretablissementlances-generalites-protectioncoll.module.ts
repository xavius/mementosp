import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesProtectioncollPage } from './gnretablissementlances-generalites-protectioncoll';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesProtectioncollPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesProtectioncollPage),
  ],
})
export class GnretablissementlancesGeneralitesProtectioncollPageModule {}
