import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnregeSyntheseTypologiePage } from './gnrege-synthese-typologie';

@NgModule({
  declarations: [
    GnregeSyntheseTypologiePage,
  ],
  imports: [
    IonicPageModule.forChild(GnregeSyntheseTypologiePage),
  ],
})
export class GnregeSyntheseTypologiePageModule {}
