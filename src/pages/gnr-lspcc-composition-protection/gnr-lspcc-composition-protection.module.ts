import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionProtectionPage } from './gnr-lspcc-composition-protection';

@NgModule({
  declarations: [
    GnrLspccCompositionProtectionPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionProtectionPage),
  ],
})
export class GnrLspccCompositionProtectionPageModule {}
