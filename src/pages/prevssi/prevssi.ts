import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevSsiFonctionnementntComponent } from '../../components/prev-ssi/prev-ssi-fonctionnementnt/prev-ssi-fonctionnementnt';
import { PrevSsiPrecautionComponent } from '../../components/prev-ssi/prev-ssi-precaution/prev-ssi-precaution';

/**
 * Generated class for the PrevssiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevssi',
  templateUrl: 'prevssi.html',
})
export class PrevssiPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevssiPage');
  }
  tab46Root=PrevSsiFonctionnementntComponent;
  tab47Root=PrevSsiPrecautionComponent;
}
