import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevssiPage } from './prevssi';

@NgModule({
  declarations: [
    PrevssiPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevssiPage),
  ],
})
export class PrevssiPageModule {}
