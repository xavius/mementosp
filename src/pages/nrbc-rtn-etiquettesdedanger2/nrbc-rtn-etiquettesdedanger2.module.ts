import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NrbcRtnEtiquettesdedanger2Page } from './nrbc-rtn-etiquettesdedanger2';

@NgModule({
  declarations: [
    NrbcRtnEtiquettesdedanger2Page,
  ],
  imports: [
    IonicPageModule.forChild(NrbcRtnEtiquettesdedanger2Page),
  ],
})
export class NrbcRtnEtiquettesdedanger2PageModule {}
