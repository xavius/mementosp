import { NrbcRtnEtiquettesdedanger2NouveauxPage } from './../nrbc-rtn-etiquettesdedanger2-nouveaux/nrbc-rtn-etiquettesdedanger2-nouveaux';
import { NrbcRtnEtiquettesdedanger2AnciensPage } from './../nrbc-rtn-etiquettesdedanger2-anciens/nrbc-rtn-etiquettesdedanger2-anciens';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NrbcRtnEtiquettesdedanger2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nrbc-rtn-etiquettesdedanger2',
  templateUrl: 'nrbc-rtn-etiquettesdedanger2.html',
})
export class NrbcRtnEtiquettesdedanger2Page {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Anciens Pictogrammes', component: NrbcRtnEtiquettesdedanger2AnciensPage},
      {title:'Nouveau Pictogrammes', component: NrbcRtnEtiquettesdedanger2NouveauxPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NrbcRtnEtiquettesdedanger2Page');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
