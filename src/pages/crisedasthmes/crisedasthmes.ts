import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapCrisedasthmesSignesComponent } from '../../components/sap-crisedasthmes/sap-crisedasthmes-signes/sap-crisedasthmes-signes';
import { SapCrisedasthmesBilansComponent } from '../../components/sap-crisedasthmes/sap-crisedasthmes-bilans/sap-crisedasthmes-bilans';
import { SapCrisedasthmesActionsComponent } from '../../components/sap-crisedasthmes/sap-crisedasthmes-actions/sap-crisedasthmes-actions';
import { SapCrisedasthmesImportantComponent } from '../../components/sap-crisedasthmes/sap-crisedasthmes-important/sap-crisedasthmes-important';

/**
 * Generated class for the CrisedasthmesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crisedasthmes',
  templateUrl: 'crisedasthmes.html',
})
export class CrisedasthmesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrisedasthmesPage');
  }
  tab7Root=SapCrisedasthmesSignesComponent;
  tab8Root=SapCrisedasthmesBilansComponent;
  tab9Root=SapCrisedasthmesActionsComponent;
  tab10Root=SapCrisedasthmesImportantComponent;
}
