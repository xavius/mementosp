import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrisedasthmesPage } from './crisedasthmes';

@NgModule({
  declarations: [
    CrisedasthmesPage,
  ],
  imports: [
    IonicPageModule.forChild(CrisedasthmesPage),
  ],
})
export class CrisedasthmesPageModule {}
