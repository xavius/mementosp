import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionCordelettePage } from './gnr-lspcc-composition-cordelette';

@NgModule({
  declarations: [
    GnrLspccCompositionCordelettePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionCordelettePage),
  ],
})
export class GnrLspccCompositionCordelettePageModule {}
