import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrLspccCompositionCordelettePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-composition-cordelette',
  templateUrl: 'gnr-lspcc-composition-cordelette.html',
})
export class GnrLspccCompositionCordelettePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccCompositionCordelettePage');
  }

}
