import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriEquipementCornedappelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-equipement-cornedappel',
  templateUrl: 'gnr-ari-equipement-cornedappel.html',
})
export class GnrAriEquipementCornedappelPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriEquipementCornedappelPage');
  }

}
