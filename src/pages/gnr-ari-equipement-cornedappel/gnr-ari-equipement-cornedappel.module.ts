import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementCornedappelPage } from './gnr-ari-equipement-cornedappel';

@NgModule({
  declarations: [
    GnrAriEquipementCornedappelPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementCornedappelPage),
  ],
})
export class GnrAriEquipementCornedappelPageModule {}
