import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapAspirateurdemucositesDescriptionComponent } from '../../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-description/materiels-sap-aspirateurdemucosites-description';
import { MaterielsSapAspirateurdemucositesIndicationsComponent } from '../../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-indications/materiels-sap-aspirateurdemucosites-indications';
import { MaterielsSapAspirateurdemucositesRisquesComponent } from '../../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-risques/materiels-sap-aspirateurdemucosites-risques';
import { MaterielsSapAspirateurdemucositesUtilisationComponent } from '../../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-utilisation/materiels-sap-aspirateurdemucosites-utilisation';
import { MaterielsSapAspirateurdemucositesPointsclesComponent } from '../../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-pointscles/materiels-sap-aspirateurdemucosites-pointscles';
import { MaterielsSapAspirateurdemucositesEfficaciteComponent } from '../../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-efficacite/materiels-sap-aspirateurdemucosites-efficacite';


/**
 * Generated class for the MaterielsSapAspirateurdemucositesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-aspirateurdemucosites',
  templateUrl: 'materiels-sap-aspirateurdemucosites.html',
})
export class MaterielsSapAspirateurdemucositesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapAspirateurdemucositesPage');
  }
  tab65Root=MaterielsSapAspirateurdemucositesDescriptionComponent;
  tab66Root=MaterielsSapAspirateurdemucositesIndicationsComponent;
  tab67Root=MaterielsSapAspirateurdemucositesRisquesComponent;
  tab68Root=MaterielsSapAspirateurdemucositesUtilisationComponent;
  tab69Root=MaterielsSapAspirateurdemucositesPointsclesComponent;
  tab70Root=MaterielsSapAspirateurdemucositesEfficaciteComponent;
}
