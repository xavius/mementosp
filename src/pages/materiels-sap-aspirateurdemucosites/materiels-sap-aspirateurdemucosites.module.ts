import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapAspirateurdemucositesPage } from './materiels-sap-aspirateurdemucosites';

@NgModule({
  declarations: [
    MaterielsSapAspirateurdemucositesPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapAspirateurdemucositesPage),
  ],
})
export class MaterielsSapAspirateurdemucositesPageModule {}
