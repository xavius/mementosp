import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudejointdedilatationPage } from './feudejointdedilatation';

@NgModule({
  declarations: [
    FeudejointdedilatationPage,
  ],
  imports: [
    IonicPageModule.forChild(FeudejointdedilatationPage),
  ],
})
export class FeudejointdedilatationPageModule {}
