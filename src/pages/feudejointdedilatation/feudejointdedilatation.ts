import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxJointdilatationAccesComponent } from '../../components/feux-jointdilatation/feux-jointdilatation-acces/feux-jointdilatation-acces';
import { FeuxJointdilatationActionComponent } from '../../components/feux-jointdilatation/feux-jointdilatation-action/feux-jointdilatation-action';
import { FeuxJointdilatationAttentionComponent } from '../../components/feux-jointdilatation/feux-jointdilatation-attention/feux-jointdilatation-attention';
import { FeuxJointdilatationInterditComponent } from '../../components/feux-jointdilatation/feux-jointdilatation-interdit/feux-jointdilatation-interdit';
import { FeuxJointdilatationRisquesComponent } from '../../components/feux-jointdilatation/feux-jointdilatation-risques/feux-jointdilatation-risques';
import { FeuxJointdilatationStrategieComponent } from '../../components/feux-jointdilatation/feux-jointdilatation-strategie/feux-jointdilatation-strategie';

/**
 * Generated class for the FeudejointdedilatationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudejointdedilatation',
  templateUrl: 'feudejointdedilatation.html',
})
export class FeudejointdedilatationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudejointdedilatationPage');
  }
  tab1Root=FeuxJointdilatationRisquesComponent;
  tab2Root=FeuxJointdilatationStrategieComponent;
  tab3Root=FeuxJointdilatationAccesComponent;
  tab4Root=FeuxJointdilatationActionComponent;
  tab5Root=FeuxJointdilatationAttentionComponent;
  tab6Root=FeuxJointdilatationInterditComponent;
}
