import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevssiapPage } from './prevssiap';

@NgModule({
  declarations: [
    PrevssiapPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevssiapPage),
  ],
})
export class PrevssiapPageModule {}
