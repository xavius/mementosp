import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevSsiapFonctionnementComponent } from '../../components/prev-ssiap/prev-ssiap-fonctionnement/prev-ssiap-fonctionnement';
import { PrevSsiapPrecautionComponent } from '../../components/prev-ssiap/prev-ssiap-precaution/prev-ssiap-precaution';

/**
 * Generated class for the PrevssiapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevssiap',
  templateUrl: 'prevssiap.html',
})
export class PrevssiapPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevssiapPage');
  }
  tab46Root=PrevSsiapFonctionnementComponent;
  tab47Root=PrevSsiapPrecautionComponent;
}
