import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauChefdagresComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdagres/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauChefdequipeComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauEquipierComponent } from '../../components/gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau-equipier/gnretablissementlances-tuyauxechevaux-ldvprisedeau-equipier';

/**
 * Generated class for the GnretablissementlancesTuyauxechevauxLdvprisedeauPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-tuyauxechevaux-ldvprisedeau',
  templateUrl: 'gnretablissementlances-tuyauxechevaux-ldvprisedeau.html',
})
export class GnretablissementlancesTuyauxechevauxLdvprisedeauPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesTuyauxechevauxLdvprisedeauPage');
  }
  tab61Root=GnretablissementlancesTuyauxechevauxLdvprisedeauChefdagresComponent;
  tab62Root=GnretablissementlancesTuyauxechevauxLdvprisedeauChefdequipeComponent;
  tab63Root=GnretablissementlancesTuyauxechevauxLdvprisedeauEquipierComponent;
}
