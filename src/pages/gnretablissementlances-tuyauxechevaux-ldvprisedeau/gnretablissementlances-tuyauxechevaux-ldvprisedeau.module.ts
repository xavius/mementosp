import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauPage } from './gnretablissementlances-tuyauxechevaux-ldvprisedeau';

@NgModule({
  declarations: [
    GnretablissementlancesTuyauxechevauxLdvprisedeauPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesTuyauxechevauxLdvprisedeauPage),
  ],
})
export class GnretablissementlancesTuyauxechevauxLdvprisedeauPageModule {}
