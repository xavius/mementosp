import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationavantChefdagresPage } from './gnr-ari-operationavant-chefdagres';

@NgModule({
  declarations: [
    GnrAriOperationavantChefdagresPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationavantChefdagresPage),
  ],
})
export class GnrAriOperationavantChefdagresPageModule {}
