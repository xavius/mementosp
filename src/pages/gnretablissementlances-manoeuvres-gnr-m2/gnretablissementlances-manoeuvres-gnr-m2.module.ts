import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM2Page } from './gnretablissementlances-manoeuvres-gnr-m2';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrM2Page,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrM2Page),
  ],
})
export class GnretablissementlancesManoeuvresGnrM2PageModule {}
