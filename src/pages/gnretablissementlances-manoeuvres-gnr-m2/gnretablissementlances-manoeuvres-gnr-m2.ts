import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM2ChefdagresComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2-chefdagres/gnretablissementlances-manoeuvres-gnr-m2-chefdagres';
import { GnretablissementlancesManoeuvresGnrM2ChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2-chefdequipe/gnretablissementlances-manoeuvres-gnr-m2-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM2EquipierComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2-equipier/gnretablissementlances-manoeuvres-gnr-m2-equipier';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrM2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-m2',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-m2.html',
})
export class GnretablissementlancesManoeuvresGnrM2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrM2Page');
  }
  tab61Root=GnretablissementlancesManoeuvresGnrM2ChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvresGnrM2ChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvresGnrM2EquipierComponent;
}
