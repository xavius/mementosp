import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapBrancardcuillerePage } from './materiels-sap-brancardcuillere';

@NgModule({
  declarations: [
    MaterielsSapBrancardcuillerePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapBrancardcuillerePage),
  ],
})
export class MaterielsSapBrancardcuillerePageModule {}
