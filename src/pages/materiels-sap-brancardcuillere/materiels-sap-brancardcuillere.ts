import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapBrancardcuillereDescriptionComponent } from '../../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-description/materiels-sap-brancardcuillere-description';
import { MaterielsSapBrancardcuillereIndicationsComponent } from '../../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-indications/materiels-sap-brancardcuillere-indications';
import { MaterielsSapBrancardcuillereRisquesComponent } from '../../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-risques/materiels-sap-brancardcuillere-risques';
import { MaterielsSapBrancardcuillereUtilisationComponent } from '../../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-utilisation/materiels-sap-brancardcuillere-utilisation';
import { MaterielsSapBrancardcuillerePointsclesComponent } from '../../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-pointscles/materiels-sap-brancardcuillere-pointscles';
import { MaterielsSapBrancardcuillereEfficaciteComponent } from '../../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-efficacite/materiels-sap-brancardcuillere-efficacite';


/**
 * Generated class for the MaterielsSapBrancardcuillerePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-brancardcuillere',
  templateUrl: 'materiels-sap-brancardcuillere.html',
})
export class MaterielsSapBrancardcuillerePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapBrancardcuillerePage');
  }
  tab65Root=MaterielsSapBrancardcuillereDescriptionComponent;
  tab66Root=MaterielsSapBrancardcuillereIndicationsComponent;
  tab67Root=MaterielsSapBrancardcuillereRisquesComponent;
  tab68Root=MaterielsSapBrancardcuillereUtilisationComponent;
  tab69Root=MaterielsSapBrancardcuillerePointsclesComponent;
  tab70Root=MaterielsSapBrancardcuillereEfficaciteComponent;
}
