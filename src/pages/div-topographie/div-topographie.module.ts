import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivTopographiePage } from './div-topographie';

@NgModule({
  declarations: [
    DivTopographiePage,
  ],
  imports: [
    IonicPageModule.forChild(DivTopographiePage),
  ],
})
export class DivTopographiePageModule {}
