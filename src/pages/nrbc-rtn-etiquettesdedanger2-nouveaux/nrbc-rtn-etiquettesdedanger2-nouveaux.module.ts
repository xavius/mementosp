import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NrbcRtnEtiquettesdedanger2NouveauxPage } from './nrbc-rtn-etiquettesdedanger2-nouveaux';

@NgModule({
  declarations: [
    NrbcRtnEtiquettesdedanger2NouveauxPage,
  ],
  imports: [
    IonicPageModule.forChild(NrbcRtnEtiquettesdedanger2NouveauxPage),
  ],
})
export class NrbcRtnEtiquettesdedanger2NouveauxPageModule {}
