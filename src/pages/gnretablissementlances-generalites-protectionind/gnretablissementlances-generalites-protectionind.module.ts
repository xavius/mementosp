import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesProtectionindPage } from './gnretablissementlances-generalites-protectionind';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesProtectionindPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesProtectionindPage),
  ],
})
export class GnretablissementlancesGeneralitesProtectionindPageModule {}
