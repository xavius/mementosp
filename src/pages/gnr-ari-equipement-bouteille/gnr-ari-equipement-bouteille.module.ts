import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementBouteillePage } from './gnr-ari-equipement-bouteille';

@NgModule({
  declarations: [
    GnrAriEquipementBouteillePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementBouteillePage),
  ],
})
export class GnrAriEquipementBouteillePageModule {}
