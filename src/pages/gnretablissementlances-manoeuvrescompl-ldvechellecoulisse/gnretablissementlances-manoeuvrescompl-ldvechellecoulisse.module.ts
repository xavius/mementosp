import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulissePage } from './gnretablissementlances-manoeuvrescompl-ldvechellecoulisse';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvrescomplLdvechellecoulissePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvrescomplLdvechellecoulissePage),
  ],
})
export class GnretablissementlancesManoeuvrescomplLdvechellecoulissePageModule {}
