import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdagresComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdagres/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdequipe/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdequipe';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseEquipierComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-equipier/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-equipier';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseBalComponent } from '../../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-bal/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-bal';

/**
 * Generated class for the GnretablissementlancesManoeuvrescomplLdvechellecoulissePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvrescompl-ldvechellecoulisse',
  templateUrl: 'gnretablissementlances-manoeuvrescompl-ldvechellecoulisse.html',
})
export class GnretablissementlancesManoeuvrescomplLdvechellecoulissePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvrescomplLdvechellecoulissePage');
  }
  tab61Root=GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvrescomplLdvechellecoulisseEquipierComponent;
  tab64Root=GnretablissementlancesManoeuvrescomplLdvechellecoulisseBalComponent;  
}
