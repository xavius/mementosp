import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfDihLimitesPage } from './gnrfdf-dih-limites';

@NgModule({
  declarations: [
    GnrfdfDihLimitesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfDihLimitesPage),
  ],
})
export class GnrfdfDihLimitesPageModule {}
