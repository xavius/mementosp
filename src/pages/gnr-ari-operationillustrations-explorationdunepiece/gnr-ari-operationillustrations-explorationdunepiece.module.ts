import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationillustrationsExplorationdunepiecePage } from './gnr-ari-operationillustrations-explorationdunepiece';

@NgModule({
  declarations: [
    GnrAriOperationillustrationsExplorationdunepiecePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationillustrationsExplorationdunepiecePage),
  ],
})
export class GnrAriOperationillustrationsExplorationdunepiecePageModule {}
