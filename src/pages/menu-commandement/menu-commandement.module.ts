import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuCommandementPage } from './menu-commandement';

@NgModule({
  declarations: [
    MenuCommandementPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuCommandementPage),
  ],
})
export class MenuCommandementPageModule {}
