import { FonctiondupcPage } from './../fonctiondupc/fonctiondupc';
import { MessageradioPage } from './../messageradio/messageradio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CadredordrePage } from '../cadredordre/cadredordre';
import { OutilsgraphiquesPage } from '../outilsgraphiques/outilsgraphiques';

/**
 * Generated class for the MenuCommandementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-commandement',
  templateUrl: 'menu-commandement.html',
})
export class MenuCommandementPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'CADRE D\'ORDRE', component: CadredordrePage},
      {title:'MESSAGE RADIO', component: MessageradioPage},
      {title:'FONCTION DU PC', component: FonctiondupcPage},
      {title:'OUTILS GRAPHIQUES (SITAC)', component: OutilsgraphiquesPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuCommandementPage');
  }

  itemSelected(item){
    this.navCtrl.push(item.component);
  }

}
