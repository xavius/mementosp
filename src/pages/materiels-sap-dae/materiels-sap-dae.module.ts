import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapDaePage } from './materiels-sap-dae';

@NgModule({
  declarations: [
    MaterielsSapDaePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapDaePage),
  ],
})
export class MaterielsSapDaePageModule {}
