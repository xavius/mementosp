import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MaterielsSapDaeDescriptionComponent } from '../../components/materiels-sap-dae/materiels-sap-dae-description/materiels-sap-dae-description';
import { MaterielsSapDaeIndicationsComponent } from '../../components/materiels-sap-dae/materiels-sap-dae-indications/materiels-sap-dae-indications';
import { MaterielsSapDaeRisquesComponent } from '../../components/materiels-sap-dae/materiels-sap-dae-risques/materiels-sap-dae-risques';
import { MaterielsSapDaeUtilisationComponent } from '../../components/materiels-sap-dae/materiels-sap-dae-utilisation/materiels-sap-dae-utilisation';
import { MaterielsSapDaePointsclesComponent } from '../../components/materiels-sap-dae/materiels-sap-dae-pointscles/materiels-sap-dae-pointscles';
import { MaterielsSapDaeEfficaciteComponent } from '../../components/materiels-sap-dae/materiels-sap-dae-efficacite/materiels-sap-dae-efficacite';


/**
 * Generated class for the MaterielsSapDaePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap-dae',
  templateUrl: 'materiels-sap-dae.html',
})
export class MaterielsSapDaePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapDaePage');
  }
  tab65Root=MaterielsSapDaeDescriptionComponent;
  tab66Root=MaterielsSapDaeIndicationsComponent;
  tab67Root=MaterielsSapDaeRisquesComponent;
  tab68Root=MaterielsSapDaeUtilisationComponent;
  tab69Root=MaterielsSapDaePointsclesComponent;
  tab70Root=MaterielsSapDaeEfficaciteComponent;
}
