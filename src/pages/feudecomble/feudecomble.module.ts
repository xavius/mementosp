import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeudecomblePage } from './feudecomble';

@NgModule({
  declarations: [
    FeudecomblePage,
  ],
  imports: [
    IonicPageModule.forChild(FeudecomblePage),
  ],
})
export class FeudecomblePageModule {}
