import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeuxCombleRisquesComponent } from '../../components/feux-comble/feux-comble-risques/feux-comble-risques';
import { FeuxCombleStrategieComponent } from '../../components/feux-comble/feux-comble-strategie/feux-comble-strategie';
import { FeuxCombleAccesComponent } from '../../components/feux-comble/feux-comble-acces/feux-comble-acces';
import { FeuxCombleActionComponent } from '../../components/feux-comble/feux-comble-action/feux-comble-action';
import { FeuxCombleAttentionComponent } from '../../components/feux-comble/feux-comble-attention/feux-comble-attention';
import { FeuxCombleInterditComponent } from '../../components/feux-comble/feux-comble-interdit/feux-comble-interdit';

/**
 * Generated class for the FeudecomblePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feudecomble',
  templateUrl: 'feudecomble.html',
})
export class FeudecomblePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeudecomblePage');
  }
  tab1Root=FeuxCombleRisquesComponent;
  tab2Root=FeuxCombleStrategieComponent;
  tab3Root=FeuxCombleAccesComponent;
  tab4Root=FeuxCombleActionComponent;
  tab5Root=FeuxCombleAttentionComponent;
  tab6Root=FeuxCombleInterditComponent;

}
