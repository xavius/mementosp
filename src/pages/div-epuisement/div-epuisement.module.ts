import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DivEpuisementPage } from './div-epuisement';

@NgModule({
  declarations: [
    DivEpuisementPage,
  ],
  imports: [
    IonicPageModule.forChild(DivEpuisementPage),
  ],
})
export class DivEpuisementPageModule {}
