import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM6Page } from './gnretablissementlances-manoeuvres-gnr-m6';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrM6Page,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrM6Page),
  ],
})
export class GnretablissementlancesManoeuvresGnrM6PageModule {}
