import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM6ChefdagresComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6-chefdagres/gnretablissementlances-manoeuvres-gnr-m6-chefdagres';
import { GnretablissementlancesManoeuvresGnrM6ChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6-chefdequipe/gnretablissementlances-manoeuvres-gnr-m6-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM6EquipierComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6-equipier/gnretablissementlances-manoeuvres-gnr-m6-equipier';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrM6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-m6',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-m6.html',
})
export class GnretablissementlancesManoeuvresGnrM6Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrM6Page');
  }
  tab61Root=GnretablissementlancesManoeuvresGnrM6ChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvresGnrM6ChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvresGnrM6EquipierComponent;
}
