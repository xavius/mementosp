import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SanglearaigneePage } from './sanglearaignee';

@NgModule({
  declarations: [
    SanglearaigneePage,
  ],
  imports: [
    IonicPageModule.forChild(SanglearaigneePage),
  ],
})
export class SanglearaigneePageModule {}
