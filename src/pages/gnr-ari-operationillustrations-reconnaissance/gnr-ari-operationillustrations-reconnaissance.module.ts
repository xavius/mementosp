import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriOperationillustrationsReconnaissancePage } from './gnr-ari-operationillustrations-reconnaissance';

@NgModule({
  declarations: [
    GnrAriOperationillustrationsReconnaissancePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriOperationillustrationsReconnaissancePage),
  ],
})
export class GnrAriOperationillustrationsReconnaissancePageModule {}
