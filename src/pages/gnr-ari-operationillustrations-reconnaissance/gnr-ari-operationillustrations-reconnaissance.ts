import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriOperationillustrationsReconnaissancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-operationillustrations-reconnaissance',
  templateUrl: 'gnr-ari-operationillustrations-reconnaissance.html',
})
export class GnrAriOperationillustrationsReconnaissancePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriOperationillustrationsReconnaissancePage');
  }

}
