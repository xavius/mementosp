import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GdoPreventionrisquesdetoxicitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gdo-preventionrisquesdetoxicite',
  templateUrl: 'gdo-preventionrisquesdetoxicite.html',
})
export class GdoPreventionrisquesdetoxicitePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GdoPreventionrisquesdetoxicitePage');
  }

}
