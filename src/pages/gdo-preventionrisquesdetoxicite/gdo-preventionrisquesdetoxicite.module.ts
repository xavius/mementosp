import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GdoPreventionrisquesdetoxicitePage } from './gdo-preventionrisquesdetoxicite';

@NgModule({
  declarations: [
    GdoPreventionrisquesdetoxicitePage,
  ],
  imports: [
    IonicPageModule.forChild(GdoPreventionrisquesdetoxicitePage),
  ],
})
export class GdoPreventionrisquesdetoxicitePageModule {}
