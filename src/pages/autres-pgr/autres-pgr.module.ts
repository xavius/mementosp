import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutresPgrPage } from './autres-pgr';

@NgModule({
  declarations: [
    AutresPgrPage,
  ],
  imports: [
    IonicPageModule.forChild(AutresPgrPage),
  ],
})
export class AutresPgrPageModule {}
