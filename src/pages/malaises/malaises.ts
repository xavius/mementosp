import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapMalaisesSignesComponent } from '../../components/sap-malaises/sap-malaises-signes/sap-malaises-signes';
import { SapMalaisesBilansComponent } from '../../components/sap-malaises/sap-malaises-bilans/sap-malaises-bilans';
import { SapMalaisesActionsComponent } from '../../components/sap-malaises/sap-malaises-actions/sap-malaises-actions';
import { SapMalaisesImportantComponent } from '../../components/sap-malaises/sap-malaises-important/sap-malaises-important';

/**
 * Generated class for the MalaisesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-malaises',
  templateUrl: 'malaises.html',
})
export class MalaisesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MalaisesPage');
  }
  tab7Root=SapMalaisesSignesComponent;
  tab8Root=SapMalaisesBilansComponent;
  tab9Root=SapMalaisesActionsComponent;
  tab10Root=SapMalaisesImportantComponent;
}
