import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MalaisesPage } from './malaises';

@NgModule({
  declarations: [
    MalaisesPage,
  ],
  imports: [
    IonicPageModule.forChild(MalaisesPage),
  ],
})
export class MalaisesPageModule {}
