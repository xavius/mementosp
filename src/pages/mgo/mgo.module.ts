import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MgoPage } from './mgo';

@NgModule({
  declarations: [
    MgoPage,
  ],
  imports: [
    IonicPageModule.forChild(MgoPage),
  ],
})
export class MgoPageModule {}
