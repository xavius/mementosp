import { GnrAriAugmentationEspacemortPage } from './../gnr-ari-augmentation-espacemort/gnr-ari-augmentation-espacemort';
import { GnrAriAugmentationThermoregulationPage } from './../gnr-ari-augmentation-thermoregulation/gnr-ari-augmentation-thermoregulation';
import { GnrAriAugmentationPoidsPage } from './../gnr-ari-augmentation-poids/gnr-ari-augmentation-poids';
import { GnrAriAugmentationStressPage } from './../gnr-ari-augmentation-stress/gnr-ari-augmentation-stress';
import { GnrAriAugmentationResistancePage } from './../gnr-ari-augmentation-resistance/gnr-ari-augmentation-resistance';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrAriAugmentationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-ari-augmentation',
  templateUrl: 'gnr-ari-augmentation.html',
})
export class GnrAriAugmentationPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Résistance Inspiratoires et Expiratoire', component: GnrAriAugmentationResistancePage},
      {title:'Stress Emotif', component: GnrAriAugmentationStressPage},
      {title:'Poids de l Appareil', component: GnrAriAugmentationPoidsPage},
      {title:'Thermorégulation', component: GnrAriAugmentationThermoregulationPage},
      {title:'Augmentation de l Espace Mort', component: GnrAriAugmentationEspacemortPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrAriAugmentationPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
