import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriAugmentationPage } from './gnr-ari-augmentation';

@NgModule({
  declarations: [
    GnrAriAugmentationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriAugmentationPage),
  ],
})
export class GnrAriAugmentationPageModule {}
