import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionEntretienPage } from './gnr-lspcc-composition-entretien';

@NgModule({
  declarations: [
    GnrLspccCompositionEntretienPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionEntretienPage),
  ],
})
export class GnrLspccCompositionEntretienPageModule {}
