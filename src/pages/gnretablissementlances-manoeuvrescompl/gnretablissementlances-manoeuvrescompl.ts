import { GnretablissementlancesManoeuvrescomplLdvenginPage } from './../gnretablissementlances-manoeuvrescompl-ldvengin/gnretablissementlances-manoeuvrescompl-ldvengin';
import { GnretablissementlancesManoeuvrescomplLdvpoteauPage } from './../gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau';
import { GnretablissementlancesManoeuvrescomplLdvcolonnePage } from './../gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulissePage } from './../gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse';
import { GnretablissementlancesManoeuvrescomplLdvexterieurPage } from './../gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesManoeuvrescomplPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvrescompl',
  templateUrl: 'gnretablissementlances-manoeuvrescompl.html',
})
export class GnretablissementlancesManoeuvrescomplPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'LDV par l Extérieur' , component: GnretablissementlancesManoeuvrescomplLdvexterieurPage},
      {title:'LDV par l Echelle à Coulisse' , component: GnretablissementlancesManoeuvrescomplLdvechellecoulissePage},
      {title:'LDV sur Colonne Sèche/Humide' , component: GnretablissementlancesManoeuvrescomplLdvcolonnePage},
      {title:'LDV sur Poteau Relais' , component: GnretablissementlancesManoeuvrescomplLdvpoteauPage},
      {title:'LDV avec Engin en Aspiration', component: GnretablissementlancesManoeuvrescomplLdvenginPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvrescomplPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
}
}
