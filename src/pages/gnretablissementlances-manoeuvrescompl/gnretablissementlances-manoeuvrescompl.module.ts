import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvrescomplPage } from './gnretablissementlances-manoeuvrescompl';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvrescomplPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvrescomplPage),
  ],
})
export class GnretablissementlancesManoeuvrescomplPageModule {}
