import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevDegagementsaccesFonctionnementComponent } from '../../components/prev-degagementsacces/prev-degagementsacces-fonctionnement/prev-degagementsacces-fonctionnement';
import { PrevDegagementsaccesPrecautionComponent } from '../../components/prev-degagementsacces/prev-degagementsacces-precaution/prev-degagementsacces-precaution';

/**
 * Generated class for the PrevdegagementsaccesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevdegagementsacces',
  templateUrl: 'prevdegagementsacces.html',
})
export class PrevdegagementsaccesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevdegagementsaccesPage');
  }
  tab46Root=PrevDegagementsaccesFonctionnementComponent;
  tab47Root=PrevDegagementsaccesPrecautionComponent;
}
