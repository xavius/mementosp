import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevdegagementsaccesPage } from './prevdegagementsacces';

@NgModule({
  declarations: [
    PrevdegagementsaccesPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevdegagementsaccesPage),
  ],
})
export class PrevdegagementsaccesPageModule {}
