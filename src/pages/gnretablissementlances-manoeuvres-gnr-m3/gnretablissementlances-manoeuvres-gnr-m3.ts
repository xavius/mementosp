import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM3ChefdagresComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3-chefdagres/gnretablissementlances-manoeuvres-gnr-m3-chefdagres';
import { GnretablissementlancesManoeuvresGnrM3ChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3-chefdequipe/gnretablissementlances-manoeuvres-gnr-m3-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM3EquipierComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3-equipier/gnretablissementlances-manoeuvres-gnr-m3-equipier';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrM3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-m3',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-m3.html',
})
export class GnretablissementlancesManoeuvresGnrM3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrM3Page');
  }
  tab61Root=GnretablissementlancesManoeuvresGnrM3ChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvresGnrM3ChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvresGnrM3EquipierComponent;
}
