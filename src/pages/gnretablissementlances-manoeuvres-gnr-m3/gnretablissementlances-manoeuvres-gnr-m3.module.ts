import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM3Page } from './gnretablissementlances-manoeuvres-gnr-m3';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrM3Page,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrM3Page),
  ],
})
export class GnretablissementlancesManoeuvresGnrM3PageModule {}
