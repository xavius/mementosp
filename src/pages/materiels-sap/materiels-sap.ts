import { MaterielsSapThermometrePage } from './../materiels-sap-thermometre/materiels-sap-thermometre';
import { MaterielsSapOxymetredepoulsPage } from './../materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls';
import { MaterielsSapMesurepressionarteriellePage } from './../materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle';
import { MaterielsSapLectureglycemiePage } from './../materiels-sap-lectureglycemie/materiels-sap-lectureglycemie';
import { MaterielsSapKitsinusPage } from './../materiels-sap-kitsinus/materiels-sap-kitsinus';
import { MaterielsSapKitsectiondemembresPage } from './../materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres';
import { MaterielsSapKitbruluresPage } from './../materiels-sap-kitbrulures/materiels-sap-kitbrulures';
import { MaterielsSapKitaesPage } from './../materiels-sap-kitaes/materiels-sap-kitaes';
import { MaterielsSapGarrotarterielPage } from './../materiels-sap-garrotarteriel/materiels-sap-garrotarteriel';
import { MaterielsSapDaePage } from './../materiels-sap-dae/materiels-sap-dae';
import { MaterielsSapBrancardcuillerePage } from './../materiels-sap-brancardcuillere/materiels-sap-brancardcuillere';
import { MaterielsSapAttellecervicothoraciquePage } from '../materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique';
import { MaterielsSapAspirateurdemucositesPage } from './../materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites';
import { MaterielsSapAttelledetractionPage } from './../materiels-sap-attelledetraction/materiels-sap-attelledetraction';
import { MaterielsSapSanglearaigneePage } from './../materiels-sap-sanglearaignee/materiels-sap-sanglearaignee';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MaterielsSapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-materiels-sap',
  templateUrl: 'materiels-sap.html',
})
export class MaterielsSapPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'Aspirateur de Mucosités', component: MaterielsSapAspirateurdemucositesPage},
      {title:'Attelle Cervico-Thoracique', component: MaterielsSapAttellecervicothoraciquePage},
      {title:'Attelle Pneumatique de Traction (DONWAY)', component: MaterielsSapAttelledetractionPage},
      {title:'Brancard Cuillère', component: MaterielsSapBrancardcuillerePage},
      {title:'DSA', component: MaterielsSapDaePage},
      {title:'Garrot', component: MaterielsSapGarrotarterielPage},
      {title:'Kit AES', component: MaterielsSapKitaesPage},
      {title:'Kit Brûlures', component: MaterielsSapKitbruluresPage},
      {title:'Kit Section de Membres', component: MaterielsSapKitsectiondemembresPage},
      {title:'Kit SINUS', component: MaterielsSapKitsinusPage},
      {title:'Lecture de Glycémie', component: MaterielsSapLectureglycemiePage},
      {title:'Mesure Pression Artériel', component: MaterielsSapMesurepressionarteriellePage},
      {title:'Oxymétrie de Pouls', component: MaterielsSapOxymetredepoulsPage},
      {title:'Sangle Araignée', component: MaterielsSapSanglearaigneePage},
      {title:'Thermométre', component: MaterielsSapThermometrePage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterielsSapPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
