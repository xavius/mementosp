import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterielsSapPage } from './materiels-sap';

@NgModule({
  declarations: [
    MaterielsSapPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterielsSapPage),
  ],
})
export class MaterielsSapPageModule {}
