import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NrbcRtnCodedangerPage } from './nrbc-rtn-codedanger';

@NgModule({
  declarations: [
    NrbcRtnCodedangerPage,
  ],
  imports: [
    IonicPageModule.forChild(NrbcRtnCodedangerPage),
  ],
})
export class NrbcRtnCodedangerPageModule {}
