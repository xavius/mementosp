import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfDihMissionsPage } from './gnrfdf-dih-missions';

@NgModule({
  declarations: [
    GnrfdfDihMissionsPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfDihMissionsPage),
  ],
})
export class GnrfdfDihMissionsPageModule {}
