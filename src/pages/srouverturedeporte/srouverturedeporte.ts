import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrOuverturedeporte_1Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-1/sr-ouverturedeporte-1';
import { SrOuverturedeporte_2Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-2/sr-ouverturedeporte-2';
import { SrOuverturedeporte_3Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-3/sr-ouverturedeporte-3';
import { SrOuverturedeporte_4Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-4/sr-ouverturedeporte-4';
import { SrOuverturedeporte_5Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-5/sr-ouverturedeporte-5';
import { SrOuverturedeporte_6Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-6/sr-ouverturedeporte-6';
import { SrOuverturedeporte_7Component } from '../../components/sr-ouverturedeporte/sr-ouverturedeporte-7/sr-ouverturedeporte-7';

/**
 * Generated class for the SrouverturedeportePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srouverturedeporte',
  templateUrl: 'srouverturedeporte.html',
})
export class SrouverturedeportePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrouverturedeportePage');
  }
  tab22Root=SrOuverturedeporte_1Component;
  tab23Root=SrOuverturedeporte_2Component;
  tab24Root=SrOuverturedeporte_3Component;
  tab25Root=SrOuverturedeporte_4Component;
  tab26Root=SrOuverturedeporte_5Component;
  tab27Root=SrOuverturedeporte_6Component;
  tab28Root=SrOuverturedeporte_7Component;
}
