import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrouverturedeportePage } from './srouverturedeporte';

@NgModule({
  declarations: [
    SrouverturedeportePage,
  ],
  imports: [
    IonicPageModule.forChild(SrouverturedeportePage),
  ],
})
export class SrouverturedeportePageModule {}
