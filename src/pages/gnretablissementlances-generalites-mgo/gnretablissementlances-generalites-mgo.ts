import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesGeneralitesMgoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-generalites-mgo',
  templateUrl: 'gnretablissementlances-generalites-mgo.html',
})
export class GnretablissementlancesGeneralitesMgoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesGeneralitesMgoPage');
  }

}
