import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesGeneralitesMgoPage } from './gnretablissementlances-generalites-mgo';

@NgModule({
  declarations: [
    GnretablissementlancesGeneralitesMgoPage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesGeneralitesMgoPage),
  ],
})
export class GnretablissementlancesGeneralitesMgoPageModule {}
