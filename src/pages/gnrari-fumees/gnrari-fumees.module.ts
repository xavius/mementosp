import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrariFumeesPage } from './gnrari-fumees';

@NgModule({
  declarations: [
    GnrariFumeesPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrariFumeesPage),
  ],
})
export class GnrariFumeesPageModule {}
