import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrfdfDihPrincipePage } from './gnrfdf-dih-principe';

@NgModule({
  declarations: [
    GnrfdfDihPrincipePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrfdfDihPrincipePage),
  ],
})
export class GnrfdfDihPrincipePageModule {}
