import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccReconnaissancePage } from './gnr-lspcc-reconnaissance';

@NgModule({
  declarations: [
    GnrLspccReconnaissancePage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccReconnaissancePage),
  ],
})
export class GnrLspccReconnaissancePageModule {}
