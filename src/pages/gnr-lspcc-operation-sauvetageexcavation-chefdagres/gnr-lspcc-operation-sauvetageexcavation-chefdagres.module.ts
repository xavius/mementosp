import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccOperationSauvetageexcavationChefdagresPage } from './gnr-lspcc-operation-sauvetageexcavation-chefdagres';

@NgModule({
  declarations: [
    GnrLspccOperationSauvetageexcavationChefdagresPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccOperationSauvetageexcavationChefdagresPage),
  ],
})
export class GnrLspccOperationSauvetageexcavationChefdagresPageModule {}
