import { SrdemipavillonarrierePage } from './../srdemipavillonarriere/srdemipavillonarriere';
import { SrrelevagetableaudebordPage } from './../srrelevagetableaudebord/srrelevagetableaudebord';
import { SrportefeuillePage } from './../srportefeuille/srportefeuille';
import { SrcoquilledhuitrePage } from './../srcoquilledhuitre/srcoquilledhuitre';
import { SrcharnierePage } from './../srcharniere/srcharniere';
import { SrdemipavillonavantPage } from './../srdemipavillonavant/srdemipavillonavant';
import { SrdepavillonnagePage } from './../srdepavillonnage/srdepavillonnage';
import { SrouverturelateralePage } from './../srouverturelaterale/srouverturelaterale';
import { SrouverturedeportePage } from './../srouverturedeporte/srouverturedeporte';
import { SrcalagePage } from './../srcalage/srcalage';
import { SrchartegraphiquePage } from './../srchartegraphique/srchartegraphique';
import { SrprincipesPage } from './../srprincipes/srprincipes';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FichessrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fichessr',
  templateUrl: 'fichessr.html',
})
export class FichessrPage {

  list: Array<{title: string, component: any}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = [
      {title:'PRINCIPES', component: SrprincipesPage},
      {title:'CHARTE GRAPHIQUE', component: SrchartegraphiquePage},
      {title:'CALAGE', component: SrcalagePage},
      {title:'OUVERTURE DE PORTE', component: SrouverturedeportePage},
      {title:'OUVERTURE LATERALE', component: SrouverturelateralePage},
      {title:'DEPAVILLONNAGE', component: SrdepavillonnagePage},
      {title:'DEMI-PAVILLON AVANT', component: SrdemipavillonavantPage},
      {title:'DEMI-PAVILLON ARRIERE', component: SrdemipavillonarrierePage},
      {title:'CHARNIERE', component: SrcharnierePage},
      {title:'COQUILLE D HUITRE', component: SrcoquilledhuitrePage},
      {title:'PORTE-FEUILLE', component: SrportefeuillePage},
      {title:'RELEVAGE TABLEAU DE BORD', component: SrrelevagetableaudebordPage}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichessrPage');
  }
  itemSelected(item){
    this.navCtrl.push(item.component);
  }
}
