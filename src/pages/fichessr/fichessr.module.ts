import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichessrPage } from './fichessr';

@NgModule({
  declarations: [
    FichessrPage,
  ],
  imports: [
    IonicPageModule.forChild(FichessrPage),
  ],
})
export class FichessrPageModule {}
