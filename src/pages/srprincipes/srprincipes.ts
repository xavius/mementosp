import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrPrincipesAccidentComponent } from '../../components/sr-principes/sr-principes-accident/sr-principes-accident';
import { SrPrincipesRecoComponent } from '../../components/sr-principes/sr-principes-reco/sr-principes-reco';
import { SrPrincipesIncComponent } from '../../components/sr-principes/sr-principes-inc/sr-principes-inc';
import { SrPrincipesDecoupeComponent } from '../../components/sr-principes/sr-principes-decoupe/sr-principes-decoupe';
import { SrPrincipesHybridesComponent } from '../../components/sr-principes/sr-principes-hybrides/sr-principes-hybrides';

/**
 * Generated class for the SrprincipesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srprincipes',
  templateUrl: 'srprincipes.html',
})
export class SrprincipesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrprincipesPage');
  }
  tab17Root=SrPrincipesAccidentComponent;
  tab18Root=SrPrincipesRecoComponent;
  tab19Root=SrPrincipesIncComponent;
  tab20Root=SrPrincipesDecoupeComponent;
  tab21Root=SrPrincipesHybridesComponent;
}
