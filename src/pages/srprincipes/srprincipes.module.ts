import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrprincipesPage } from './srprincipes';

@NgModule({
  declarations: [
    SrprincipesPage,
  ],
  imports: [
    IonicPageModule.forChild(SrprincipesPage),
  ],
})
export class SrprincipesPageModule {}
