import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GocOutilsgraphiquesCharteComponent } from '../../components/goc-outilsgraphiques/goc-outilsgraphiques-charte/goc-outilsgraphiques-charte';
import { GocOutilsgraphiquesMoyensComponent } from '../../components/goc-outilsgraphiques/goc-outilsgraphiques-moyens/goc-outilsgraphiques-moyens';
import { GocOutilsgraphiquesActionsComponent } from '../../components/goc-outilsgraphiques/goc-outilsgraphiques-actions/goc-outilsgraphiques-actions';
import { GocOutilsgraphiquesSituationComponent } from '../../components/goc-outilsgraphiques/goc-outilsgraphiques-situation/goc-outilsgraphiques-situation';

/**
 * Generated class for the OutilsgraphiquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-outilsgraphiques',
  templateUrl: 'outilsgraphiques.html',
})
export class OutilsgraphiquesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OutilsgraphiquesPage');
  }
  tab42Root=GocOutilsgraphiquesSituationComponent;
  tab43Root=GocOutilsgraphiquesActionsComponent;
  tab44Root=GocOutilsgraphiquesMoyensComponent;
  tab45Root=GocOutilsgraphiquesCharteComponent;
}
