import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutilsgraphiquesPage } from './outilsgraphiques';

@NgModule({
  declarations: [
    OutilsgraphiquesPage,
  ],
  imports: [
    IonicPageModule.forChild(OutilsgraphiquesPage),
  ],
})
export class OutilsgraphiquesPageModule {}
