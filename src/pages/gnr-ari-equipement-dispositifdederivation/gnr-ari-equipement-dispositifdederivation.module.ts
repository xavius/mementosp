import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrAriEquipementDispositifdederivationPage } from './gnr-ari-equipement-dispositifdederivation';

@NgModule({
  declarations: [
    GnrAriEquipementDispositifdederivationPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrAriEquipementDispositifdederivationPage),
  ],
})
export class GnrAriEquipementDispositifdederivationPageModule {}
