import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GdoVentilationoperationnellePage } from './gdo-ventilationoperationnelle';

@NgModule({
  declarations: [
    GdoVentilationoperationnellePage,
  ],
  imports: [
    IonicPageModule.forChild(GdoVentilationoperationnellePage),
  ],
})
export class GdoVentilationoperationnellePageModule {}
