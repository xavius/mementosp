import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GdoVentilationoperationnellePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gdo-ventilationoperationnelle',
  templateUrl: 'gdo-ventilationoperationnelle.html',
})
export class GdoVentilationoperationnellePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GdoVentilationoperationnellePage');
  }

}
