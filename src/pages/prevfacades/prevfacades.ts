import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrevFacadesFonctionnementComponent } from '../../components/prev-facades/prev-facades-fonctionnement/prev-facades-fonctionnement';
import { PrevFacadesPrecautionComponent } from '../../components/prev-facades/prev-facades-precaution/prev-facades-precaution';

/**
 * Generated class for the PrevfacadesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prevfacades',
  templateUrl: 'prevfacades.html',
})
export class PrevfacadesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrevfacadesPage');
  }
  tab46Root=PrevFacadesFonctionnementComponent;
  tab47Root=PrevFacadesPrecautionComponent;
}
