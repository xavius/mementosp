import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevfacadesPage } from './prevfacades';

@NgModule({
  declarations: [
    PrevfacadesPage,
  ],
  imports: [
    IonicPageModule.forChild(PrevfacadesPage),
  ],
})
export class PrevfacadesPageModule {}
