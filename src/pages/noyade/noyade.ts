import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SapNoyadeSignesComponent } from '../../components/sap-noyade/sap-noyade-signes/sap-noyade-signes';
import { SapNoyadeBilansComponent } from '../../components/sap-noyade/sap-noyade-bilans/sap-noyade-bilans';
import { SapNoyadeActionsComponent } from '../../components/sap-noyade/sap-noyade-actions/sap-noyade-actions';
import { SapNoyadeImportantComponent } from '../../components/sap-noyade/sap-noyade-important/sap-noyade-important';

/**
 * Generated class for the NoyadePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noyade',
  templateUrl: 'noyade.html',
})
export class NoyadePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoyadePage');
  }
  tab7Root=SapNoyadeSignesComponent;
  tab8Root=SapNoyadeBilansComponent;
  tab9Root=SapNoyadeActionsComponent;
  tab10Root=SapNoyadeImportantComponent;
}
