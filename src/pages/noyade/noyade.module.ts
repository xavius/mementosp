import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoyadePage } from './noyade';

@NgModule({
  declarations: [
    NoyadePage,
  ],
  imports: [
    IonicPageModule.forChild(NoyadePage),
  ],
})
export class NoyadePageModule {}
