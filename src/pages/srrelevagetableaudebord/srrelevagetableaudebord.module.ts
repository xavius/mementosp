import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrrelevagetableaudebordPage } from './srrelevagetableaudebord';

@NgModule({
  declarations: [
    SrrelevagetableaudebordPage,
  ],
  imports: [
    IonicPageModule.forChild(SrrelevagetableaudebordPage),
  ],
})
export class SrrelevagetableaudebordPageModule {}
