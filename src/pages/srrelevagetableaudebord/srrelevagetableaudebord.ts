import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SrRelevagetableaudebord_1Component } from '../../components/sr-relevagetableaudebord/sr-relevagetableaudebord-1/sr-relevagetableaudebord-1';
import { SrRelevagetableaudebord_2Component } from '../../components/sr-relevagetableaudebord/sr-relevagetableaudebord-2/sr-relevagetableaudebord-2';
import { SrRelevagetableaudebord_3Component } from '../../components/sr-relevagetableaudebord/sr-relevagetableaudebord-3/sr-relevagetableaudebord-3';

/**
 * Generated class for the SrrelevagetableaudebordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srrelevagetableaudebord',
  templateUrl: 'srrelevagetableaudebord.html',
})
export class SrrelevagetableaudebordPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrrelevagetableaudebordPage');
  }
  tab22Root=SrRelevagetableaudebord_1Component;
  tab23Root=SrRelevagetableaudebord_2Component;
  tab24Root=SrRelevagetableaudebord_3Component;
}
