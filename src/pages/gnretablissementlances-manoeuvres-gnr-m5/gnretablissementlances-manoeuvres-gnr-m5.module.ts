import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM5Page } from './gnretablissementlances-manoeuvres-gnr-m5';

@NgModule({
  declarations: [
    GnretablissementlancesManoeuvresGnrM5Page,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesManoeuvresGnrM5Page),
  ],
})
export class GnretablissementlancesManoeuvresGnrM5PageModule {}
