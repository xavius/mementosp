import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GnretablissementlancesManoeuvresGnrM5ChefdagresComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-chefdagres/gnretablissementlances-manoeuvres-gnr-m5-chefdagres';
import { GnretablissementlancesManoeuvresGnrM5ChefdequipeComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-chefdequipe/gnretablissementlances-manoeuvres-gnr-m5-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM5EquipierComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-equipier/gnretablissementlances-manoeuvres-gnr-m5-equipier';
import { GnretablissementlancesManoeuvresGnrM5BalComponent } from '../../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-bal/gnretablissementlances-manoeuvres-gnr-m5-bal';

/**
 * Generated class for the GnretablissementlancesManoeuvresGnrM5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-manoeuvres-gnr-m5',
  templateUrl: 'gnretablissementlances-manoeuvres-gnr-m5.html',
})
export class GnretablissementlancesManoeuvresGnrM5Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesManoeuvresGnrM5Page');
  }
  tab61Root=GnretablissementlancesManoeuvresGnrM5ChefdagresComponent;
  tab62Root=GnretablissementlancesManoeuvresGnrM5ChefdequipeComponent;
  tab63Root=GnretablissementlancesManoeuvresGnrM5EquipierComponent;
  tab64Root=GnretablissementlancesManoeuvresGnrM5BalComponent;
}
