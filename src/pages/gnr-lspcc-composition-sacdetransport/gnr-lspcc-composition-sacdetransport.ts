import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnrLspccCompositionSacdetransportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnr-lspcc-composition-sacdetransport',
  templateUrl: 'gnr-lspcc-composition-sacdetransport.html',
})
export class GnrLspccCompositionSacdetransportPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnrLspccCompositionSacdetransportPage');
  }

}
