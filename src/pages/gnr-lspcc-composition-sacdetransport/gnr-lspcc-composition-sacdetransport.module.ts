import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnrLspccCompositionSacdetransportPage } from './gnr-lspcc-composition-sacdetransport';

@NgModule({
  declarations: [
    GnrLspccCompositionSacdetransportPage,
  ],
  imports: [
    IonicPageModule.forChild(GnrLspccCompositionSacdetransportPage),
  ],
})
export class GnrLspccCompositionSacdetransportPageModule {}
