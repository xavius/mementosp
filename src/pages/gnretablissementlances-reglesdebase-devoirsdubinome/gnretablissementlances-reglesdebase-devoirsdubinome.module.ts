import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnretablissementlancesReglesdebaseDevoirsdubinomePage } from './gnretablissementlances-reglesdebase-devoirsdubinome';

@NgModule({
  declarations: [
    GnretablissementlancesReglesdebaseDevoirsdubinomePage,
  ],
  imports: [
    IonicPageModule.forChild(GnretablissementlancesReglesdebaseDevoirsdubinomePage),
  ],
})
export class GnretablissementlancesReglesdebaseDevoirsdubinomePageModule {}
