import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GnretablissementlancesReglesdebaseDevoirsdubinomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gnretablissementlances-reglesdebase-devoirsdubinome',
  templateUrl: 'gnretablissementlances-reglesdebase-devoirsdubinome.html',
})
export class GnretablissementlancesReglesdebaseDevoirsdubinomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GnretablissementlancesReglesdebaseDevoirsdubinomePage');
  }

}
