import { Component } from '@angular/core';

/**
 * Generated class for the GocFonctiondupcTransComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'goc-fonctiondupc-trans',
  templateUrl: 'goc-fonctiondupc-trans.html'
})
export class GocFonctiondupcTransComponent {

  text: string;

  constructor() {
    console.log('Hello GocFonctiondupcTransComponent Component');
    this.text = 'Hello World';
  }

}
