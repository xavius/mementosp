import { Component } from '@angular/core';

/**
 * Generated class for the SapTetaniespasmophilieActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-tetaniespasmophilie-actions',
  templateUrl: 'sap-tetaniespasmophilie-actions.html'
})
export class SapTetaniespasmophilieActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapTetaniespasmophilieActionsComponent Component');
    this.text = 'Hello World';
  }

}
