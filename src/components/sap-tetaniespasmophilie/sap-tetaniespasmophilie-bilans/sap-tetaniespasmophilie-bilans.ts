import { Component } from '@angular/core';

/**
 * Generated class for the SapTetaniespasmophilieBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-tetaniespasmophilie-bilans',
  templateUrl: 'sap-tetaniespasmophilie-bilans.html'
})
export class SapTetaniespasmophilieBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapTetaniespasmophilieBilansComponent Component');
    this.text = 'Hello World';
  }

}
