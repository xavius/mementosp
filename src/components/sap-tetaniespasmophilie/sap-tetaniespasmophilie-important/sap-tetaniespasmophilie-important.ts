import { Component } from '@angular/core';

/**
 * Generated class for the SapTetaniespasmophilieImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-tetaniespasmophilie-important',
  templateUrl: 'sap-tetaniespasmophilie-important.html'
})
export class SapTetaniespasmophilieImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapTetaniespasmophilieImportantComponent Component');
    this.text = 'Hello World';
  }

}
