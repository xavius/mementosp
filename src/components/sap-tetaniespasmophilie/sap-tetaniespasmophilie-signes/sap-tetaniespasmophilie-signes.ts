import { Component } from '@angular/core';

/**
 * Generated class for the SapTetaniespasmophilieSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-tetaniespasmophilie-signes',
  templateUrl: 'sap-tetaniespasmophilie-signes.html'
})
export class SapTetaniespasmophilieSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapTetaniespasmophilieSignesComponent Component');
    this.text = 'Hello World';
  }

}
