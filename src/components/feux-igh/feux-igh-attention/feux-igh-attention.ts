import { Component } from '@angular/core';

/**
 * Generated class for the FeuxIghAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-igh-attention',
  templateUrl: 'feux-igh-attention.html'
})
export class FeuxIghAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxIghAttentionComponent Component');
    this.text = 'Hello World';
  }

}
