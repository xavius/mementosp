import { Component } from '@angular/core';

/**
 * Generated class for the FeuxIghActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-igh-action',
  templateUrl: 'feux-igh-action.html'
})
export class FeuxIghActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxIghActionComponent Component');
    this.text = 'Hello World';
  }

}
