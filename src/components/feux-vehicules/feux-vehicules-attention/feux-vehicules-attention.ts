import { Component } from '@angular/core';

/**
 * Generated class for the FeuxVehiculesAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-vehicules-attention',
  templateUrl: 'feux-vehicules-attention.html'
})
export class FeuxVehiculesAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxVehiculesAttentionComponent Component');
    this.text = 'Hello World';
  }

}
