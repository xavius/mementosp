import { Component } from '@angular/core';

/**
 * Generated class for the FeuxVehiculesAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-vehicules-acces',
  templateUrl: 'feux-vehicules-acces.html'
})
export class FeuxVehiculesAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxVehiculesAccesComponent Component');
    this.text = 'Hello World';
  }

}
