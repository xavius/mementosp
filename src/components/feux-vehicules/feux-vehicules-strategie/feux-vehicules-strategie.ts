import { Component } from '@angular/core';

/**
 * Generated class for the FeuxVehiculesStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-vehicules-strategie',
  templateUrl: 'feux-vehicules-strategie.html'
})
export class FeuxVehiculesStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxVehiculesStrategieComponent Component');
    this.text = 'Hello World';
  }

}
