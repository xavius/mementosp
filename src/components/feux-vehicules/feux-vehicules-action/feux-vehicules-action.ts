import { Component } from '@angular/core';

/**
 * Generated class for the FeuxVehiculesActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-vehicules-action',
  templateUrl: 'feux-vehicules-action.html'
})
export class FeuxVehiculesActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxVehiculesActionComponent Component');
    this.text = 'Hello World';
  }

}
