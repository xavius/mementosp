import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapSanglearaigneePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-sanglearaignee-pointscles',
  templateUrl: 'materiels-sap-sanglearaignee-pointscles.html'
})
export class MaterielsSapSanglearaigneePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapSanglearaigneePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
