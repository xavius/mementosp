import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapSanglearaigneeDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-sanglearaignee-description',
  templateUrl: 'materiels-sap-sanglearaignee-description.html'
})
export class MaterielsSapSanglearaigneeDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapSanglearaigneeDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
