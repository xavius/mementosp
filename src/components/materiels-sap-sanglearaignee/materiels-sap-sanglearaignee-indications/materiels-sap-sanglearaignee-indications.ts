import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapSanglearaigneeIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-sanglearaignee-indications',
  templateUrl: 'materiels-sap-sanglearaignee-indications.html'
})
export class MaterielsSapSanglearaigneeIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapSanglearaigneeIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
