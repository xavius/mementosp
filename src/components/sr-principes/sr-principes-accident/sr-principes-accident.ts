import { Component } from '@angular/core';

/**
 * Generated class for the SrPrincipesAccidentComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-principes-accident',
  templateUrl: 'sr-principes-accident.html'
})
export class SrPrincipesAccidentComponent {

  text: string;

  constructor() {
    console.log('Hello SrPrincipesAccidentComponent Component');
    this.text = 'Hello World';
  }

}
