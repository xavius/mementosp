import { Component } from '@angular/core';

/**
 * Generated class for the SrPrincipesIncComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-principes-inc',
  templateUrl: 'sr-principes-inc.html'
})
export class SrPrincipesIncComponent {

  text: string;

  constructor() {
    console.log('Hello SrPrincipesIncComponent Component');
    this.text = 'Hello World';
  }

}
