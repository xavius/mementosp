import { Component } from '@angular/core';

/**
 * Generated class for the SrPrincipesRecoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-principes-reco',
  templateUrl: 'sr-principes-reco.html'
})
export class SrPrincipesRecoComponent {

  text: string;

  constructor() {
    console.log('Hello SrPrincipesRecoComponent Component');
    this.text = 'Hello World';
  }

}
