import { Component } from '@angular/core';

/**
 * Generated class for the SrPrincipesHybridesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-principes-hybrides',
  templateUrl: 'sr-principes-hybrides.html'
})
export class SrPrincipesHybridesComponent {

  text: string;

  constructor() {
    console.log('Hello SrPrincipesHybridesComponent Component');
    this.text = 'Hello World';
  }

}
