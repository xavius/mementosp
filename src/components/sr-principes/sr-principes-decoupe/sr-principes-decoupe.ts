import { Component } from '@angular/core';

/**
 * Generated class for the SrPrincipesDecoupeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-principes-decoupe',
  templateUrl: 'sr-principes-decoupe.html'
})
export class SrPrincipesDecoupeComponent {

  text: string;

  constructor() {
    console.log('Hello SrPrincipesDecoupeComponent Component');
    this.text = 'Hello World';
  }

}
