import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapThermometreRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-thermometre-risques',
  templateUrl: 'materiels-sap-thermometre-risques.html'
})
export class MaterielsSapThermometreRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapThermometreRisquesComponent Component');
    this.text = 'Hello World';
  }

}
