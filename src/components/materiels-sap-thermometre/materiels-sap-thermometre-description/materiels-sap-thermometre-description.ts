import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapThermometreDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-thermometre-description',
  templateUrl: 'materiels-sap-thermometre-description.html'
})
export class MaterielsSapThermometreDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapThermometreDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
