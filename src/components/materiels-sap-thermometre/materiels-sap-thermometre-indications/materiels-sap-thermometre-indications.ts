import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapThermometreIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-thermometre-indications',
  templateUrl: 'materiels-sap-thermometre-indications.html'
})
export class MaterielsSapThermometreIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapThermometreIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
