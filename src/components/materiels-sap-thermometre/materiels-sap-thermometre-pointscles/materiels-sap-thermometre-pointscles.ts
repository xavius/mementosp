import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapThermometrePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-thermometre-pointscles',
  templateUrl: 'materiels-sap-thermometre-pointscles.html'
})
export class MaterielsSapThermometrePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapThermometrePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
