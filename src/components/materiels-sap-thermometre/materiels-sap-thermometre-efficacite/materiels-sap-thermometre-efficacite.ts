import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapThermometreEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-thermometre-efficacite',
  templateUrl: 'materiels-sap-thermometre-efficacite.html'
})
export class MaterielsSapThermometreEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapThermometreEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
