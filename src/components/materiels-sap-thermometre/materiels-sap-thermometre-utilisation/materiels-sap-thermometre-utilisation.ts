import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapThermometreUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-thermometre-utilisation',
  templateUrl: 'materiels-sap-thermometre-utilisation.html'
})
export class MaterielsSapThermometreUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapThermometreUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
