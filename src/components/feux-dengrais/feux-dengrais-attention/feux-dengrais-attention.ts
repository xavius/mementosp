import { Component } from '@angular/core';

/**
 * Generated class for the FeuxDengraisAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-dengrais-attention',
  templateUrl: 'feux-dengrais-attention.html'
})
export class FeuxDengraisAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxDengraisAttentionComponent Component');
    this.text = 'Hello World';
  }

}
