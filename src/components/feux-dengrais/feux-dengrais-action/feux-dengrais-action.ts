import { Component } from '@angular/core';

/**
 * Generated class for the FeuxDengraisActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-dengrais-action',
  templateUrl: 'feux-dengrais-action.html'
})
export class FeuxDengraisActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxDengraisActionComponent Component');
    this.text = 'Hello World';
  }

}
