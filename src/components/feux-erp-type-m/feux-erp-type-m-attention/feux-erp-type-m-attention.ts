import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeMAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-m-attention',
  templateUrl: 'feux-erp-type-m-attention.html'
})
export class FeuxErpTypeMAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeMAttentionComponent Component');
    this.text = 'Hello World';
  }

}
