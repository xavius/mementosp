import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeMStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-m-strategie',
  templateUrl: 'feux-erp-type-m-strategie.html'
})
export class FeuxErpTypeMStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeMStrategieComponent Component');
    this.text = 'Hello World';
  }

}
