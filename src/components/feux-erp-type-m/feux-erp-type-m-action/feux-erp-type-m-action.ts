import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeMActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-m-action',
  templateUrl: 'feux-erp-type-m-action.html'
})
export class FeuxErpTypeMActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeMActionComponent Component');
    this.text = 'Hello World';
  }

}
