import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeMAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-m-acces',
  templateUrl: 'feux-erp-type-m-acces.html'
})
export class FeuxErpTypeMAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeMAccesComponent Component');
    this.text = 'Hello World';
  }

}
