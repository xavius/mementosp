import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeMInterditComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-m-interdit',
  templateUrl: 'feux-erp-type-m-interdit.html'
})
export class FeuxErpTypeMInterditComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeMInterditComponent Component');
    this.text = 'Hello World';
  }

}
