import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCamionciterneActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-camionciterne-action',
  templateUrl: 'feux-camionciterne-action.html'
})
export class FeuxCamionciterneActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCamionciterneActionComponent Component');
    this.text = 'Hello World';
  }

}
