import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCamionciterneAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-camionciterne-attention',
  templateUrl: 'feux-camionciterne-attention.html'
})
export class FeuxCamionciterneAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCamionciterneAttentionComponent Component');
    this.text = 'Hello World';
  }

}
