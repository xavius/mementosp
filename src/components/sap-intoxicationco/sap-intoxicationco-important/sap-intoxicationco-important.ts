import { Component } from '@angular/core';

/**
 * Generated class for the SapIntoxicationcoImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-intoxicationco-important',
  templateUrl: 'sap-intoxicationco-important.html'
})
export class SapIntoxicationcoImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapIntoxicationcoImportantComponent Component');
    this.text = 'Hello World';
  }

}
