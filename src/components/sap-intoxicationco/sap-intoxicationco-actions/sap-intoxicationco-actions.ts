import { Component } from '@angular/core';

/**
 * Generated class for the SapIntoxicationcoActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-intoxicationco-actions',
  templateUrl: 'sap-intoxicationco-actions.html'
})
export class SapIntoxicationcoActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapIntoxicationcoActionsComponent Component');
    this.text = 'Hello World';
  }

}
