import { Component } from '@angular/core';

/**
 * Generated class for the FeuxJointdilatationActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-jointdilatation-action',
  templateUrl: 'feux-jointdilatation-action.html'
})
export class FeuxJointdilatationActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxJointdilatationActionComponent Component');
    this.text = 'Hello World';
  }

}
