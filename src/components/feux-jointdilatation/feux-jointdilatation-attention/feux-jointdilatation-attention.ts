import { Component } from '@angular/core';

/**
 * Generated class for the FeuxJointdilatationAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-jointdilatation-attention',
  templateUrl: 'feux-jointdilatation-attention.html'
})
export class FeuxJointdilatationAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxJointdilatationAttentionComponent Component');
    this.text = 'Hello World';
  }

}
