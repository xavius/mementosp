import { Component } from '@angular/core';

/**
 * Generated class for the FeuxFermeAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-ferme-attention',
  templateUrl: 'feux-ferme-attention.html'
})
export class FeuxFermeAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxFermeAttentionComponent Component');
    this.text = 'Hello World';
  }

}
