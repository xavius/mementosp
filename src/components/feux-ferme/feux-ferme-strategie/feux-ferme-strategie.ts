import { Component } from '@angular/core';

/**
 * Generated class for the FeuxFermeStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-ferme-strategie',
  templateUrl: 'feux-ferme-strategie.html'
})
export class FeuxFermeStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxFermeStrategieComponent Component');
    this.text = 'Hello World';
  }

}
