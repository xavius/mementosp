import { Component } from '@angular/core';

/**
 * Generated class for the FeuxFermeAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-ferme-acces',
  templateUrl: 'feux-ferme-acces.html'
})
export class FeuxFermeAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxFermeAccesComponent Component');
    this.text = 'Hello World';
  }

}
