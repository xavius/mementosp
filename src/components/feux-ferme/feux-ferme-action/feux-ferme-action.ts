import { Component } from '@angular/core';

/**
 * Generated class for the FeuxFermeActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-ferme-action',
  templateUrl: 'feux-ferme-action.html'
})
export class FeuxFermeActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxFermeActionComponent Component');
    this.text = 'Hello World';
  }

}
