import { Component } from '@angular/core';

/**
 * Generated class for the SapIvresseBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ivresse-bilans',
  templateUrl: 'sap-ivresse-bilans.html'
})
export class SapIvresseBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapIvresseBilansComponent Component');
    this.text = 'Hello World';
  }

}
