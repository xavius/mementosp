import { Component } from '@angular/core';

/**
 * Generated class for the SapIvresseSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ivresse-signes',
  templateUrl: 'sap-ivresse-signes.html'
})
export class SapIvresseSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapIvresseSignesComponent Component');
    this.text = 'Hello World';
  }

}
