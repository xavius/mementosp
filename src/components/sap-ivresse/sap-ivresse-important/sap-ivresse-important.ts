import { Component } from '@angular/core';

/**
 * Generated class for the SapIvresseImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ivresse-important',
  templateUrl: 'sap-ivresse-important.html'
})
export class SapIvresseImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapIvresseImportantComponent Component');
    this.text = 'Hello World';
  }

}
