import { Component } from '@angular/core';

/**
 * Generated class for the SapIvresseActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ivresse-actions',
  templateUrl: 'sap-ivresse-actions.html'
})
export class SapIvresseActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapIvresseActionsComponent Component');
    this.text = 'Hello World';
  }

}
