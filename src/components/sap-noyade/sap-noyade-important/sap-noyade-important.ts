import { Component } from '@angular/core';

/**
 * Generated class for the SapNoyadeImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-noyade-important',
  templateUrl: 'sap-noyade-important.html'
})
export class SapNoyadeImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapNoyadeImportantComponent Component');
    this.text = 'Hello World';
  }

}
