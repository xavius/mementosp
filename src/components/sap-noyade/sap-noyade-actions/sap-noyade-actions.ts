import { Component } from '@angular/core';

/**
 * Generated class for the SapNoyadeActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-noyade-actions',
  templateUrl: 'sap-noyade-actions.html'
})
export class SapNoyadeActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapNoyadeActionsComponent Component');
    this.text = 'Hello World';
  }

}
