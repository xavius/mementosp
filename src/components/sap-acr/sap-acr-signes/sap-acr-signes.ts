import { Component } from '@angular/core';

/**
 * Generated class for the SapAcrSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-acr-signes',
  templateUrl: 'sap-acr-signes.html'
})
export class SapAcrSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapAcrSignesComponent Component');
    this.text = 'Hello World';
  }

}
