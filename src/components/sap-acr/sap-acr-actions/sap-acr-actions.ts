import { Component } from '@angular/core';

/**
 * Generated class for the SapAcrActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-acr-actions',
  templateUrl: 'sap-acr-actions.html'
})
export class SapAcrActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapAcrActionsComponent Component');
    this.text = 'Hello World';
  }

}
