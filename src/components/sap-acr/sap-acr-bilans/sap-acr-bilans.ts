import { Component } from '@angular/core';

/**
 * Generated class for the SapAcrBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-acr-bilans',
  templateUrl: 'sap-acr-bilans.html'
})
export class SapAcrBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapAcrBilansComponent Component');
    this.text = 'Hello World';
  }

}
