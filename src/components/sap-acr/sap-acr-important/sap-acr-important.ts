import { Component } from '@angular/core';

/**
 * Generated class for the SapAcrImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-acr-important',
  templateUrl: 'sap-acr-important.html'
})
export class SapAcrImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapAcrImportantComponent Component');
    this.text = 'Hello World';
  }

}
