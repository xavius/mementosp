import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPlanchersAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-planchers-acces',
  templateUrl: 'feux-planchers-acces.html'
})
export class FeuxPlanchersAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPlanchersAccesComponent Component');
    this.text = 'Hello World';
  }

}
