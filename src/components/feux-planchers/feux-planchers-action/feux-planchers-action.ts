import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPlanchersActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-planchers-action',
  templateUrl: 'feux-planchers-action.html'
})
export class FeuxPlanchersActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPlanchersActionComponent Component');
    this.text = 'Hello World';
  }

}
