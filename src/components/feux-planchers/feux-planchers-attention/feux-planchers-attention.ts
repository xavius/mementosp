import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPlanchersAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-planchers-attention',
  templateUrl: 'feux-planchers-attention.html'
})
export class FeuxPlanchersAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPlanchersAttentionComponent Component');
    this.text = 'Hello World';
  }

}
