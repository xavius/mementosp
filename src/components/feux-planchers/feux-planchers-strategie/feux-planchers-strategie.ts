import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPlanchersStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-planchers-strategie',
  templateUrl: 'feux-planchers-strategie.html'
})
export class FeuxPlanchersStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPlanchersStrategieComponent Component');
    this.text = 'Hello World';
  }

}
