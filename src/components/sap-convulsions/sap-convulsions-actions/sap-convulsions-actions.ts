import { Component } from '@angular/core';

/**
 * Generated class for the SapConvulsionsActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-convulsions-actions',
  templateUrl: 'sap-convulsions-actions.html'
})
export class SapConvulsionsActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapConvulsionsActionsComponent Component');
    this.text = 'Hello World';
  }

}
