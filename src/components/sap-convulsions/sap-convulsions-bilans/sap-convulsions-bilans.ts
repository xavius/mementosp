import { Component } from '@angular/core';

/**
 * Generated class for the SapConvulsionsBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-convulsions-bilans',
  templateUrl: 'sap-convulsions-bilans.html'
})
export class SapConvulsionsBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapConvulsionsBilansComponent Component');
    this.text = 'Hello World';
  }

}
