import { Component } from '@angular/core';

/**
 * Generated class for the SapConvulsionsSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-convulsions-signes',
  templateUrl: 'sap-convulsions-signes.html'
})
export class SapConvulsionsSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapConvulsionsSignesComponent Component');
    this.text = 'Hello World';
  }

}
