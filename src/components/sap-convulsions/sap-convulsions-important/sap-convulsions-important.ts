import { Component } from '@angular/core';

/**
 * Generated class for the SapConvulsionsImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-convulsions-important',
  templateUrl: 'sap-convulsions-important.html'
})
export class SapConvulsionsImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapConvulsionsImportantComponent Component');
    this.text = 'Hello World';
  }

}
