import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapMesurepressionarteriellePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-mesurepressionarterielle-pointscles',
  templateUrl: 'materiels-sap-mesurepressionarterielle-pointscles.html'
})
export class MaterielsSapMesurepressionarteriellePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapMesurepressionarteriellePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
