import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapMesurepressionarterielleEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-mesurepressionarterielle-efficacite',
  templateUrl: 'materiels-sap-mesurepressionarterielle-efficacite.html'
})
export class MaterielsSapMesurepressionarterielleEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapMesurepressionarterielleEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
