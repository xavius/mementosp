import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapMesurepressionarterielleIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-mesurepressionarterielle-indications',
  templateUrl: 'materiels-sap-mesurepressionarterielle-indications.html'
})
export class MaterielsSapMesurepressionarterielleIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapMesurepressionarterielleIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
