import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapMesurepressionarterielleRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-mesurepressionarterielle-risques',
  templateUrl: 'materiels-sap-mesurepressionarterielle-risques.html'
})
export class MaterielsSapMesurepressionarterielleRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapMesurepressionarterielleRisquesComponent Component');
    this.text = 'Hello World';
  }

}
