import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapMesurepressionarterielleUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-mesurepressionarterielle-utilisation',
  templateUrl: 'materiels-sap-mesurepressionarterielle-utilisation.html'
})
export class MaterielsSapMesurepressionarterielleUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapMesurepressionarterielleUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
