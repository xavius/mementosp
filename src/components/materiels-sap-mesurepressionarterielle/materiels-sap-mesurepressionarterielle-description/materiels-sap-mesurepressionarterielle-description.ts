import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapMesurepressionarterielleDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-mesurepressionarterielle-description',
  templateUrl: 'materiels-sap-mesurepressionarterielle-description.html'
})
export class MaterielsSapMesurepressionarterielleDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapMesurepressionarterielleDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
