import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRadiologiqueRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-radiologique-risques',
  templateUrl: 'feux-radiologique-risques.html'
})
export class FeuxRadiologiqueRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRadiologiqueRisquesComponent Component');
    this.text = 'Hello World';
  }

}
