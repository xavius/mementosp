import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRadiologiqueActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-radiologique-action',
  templateUrl: 'feux-radiologique-action.html'
})
export class FeuxRadiologiqueActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRadiologiqueActionComponent Component');
    this.text = 'Hello World';
  }

}
