import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRadiologiqueAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-radiologique-attention',
  templateUrl: 'feux-radiologique-attention.html'
})
export class FeuxRadiologiqueAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRadiologiqueAttentionComponent Component');
    this.text = 'Hello World';
  }

}
