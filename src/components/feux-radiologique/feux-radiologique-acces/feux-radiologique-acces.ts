import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRadiologiqueAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-radiologique-acces',
  templateUrl: 'feux-radiologique-acces.html'
})
export class FeuxRadiologiqueAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRadiologiqueAccesComponent Component');
    this.text = 'Hello World';
  }

}
