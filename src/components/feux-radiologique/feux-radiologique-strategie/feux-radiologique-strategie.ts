import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRadiologiqueStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-radiologique-strategie',
  templateUrl: 'feux-radiologique-strategie.html'
})
export class FeuxRadiologiqueStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRadiologiqueStrategieComponent Component');
    this.text = 'Hello World';
  }

}
