import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRadiologiqueInterditComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-radiologique-interdit',
  templateUrl: 'feux-radiologique-interdit.html'
})
export class FeuxRadiologiqueInterditComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRadiologiqueInterditComponent Component');
    this.text = 'Hello World';
  }

}
