import { Component } from '@angular/core';

/**
 * Generated class for the SapDesincarserationImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-desincarseration-important',
  templateUrl: 'sap-desincarseration-important.html'
})
export class SapDesincarserationImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapDesincarserationImportantComponent Component');
    this.text = 'Hello World';
  }

}
