import { Component } from '@angular/core';

/**
 * Generated class for the SapDesincarserationActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-desincarseration-actions',
  templateUrl: 'sap-desincarseration-actions.html'
})
export class SapDesincarserationActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapDesincarserationActionsComponent Component');
    this.text = 'Hello World';
  }

}
