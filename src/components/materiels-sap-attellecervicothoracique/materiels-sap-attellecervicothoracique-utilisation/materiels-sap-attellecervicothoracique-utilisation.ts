import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapAttellecervicothoraciqueUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-attellecervicothoracique-utilisation',
  templateUrl: 'materiels-sap-attellecervicothoracique-utilisation.html'
})
export class MaterielsSapAttellecervicothoraciqueUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapAttellecervicothoraciqueUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
