import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapAttellecervicothoraciqueRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-attellecervicothoracique-risques',
  templateUrl: 'materiels-sap-attellecervicothoracique-risques.html'
})
export class MaterielsSapAttellecervicothoraciqueRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapAttellecervicothoraciqueRisquesComponent Component');
    this.text = 'Hello World';
  }

}
