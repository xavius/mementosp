import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapAttellecervicothoraciquePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-attellecervicothoracique-pointscles',
  templateUrl: 'materiels-sap-attellecervicothoracique-pointscles.html'
})
export class MaterielsSapAttellecervicothoraciquePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapAttellecervicothoraciquePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
