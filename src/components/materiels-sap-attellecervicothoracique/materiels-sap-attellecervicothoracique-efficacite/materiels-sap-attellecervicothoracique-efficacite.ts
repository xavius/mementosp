import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapAttellecervicothoraciqueEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-attellecervicothoracique-efficacite',
  templateUrl: 'materiels-sap-attellecervicothoracique-efficacite.html'
})
export class MaterielsSapAttellecervicothoraciqueEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapAttellecervicothoraciqueEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
