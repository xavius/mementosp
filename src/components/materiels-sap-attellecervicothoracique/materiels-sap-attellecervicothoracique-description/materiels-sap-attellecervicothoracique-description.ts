import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapAttellecervicothoraciqueDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-attellecervicothoracique-description',
  templateUrl: 'materiels-sap-attellecervicothoracique-description.html'
})
export class MaterielsSapAttellecervicothoraciqueDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapAttellecervicothoraciqueDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
