import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitaesDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitaes-description',
  templateUrl: 'materiels-sap-kitaes-description.html'
})
export class MaterielsSapKitaesDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitaesDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
