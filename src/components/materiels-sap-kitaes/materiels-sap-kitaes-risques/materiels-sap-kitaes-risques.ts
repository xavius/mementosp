import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitaesRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitaes-risques',
  templateUrl: 'materiels-sap-kitaes-risques.html'
})
export class MaterielsSapKitaesRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitaesRisquesComponent Component');
    this.text = 'Hello World';
  }

}
