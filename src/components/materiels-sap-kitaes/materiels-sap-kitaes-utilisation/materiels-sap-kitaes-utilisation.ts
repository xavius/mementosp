import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitaesUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitaes-utilisation',
  templateUrl: 'materiels-sap-kitaes-utilisation.html'
})
export class MaterielsSapKitaesUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitaesUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
