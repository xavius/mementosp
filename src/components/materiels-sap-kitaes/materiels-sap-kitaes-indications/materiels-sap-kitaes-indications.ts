import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitaesIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitaes-indications',
  templateUrl: 'materiels-sap-kitaes-indications.html'
})
export class MaterielsSapKitaesIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitaesIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
