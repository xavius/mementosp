import { Component } from '@angular/core';

/**
 * Generated class for the SapCrisedasthmesBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-crisedasthmes-bilans',
  templateUrl: 'sap-crisedasthmes-bilans.html'
})
export class SapCrisedasthmesBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapCrisedasthmesBilansComponent Component');
    this.text = 'Hello World';
  }

}
