import { Component } from '@angular/core';

/**
 * Generated class for the SapHemorragieActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-hemorragie-actions',
  templateUrl: 'sap-hemorragie-actions.html'
})
export class SapHemorragieActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapHemorragieActionsComponent Component');
    this.text = 'Hello World';
  }

}
