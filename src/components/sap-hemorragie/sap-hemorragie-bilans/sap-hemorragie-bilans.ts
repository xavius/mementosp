import { Component } from '@angular/core';

/**
 * Generated class for the SapHemorragieBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-hemorragie-bilans',
  templateUrl: 'sap-hemorragie-bilans.html'
})
export class SapHemorragieBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapHemorragieBilansComponent Component');
    this.text = 'Hello World';
  }

}
