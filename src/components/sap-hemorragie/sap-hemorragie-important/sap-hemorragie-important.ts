import { Component } from '@angular/core';

/**
 * Generated class for the SapHemorragieImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-hemorragie-important',
  templateUrl: 'sap-hemorragie-important.html'
})
export class SapHemorragieImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapHemorragieImportantComponent Component');
    this.text = 'Hello World';
  }

}
