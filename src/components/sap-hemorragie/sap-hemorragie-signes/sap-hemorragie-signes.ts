import { Component } from '@angular/core';

/**
 * Generated class for the SapHemorragieSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-hemorragie-signes',
  templateUrl: 'sap-hemorragie-signes.html'
})
export class SapHemorragieSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapHemorragieSignesComponent Component');
    this.text = 'Hello World';
  }

}
