import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCaveAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cave-attention',
  templateUrl: 'feux-cave-attention.html'
})
export class FeuxCaveAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCaveAttentionComponent Component');
    this.text = 'Hello World';
  }

}
