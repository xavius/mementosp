import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCaveAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cave-acces',
  templateUrl: 'feux-cave-acces.html'
})
export class FeuxCaveAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCaveAccesComponent Component');
    this.text = 'Hello World';
  }

}
