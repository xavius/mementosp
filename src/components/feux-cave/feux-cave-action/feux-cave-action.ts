import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCaveActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cave-action',
  templateUrl: 'feux-cave-action.html'
})
export class FeuxCaveActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCaveActionComponent Component');
    this.text = 'Hello World';
  }

}
