import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCaveStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cave-strategie',
  templateUrl: 'feux-cave-strategie.html'
})
export class FeuxCaveStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCaveStrategieComponent Component');
    this.text = 'Hello World';
  }

}
