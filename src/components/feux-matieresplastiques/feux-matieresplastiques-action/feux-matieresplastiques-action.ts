import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMatieresplastiquesActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-matieresplastiques-action',
  templateUrl: 'feux-matieresplastiques-action.html'
})
export class FeuxMatieresplastiquesActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMatieresplastiquesActionComponent Component');
    this.text = 'Hello World';
  }

}
