import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexterieurEquipierComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexterieur-equipier',
  templateUrl: 'gnr-lspcc-operation-sauvetageexterieur-equipier.html'
})
export class GnrLspccOperationSauvetageexterieurEquipierComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexterieurEquipierComponent Component');
    this.text = 'Hello World';
  }

}
