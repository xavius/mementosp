import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexterieurChefdequipeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexterieur-chefdequipe',
  templateUrl: 'gnr-lspcc-operation-sauvetageexterieur-chefdequipe.html'
})
export class GnrLspccOperationSauvetageexterieurChefdequipeComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexterieurChefdequipeComponent Component');
    this.text = 'Hello World';
  }

}
