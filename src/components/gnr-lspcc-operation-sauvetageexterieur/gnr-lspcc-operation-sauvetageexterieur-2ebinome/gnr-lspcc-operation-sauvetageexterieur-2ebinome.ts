import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexterieur_2ebinomeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexterieur-2ebinome',
  templateUrl: 'gnr-lspcc-operation-sauvetageexterieur-2ebinome.html'
})
export class GnrLspccOperationSauvetageexterieur_2ebinomeComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexterieur_2ebinomeComponent Component');
    this.text = 'Hello World';
  }

}
