import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChimiqueAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chimique-acces',
  templateUrl: 'feux-chimique-acces.html'
})
export class FeuxChimiqueAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChimiqueAccesComponent Component');
    this.text = 'Hello World';
  }

}
