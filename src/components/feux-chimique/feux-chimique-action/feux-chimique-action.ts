import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChimiqueActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chimique-action',
  templateUrl: 'feux-chimique-action.html'
})
export class FeuxChimiqueActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChimiqueActionComponent Component');
    this.text = 'Hello World';
  }

}
