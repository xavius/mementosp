import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChimiqueRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chimique-risques',
  templateUrl: 'feux-chimique-risques.html'
})
export class FeuxChimiqueRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChimiqueRisquesComponent Component');
    this.text = 'Hello World';
  }

}
