import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChimiqueStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chimique-strategie',
  templateUrl: 'feux-chimique-strategie.html'
})
export class FeuxChimiqueStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChimiqueStrategieComponent Component');
    this.text = 'Hello World';
  }

}
