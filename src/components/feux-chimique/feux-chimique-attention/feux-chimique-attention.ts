import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChimiqueAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chimique-attention',
  templateUrl: 'feux-chimique-attention.html'
})
export class FeuxChimiqueAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChimiqueAttentionComponent Component');
    this.text = 'Hello World';
  }

}
