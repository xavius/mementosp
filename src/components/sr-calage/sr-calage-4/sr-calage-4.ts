import { Component } from '@angular/core';

/**
 * Generated class for the SrCalage_4Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-calage-4',
  templateUrl: 'sr-calage-4.html'
})
export class SrCalage_4Component {

  text: string;

  constructor() {
    console.log('Hello SrCalage_4Component Component');
    this.text = 'Hello World';
  }

}
