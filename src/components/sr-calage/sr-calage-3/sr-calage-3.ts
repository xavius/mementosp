import { Component } from '@angular/core';

/**
 * Generated class for the SrCalage_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-calage-3',
  templateUrl: 'sr-calage-3.html'
})
export class SrCalage_3Component {

  text: string;

  constructor() {
    console.log('Hello SrCalage_3Component Component');
    this.text = 'Hello World';
  }

}
