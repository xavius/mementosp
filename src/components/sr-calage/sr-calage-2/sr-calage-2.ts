import { Component } from '@angular/core';

/**
 * Generated class for the SrCalage_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-calage-2',
  templateUrl: 'sr-calage-2.html'
})
export class SrCalage_2Component {

  text: string;

  constructor() {
    console.log('Hello SrCalage_2Component Component');
    this.text = 'Hello World';
  }

}
