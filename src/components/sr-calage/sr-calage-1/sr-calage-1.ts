import { Component } from '@angular/core';

/**
 * Generated class for the SrCalage_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-calage-1',
  templateUrl: 'sr-calage-1.html'
})
export class SrCalage_1Component {

  text: string;

  constructor() {
    console.log('Hello SrCalage_1Component Component');
    this.text = 'Hello World';
  }

}
