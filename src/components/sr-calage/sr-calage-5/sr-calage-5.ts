import { Component } from '@angular/core';

/**
 * Generated class for the SrCalage_5Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-calage-5',
  templateUrl: 'sr-calage-5.html'
})
export class SrCalage_5Component {

  text: string;

  constructor() {
    console.log('Hello SrCalage_5Component Component');
    this.text = 'Hello World';
  }

}
