import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPesticidesActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-pesticides-action',
  templateUrl: 'feux-pesticides-action.html'
})
export class FeuxPesticidesActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPesticidesActionComponent Component');
    this.text = 'Hello World';
  }

}
