import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPesticidesAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-pesticides-acces',
  templateUrl: 'feux-pesticides-acces.html'
})
export class FeuxPesticidesAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPesticidesAccesComponent Component');
    this.text = 'Hello World';
  }

}
