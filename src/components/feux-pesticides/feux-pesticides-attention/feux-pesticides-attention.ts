import { Component } from '@angular/core';

/**
 * Generated class for the FeuxPesticidesAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-pesticides-attention',
  templateUrl: 'feux-pesticides-attention.html'
})
export class FeuxPesticidesAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxPesticidesAttentionComponent Component');
    this.text = 'Hello World';
  }

}
