import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemedequinckeSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemedequincke-signes',
  templateUrl: 'sap-oedemedequincke-signes.html'
})
export class SapOedemedequinckeSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemedequinckeSignesComponent Component');
    this.text = 'Hello World';
  }

}
