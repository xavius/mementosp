import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemedequinckeImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemedequincke-important',
  templateUrl: 'sap-oedemedequincke-important.html'
})
export class SapOedemedequinckeImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemedequinckeImportantComponent Component');
    this.text = 'Hello World';
  }

}
