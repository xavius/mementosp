import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemedequinckeBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemedequincke-bilans',
  templateUrl: 'sap-oedemedequincke-bilans.html'
})
export class SapOedemedequinckeBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemedequinckeBilansComponent Component');
    this.text = 'Hello World';
  }

}
