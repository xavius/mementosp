import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemedequinckeActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemedequincke-actions',
  templateUrl: 'sap-oedemedequincke-actions.html'
})
export class SapOedemedequinckeActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemedequinckeActionsComponent Component');
    this.text = 'Hello World';
  }

}
