import { Component } from '@angular/core';

/**
 * Generated class for the SapPlaiesImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-plaies-important',
  templateUrl: 'sap-plaies-important.html'
})
export class SapPlaiesImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapPlaiesImportantComponent Component');
    this.text = 'Hello World';
  }

}
