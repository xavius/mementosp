import { Component } from '@angular/core';

/**
 * Generated class for the SapPlaiesActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-plaies-actions',
  templateUrl: 'sap-plaies-actions.html'
})
export class SapPlaiesActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapPlaiesActionsComponent Component');
    this.text = 'Hello World';
  }

}
