import { Component } from '@angular/core';

/**
 * Generated class for the SapPlaiesSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-plaies-signes',
  templateUrl: 'sap-plaies-signes.html'
})
export class SapPlaiesSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapPlaiesSignesComponent Component');
    this.text = 'Hello World';
  }

}
