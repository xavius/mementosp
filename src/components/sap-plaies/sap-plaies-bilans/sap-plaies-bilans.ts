import { Component } from '@angular/core';

/**
 * Generated class for the SapPlaiesBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-plaies-bilans',
  templateUrl: 'sap-plaies-bilans.html'
})
export class SapPlaiesBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapPlaiesBilansComponent Component');
    this.text = 'Hello World';
  }

}
