import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMetauxActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-metaux-action',
  templateUrl: 'feux-metaux-action.html'
})
export class FeuxMetauxActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMetauxActionComponent Component');
    this.text = 'Hello World';
  }

}
