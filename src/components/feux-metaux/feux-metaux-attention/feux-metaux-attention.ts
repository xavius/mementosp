import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMetauxAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-metaux-attention',
  templateUrl: 'feux-metaux-attention.html'
})
export class FeuxMetauxAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMetauxAttentionComponent Component');
    this.text = 'Hello World';
  }

}
