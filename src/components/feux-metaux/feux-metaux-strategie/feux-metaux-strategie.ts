import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMetauxStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-metaux-strategie',
  templateUrl: 'feux-metaux-strategie.html'
})
export class FeuxMetauxStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMetauxStrategieComponent Component');
    this.text = 'Hello World';
  }

}
