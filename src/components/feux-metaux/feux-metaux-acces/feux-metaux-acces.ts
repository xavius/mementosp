import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMetauxAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-metaux-acces',
  templateUrl: 'feux-metaux-acces.html'
})
export class FeuxMetauxAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMetauxAccesComponent Component');
    this.text = 'Hello World';
  }

}
