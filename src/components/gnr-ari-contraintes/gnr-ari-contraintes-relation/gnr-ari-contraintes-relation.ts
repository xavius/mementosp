import { Component } from '@angular/core';

/**
 * Generated class for the GnrAriContraintesRelationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-ari-contraintes-relation',
  templateUrl: 'gnr-ari-contraintes-relation.html'
})
export class GnrAriContraintesRelationComponent {

  text: string;

  constructor() {
    console.log('Hello GnrAriContraintesRelationComponent Component');
    this.text = 'Hello World';
  }

}
