import { Component } from '@angular/core';

/**
 * Generated class for the GnrAriContraintesSensorielComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-ari-contraintes-sensoriel',
  templateUrl: 'gnr-ari-contraintes-sensoriel.html'
})
export class GnrAriContraintesSensorielComponent {

  text: string;

  constructor() {
    console.log('Hello GnrAriContraintesSensorielComponent Component');
    this.text = 'Hello World';
  }

}
