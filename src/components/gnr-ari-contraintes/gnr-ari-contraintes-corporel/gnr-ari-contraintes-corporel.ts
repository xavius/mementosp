import { Component } from '@angular/core';

/**
 * Generated class for the GnrAriContraintesCorporelComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-ari-contraintes-corporel',
  templateUrl: 'gnr-ari-contraintes-corporel.html'
})
export class GnrAriContraintesCorporelComponent {

  text: string;

  constructor() {
    console.log('Hello GnrAriContraintesCorporelComponent Component');
    this.text = 'Hello World';
  }

}
