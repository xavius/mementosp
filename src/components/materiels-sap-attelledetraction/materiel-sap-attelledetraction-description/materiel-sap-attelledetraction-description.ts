import { Component } from '@angular/core';

/**
 * Generated class for the MaterielSapAttelledetractionDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiel-sap-attelledetraction-description',
  templateUrl: 'materiel-sap-attelledetraction-description.html'
})
export class MaterielSapAttelledetractionDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielSapAttelledetractionDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
