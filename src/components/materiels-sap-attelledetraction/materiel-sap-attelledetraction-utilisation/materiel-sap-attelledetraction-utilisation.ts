import { Component } from '@angular/core';

/**
 * Generated class for the MaterielSapAttelledetractionUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiel-sap-attelledetraction-utilisation',
  templateUrl: 'materiel-sap-attelledetraction-utilisation.html'
})
export class MaterielSapAttelledetractionUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielSapAttelledetractionUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
