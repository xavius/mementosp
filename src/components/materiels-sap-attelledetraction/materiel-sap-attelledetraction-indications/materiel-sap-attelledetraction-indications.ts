import { Component } from '@angular/core';

/**
 * Generated class for the MaterielSapAttelledetractionIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiel-sap-attelledetraction-indications',
  templateUrl: 'materiel-sap-attelledetraction-indications.html'
})
export class MaterielSapAttelledetractionIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielSapAttelledetractionIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
