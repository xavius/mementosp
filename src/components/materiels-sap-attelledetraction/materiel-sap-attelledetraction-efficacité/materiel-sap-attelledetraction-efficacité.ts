import { Component } from '@angular/core';

/**
 * Generated class for the MaterielSapAttelledetractionEfficacitéComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiel-sap-attelledetraction-efficacité',
  templateUrl: 'materiel-sap-attelledetraction-efficacité.html'
})
export class MaterielSapAttelledetractionEfficacitéComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielSapAttelledetractionEfficacitéComponent Component');
    this.text = 'Hello World';
  }

}
