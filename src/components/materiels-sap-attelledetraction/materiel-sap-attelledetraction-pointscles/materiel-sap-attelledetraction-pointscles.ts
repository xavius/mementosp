import { Component } from '@angular/core';

/**
 * Generated class for the MaterielSapAttelledetractionPointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiel-sap-attelledetraction-pointscles',
  templateUrl: 'materiel-sap-attelledetraction-pointscles.html'
})
export class MaterielSapAttelledetractionPointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielSapAttelledetractionPointsclesComponent Component');
    this.text = 'Hello World';
  }

}
