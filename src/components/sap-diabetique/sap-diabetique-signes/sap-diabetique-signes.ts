import { Component } from '@angular/core';

/**
 * Generated class for the SapDiabetiqueSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-diabetique-signes',
  templateUrl: 'sap-diabetique-signes.html'
})
export class SapDiabetiqueSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapDiabetiqueSignesComponent Component');
    this.text = 'Hello World';
  }

}
