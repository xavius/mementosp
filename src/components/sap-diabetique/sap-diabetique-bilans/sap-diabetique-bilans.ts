import { Component } from '@angular/core';

/**
 * Generated class for the SapDiabetiqueBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-diabetique-bilans',
  templateUrl: 'sap-diabetique-bilans.html'
})
export class SapDiabetiqueBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapDiabetiqueBilansComponent Component');
    this.text = 'Hello World';
  }

}
