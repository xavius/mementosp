import { Component } from '@angular/core';

/**
 * Generated class for the SapDiabetiqueActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-diabetique-actions',
  templateUrl: 'sap-diabetique-actions.html'
})
export class SapDiabetiqueActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapDiabetiqueActionsComponent Component');
    this.text = 'Hello World';
  }

}
