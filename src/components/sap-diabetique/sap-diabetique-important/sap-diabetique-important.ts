import { Component } from '@angular/core';

/**
 * Generated class for the SapDiabetiqueImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-diabetique-important',
  templateUrl: 'sap-diabetique-important.html'
})
export class SapDiabetiqueImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapDiabetiqueImportantComponent Component');
    this.text = 'Hello World';
  }

}
