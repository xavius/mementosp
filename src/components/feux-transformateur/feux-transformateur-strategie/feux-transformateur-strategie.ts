import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTransformateurStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-transformateur-strategie',
  templateUrl: 'feux-transformateur-strategie.html'
})
export class FeuxTransformateurStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTransformateurStrategieComponent Component');
    this.text = 'Hello World';
  }

}
