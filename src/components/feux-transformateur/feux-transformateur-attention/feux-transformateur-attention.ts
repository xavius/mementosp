import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTransformateurAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-transformateur-attention',
  templateUrl: 'feux-transformateur-attention.html'
})
export class FeuxTransformateurAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTransformateurAttentionComponent Component');
    this.text = 'Hello World';
  }

}
