import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTransformateurAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-transformateur-acces',
  templateUrl: 'feux-transformateur-acces.html'
})
export class FeuxTransformateurAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTransformateurAccesComponent Component');
    this.text = 'Hello World';
  }

}
