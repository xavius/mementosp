import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTransformateurActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-transformateur-action',
  templateUrl: 'feux-transformateur-action.html'
})
export class FeuxTransformateurActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTransformateurActionComponent Component');
    this.text = 'Hello World';
  }

}
