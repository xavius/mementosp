import { NgModule, Component } from '@angular/core';
import { FeuxCaveRisquesComponent } from '../components/feux-cave/feux-cave-risques/feux-cave-risques';
import { FeuxCaveStrategieComponent } from '../components/feux-cave/feux-cave-strategie/feux-cave-strategie';
import { FeuxCaveAccesComponent } from '../components/feux-cave/feux-cave-acces/feux-cave-acces';
import { FeuxCaveActionComponent } from '../components/feux-cave/feux-cave-action/feux-cave-action';
import { FeuxCaveAttentionComponent } from '../components/feux-cave/feux-cave-attention/feux-cave-attention';
import { FeuxCaveInterditComponent } from '../components/feux-cave/feux-cave-interdit/feux-cave-interdit';
import { FeuxCombleRisquesComponent } from '../components/feux-comble/feux-comble-risques/feux-comble-risques';
import { FeuxCombleStrategieComponent } from '../components/feux-comble/feux-comble-strategie/feux-comble-strategie';
import { FeuxCombleAccesComponent } from '../components/feux-comble/feux-comble-acces/feux-comble-acces';
import { FeuxCombleActionComponent } from '../components/feux-comble/feux-comble-action/feux-comble-action';
import { FeuxCombleAttentionComponent } from '../components/feux-comble/feux-comble-attention/feux-comble-attention';
import { FeuxCombleInterditComponent } from '../components/feux-comble/feux-comble-interdit/feux-comble-interdit';
import { FeuxTerrasseAccesComponent } from '../components/feux-terrasse/feux-terrasse-acces/feux-terrasse-acces';
import { FeuxTerrasseActionComponent } from '../components/feux-terrasse/feux-terrasse-action/feux-terrasse-action';
import { FeuxTerrasseAttentionComponent } from '../components/feux-terrasse/feux-terrasse-attention/feux-terrasse-attention';
import { FeuxTerrasseInterditComponent } from '../components/feux-terrasse/feux-terrasse-interdit/feux-terrasse-interdit';
import { FeuxTerrasseRisquesComponent } from '../components/feux-terrasse/feux-terrasse-risques/feux-terrasse-risques';
import { FeuxTerrasseStrategieComponent } from '../components/feux-terrasse/feux-terrasse-strategie/feux-terrasse-strategie';
import { FeuxCagedescalierAccesComponent } from '../components/feux-cagedescalier/feux-cagedescalier-acces/feux-cagedescalier-acces';
import { FeuxCagedescalierActionComponent } from '../components/feux-cagedescalier/feux-cagedescalier-action/feux-cagedescalier-action';
import { FeuxCagedescalierAttentionComponent } from '../components/feux-cagedescalier/feux-cagedescalier-attention/feux-cagedescalier-attention';
import { FeuxCagedescalierInterditComponent } from '../components/feux-cagedescalier/feux-cagedescalier-interdit/feux-cagedescalier-interdit';
import { FeuxCagedescalierRisquesComponent } from '../components/feux-cagedescalier/feux-cagedescalier-risques/feux-cagedescalier-risques';
import { FeuxCagedescalierStrategieComponent } from '../components/feux-cagedescalier/feux-cagedescalier-strategie/feux-cagedescalier-strategie';
import { FeuxChemineeAccesComponent } from '../components/feux-cheminee/feux-cheminee-acces/feux-cheminee-acces';
import { FeuxChemineeActionComponent } from '../components/feux-cheminee/feux-cheminee-action/feux-cheminee-action';
import { FeuxChemineeAttentionComponent } from '../components/feux-cheminee/feux-cheminee-attention/feux-cheminee-attention';
import { FeuxChemineeInterditComponent } from '../components/feux-cheminee/feux-cheminee-interdit/feux-cheminee-interdit';
import { FeuxChemineeRisquesComponent } from '../components/feux-cheminee/feux-cheminee-risques/feux-cheminee-risques';
import { FeuxChemineeStrategieComponent } from '../components/feux-cheminee/feux-cheminee-strategie/feux-cheminee-strategie';
import { FeuxPlanchersAccesComponent } from '../components/feux-planchers/feux-planchers-acces/feux-planchers-acces';
import { FeuxPlanchersActionComponent } from '../components/feux-planchers/feux-planchers-action/feux-planchers-action';
import { FeuxPlanchersAttentionComponent } from '../components/feux-planchers/feux-planchers-attention/feux-planchers-attention';
import { FeuxPlanchersInterditComponent } from '../components/feux-planchers/feux-planchers-interdit/feux-planchers-interdit';
import { FeuxPlanchersRisquesComponent } from '../components/feux-planchers/feux-planchers-risques/feux-planchers-risques';
import { FeuxPlanchersStrategieComponent } from '../components/feux-planchers/feux-planchers-strategie/feux-planchers-strategie';
import { FeuxJointdilatationAccesComponent } from '../components/feux-jointdilatation/feux-jointdilatation-acces/feux-jointdilatation-acces';
import { FeuxJointdilatationActionComponent } from '../components/feux-jointdilatation/feux-jointdilatation-action/feux-jointdilatation-action';
import { FeuxJointdilatationAttentionComponent } from '../components/feux-jointdilatation/feux-jointdilatation-attention/feux-jointdilatation-attention';
import { FeuxJointdilatationInterditComponent } from '../components/feux-jointdilatation/feux-jointdilatation-interdit/feux-jointdilatation-interdit';
import { FeuxJointdilatationRisquesComponent } from '../components/feux-jointdilatation/feux-jointdilatation-risques/feux-jointdilatation-risques';
import { FeuxJointdilatationStrategieComponent } from '../components/feux-jointdilatation/feux-jointdilatation-strategie/feux-jointdilatation-strategie';
import { FeuxParkingAccesComponent } from '../components/feux-parking/feux-parking-acces/feux-parking-acces';
import { FeuxParkingActionComponent } from '../components/feux-parking/feux-parking-action/feux-parking-action';
import { FeuxParkingAttentionComponent } from '../components/feux-parking/feux-parking-attention/feux-parking-attention';
import { FeuxParkingInterditComponent } from '../components/feux-parking/feux-parking-interdit/feux-parking-interdit';
import { FeuxParkingRisquesComponent } from '../components/feux-parking/feux-parking-risques/feux-parking-risques';
import { FeuxParkingStrategieComponent } from '../components/feux-parking/feux-parking-strategie/feux-parking-strategie';
import { FeuxMagasinAccesComponent } from '../components/feux-magasin/feux-magasin-acces/feux-magasin-acces';
import { FeuxMagasinActionComponent } from '../components/feux-magasin/feux-magasin-action/feux-magasin-action';
import { FeuxMagasinAttentionComponent } from '../components/feux-magasin/feux-magasin-attention/feux-magasin-attention';
import { FeuxMagasinInterditComponent } from '../components/feux-magasin/feux-magasin-interdit/feux-magasin-interdit';
import { FeuxMagasinRisquesComponent } from '../components/feux-magasin/feux-magasin-risques/feux-magasin-risques';
import { FeuxMagasinStrategieComponent } from '../components/feux-magasin/feux-magasin-strategie/feux-magasin-strategie';
import { FeuxDentrepôtAccesComponent } from '../components/feux-dentrepôt/feux-dentrepôt-acces/feux-dentrepôt-acces';
import { FeuxDentrepôtActionComponent } from '../components/feux-dentrepôt/feux-dentrepôt-action/feux-dentrepôt-action';
import { FeuxDentrepôtAttentionComponent } from '../components/feux-dentrepôt/feux-dentrepôt-attention/feux-dentrepôt-attention';
import { FeuxDentrepôtInterditComponent } from '../components/feux-dentrepôt/feux-dentrepôt-interdit/feux-dentrepôt-interdit';
import { FeuxDentrepôtRisquesComponent } from '../components/feux-dentrepôt/feux-dentrepôt-risques/feux-dentrepôt-risques';
import { FeuxDentrepôtStrategieComponent } from '../components/feux-dentrepôt/feux-dentrepôt-strategie/feux-dentrepôt-strategie';
import { FeuxErpTypeMAccesComponent } from '../components/feux-erp-type-m/feux-erp-type-m-acces/feux-erp-type-m-acces';
import { FeuxErpTypeMActionComponent } from '../components/feux-erp-type-m/feux-erp-type-m-action/feux-erp-type-m-action';
import { FeuxErpTypeMAttentionComponent } from '../components/feux-erp-type-m/feux-erp-type-m-attention/feux-erp-type-m-attention';
import { FeuxErpTypeMInterditComponent } from '../components/feux-erp-type-m/feux-erp-type-m-interdit/feux-erp-type-m-interdit';
import { FeuxErpTypeMRisquesComponent } from '../components/feux-erp-type-m/feux-erp-type-m-risques/feux-erp-type-m-risques';
import { FeuxErpTypeMStrategieComponent } from '../components/feux-erp-type-m/feux-erp-type-m-strategie/feux-erp-type-m-strategie';
import { FeuxErpTypeSvytAccesComponent } from '../components/feux-erp-type-svyt/feux-erp-type-svyt-acces/feux-erp-type-svyt-acces';
import { FeuxErpTypeSvytActionComponent } from '../components/feux-erp-type-svyt/feux-erp-type-svyt-action/feux-erp-type-svyt-action';
import { FeuxErpTypeSvytAttentionComponent } from '../components/feux-erp-type-svyt/feux-erp-type-svyt-attention/feux-erp-type-svyt-attention';
import { FeuxErpTypeSvytInterditComponent } from '../components/feux-erp-type-svyt/feux-erp-type-svyt-interdit/feux-erp-type-svyt-interdit';
import { FeuxErpTypeSvytRisquesComponent } from '../components/feux-erp-type-svyt/feux-erp-type-svyt-risques/feux-erp-type-svyt-risques';
import { FeuxErpTypeSvytStrategieComponent } from '../components/feux-erp-type-svyt/feux-erp-type-svyt-strategie/feux-erp-type-svyt-strategie';
import { FeuxErpTypeJuAccesComponent } from '../components/feux-erp-type-ju/feux-erp-type-ju-acces/feux-erp-type-ju-acces';
import { FeuxErpTypeJuActionComponent } from '../components/feux-erp-type-ju/feux-erp-type-ju-action/feux-erp-type-ju-action';
import { FeuxErpTypeJuAttentionComponent } from '../components/feux-erp-type-ju/feux-erp-type-ju-attention/feux-erp-type-ju-attention';
import { FeuxErpTypeJuInterditComponent } from '../components/feux-erp-type-ju/feux-erp-type-ju-interdit/feux-erp-type-ju-interdit';
import { FeuxErpTypeJuRisquesComponent } from '../components/feux-erp-type-ju/feux-erp-type-ju-risques/feux-erp-type-ju-risques';
import { FeuxErpTypeJuStrategieComponent } from '../components/feux-erp-type-ju/feux-erp-type-ju-strategie/feux-erp-type-ju-strategie';
import { FeuxIghAccesComponent } from '../components/feux-igh/feux-igh-acces/feux-igh-acces';
import { FeuxIghActionComponent } from '../components/feux-igh/feux-igh-action/feux-igh-action';
import { FeuxIghAttentionComponent } from '../components/feux-igh/feux-igh-attention/feux-igh-attention';
import { FeuxIghInterditComponent } from '../components/feux-igh/feux-igh-interdit/feux-igh-interdit';
import { FeuxIghRisquesComponent } from '../components/feux-igh/feux-igh-risques/feux-igh-risques';
import { FeuxIghStrategieComponent } from '../components/feux-igh/feux-igh-strategie/feux-igh-strategie';
import { FeuxFermeAccesComponent } from '../components/feux-ferme/feux-ferme-acces/feux-ferme-acces';
import { FeuxFermeActionComponent } from '../components/feux-ferme/feux-ferme-action/feux-ferme-action';
import { FeuxFermeAttentionComponent } from '../components/feux-ferme/feux-ferme-attention/feux-ferme-attention';
import { FeuxFermeInterditComponent } from '../components/feux-ferme/feux-ferme-interdit/feux-ferme-interdit';
import { FeuxFermeRisquesComponent } from '../components/feux-ferme/feux-ferme-risques/feux-ferme-risques';
import { FeuxFermeStrategieComponent } from '../components/feux-ferme/feux-ferme-strategie/feux-ferme-strategie';
import { FeuxCentreequestreAccesComponent } from '../components/feux-centreequestre/feux-centreequestre-acces/feux-centreequestre-acces';
import { FeuxCentreequestreActionComponent } from '../components/feux-centreequestre/feux-centreequestre-action/feux-centreequestre-action';
import { FeuxCentreequestreAttentionComponent } from '../components/feux-centreequestre/feux-centreequestre-attention/feux-centreequestre-attention';
import { FeuxCentreequestreInterditComponent } from '../components/feux-centreequestre/feux-centreequestre-interdit/feux-centreequestre-interdit';
import { FeuxCentreequestreRisquesComponent } from '../components/feux-centreequestre/feux-centreequestre-risques/feux-centreequestre-risques';
import { FeuxCentreequestreStrategieComponent } from '../components/feux-centreequestre/feux-centreequestre-strategie/feux-centreequestre-strategie';
import { FeuxRecolteAccesComponent } from '../components/feux-recolte/feux-recolte-acces/feux-recolte-acces';
import { FeuxRecolteActionComponent } from '../components/feux-recolte/feux-recolte-action/feux-recolte-action';
import { FeuxRecolteAttentionComponent } from '../components/feux-recolte/feux-recolte-attention/feux-recolte-attention';
import { FeuxRecolteInterditComponent } from '../components/feux-recolte/feux-recolte-interdit/feux-recolte-interdit';
import { FeuxRecolteRisquesComponent } from '../components/feux-recolte/feux-recolte-risques/feux-recolte-risques';
import { FeuxRecolteStrategieComponent } from '../components/feux-recolte/feux-recolte-strategie/feux-recolte-strategie';
import { FeuxDengraisAccesComponent } from '../components/feux-dengrais/feux-dengrais-acces/feux-dengrais-acces';
import { FeuxDengraisActionComponent } from '../components/feux-dengrais/feux-dengrais-action/feux-dengrais-action';
import { FeuxDengraisAttentionComponent } from '../components/feux-dengrais/feux-dengrais-attention/feux-dengrais-attention';
import { FeuxDengraisInterditComponent } from '../components/feux-dengrais/feux-dengrais-interdit/feux-dengrais-interdit';
import { FeuxDengraisRisquesComponent } from '../components/feux-dengrais/feux-dengrais-risques/feux-dengrais-risques';
import { FeuxDengraisStrategieComponent } from '../components/feux-dengrais/feux-dengrais-strategie/feux-dengrais-strategie';
import { FeuxChaufferieAccesComponent } from '../components/feux-chaufferie/feux-chaufferie-acces/feux-chaufferie-acces';
import { FeuxChaufferieActionComponent } from '../components/feux-chaufferie/feux-chaufferie-action/feux-chaufferie-action';
import { FeuxChaufferieAttentionComponent } from '../components/feux-chaufferie/feux-chaufferie-attention/feux-chaufferie-attention';
import { FeuxChaufferieInterditComponent } from '../components/feux-chaufferie/feux-chaufferie-interdit/feux-chaufferie-interdit';
import { FeuxChaufferieRisquesComponent } from '../components/feux-chaufferie/feux-chaufferie-risques/feux-chaufferie-risques';
import { FeuxChaufferieStrategieComponent } from '../components/feux-chaufferie/feux-chaufferie-strategie/feux-chaufferie-strategie';
import { FeuxChaufferieindustrielleAccesComponent } from '../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-acces/feux-chaufferieindustrielle-acces';
import { FeuxChaufferieindustrielleActionComponent } from '../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-action/feux-chaufferieindustrielle-action';
import { FeuxChaufferieindustrielleAttentionComponent } from '../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-attention/feux-chaufferieindustrielle-attention';
import { FeuxChaufferieindustrielleInterditComponent } from '../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-interdit/feux-chaufferieindustrielle-interdit';
import { FeuxChaufferieindustrielleRisquesComponent } from '../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-risques/feux-chaufferieindustrielle-risques';
import { FeuxChaufferieindustrielleStrategieComponent } from '../components/feux-chaufferieindustrielle/feux-chaufferieindustrielle-strategie/feux-chaufferieindustrielle-strategie';
import { FeuxTransformateurAccesComponent } from '../components/feux-transformateur/feux-transformateur-acces/feux-transformateur-acces';
import { FeuxTransformateurActionComponent } from '../components/feux-transformateur/feux-transformateur-action/feux-transformateur-action';
import { FeuxTransformateurAttentionComponent } from '../components/feux-transformateur/feux-transformateur-attention/feux-transformateur-attention';
import { FeuxTransformateurInterditComponent } from '../components/feux-transformateur/feux-transformateur-interdit/feux-transformateur-interdit';
import { FeuxTransformateurRisquesComponent } from '../components/feux-transformateur/feux-transformateur-risques/feux-transformateur-risques';
import { FeuxTransformateurStrategieComponent } from '../components/feux-transformateur/feux-transformateur-strategie/feux-transformateur-strategie';
import { FeuxRadiologiqueAccesComponent } from '../components/feux-radiologique/feux-radiologique-acces/feux-radiologique-acces';
import { FeuxRadiologiqueActionComponent } from '../components/feux-radiologique/feux-radiologique-action/feux-radiologique-action';
import { FeuxRadiologiqueAttentionComponent } from '../components/feux-radiologique/feux-radiologique-attention/feux-radiologique-attention';
import { FeuxRadiologiqueInterditComponent } from '../components/feux-radiologique/feux-radiologique-interdit/feux-radiologique-interdit';
import { FeuxRadiologiqueRisquesComponent } from '../components/feux-radiologique/feux-radiologique-risques/feux-radiologique-risques';
import { FeuxRadiologiqueStrategieComponent } from '../components/feux-radiologique/feux-radiologique-strategie/feux-radiologique-strategie';
import { FeuxChimiqueAccesComponent } from '../components/feux-chimique/feux-chimique-acces/feux-chimique-acces';
import { FeuxChimiqueActionComponent } from '../components/feux-chimique/feux-chimique-action/feux-chimique-action';
import { FeuxChimiqueAttentionComponent } from '../components/feux-chimique/feux-chimique-attention/feux-chimique-attention';
import { FeuxChimiqueInterditComponent } from '../components/feux-chimique/feux-chimique-interdit/feux-chimique-interdit';
import { FeuxChimiqueRisquesComponent } from '../components/feux-chimique/feux-chimique-risques/feux-chimique-risques';
import { FeuxChimiqueStrategieComponent } from '../components/feux-chimique/feux-chimique-strategie/feux-chimique-strategie';
import { FeuxMatieresplastiquesAccesComponent } from '../components/feux-matieresplastiques/feux-matieresplastiques-acces/feux-matieresplastiques-acces';
import { FeuxMatieresplastiquesActionComponent } from '../components/feux-matieresplastiques/feux-matieresplastiques-action/feux-matieresplastiques-action';
import { FeuxMatieresplastiquesAttentionComponent } from '../components/feux-matieresplastiques/feux-matieresplastiques-attention/feux-matieresplastiques-attention';
import { FeuxMatieresplastiquesInterditComponent } from '../components/feux-matieresplastiques/feux-matieresplastiques-interdit/feux-matieresplastiques-interdit';
import { FeuxMatieresplastiquesRisquesComponent } from '../components/feux-matieresplastiques/feux-matieresplastiques-risques/feux-matieresplastiques-risques';
import { FeuxMatieresplastiquesStrategieComponent } from '../components/feux-matieresplastiques/feux-matieresplastiques-strategie/feux-matieresplastiques-strategie';
import { FeuxPesticidesAccesComponent } from '../components/feux-pesticides/feux-pesticides-acces/feux-pesticides-acces';
import { FeuxPesticidesActionComponent } from '../components/feux-pesticides/feux-pesticides-action/feux-pesticides-action';
import { FeuxPesticidesAttentionComponent } from '../components/feux-pesticides/feux-pesticides-attention/feux-pesticides-attention';
import { FeuxPesticidesInterditComponent } from '../components/feux-pesticides/feux-pesticides-interdit/feux-pesticides-interdit';
import { FeuxPesticidesRisquesComponent } from '../components/feux-pesticides/feux-pesticides-risques/feux-pesticides-risques';
import { FeuxPesticidesStrategieComponent } from '../components/feux-pesticides/feux-pesticides-strategie/feux-pesticides-strategie';
import { FeuxSilosAccesComponent } from '../components/feux-silos/feux-silos-acces/feux-silos-acces';
import { FeuxSilosActionComponent } from '../components/feux-silos/feux-silos-action/feux-silos-action';
import { FeuxSilosAttentionComponent } from '../components/feux-silos/feux-silos-attention/feux-silos-attention';
import { FeuxSilosInterditComponent } from '../components/feux-silos/feux-silos-interdit/feux-silos-interdit';
import { FeuxSilosRisquesComponent } from '../components/feux-silos/feux-silos-risques/feux-silos-risques';
import { FeuxSilosStrategieComponent } from '../components/feux-silos/feux-silos-strategie/feux-silos-strategie';
import { FeuxLiquidesinflammablesAccesComponent } from '../components/feux-liquidesinflammables/feux-liquidesinflammables-acces/feux-liquidesinflammables-acces';
import { FeuxLiquidesinflammablesActionComponent } from '../components/feux-liquidesinflammables/feux-liquidesinflammables-action/feux-liquidesinflammables-action';
import { FeuxLiquidesinflammablesAttentionComponent } from '../components/feux-liquidesinflammables/feux-liquidesinflammables-attention/feux-liquidesinflammables-attention';
import { FeuxLiquidesinflammablesInterditComponent } from '../components/feux-liquidesinflammables/feux-liquidesinflammables-interdit/feux-liquidesinflammables-interdit';
import { FeuxLiquidesinflammablesRisquesComponent } from '../components/feux-liquidesinflammables/feux-liquidesinflammables-risques/feux-liquidesinflammables-risques';
import { FeuxLiquidesinflammablesStrategieComponent } from '../components/feux-liquidesinflammables/feux-liquidesinflammables-strategie/feux-liquidesinflammables-strategie';
import { FeuxMetauxAccesComponent } from '../components/feux-metaux/feux-metaux-acces/feux-metaux-acces';
import { FeuxMetauxActionComponent } from '../components/feux-metaux/feux-metaux-action/feux-metaux-action';
import { FeuxMetauxAttentionComponent } from '../components/feux-metaux/feux-metaux-attention/feux-metaux-attention';
import { FeuxMetauxInterditComponent } from '../components/feux-metaux/feux-metaux-interdit/feux-metaux-interdit';
import { FeuxMetauxRisquesComponent } from '../components/feux-metaux/feux-metaux-risques/feux-metaux-risques';
import { FeuxMetauxStrategieComponent } from '../components/feux-metaux/feux-metaux-strategie/feux-metaux-strategie';
import { FeuxVehiculesAccesComponent } from '../components/feux-vehicules/feux-vehicules-acces/feux-vehicules-acces';
import { FeuxVehiculesActionComponent } from '../components/feux-vehicules/feux-vehicules-action/feux-vehicules-action';
import { FeuxVehiculesAttentionComponent } from '../components/feux-vehicules/feux-vehicules-attention/feux-vehicules-attention';
import { FeuxVehiculesInterditComponent } from '../components/feux-vehicules/feux-vehicules-interdit/feux-vehicules-interdit';
import { FeuxVehiculesRisquesComponent } from '../components/feux-vehicules/feux-vehicules-risques/feux-vehicules-risques';
import { FeuxVehiculesStrategieComponent } from '../components/feux-vehicules/feux-vehicules-strategie/feux-vehicules-strategie';
import { FeuxTrainAccesComponent } from '../components/feux-train/feux-train-acces/feux-train-acces';
import { FeuxTrainActionComponent } from '../components/feux-train/feux-train-action/feux-train-action';
import { FeuxTrainAttentionComponent } from '../components/feux-train/feux-train-attention/feux-train-attention';
import { FeuxTrainInterditComponent } from '../components/feux-train/feux-train-interdit/feux-train-interdit';
import { FeuxTrainRisquesComponent } from '../components/feux-train/feux-train-risques/feux-train-risques';
import { FeuxTrainStrategieComponent } from '../components/feux-train/feux-train-strategie/feux-train-strategie';
import { FeuxBateauAccesComponent } from '../components/feux-bateau/feux-bateau-acces/feux-bateau-acces';
import { FeuxBateauActionComponent } from '../components/feux-bateau/feux-bateau-action/feux-bateau-action';
import { FeuxBateauAttentionComponent } from '../components/feux-bateau/feux-bateau-attention/feux-bateau-attention';
import { FeuxBateauInterditComponent } from '../components/feux-bateau/feux-bateau-interdit/feux-bateau-interdit';
import { FeuxBateauRisquesComponent } from '../components/feux-bateau/feux-bateau-risques/feux-bateau-risques';
import { FeuxBateauStrategieComponent } from '../components/feux-bateau/feux-bateau-strategie/feux-bateau-strategie';
import { FeuxPenicheAccesComponent } from '../components/feux-peniche/feux-peniche-acces/feux-peniche-acces';
import { FeuxPenicheActionComponent } from '../components/feux-peniche/feux-peniche-action/feux-peniche-action';
import { FeuxPenicheAttentionComponent } from '../components/feux-peniche/feux-peniche-attention/feux-peniche-attention';
import { FeuxPenicheInterditComponent } from '../components/feux-peniche/feux-peniche-interdit/feux-peniche-interdit';
import { FeuxPenicheRisquesComponent } from '../components/feux-peniche/feux-peniche-risques/feux-peniche-risques';
import { FeuxPenicheStrategieComponent } from '../components/feux-peniche/feux-peniche-strategie/feux-peniche-strategie';
import { FeuxAeronefAccesComponent } from '../components/feux-aeronef/feux-aeronef-acces/feux-aeronef-acces';
import { FeuxAeronefActionComponent } from '../components/feux-aeronef/feux-aeronef-action/feux-aeronef-action';
import { FeuxAeronefAttentionComponent } from '../components/feux-aeronef/feux-aeronef-attention/feux-aeronef-attention';
import { FeuxAeronefInterditComponent } from '../components/feux-aeronef/feux-aeronef-interdit/feux-aeronef-interdit';
import { FeuxAeronefRisquesComponent } from '../components/feux-aeronef/feux-aeronef-risques/feux-aeronef-risques';
import { FeuxAeronefStrategieComponent } from '../components/feux-aeronef/feux-aeronef-strategie/feux-aeronef-strategie';
import { FeuxTunnelAccesComponent } from '../components/feux-tunnel/feux-tunnel-acces/feux-tunnel-acces';
import { FeuxTunnelActionComponent } from '../components/feux-tunnel/feux-tunnel-action/feux-tunnel-action';
import { FeuxTunnelAttentionComponent } from '../components/feux-tunnel/feux-tunnel-attention/feux-tunnel-attention';
import { FeuxTunnelInterditComponent } from '../components/feux-tunnel/feux-tunnel-interdit/feux-tunnel-interdit';
import { FeuxTunnelRisquesComponent } from '../components/feux-tunnel/feux-tunnel-risques/feux-tunnel-risques';
import { FeuxTunnelStrategieComponent } from '../components/feux-tunnel/feux-tunnel-strategie/feux-tunnel-strategie';
import { FeuxCamionciterneAccesComponent } from '../components/feux-camionciterne/feux-camionciterne-acces/feux-camionciterne-acces';
import { FeuxCamionciterneActionComponent } from '../components/feux-camionciterne/feux-camionciterne-action/feux-camionciterne-action';
import { FeuxCamionciterneAttentionComponent } from '../components/feux-camionciterne/feux-camionciterne-attention/feux-camionciterne-attention';
import { FeuxCamionciterneInterditComponent } from '../components/feux-camionciterne/feux-camionciterne-interdit/feux-camionciterne-interdit';
import { FeuxCamionciterneRisquesComponent } from '../components/feux-camionciterne/feux-camionciterne-risques/feux-camionciterne-risques';
import { FeuxCamionciterneStrategieComponent } from '../components/feux-camionciterne/feux-camionciterne-strategie/feux-camionciterne-strategie';
import { SapAcrSignesComponent } from '../components/sap-acr/sap-acr-signes/sap-acr-signes';
import { SapAcrBilansComponent } from '../components/sap-acr/sap-acr-bilans/sap-acr-bilans';
import { SapAcrActionsComponent } from '../components/sap-acr/sap-acr-actions/sap-acr-actions';
import { SapAcrImportantComponent } from '../components/sap-acr/sap-acr-important/sap-acr-important';
import { SapBilansCirconstancielComponent } from '../components/sap-bilans/sap-bilans-circonstanciel/sap-bilans-circonstanciel';
import { SapBilansVitalComponent } from '../components/sap-bilans/sap-bilans-vital/sap-bilans-vital';
import { SapBilansLesionnelComponent } from '../components/sap-bilans/sap-bilans-lesionnel/sap-bilans-lesionnel';
import { SapBilansFonctionnelComponent } from '../components/sap-bilans/sap-bilans-fonctionnel/sap-bilans-fonctionnel';
import { SapBilansRadioComponent } from '../components/sap-bilans/sap-bilans-radio/sap-bilans-radio';
import { SapBilansEvolutifComponent } from '../components/sap-bilans/sap-bilans-evolutif/sap-bilans-evolutif';
import { SapAvcSignesComponent } from '../components/sap-avc/sap-avc-signes/sap-avc-signes';
import { SapAvcBilansComponent } from '../components/sap-avc/sap-avc-bilans/sap-avc-bilans';
import { SapAvcActionsComponent } from '../components/sap-avc/sap-avc-actions/sap-avc-actions';
import { SapAvcImportantComponent } from '../components/sap-avc/sap-avc-important/sap-avc-important';
import { SapAccouchementSignesComponent } from '../components/sap-accouchement/sap-accouchement-signes/sap-accouchement-signes';
import { SapAccouchementBilansComponent } from '../components/sap-accouchement/sap-accouchement-bilans/sap-accouchement-bilans';
import { SapAccouchementActionsComponent } from '../components/sap-accouchement/sap-accouchement-actions/sap-accouchement-actions';
import { SapAccouchementImportantComponent } from '../components/sap-accouchement/sap-accouchement-important/sap-accouchement-important';
import { SapInfarctusSignesComponent } from '../components/sap-infarctus/sap-infarctus-signes/sap-infarctus-signes';
import { SapInfarctusBilansComponent } from '../components/sap-infarctus/sap-infarctus-bilans/sap-infarctus-bilans';
import { SapInfarctusActionsComponent } from '../components/sap-infarctus/sap-infarctus-actions/sap-infarctus-actions';
import { SapInfarctusImportantComponent } from '../components/sap-infarctus/sap-infarctus-important/sap-infarctus-important';
import { SapBruluresSignesComponent } from '../components/sap-brulure/sap-brulures-signes/sap-brulures-signes';
import { SapBruluresBilansComponent } from '../components/sap-brulure/sap-brulures-bilans/sap-brulures-bilans';
import { SapBruluresActionsComponent } from '../components/sap-brulure/sap-brulures-actions/sap-brulures-actions';
import { SapBruluresImportantComponent } from '../components/sap-brulure/sap-brulures-important/sap-brulures-important';
import { SapConvulsionsSignesComponent } from '../components/sap-convulsions/sap-convulsions-signes/sap-convulsions-signes';
import { SapConvulsionsBilansComponent } from '../components/sap-convulsions/sap-convulsions-bilans/sap-convulsions-bilans';
import { SapConvulsionsActionsComponent } from '../components/sap-convulsions/sap-convulsions-actions/sap-convulsions-actions';
import { SapConvulsionsImportantComponent } from '../components/sap-convulsions/sap-convulsions-important/sap-convulsions-important';
import { SapCrisedasthmesSignesComponent } from '../components/sap-crisedasthmes/sap-crisedasthmes-signes/sap-crisedasthmes-signes';
import { SapCrisedasthmesBilansComponent } from '../components/sap-crisedasthmes/sap-crisedasthmes-bilans/sap-crisedasthmes-bilans';
import { SapCrisedasthmesActionsComponent } from '../components/sap-crisedasthmes/sap-crisedasthmes-actions/sap-crisedasthmes-actions';
import { SapCrisedasthmesImportantComponent } from '../components/sap-crisedasthmes/sap-crisedasthmes-important/sap-crisedasthmes-important';
import { SapDesincarserationSignesComponent } from '../components/sap-desincarseration/sap-desincarseration-signes/sap-desincarseration-signes';
import { SapDesincarserationBilansComponent } from '../components/sap-desincarseration/sap-desincarseration-bilans/sap-desincarseration-bilans';
import { SapDesincarserationActionsComponent } from '../components/sap-desincarseration/sap-desincarseration-actions/sap-desincarseration-actions';
import { SapDesincarserationImportantComponent } from '../components/sap-desincarseration/sap-desincarseration-important/sap-desincarseration-important';
import { SapDiabetiqueSignesComponent } from '../components/sap-diabetique/sap-diabetique-signes/sap-diabetique-signes';
import { SapDiabetiqueBilansComponent } from '../components/sap-diabetique/sap-diabetique-bilans/sap-diabetique-bilans';
import { SapDiabetiqueActionsComponent } from '../components/sap-diabetique/sap-diabetique-actions/sap-diabetique-actions';
import { SapDiabetiqueImportantComponent } from '../components/sap-diabetique/sap-diabetique-important/sap-diabetique-important';
import { SapEcrasementSignesComponent } from '../components/sap-ecrasement/sap-ecrasement-signes/sap-ecrasement-signes';
import { SapEcrasementBilansComponent } from '../components/sap-ecrasement/sap-ecrasement-bilans/sap-ecrasement-bilans';
import { SapEcrasementActionsComponent } from '../components/sap-ecrasement/sap-ecrasement-actions/sap-ecrasement-actions';
import { SapEcrasementImportantComponent } from '../components/sap-ecrasement/sap-ecrasement-important/sap-ecrasement-important';
import { SapElectrisationSignesComponent } from '../components/sap-electrisation/sap-electrisation-signes/sap-electrisation-signes';
import { SapElectrisationBilansComponent } from '../components/sap-electrisation/sap-electrisation-bilans/sap-electrisation-bilans';
import { SapElectrisationActionsComponent } from '../components/sap-electrisation/sap-electrisation-actions/sap-electrisation-actions';
import { SapElectrisationImportantComponent } from '../components/sap-electrisation/sap-electrisation-important/sap-electrisation-important';
import { SapEpilepsieSignesComponent } from '../components/sap-epilepsie/sap-epilepsie-signes/sap-epilepsie-signes';
import { SapEpilepsieBilansComponent } from '../components/sap-epilepsie/sap-epilepsie-bilans/sap-epilepsie-bilans';
import { SapEpilepsieActionsComponent } from '../components/sap-epilepsie/sap-epilepsie-actions/sap-epilepsie-actions';
import { SapEpilepsieImportantComponent } from '../components/sap-epilepsie/sap-epilepsie-important/sap-epilepsie-important';
import { SapHemorragieSignesComponent } from '../components/sap-hemorragie/sap-hemorragie-signes/sap-hemorragie-signes';
import { SapHemorragieBilansComponent } from '../components/sap-hemorragie/sap-hemorragie-bilans/sap-hemorragie-bilans';
import { SapHemorragieActionsComponent } from '../components/sap-hemorragie/sap-hemorragie-actions/sap-hemorragie-actions';
import { SapHemorragieImportantComponent } from '../components/sap-hemorragie/sap-hemorragie-important/sap-hemorragie-important';
import { SapIntoxicationcoSignesComponent } from '../components/sap-intoxicationco/sap-intoxicationco-signes/sap-intoxicationco-signes';
import { SapIntoxicationcoBilansComponent } from '../components/sap-intoxicationco/sap-intoxicationco-bilans/sap-intoxicationco-bilans';
import { SapIntoxicationcoActionsComponent } from '../components/sap-intoxicationco/sap-intoxicationco-actions/sap-intoxicationco-actions';
import { SapIntoxicationcoImportantComponent } from '../components/sap-intoxicationco/sap-intoxicationco-important/sap-intoxicationco-important';
import { SapIntoxicationmedSignesComponent } from '../components/sap-intoxicationmed/sap-intoxicationmed-signes/sap-intoxicationmed-signes';
import { SapIntoxicationmedBilansComponent } from '../components/sap-intoxicationmed/sap-intoxicationmed-bilans/sap-intoxicationmed-bilans';
import { SapIntoxicationmedActionsComponent } from '../components/sap-intoxicationmed/sap-intoxicationmed-actions/sap-intoxicationmed-actions';
import { SapIntoxicationmedImportantComponent } from '../components/sap-intoxicationmed/sap-intoxicationmed-important/sap-intoxicationmed-important';
import { SapIvresseSignesComponent } from '../components/sap-ivresse/sap-ivresse-signes/sap-ivresse-signes';
import { SapIvresseBilansComponent } from '../components/sap-ivresse/sap-ivresse-bilans/sap-ivresse-bilans';
import { SapIvresseActionsComponent } from '../components/sap-ivresse/sap-ivresse-actions/sap-ivresse-actions';
import { SapIvresseImportantComponent } from '../components/sap-ivresse/sap-ivresse-important/sap-ivresse-important';
import { SapMalaisesSignesComponent } from '../components/sap-malaises/sap-malaises-signes/sap-malaises-signes';
import { SapMalaisesBilansComponent } from '../components/sap-malaises/sap-malaises-bilans/sap-malaises-bilans';
import { SapMalaisesActionsComponent } from '../components/sap-malaises/sap-malaises-actions/sap-malaises-actions';
import { SapMalaisesImportantComponent } from '../components/sap-malaises/sap-malaises-important/sap-malaises-important';
import { SapNoyadeSignesComponent } from '../components/sap-noyade/sap-noyade-signes/sap-noyade-signes';
import { SapNoyadeBilansComponent } from '../components/sap-noyade/sap-noyade-bilans/sap-noyade-bilans';
import { SapNoyadeActionsComponent } from '../components/sap-noyade/sap-noyade-actions/sap-noyade-actions';
import { SapNoyadeImportantComponent } from '../components/sap-noyade/sap-noyade-important/sap-noyade-important';
import { SapOedemepoumonSignesComponent } from '../components/sap-oedemepoumon/sap-oedemepoumon-signes/sap-oedemepoumon-signes';
import { SapOedemepoumonBilansComponent } from '../components/sap-oedemepoumon/sap-oedemepoumon-bilans/sap-oedemepoumon-bilans';
import { SapOedemepoumonActionsComponent } from '../components/sap-oedemepoumon/sap-oedemepoumon-actions/sap-oedemepoumon-actions';
import { SapOedemepoumonImportantComponent } from '../components/sap-oedemepoumon/sap-oedemepoumon-important/sap-oedemepoumon-important';
import { SapOedemedequinckeSignesComponent } from '../components/sap-oedemedequincke/sap-oedemedequincke-signes/sap-oedemedequincke-signes';
import { SapOedemedequinckeBilansComponent } from '../components/sap-oedemedequincke/sap-oedemedequincke-bilans/sap-oedemedequincke-bilans';
import { SapOedemedequinckeActionsComponent } from '../components/sap-oedemedequincke/sap-oedemedequincke-actions/sap-oedemedequincke-actions';
import { SapOedemedequinckeImportantComponent } from '../components/sap-oedemedequincke/sap-oedemedequincke-important/sap-oedemedequincke-important';
import { SapPendaisonSignesComponent } from '../components/sap-pendaison/sap-pendaison-signes/sap-pendaison-signes';
import { SapPendaisonBilansComponent } from '../components/sap-pendaison/sap-pendaison-bilans/sap-pendaison-bilans';
import { SapPendaisonActionsComponent } from '../components/sap-pendaison/sap-pendaison-actions/sap-pendaison-actions';
import { SapPendaisonImportantComponent } from '../components/sap-pendaison/sap-pendaison-important/sap-pendaison-important';
import { SapPiqureshymenopteresSignesComponent } from '../components/sap-piqureshymenopteres/sap-piqureshymenopteres-signes/sap-piqureshymenopteres-signes';
import { SapPiqureshymenopteresBilansComponent } from '../components/sap-piqureshymenopteres/sap-piqureshymenopteres-bilans/sap-piqureshymenopteres-bilans';
import { SapPiqureshymenopteresActionsComponent } from '../components/sap-piqureshymenopteres/sap-piqureshymenopteres-actions/sap-piqureshymenopteres-actions';
import { SapPiqureshymenopteresImportantComponent } from '../components/sap-piqureshymenopteres/sap-piqureshymenopteres-important/sap-piqureshymenopteres-important';
import { SapPlaiesSignesComponent } from '../components/sap-plaies/sap-plaies-signes/sap-plaies-signes';
import { SapPlaiesBilansComponent } from '../components/sap-plaies/sap-plaies-bilans/sap-plaies-bilans';
import { SapPlaiesActionsComponent } from '../components/sap-plaies/sap-plaies-actions/sap-plaies-actions';
import { SapPlaiesImportantComponent } from '../components/sap-plaies/sap-plaies-important/sap-plaies-important';
import { SapPolytraumatiseSignesComponent } from '../components/sap-polytraumatise/sap-polytraumatise-signes/sap-polytraumatise-signes';
import { SapPolytraumatiseBilansComponent } from '../components/sap-polytraumatise/sap-polytraumatise-bilans/sap-polytraumatise-bilans';
import { SapPolytraumatiseActionsComponent } from '../components/sap-polytraumatise/sap-polytraumatise-actions/sap-polytraumatise-actions';
import { SapPolytraumatiseImportantComponent } from '../components/sap-polytraumatise/sap-polytraumatise-important/sap-polytraumatise-important';
import { SapTetaniespasmophilieSignesComponent } from '../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-signes/sap-tetaniespasmophilie-signes';
import { SapTetaniespasmophilieBilansComponent } from '../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-bilans/sap-tetaniespasmophilie-bilans';
import { SapTetaniespasmophilieActionsComponent } from '../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-actions/sap-tetaniespasmophilie-actions';
import { SapTetaniespasmophilieImportantComponent } from '../components/sap-tetaniespasmophilie/sap-tetaniespasmophilie-important/sap-tetaniespasmophilie-important';
import { SapTraumatismecolonnevertebraleSignesComponent } from '../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-signes/sap-traumatismecolonnevertebrale-signes';
import { SapTraumatismecolonnevertebraleBilansComponent } from '../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-bilans/sap-traumatismecolonnevertebrale-bilans';
import { SapTraumatismecolonnevertebraleActionsComponent } from '../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-actions/sap-traumatismecolonnevertebrale-actions';
import { SapTraumatismecolonnevertebraleImportantComponent } from '../components/sap-traumatismecolonnevertebrale/sap-traumatismecolonnevertebrale-important/sap-traumatismecolonnevertebrale-important';
import { SapTraumatismecranienSignesComponent } from '../components/sap-traumatismecranien/sap-traumatismecranien-signes/sap-traumatismecranien-signes';
import { SapTraumatismecranienBilansComponent } from '../components/sap-traumatismecranien/sap-traumatismecranien-bilans/sap-traumatismecranien-bilans';
import { SapTraumatismecranienActionsComponent } from '../components/sap-traumatismecranien/sap-traumatismecranien-actions/sap-traumatismecranien-actions';
import { SapTraumatismecranienImportantComponent } from '../components/sap-traumatismecranien/sap-traumatismecranien-important/sap-traumatismecranien-important';
import { SapTraumatismedesmembresSignesComponent } from '../components/sap-traumatismedesmembres/sap-traumatismedesmembres-signes/sap-traumatismedesmembres-signes';
import { SapTraumatismedesmembresBilansComponent } from '../components/sap-traumatismedesmembres/sap-traumatismedesmembres-bilans/sap-traumatismedesmembres-bilans';
import { SapTraumatismedesmembresActionsComponent } from '../components/sap-traumatismedesmembres/sap-traumatismedesmembres-actions/sap-traumatismedesmembres-actions';
import { SapTraumatismedesmembresImportantComponent } from '../components/sap-traumatismedesmembres/sap-traumatismedesmembres-important/sap-traumatismedesmembres-important';
import { SrPrincipesAccidentComponent } from '../components/sr-principes/sr-principes-accident/sr-principes-accident';
import { SrPrincipesRecoComponent } from '../components/sr-principes/sr-principes-reco/sr-principes-reco';
import { SrPrincipesIncComponent } from '../components/sr-principes/sr-principes-inc/sr-principes-inc';
import { SrPrincipesDecoupeComponent } from '../components/sr-principes/sr-principes-decoupe/sr-principes-decoupe';
import { SrPrincipesHybridesComponent } from '../components/sr-principes/sr-principes-hybrides/sr-principes-hybrides';
import { SrCalage_1Component } from '../components/sr-calage/sr-calage-1/sr-calage-1';
import { SrCalage_2Component } from '../components/sr-calage/sr-calage-2/sr-calage-2';
import { SrCalage_3Component } from '../components/sr-calage/sr-calage-3/sr-calage-3';
import { SrCalage_4Component } from '../components/sr-calage/sr-calage-4/sr-calage-4';
import { SrCalage_5Component } from '../components/sr-calage/sr-calage-5/sr-calage-5';
import { SrOuverturedeporte_1Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-1/sr-ouverturedeporte-1';
import { SrOuverturedeporte_2Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-2/sr-ouverturedeporte-2';
import { SrOuverturedeporte_3Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-3/sr-ouverturedeporte-3';
import { SrOuverturedeporte_4Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-4/sr-ouverturedeporte-4';
import { SrOuverturedeporte_5Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-5/sr-ouverturedeporte-5';
import { SrOuverturedeporte_6Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-6/sr-ouverturedeporte-6';
import { SrOuverturedeporte_7Component } from '../components/sr-ouverturedeporte/sr-ouverturedeporte-7/sr-ouverturedeporte-7';
import { SrOuverturelaterale_1Component } from '../components/sr-ouverturelaterale/sr-ouverturelaterale-1/sr-ouverturelaterale-1';
import { SrOuverturelaterale_2Component } from '../components/sr-ouverturelaterale/sr-ouverturelaterale-2/sr-ouverturelaterale-2';
import { SrOuverturelaterale_3Component } from '../components/sr-ouverturelaterale/sr-ouverturelaterale-3/sr-ouverturelaterale-3';
import { SrDepavillonnage_1Component } from '../components/sr-depavillonnage/sr-depavillonnage-1/sr-depavillonnage-1';
import { SrDepavillonnage_2Component } from '../components/sr-depavillonnage/sr-depavillonnage-2/sr-depavillonnage-2';
import { SrDepavillonnage_3Component } from '../components/sr-depavillonnage/sr-depavillonnage-3/sr-depavillonnage-3';
import { SrDemipavillonavant_1Component } from '../components/sr-demipavillonavant/sr-demipavillonavant-1/sr-demipavillonavant-1';
import { SrDemipavillonavant_2Component } from '../components/sr-demipavillonavant/sr-demipavillonavant-2/sr-demipavillonavant-2';
import { SrDemipavillonavant_3Component } from '../components/sr-demipavillonavant/sr-demipavillonavant-3/sr-demipavillonavant-3';
import { SrDemipavillonarriere_1Component } from '../components/sr-demipavillonarriere/sr-demipavillonarriere-1/sr-demipavillonarriere-1';
import { SrDemipavillonarriere_2Component } from '../components/sr-demipavillonarriere/sr-demipavillonarriere-2/sr-demipavillonarriere-2';
import { SrDemipavillonarriere_3Component } from '../components/sr-demipavillonarriere/sr-demipavillonarriere-3/sr-demipavillonarriere-3';
import { SrCharniere_1Component } from '../components/sr-charniere/sr-charniere-1/sr-charniere-1';
import { SrCharniere_2Component } from '../components/sr-charniere/sr-charniere-2/sr-charniere-2';
import { SrCharniere_3Component } from '../components/sr-charniere/sr-charniere-3/sr-charniere-3';
import { SrCoquilledhuitre_1Component } from '../components/sr-coquilledhuitre/sr-coquilledhuitre-1/sr-coquilledhuitre-1';
import { SrCoquilledhuitre_2Component } from '../components/sr-coquilledhuitre/sr-coquilledhuitre-2/sr-coquilledhuitre-2';
import { SrCoquilledhuitre_3Component } from '../components/sr-coquilledhuitre/sr-coquilledhuitre-3/sr-coquilledhuitre-3';
import { SrCoquilledhuitre_4Component } from '../components/sr-coquilledhuitre/sr-coquilledhuitre-4/sr-coquilledhuitre-4';
import { SrPortefeuille_1Component } from '../components/sr-portefeuille/sr-portefeuille-1/sr-portefeuille-1';
import { SrPortefeuille_2Component } from '../components/sr-portefeuille/sr-portefeuille-2/sr-portefeuille-2';
import { SrPortefeuille_3Component } from '../components/sr-portefeuille/sr-portefeuille-3/sr-portefeuille-3';
import { SrRelevagetableaudebord_1Component } from '../components/sr-relevagetableaudebord/sr-relevagetableaudebord-1/sr-relevagetableaudebord-1';
import { SrRelevagetableaudebord_2Component } from '../components/sr-relevagetableaudebord/sr-relevagetableaudebord-2/sr-relevagetableaudebord-2';
import { SrRelevagetableaudebord_3Component } from '../components/sr-relevagetableaudebord/sr-relevagetableaudebord-3/sr-relevagetableaudebord-3';
import { GocCadredordrePatracdrComponent } from '../components/goc-cadredordre/goc-cadredordre-patracdr/goc-cadredordre-patracdr';
import { GocCadredordreDpifComponent } from '../components/goc-cadredordre/goc-cadredordre-dpif/goc-cadredordre-dpif';
import { GocCadredordreSmesComponent } from '../components/goc-cadredordre/goc-cadredordre-smes/goc-cadredordre-smes';
import { GocCadredordreSoiecComponent } from '../components/goc-cadredordre/goc-cadredordre-soiec/goc-cadredordre-soiec';
import { GocMessageradioAmbianceComponent } from '../components/goc-messageradio/goc-messageradio-ambiance/goc-messageradio-ambiance';
import { GocMessageradioRenseignementComponent } from '../components/goc-messageradio/goc-messageradio-renseignement/goc-messageradio-renseignement';
import { GocFonctiondupcCrmComponent } from '../components/goc-fonctiondupc/goc-fonctiondupc-crm/goc-fonctiondupc-crm';
import { GocFonctiondupcTransComponent } from '../components/goc-fonctiondupc/goc-fonctiondupc-trans/goc-fonctiondupc-trans';
import { GocFonctiondupcRensComponent } from '../components/goc-fonctiondupc/goc-fonctiondupc-rens/goc-fonctiondupc-rens';
import { PrevClassementdesbatimentsErpComponent } from '../components/prev-classementdesbatiments/prev-classementdesbatiments-erp/prev-classementdesbatiments-erp';
import { PrevClassementdesbatimentsIghComponent } from '../components/prev-classementdesbatiments/prev-classementdesbatiments-igh/prev-classementdesbatiments-igh';
import { PrevClassementdesbatimentsHabitationComponent } from '../components/prev-classementdesbatiments/prev-classementdesbatiments-habitation/prev-classementdesbatiments-habitation';
import { PrevClassementdesbatimentsTravailComponent } from '../components/prev-classementdesbatiments/prev-classementdesbatiments-travail/prev-classementdesbatiments-travail';
import { PrevVoiesenginsetechellesFonctionnementComponent } from '../components/prev-voiesenginsetechelles/prev-voiesenginsetechelles-fonctionnement/prev-voiesenginsetechelles-fonctionnement';
import { PrevVoiesenginsetechellesPrecautionComponent } from '../components/prev-voiesenginsetechelles/prev-voiesenginsetechelles-precaution/prev-voiesenginsetechelles-precaution';
import { PrevFacadesFonctionnementComponent } from '../components/prev-facades/prev-facades-fonctionnement/prev-facades-fonctionnement';
import { PrevFacadesPrecautionComponent } from '../components/prev-facades/prev-facades-precaution/prev-facades-precaution';
import { PrevGrosetsecondoeuvreFonctionnementComponent } from '../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-fonctionnement/prev-grosetsecondoeuvre-fonctionnement';
import { PrevGrosetsecondoeuvrePrecautionComponent } from '../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-precaution/prev-grosetsecondoeuvre-precaution';
import { PrevDegagementsaccesFonctionnementComponent } from '../components/prev-degagementsacces/prev-degagementsacces-fonctionnement/prev-degagementsacces-fonctionnement';
import { PrevDegagementsaccesPrecautionComponent } from '../components/prev-degagementsacces/prev-degagementsacces-precaution/prev-degagementsacces-precaution';
import { PrevLocauxarisquesFonctionnementComponent } from '../components/prev-locauxarisques/prev-locauxarisques-fonctionnement/prev-locauxarisques-fonctionnement';
import { PrevLocauxarisquesPrecautionComponent } from '../components/prev-locauxarisques/prev-locauxarisques-precaution/prev-locauxarisques-precaution';
import { PrevDesenfumageFonctionnementComponent } from '../components/prev-desenfumage/prev-desenfumage-fonctionnement/prev-desenfumage-fonctionnement';
import { PrevDesenfumagePrecautionComponent } from '../components/prev-desenfumage/prev-desenfumage-precaution/prev-desenfumage-precaution';
import { PrevMoyensdextinctionFonctionnementComponent } from '../components/prev-moyensdextinction/prev-moyensdextinction-fonctionnement/prev-moyensdextinction-fonctionnement';
import { PrevMoyensdextinctionPrecautionComponent } from '../components/prev-moyensdextinction/prev-moyensdextinction-precaution/prev-moyensdextinction-precaution';
import { PrevSsiapFonctionnementComponent } from '../components/prev-ssiap/prev-ssiap-fonctionnement/prev-ssiap-fonctionnement';
import { PrevSsiapPrecautionComponent } from '../components/prev-ssiap/prev-ssiap-precaution/prev-ssiap-precaution';
import { PrevSsiFonctionnementntComponent } from '../components/prev-ssi/prev-ssi-fonctionnementnt/prev-ssi-fonctionnementnt';
import { PrevSsiPrecautionComponent } from '../components/prev-ssi/prev-ssi-precaution/prev-ssi-precaution';
import { PrevGrosetsecondoeuvreDistributionComponent } from '../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-distribution/prev-grosetsecondoeuvre-distribution';
import { PrevGrosetsecondoeuvreOuvertureComponent } from '../components/prev-grosetsecondoeuvre/prev-grosetsecondoeuvre-ouverture/prev-grosetsecondoeuvre-ouverture';
import { PrevPlansdinterventionBaseComponent } from '../components/prev-plansdintervention/prev-plansdintervention-base/prev-plansdintervention-base';
import { PrevPlansdinterventionEauComponent } from '../components/prev-plansdintervention/prev-plansdintervention-eau/prev-plansdintervention-eau';
import { PrevPlansdinterventionIncComponent } from '../components/prev-plansdintervention/prev-plansdintervention-inc/prev-plansdintervention-inc';
import { PrevPlansdinterventionTechComponent } from '../components/prev-plansdintervention/prev-plansdintervention-tech/prev-plansdintervention-tech';
import { GnrAriReglesdebase_1Component } from '../components/gnr-ari-reglesdebase/gnr-ari-reglesdebase-1/gnr-ari-reglesdebase-1';
import { GnrAriReglesdebase_2Component } from '../components/gnr-ari-reglesdebase/gnr-ari-reglesdebase-2/gnr-ari-reglesdebase-2';
import { GnrAriReglesdebase_3Component } from '../components/gnr-ari-reglesdebase/gnr-ari-reglesdebase-3/gnr-ari-reglesdebase-3';
import { GnrAriContraintesCorporelComponent } from '../components/gnr-ari-contraintes/gnr-ari-contraintes-corporel/gnr-ari-contraintes-corporel';
import { GnrAriContraintesSensorielComponent } from '../components/gnr-ari-contraintes/gnr-ari-contraintes-sensoriel/gnr-ari-contraintes-sensoriel';
import { GnrAriContraintesRelationComponent } from '../components/gnr-ari-contraintes/gnr-ari-contraintes-relation/gnr-ari-contraintes-relation';
import { GnrLspccAmarrageetnoeudsPointsfixesComponent } from '../components/gnr-lspcc-amarrageetnoeuds/gnr-lspcc-amarrageetnoeuds-pointsfixes/gnr-lspcc-amarrageetnoeuds-pointsfixes';
import { GnrLspccAmarrageetnoeudsNoeudsComponent } from '../components/gnr-lspcc-amarrageetnoeuds/gnr-lspcc-amarrageetnoeuds-noeuds/gnr-lspcc-amarrageetnoeuds-noeuds';
import { GnrLspccOperationReglesdebasesAvantComponent } from '../components/gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases-avant/gnr-lspcc-operation-reglesdebases-avant';
import { GnrLspccOperationReglesdebasesPendantComponent } from '../components/gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases-pendant/gnr-lspcc-operation-reglesdebases-pendant';
import { GnrLspccOperationReglesdebasesApresComponent } from '../components/gnr-lspcc-operation-reglesdebases/gnr-lspcc-operation-reglesdebases-apres/gnr-lspcc-operation-reglesdebases-apres';
import { GnrLspccOperationSauvetageexterieurChefdagresComponent } from '../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-chefdagres/gnr-lspcc-operation-sauvetageexterieur-chefdagres';
import { GnrLspccOperationSauvetageexterieurChefdequipeComponent } from '../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-chefdequipe/gnr-lspcc-operation-sauvetageexterieur-chefdequipe';
import { GnrLspccOperationSauvetageexterieurEquipierComponent } from '../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-equipier/gnr-lspcc-operation-sauvetageexterieur-equipier';
import { GnrLspccOperationSauvetageexterieur_2ebinomeComponent } from '../components/gnr-lspcc-operation-sauvetageexterieur/gnr-lspcc-operation-sauvetageexterieur-2ebinome/gnr-lspcc-operation-sauvetageexterieur-2ebinome';
import { GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent } from '../components/gnr-lspcc-operation-sauvetageexcavation-exploration/gnr-lspcc-operation-sauvetageexcavation-exploration-chefdequipe/gnr-lspcc-operation-sauvetageexcavation-exploration-chefdequipe';
import { GnrLspccOperationSauvetageexcavationExplorationEquipierComponent } from '../components/gnr-lspcc-operation-sauvetageexcavation-exploration/gnr-lspcc-operation-sauvetageexcavation-exploration-equipier/gnr-lspcc-operation-sauvetageexcavation-exploration-equipier';
import { GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent } from '../components/gnr-lspcc-operation-sauvetageexcavation-remonte/gnr-lspcc-operation-sauvetageexcavation-remonte-chefdagres/gnr-lspcc-operation-sauvetageexcavation-remonte-chefdagres';
import { GnrLspccOperationSauvetageexcavationRemonteEquipierComponent } from '../components/gnr-lspcc-operation-sauvetageexcavation-remonte/gnr-lspcc-operation-sauvetageexcavation-remonte-equipier/gnr-lspcc-operation-sauvetageexcavation-remonte-equipier';
import { GnrLspccOperationProtectionchutesChefdagresComponent } from '../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-chefdagres/gnr-lspcc-operation-protectionchutes-chefdagres';
import { GnrLspccOperationProtectionchutesChefdequipeComponent } from '../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-chefdequipe/gnr-lspcc-operation-protectionchutes-chefdequipe';
import { GnrLspccOperationProtectionchutesEquipierComponent } from '../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-equipier/gnr-lspcc-operation-protectionchutes-equipier';
import { GnrLspccOperationProtectionchutes_2ebinomeComponent } from '../components/gnr-lspcc-operation-protectionchutes/gnr-lspcc-operation-protectionchutes-2ebinome/gnr-lspcc-operation-protectionchutes-2ebinome';
import { GnrLspccOperationProtectionfixehumainChefdagresComponent } from '../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-chefdagres/gnr-lspcc-operation-protectionfixehumain-chefdagres';
import { GnrLspccOperationProtectionfixehumainChefdequipeComponent } from '../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-chefdequipe/gnr-lspcc-operation-protectionfixehumain-chefdequipe';
import { GnrLspccOperationProtectionfixehumainEquipierComponent } from '../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-equipier/gnr-lspcc-operation-protectionfixehumain-equipier';
import { GnrLspccOperationProtectionfixehumain_2ebinomeComponent } from '../components/gnr-lspcc-operation-protectionfixehumain/gnr-lspcc-operation-protectionfixehumain-2ebinome/gnr-lspcc-operation-protectionfixehumain-2ebinome';
import { GnretablissementlancesManoeuvresGnrM1ChefdagresComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-chefdagres/gnretablissementlances-manoeuvres-gnr-m1-chefdagres';
import { GnretablissementlancesManoeuvresGnrM1ChefdequipeComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-chefdequipe/gnretablissementlances-manoeuvres-gnr-m1-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM1EquipierComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-equipier/gnretablissementlances-manoeuvres-gnr-m1-equipier';
import { GnretablissementlancesManoeuvresGnrM1BalComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m1/gnretablissementlances-manoeuvres-gnr-m1-bal/gnretablissementlances-manoeuvres-gnr-m1-bal';
import { GnretablissementlancesManoeuvresGnrM2ChefdagresComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2-chefdagres/gnretablissementlances-manoeuvres-gnr-m2-chefdagres';
import { GnretablissementlancesManoeuvresGnrM2ChefdequipeComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2-chefdequipe/gnretablissementlances-manoeuvres-gnr-m2-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM2EquipierComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m2/gnretablissementlances-manoeuvres-gnr-m2-equipier/gnretablissementlances-manoeuvres-gnr-m2-equipier';
import { GnretablissementlancesManoeuvresGnrM3ChefdagresComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3-chefdagres/gnretablissementlances-manoeuvres-gnr-m3-chefdagres';
import { GnretablissementlancesManoeuvresGnrM3ChefdequipeComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3-chefdequipe/gnretablissementlances-manoeuvres-gnr-m3-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM3EquipierComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m3/gnretablissementlances-manoeuvres-gnr-m3-equipier/gnretablissementlances-manoeuvres-gnr-m3-equipier';
import { GnretablissementlancesManoeuvresGnrM4ChefdagresComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4-chefdagres/gnretablissementlances-manoeuvres-gnr-m4-chefdagres';
import { GnretablissementlancesManoeuvresGnrM4ChefdequipeComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4-chefdequipe/gnretablissementlances-manoeuvres-gnr-m4-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM4EquipierComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m4/gnretablissementlances-manoeuvres-gnr-m4-equipier/gnretablissementlances-manoeuvres-gnr-m4-equipier';
import { GnretablissementlancesManoeuvresGnrM5ChefdagresComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-chefdagres/gnretablissementlances-manoeuvres-gnr-m5-chefdagres';
import { GnretablissementlancesManoeuvresGnrM5ChefdequipeComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-chefdequipe/gnretablissementlances-manoeuvres-gnr-m5-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM5EquipierComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-equipier/gnretablissementlances-manoeuvres-gnr-m5-equipier';
import { GnretablissementlancesManoeuvresGnrM5BalComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m5/gnretablissementlances-manoeuvres-gnr-m5-bal/gnretablissementlances-manoeuvres-gnr-m5-bal';
import { GnretablissementlancesManoeuvresGnrM6ChefdagresComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6-chefdagres/gnretablissementlances-manoeuvres-gnr-m6-chefdagres';
import { GnretablissementlancesManoeuvresGnrM6ChefdequipeComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6-chefdequipe/gnretablissementlances-manoeuvres-gnr-m6-chefdequipe';
import { GnretablissementlancesManoeuvresGnrM6EquipierComponent } from '../components/gnretablissementlances-manoeuvres-gnr-m6/gnretablissementlances-manoeuvres-gnr-m6-equipier/gnretablissementlances-manoeuvres-gnr-m6-equipier';
import { GnretablissementlancesManoeuvrescomplLdvexterieurChefdagresComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdagres/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvexterieurChefdequipeComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdequipe/gnretablissementlances-manoeuvrescompl-ldvexterieur-chefdequipe';
import { GnretablissementlancesManoeuvrescomplLdvexterieurEquipierComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-equipier/gnretablissementlances-manoeuvrescompl-ldvexterieur-equipier';
import { GnretablissementlancesManoeuvrescomplLdvexterieurBalComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvexterieur/gnretablissementlances-manoeuvrescompl-ldvexterieur-bal/gnretablissementlances-manoeuvrescompl-ldvexterieur-bal';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdagresComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdagres/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdequipeComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdequipe/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-chefdequipe';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseEquipierComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-equipier/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-equipier';
import { GnretablissementlancesManoeuvrescomplLdvechellecoulisseBalComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-bal/gnretablissementlances-manoeuvrescompl-ldvechellecoulisse-bal';
import { GnretablissementlancesManoeuvrescomplLdvcolonneChefdagresComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdagres/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvcolonneChefdequipeComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdequipe/gnretablissementlances-manoeuvrescompl-ldvcolonne-chefdequipe';
import { GnretablissementlancesManoeuvrescomplLdvcolonneEquipierComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-equipier/gnretablissementlances-manoeuvrescompl-ldvcolonne-equipier';
import { GnretablissementlancesManoeuvrescomplLdvcolonneBalComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvcolonne/gnretablissementlances-manoeuvrescompl-ldvcolonne-bal/gnretablissementlances-manoeuvrescompl-ldvcolonne-bal';
import { GnretablissementlancesManoeuvrescomplLdvpoteauChefdagresComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau-chefdagres/gnretablissementlances-manoeuvrescompl-ldvpoteau-chefdagres';
import { GnretablissementlancesManoeuvrescomplLdvpoteauBatComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau-bat/gnretablissementlances-manoeuvrescompl-ldvpoteau-bat';
import { GnretablissementlancesManoeuvrescomplLdvpoteauBalComponent } from '../components/gnretablissementlances-manoeuvrescompl-ldvpoteau/gnretablissementlances-manoeuvrescompl-ldvpoteau-bal/gnretablissementlances-manoeuvrescompl-ldvpoteau-bal';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauChefdagresComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdagres/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauChefdequipeComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvprisedeau-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvprisedeauEquipierComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvprisedeau/gnretablissementlances-tuyauxechevaux-ldvprisedeau-equipier/gnretablissementlances-tuyauxechevaux-ldvprisedeau-equipier';
import { GnretablissementlancesTuyauxechevauxLdvattaqueChefdagresComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdagres/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvattaqueChefdequipeComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvattaque-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvattaqueEquipierComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvattaque/gnretablissementlances-tuyauxechevaux-ldvattaque-equipier/gnretablissementlances-tuyauxechevaux-ldvattaque-equipier';
import { GnretablissementlancesTuyauxechevauxLdvexterieurChefdagresComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdagres/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvexterieurChefdequipeComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvexterieur-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvexterieurEquipierComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-equipier/gnretablissementlances-tuyauxechevaux-ldvexterieur-equipier';
import { GnretablissementlancesTuyauxechevauxLdvexterieurBalComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvexterieur/gnretablissementlances-tuyauxechevaux-ldvexterieur-bal/gnretablissementlances-tuyauxechevaux-ldvexterieur-bal';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdagresComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdagres/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdagres';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdequipeComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdequipe/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-chefdequipe';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseEquipierComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-equipier/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-equipier';
import { GnretablissementlancesTuyauxechevauxLdvechelleacoulisseBalComponent } from '../components/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-bal/gnretablissementlances-tuyauxechevaux-ldvechelleacoulisse-bal';
import { GnretablissementlancesSacdattaqueLdvcommunicationChefdagresComponent } from '../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-chefdagres/gnretablissementlances-sacdattaque-ldvcommunication-chefdagres';
import { GnretablissementlancesSacdattaqueLdvcommunicationChefdequipeComponent } from '../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-chefdequipe/gnretablissementlances-sacdattaque-ldvcommunication-chefdequipe';
import { GnretablissementlancesSacdattaqueLdvcommunicationEquipierComponent } from '../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-equipier/gnretablissementlances-sacdattaque-ldvcommunication-equipier';
import { GnretablissementlancesSacdattaqueLdvcommunicationBalComponent } from '../components/gnretablissementlances-sacdattaque-ldvcommunication/gnretablissementlances-sacdattaque-ldvcommunication-bal/gnretablissementlances-sacdattaque-ldvcommunication-bal';
import { GnretablissementlancesSacdattaqueLdvexterieurChefdadresComponent } from '../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-chefdadres/gnretablissementlances-sacdattaque-ldvexterieur-chefdadres';
import { GnretablissementlancesSacdattaqueLdvexterieurChefdequipeComponent } from '../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-chefdequipe/gnretablissementlances-sacdattaque-ldvexterieur-chefdequipe';
import { GnretablissementlancesSacdattaqueLdvexterieurEquipierComponent } from '../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-equipier/gnretablissementlances-sacdattaque-ldvexterieur-equipier';
import { GnretablissementlancesSacdattaqueLdvexterieurBalComponent } from '../components/gnretablissementlances-sacdattaque-ldvexterieur/gnretablissementlances-sacdattaque-ldvexterieur-bal/gnretablissementlances-sacdattaque-ldvexterieur-bal';
import { GocOutilsgraphiquesSituationComponent } from '../components/goc-outilsgraphiques/goc-outilsgraphiques-situation/goc-outilsgraphiques-situation';
import { GocOutilsgraphiquesActionsComponent } from '../components/goc-outilsgraphiques/goc-outilsgraphiques-actions/goc-outilsgraphiques-actions';
import { GocOutilsgraphiquesMoyensComponent } from '../components/goc-outilsgraphiques/goc-outilsgraphiques-moyens/goc-outilsgraphiques-moyens';
import { GocOutilsgraphiquesCharteComponent } from '../components/goc-outilsgraphiques/goc-outilsgraphiques-charte/goc-outilsgraphiques-charte';
import { MaterielsSapSanglearaigneeDescriptionComponent } from '../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-description/materiels-sap-sanglearaignee-description';
import { MaterielsSapSanglearaigneeIndicationsComponent } from '../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-indications/materiels-sap-sanglearaignee-indications';
import { MaterielsSapSanglearaigneeRisquesComponent } from '../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-risques/materiels-sap-sanglearaignee-risques';
import { MaterielsSapSanglearaigneeUtilisationComponent } from '../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-utilisation/materiels-sap-sanglearaignee-utilisation';
import { MaterielsSapSanglearaigneePointsclesComponent } from '../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-pointscles/materiels-sap-sanglearaignee-pointscles';
import { MaterielsSapSanglearaigneeCriteresdefficaciteComponent } from '../components/materiels-sap-sanglearaignee/materiels-sap-sanglearaignee-criteresdefficacite/materiels-sap-sanglearaignee-criteresdefficacite';
import { MaterielSapAttelledetractionDescriptionComponent } from '../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-description/materiel-sap-attelledetraction-description';
import { MaterielSapAttelledetractionIndicationsComponent } from '../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-indications/materiel-sap-attelledetraction-indications';
import { MaterielSapAttelledetractionRisquesComponent } from '../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-risques/materiel-sap-attelledetraction-risques';
import { MaterielSapAttelledetractionUtilisationComponent } from '../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-utilisation/materiel-sap-attelledetraction-utilisation';
import { MaterielSapAttelledetractionPointsclesComponent } from '../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-pointscles/materiel-sap-attelledetraction-pointscles';
import { MaterielSapAttelledetractionEfficacitéComponent } from '../components/materiels-sap-attelledetraction/materiel-sap-attelledetraction-efficacité/materiel-sap-attelledetraction-efficacité';
import { MaterielsSapAspirateurdemucositesDescriptionComponent } from '../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-description/materiels-sap-aspirateurdemucosites-description';
import { MaterielsSapAspirateurdemucositesIndicationsComponent } from '../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-indications/materiels-sap-aspirateurdemucosites-indications';
import { MaterielsSapAspirateurdemucositesRisquesComponent } from '../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-risques/materiels-sap-aspirateurdemucosites-risques';
import { MaterielsSapAspirateurdemucositesUtilisationComponent } from '../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-utilisation/materiels-sap-aspirateurdemucosites-utilisation';
import { MaterielsSapAspirateurdemucositesPointsclesComponent } from '../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-pointscles/materiels-sap-aspirateurdemucosites-pointscles';
import { MaterielsSapAspirateurdemucositesEfficaciteComponent } from '../components/materiels-sap-aspirateurdemucosites/materiels-sap-aspirateurdemucosites-efficacite/materiels-sap-aspirateurdemucosites-efficacite';
import { MaterielsSapAttellecervicothoraciqueDescriptionComponent } from '../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-description/materiels-sap-attellecervicothoracique-description';
import { MaterielsSapAttellecervicothoraciqueIndicationsComponent } from '../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-indications/materiels-sap-attellecervicothoracique-indications';
import { MaterielsSapAttellecervicothoraciqueRisquesComponent } from '../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-risques/materiels-sap-attellecervicothoracique-risques';
import { MaterielsSapAttellecervicothoraciqueUtilisationComponent } from '../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-utilisation/materiels-sap-attellecervicothoracique-utilisation';
import { MaterielsSapAttellecervicothoraciquePointsclesComponent } from '../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-pointscles/materiels-sap-attellecervicothoracique-pointscles';
import { MaterielsSapAttellecervicothoraciqueEfficaciteComponent } from '../components/materiels-sap-attellecervicothoracique/materiels-sap-attellecervicothoracique-efficacite/materiels-sap-attellecervicothoracique-efficacite';
import { MaterielsSapBrancardcuillereDescriptionComponent } from '../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-description/materiels-sap-brancardcuillere-description';
import { MaterielsSapBrancardcuillereIndicationsComponent } from '../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-indications/materiels-sap-brancardcuillere-indications';
import { MaterielsSapBrancardcuillereRisquesComponent } from '../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-risques/materiels-sap-brancardcuillere-risques';
import { MaterielsSapBrancardcuillereUtilisationComponent } from '../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-utilisation/materiels-sap-brancardcuillere-utilisation';
import { MaterielsSapBrancardcuillerePointsclesComponent } from '../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-pointscles/materiels-sap-brancardcuillere-pointscles';
import { MaterielsSapBrancardcuillereEfficaciteComponent } from '../components/materiels-sap-brancardcuillere/materiels-sap-brancardcuillere-efficacite/materiels-sap-brancardcuillere-efficacite';
import { MaterielsSapDaeDescriptionComponent } from '../components/materiels-sap-dae/materiels-sap-dae-description/materiels-sap-dae-description';
import { MaterielsSapDaeIndicationsComponent } from '../components/materiels-sap-dae/materiels-sap-dae-indications/materiels-sap-dae-indications';
import { MaterielsSapDaeRisquesComponent } from '../components/materiels-sap-dae/materiels-sap-dae-risques/materiels-sap-dae-risques';
import { MaterielsSapDaeUtilisationComponent } from '../components/materiels-sap-dae/materiels-sap-dae-utilisation/materiels-sap-dae-utilisation';
import { MaterielsSapDaePointsclesComponent } from '../components/materiels-sap-dae/materiels-sap-dae-pointscles/materiels-sap-dae-pointscles';
import { MaterielsSapDaeEfficaciteComponent } from '../components/materiels-sap-dae/materiels-sap-dae-efficacite/materiels-sap-dae-efficacite';
import { MaterielsSapGarrotDescriptionComponent } from '../components/materiels-sap-garrot/materiels-sap-garrot-description/materiels-sap-garrot-description';
import { MaterielsSapGarrotIndicationsComponent } from '../components/materiels-sap-garrot/materiels-sap-garrot-indications/materiels-sap-garrot-indications';
import { MaterielsSapGarrotRisquesComponent } from '../components/materiels-sap-garrot/materiels-sap-garrot-risques/materiels-sap-garrot-risques';
import { MaterielsSapGarrotUtilisationComponent } from '../components/materiels-sap-garrot/materiels-sap-garrot-utilisation/materiels-sap-garrot-utilisation';
import { MaterielsSapGarrotPointsclesComponent } from '../components/materiels-sap-garrot/materiels-sap-garrot-pointscles/materiels-sap-garrot-pointscles';
import { MaterielsSapGarrotEfficaciteComponent } from '../components/materiels-sap-garrot/materiels-sap-garrot-efficacite/materiels-sap-garrot-efficacite';
import { MaterielsSapKitaesDescriptionComponent } from '../components/materiels-sap-kitaes/materiels-sap-kitaes-description/materiels-sap-kitaes-description';
import { MaterielsSapKitaesIndicationsComponent } from '../components/materiels-sap-kitaes/materiels-sap-kitaes-indications/materiels-sap-kitaes-indications';
import { MaterielsSapKitaesRisquesComponent } from '../components/materiels-sap-kitaes/materiels-sap-kitaes-risques/materiels-sap-kitaes-risques';
import { MaterielsSapKitaesUtilisationComponent } from '../components/materiels-sap-kitaes/materiels-sap-kitaes-utilisation/materiels-sap-kitaes-utilisation';
import { MaterielsSapKitbruluresDescriptionComponent } from '../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-description/materiels-sap-kitbrulures-description';
import { MaterielsSapKitbruluresIndicationsComponent } from '../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-indications/materiels-sap-kitbrulures-indications';
import { MaterielsSapKitbruluresRisquesComponent } from '../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-risques/materiels-sap-kitbrulures-risques';
import { MaterielsSapKitbruluresUtilisationComponent } from '../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-utilisation/materiels-sap-kitbrulures-utilisation';
import { MaterielsSapKitbruluresPointsclesComponent } from '../components/materiels-sap-kitbrulures/materiels-sap-kitbrulures-pointscles/materiels-sap-kitbrulures-pointscles';
import { MaterielsSapKitsectiondemembresDescriptionComponent } from '../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-description/materiels-sap-kitsectiondemembres-description';
import { MaterielsSapKitsectiondemembresIndicationsComponent } from '../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-indications/materiels-sap-kitsectiondemembres-indications';
import { MaterielsSapKitsectiondemembresRisquesComponent } from '../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-risques/materiels-sap-kitsectiondemembres-risques';
import { MaterielsSapKitsectiondemembresUtilisationComponent } from '../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-utilisation/materiels-sap-kitsectiondemembres-utilisation';
import { MaterielsSapKitsectiondemembresPointsclesComponent } from '../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-pointscles/materiels-sap-kitsectiondemembres-pointscles';
import { MaterielsSapKitsectiondemembresEfficaciteComponent } from '../components/materiels-sap-kitsectiondemembres/materiels-sap-kitsectiondemembres-efficacite/materiels-sap-kitsectiondemembres-efficacite';
import { MaterielsSapKitsinusDescriptionComponent } from '../components/materiels-sap-kitsinus/materiels-sap-kitsinus-description/materiels-sap-kitsinus-description';
import { MaterielsSapKitsinusIndicationsComponent } from '../components/materiels-sap-kitsinus/materiels-sap-kitsinus-indications/materiels-sap-kitsinus-indications';
import { MaterielsSapKitsinusRisquesComponent } from '../components/materiels-sap-kitsinus/materiels-sap-kitsinus-risques/materiels-sap-kitsinus-risques';
import { MaterielsSapKitsinusUtilisationComponent } from '../components/materiels-sap-kitsinus/materiels-sap-kitsinus-utilisation/materiels-sap-kitsinus-utilisation';
import { MaterielsSapLectureglycemieDescriptionComponent } from '../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-description/materiels-sap-lectureglycemie-description';
import { MaterielsSapLectureglycemieIndicationsComponent } from '../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-indications/materiels-sap-lectureglycemie-indications';
import { MaterielsSapLectureglycemieRisquesComponent } from '../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-risques/materiels-sap-lectureglycemie-risques';
import { MaterielsSapLectureglycemieUtilisationComponent } from '../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-utilisation/materiels-sap-lectureglycemie-utilisation';
import { MaterielsSapLectureglycemiePointsclesComponent } from '../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-pointscles/materiels-sap-lectureglycemie-pointscles';
import { MaterielsSapLectureglycemieEfficaciteComponent } from '../components/materiels-sap-lectureglycemie/materiels-sap-lectureglycemie-efficacite/materiels-sap-lectureglycemie-efficacite';
import { MaterielsSapMesurepressionarterielleDescriptionComponent } from '../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-description/materiels-sap-mesurepressionarterielle-description';
import { MaterielsSapMesurepressionarterielleIndicationsComponent } from '../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-indications/materiels-sap-mesurepressionarterielle-indications';
import { MaterielsSapMesurepressionarterielleRisquesComponent } from '../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-risques/materiels-sap-mesurepressionarterielle-risques';
import { MaterielsSapMesurepressionarterielleUtilisationComponent } from '../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-utilisation/materiels-sap-mesurepressionarterielle-utilisation';
import { MaterielsSapMesurepressionarteriellePointsclesComponent } from '../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-pointscles/materiels-sap-mesurepressionarterielle-pointscles';
import { MaterielsSapMesurepressionarterielleEfficaciteComponent } from '../components/materiels-sap-mesurepressionarterielle/materiels-sap-mesurepressionarterielle-efficacite/materiels-sap-mesurepressionarterielle-efficacite';
import { MaterielsSapOxymetredepoulsDescriptionComponent } from '../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-description/materiels-sap-oxymetredepouls-description';
import { MaterielsSapOxymetredepoulsIndicationsComponent } from '../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-indications/materiels-sap-oxymetredepouls-indications';
import { MaterielsSapOxymetredepoulsRisquesComponent } from '../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-risques/materiels-sap-oxymetredepouls-risques';
import { MaterielsSapOxymetredepoulsUtilisationComponent } from '../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-utilisation/materiels-sap-oxymetredepouls-utilisation';
import { MaterielsSapOxymetredepoulsPointsclesComponent } from '../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-pointscles/materiels-sap-oxymetredepouls-pointscles';
import { MaterielsSapOxymetredepoulsEfficaciteComponent } from '../components/materiels-sap-oxymetredepouls/materiels-sap-oxymetredepouls-efficacite/materiels-sap-oxymetredepouls-efficacite';
import { MaterielsSapThermometreDescriptionComponent } from '../components/materiels-sap-thermometre/materiels-sap-thermometre-description/materiels-sap-thermometre-description';
import { MaterielsSapThermometreIndicationsComponent } from '../components/materiels-sap-thermometre/materiels-sap-thermometre-indications/materiels-sap-thermometre-indications';
import { MaterielsSapThermometreRisquesComponent } from '../components/materiels-sap-thermometre/materiels-sap-thermometre-risques/materiels-sap-thermometre-risques';
import { MaterielsSapThermometreUtilisationComponent } from '../components/materiels-sap-thermometre/materiels-sap-thermometre-utilisation/materiels-sap-thermometre-utilisation';
import { MaterielsSapThermometrePointsclesComponent } from '../components/materiels-sap-thermometre/materiels-sap-thermometre-pointscles/materiels-sap-thermometre-pointscles';
import { MaterielsSapThermometreEfficaciteComponent } from '../components/materiels-sap-thermometre/materiels-sap-thermometre-efficacite/materiels-sap-thermometre-efficacite';




@NgModule({
	declarations: [
    FeuxCaveRisquesComponent,
    FeuxCaveStrategieComponent,
    FeuxCaveAccesComponent,
    FeuxCaveActionComponent,
    FeuxCaveAttentionComponent,
    FeuxCaveInterditComponent,
    FeuxCombleRisquesComponent,
    FeuxCombleStrategieComponent,
    FeuxCombleAccesComponent,
    FeuxCombleActionComponent,
    FeuxCombleAttentionComponent,
    FeuxCombleInterditComponent,
    FeuxTerrasseAccesComponent,
    FeuxTerrasseActionComponent,
    FeuxTerrasseAttentionComponent,
    FeuxTerrasseInterditComponent,
    FeuxTerrasseRisquesComponent,
    FeuxTerrasseStrategieComponent,
    FeuxCagedescalierAccesComponent,
    FeuxCagedescalierActionComponent,
    FeuxCagedescalierAttentionComponent,
    FeuxCagedescalierInterditComponent,
    FeuxCagedescalierRisquesComponent,
    FeuxCagedescalierStrategieComponent,
    FeuxChemineeAccesComponent,
    FeuxChemineeActionComponent,
    FeuxChemineeAttentionComponent,
    FeuxChemineeInterditComponent,
    FeuxChemineeRisquesComponent,
    FeuxChemineeStrategieComponent,
    FeuxPlanchersAccesComponent,
    FeuxPlanchersActionComponent,
    FeuxPlanchersAttentionComponent,
    FeuxPlanchersInterditComponent,
    FeuxPlanchersRisquesComponent,
    FeuxPlanchersStrategieComponent,
    FeuxJointdilatationAccesComponent,
    FeuxJointdilatationActionComponent,
    FeuxJointdilatationAttentionComponent,
    FeuxJointdilatationInterditComponent,
    FeuxJointdilatationRisquesComponent,
    FeuxJointdilatationStrategieComponent,
    FeuxParkingAccesComponent,
    FeuxParkingActionComponent,
    FeuxParkingAttentionComponent,
    FeuxParkingInterditComponent,
    FeuxParkingRisquesComponent,
    FeuxParkingStrategieComponent,
    FeuxMagasinAccesComponent,
    FeuxMagasinActionComponent,
    FeuxMagasinAttentionComponent,
    FeuxMagasinInterditComponent,
    FeuxMagasinRisquesComponent,
    FeuxMagasinStrategieComponent,
    FeuxDentrepôtAccesComponent,
    FeuxDentrepôtActionComponent,
    FeuxDentrepôtAttentionComponent,
    FeuxDentrepôtInterditComponent,
    FeuxDentrepôtRisquesComponent,
    FeuxDentrepôtStrategieComponent,
    FeuxErpTypeMAccesComponent,
    FeuxErpTypeMActionComponent,
    FeuxErpTypeMAttentionComponent,
    FeuxErpTypeMInterditComponent,
    FeuxErpTypeMRisquesComponent,
    FeuxErpTypeMStrategieComponent,
    FeuxErpTypeSvytAccesComponent,
    FeuxErpTypeSvytActionComponent,
    FeuxErpTypeSvytAttentionComponent,
    FeuxErpTypeSvytInterditComponent,
    FeuxErpTypeSvytRisquesComponent,
    FeuxErpTypeSvytStrategieComponent,
    FeuxErpTypeJuAccesComponent,
    FeuxErpTypeJuActionComponent,
    FeuxErpTypeJuAttentionComponent,
    FeuxErpTypeJuInterditComponent,
    FeuxErpTypeJuRisquesComponent,
    FeuxErpTypeJuStrategieComponent,
    FeuxIghAccesComponent,
    FeuxIghActionComponent,
    FeuxIghAttentionComponent,
    FeuxIghInterditComponent,
    FeuxIghRisquesComponent,
    FeuxIghStrategieComponent,
    FeuxFermeAccesComponent,
    FeuxFermeActionComponent,
    FeuxFermeAttentionComponent,
    FeuxFermeInterditComponent,
    FeuxFermeRisquesComponent,
    FeuxFermeStrategieComponent,
    FeuxCentreequestreAccesComponent,
    FeuxCentreequestreActionComponent,
    FeuxCentreequestreAttentionComponent,
    FeuxCentreequestreInterditComponent,
    FeuxCentreequestreRisquesComponent,
    FeuxCentreequestreStrategieComponent,
    FeuxRecolteAccesComponent,
    FeuxRecolteActionComponent,
    FeuxRecolteAttentionComponent,
    FeuxRecolteInterditComponent,
    FeuxRecolteRisquesComponent,
    FeuxRecolteStrategieComponent,
    FeuxDengraisAccesComponent,
    FeuxDengraisActionComponent,
    FeuxDengraisAttentionComponent,
    FeuxDengraisInterditComponent,
    FeuxDengraisRisquesComponent,
    FeuxDengraisStrategieComponent,
    FeuxChaufferieAccesComponent,
    FeuxChaufferieActionComponent,
    FeuxChaufferieAttentionComponent,
    FeuxChaufferieInterditComponent,
    FeuxChaufferieRisquesComponent,
    FeuxChaufferieStrategieComponent,
    FeuxChaufferieindustrielleAccesComponent,
    FeuxChaufferieindustrielleActionComponent,
    FeuxChaufferieindustrielleAttentionComponent,
    FeuxChaufferieindustrielleInterditComponent,
    FeuxChaufferieindustrielleRisquesComponent,
    FeuxChaufferieindustrielleStrategieComponent,
    FeuxTransformateurAccesComponent,
    FeuxTransformateurActionComponent,
    FeuxTransformateurAttentionComponent,
    FeuxTransformateurInterditComponent,
    FeuxTransformateurRisquesComponent,
    FeuxTransformateurStrategieComponent,
    FeuxRadiologiqueAccesComponent,
    FeuxRadiologiqueActionComponent,
    FeuxRadiologiqueAttentionComponent,
    FeuxRadiologiqueInterditComponent,
    FeuxRadiologiqueRisquesComponent,
    FeuxRadiologiqueStrategieComponent,
    FeuxChimiqueAccesComponent,
    FeuxChimiqueActionComponent,
    FeuxChimiqueAttentionComponent,
    FeuxChimiqueInterditComponent,
    FeuxChimiqueRisquesComponent,
    FeuxChimiqueStrategieComponent,
    FeuxMatieresplastiquesAccesComponent,
    FeuxMatieresplastiquesActionComponent,
    FeuxMatieresplastiquesAttentionComponent,
    FeuxMatieresplastiquesInterditComponent,
    FeuxMatieresplastiquesRisquesComponent,
    FeuxMatieresplastiquesStrategieComponent,
    FeuxPesticidesAccesComponent,
    FeuxPesticidesActionComponent,
    FeuxPesticidesAttentionComponent,
    FeuxPesticidesInterditComponent,
    FeuxPesticidesRisquesComponent,
    FeuxPesticidesStrategieComponent,
    FeuxSilosAccesComponent,
    FeuxSilosActionComponent,
    FeuxSilosAttentionComponent,
    FeuxSilosInterditComponent,
    FeuxSilosRisquesComponent,
    FeuxSilosStrategieComponent,
    FeuxLiquidesinflammablesAccesComponent,
    FeuxLiquidesinflammablesActionComponent,
    FeuxLiquidesinflammablesAttentionComponent,
    FeuxLiquidesinflammablesInterditComponent,
    FeuxLiquidesinflammablesRisquesComponent,
    FeuxLiquidesinflammablesStrategieComponent,
    FeuxMetauxAccesComponent,
    FeuxMetauxActionComponent,
    FeuxMetauxAttentionComponent,
    FeuxMetauxInterditComponent,
    FeuxMetauxRisquesComponent,
    FeuxMetauxStrategieComponent,
    FeuxVehiculesAccesComponent,
    FeuxVehiculesActionComponent,
    FeuxVehiculesAttentionComponent,
    FeuxVehiculesInterditComponent,
    FeuxVehiculesRisquesComponent,
    FeuxVehiculesStrategieComponent,
    FeuxTrainAccesComponent,
    FeuxTrainActionComponent,
    FeuxTrainAttentionComponent,
    FeuxTrainInterditComponent,
    FeuxTrainRisquesComponent,
    FeuxTrainStrategieComponent,
    FeuxBateauAccesComponent,
    FeuxBateauActionComponent,
    FeuxBateauAttentionComponent,
    FeuxBateauInterditComponent,
    FeuxBateauRisquesComponent,
    FeuxBateauStrategieComponent,
    FeuxPenicheAccesComponent,
    FeuxPenicheActionComponent,
    FeuxPenicheAttentionComponent,
    FeuxPenicheInterditComponent,
    FeuxPenicheRisquesComponent,
    FeuxPenicheStrategieComponent,
    FeuxAeronefAccesComponent,
    FeuxAeronefActionComponent,
    FeuxAeronefAttentionComponent,
    FeuxAeronefInterditComponent,
    FeuxAeronefRisquesComponent,
    FeuxAeronefStrategieComponent,
    FeuxTunnelAccesComponent,
    FeuxTunnelActionComponent,
    FeuxTunnelAttentionComponent,
    FeuxTunnelInterditComponent,
    FeuxTunnelRisquesComponent,
    FeuxTunnelStrategieComponent,
    FeuxCamionciterneAccesComponent,
    FeuxCamionciterneActionComponent,
    FeuxCamionciterneAttentionComponent,
    FeuxCamionciterneInterditComponent,
    FeuxCamionciterneRisquesComponent,
    FeuxCamionciterneStrategieComponent,
    SapAcrSignesComponent,
    SapAcrBilansComponent,
    SapAcrActionsComponent,
    SapAcrImportantComponent,
    SapBilansCirconstancielComponent,
    SapBilansVitalComponent,
    SapBilansLesionnelComponent,
    SapBilansFonctionnelComponent,
    SapBilansRadioComponent,
    SapBilansEvolutifComponent,
    SapAvcSignesComponent,
    SapAvcBilansComponent,
    SapAvcActionsComponent,
    SapAvcImportantComponent,
    SapAccouchementSignesComponent,
    SapAccouchementBilansComponent,
    SapAccouchementActionsComponent,
    SapAccouchementImportantComponent,
    SapInfarctusSignesComponent,
    SapInfarctusBilansComponent,
    SapInfarctusActionsComponent,
    SapInfarctusImportantComponent,
    SapBruluresSignesComponent,
    SapBruluresBilansComponent,
    SapBruluresActionsComponent,
    SapBruluresImportantComponent,
    SapConvulsionsSignesComponent,
    SapConvulsionsBilansComponent,
    SapConvulsionsActionsComponent,
    SapConvulsionsImportantComponent,
    SapCrisedasthmesSignesComponent,
    SapCrisedasthmesBilansComponent,
    SapCrisedasthmesActionsComponent,
    SapCrisedasthmesImportantComponent,
    SapDesincarserationSignesComponent,
    SapDesincarserationBilansComponent,
    SapDesincarserationActionsComponent,
    SapDesincarserationImportantComponent,
    SapDiabetiqueSignesComponent,
    SapDiabetiqueBilansComponent,
    SapDiabetiqueActionsComponent,
    SapDiabetiqueImportantComponent,
    SapEcrasementSignesComponent,
    SapEcrasementBilansComponent,
    SapEcrasementActionsComponent,
    SapEcrasementImportantComponent,
    SapElectrisationSignesComponent,
    SapElectrisationBilansComponent,
    SapElectrisationActionsComponent,
    SapElectrisationImportantComponent,
    SapEpilepsieSignesComponent,
    SapEpilepsieBilansComponent,
    SapEpilepsieActionsComponent,
    SapEpilepsieImportantComponent,
    SapHemorragieSignesComponent,
    SapHemorragieBilansComponent,
    SapHemorragieActionsComponent,
    SapHemorragieImportantComponent,
    SapIntoxicationcoSignesComponent,
    SapIntoxicationcoBilansComponent,
    SapIntoxicationcoActionsComponent,
    SapIntoxicationcoImportantComponent,
    SapIntoxicationmedSignesComponent,
    SapIntoxicationmedBilansComponent,
    SapIntoxicationmedActionsComponent,
    SapIntoxicationmedImportantComponent,
    SapIvresseSignesComponent,
    SapIvresseBilansComponent,
    SapIvresseActionsComponent,
    SapIvresseImportantComponent,
    SapMalaisesSignesComponent,
    SapMalaisesBilansComponent,
    SapMalaisesActionsComponent,
    SapMalaisesImportantComponent,
    SapNoyadeSignesComponent,
    SapNoyadeBilansComponent,
    SapNoyadeActionsComponent,
    SapNoyadeImportantComponent,
    SapOedemepoumonSignesComponent,
    SapOedemepoumonBilansComponent,
    SapOedemepoumonActionsComponent,
    SapOedemepoumonImportantComponent,
    SapOedemedequinckeSignesComponent,
    SapOedemedequinckeBilansComponent,
    SapOedemedequinckeActionsComponent,
    SapOedemedequinckeImportantComponent,
    SapPendaisonSignesComponent,
    SapPendaisonBilansComponent,
    SapPendaisonActionsComponent,
    SapPendaisonImportantComponent,
    SapPiqureshymenopteresSignesComponent,
    SapPiqureshymenopteresBilansComponent,
    SapPiqureshymenopteresActionsComponent,
    SapPiqureshymenopteresImportantComponent,
    SapPlaiesSignesComponent,
    SapPlaiesBilansComponent,
    SapPlaiesActionsComponent,
    SapPlaiesImportantComponent,
    SapPolytraumatiseSignesComponent,
    SapPolytraumatiseBilansComponent,
    SapPolytraumatiseActionsComponent,
    SapPolytraumatiseImportantComponent,
    SapTetaniespasmophilieSignesComponent,
    SapTetaniespasmophilieBilansComponent,
    SapTetaniespasmophilieActionsComponent,
    SapTetaniespasmophilieImportantComponent,
    SapTraumatismecolonnevertebraleSignesComponent,
    SapTraumatismecolonnevertebraleBilansComponent,
    SapTraumatismecolonnevertebraleActionsComponent,
    SapTraumatismecolonnevertebraleImportantComponent,
    SapTraumatismecranienSignesComponent,
    SapTraumatismecranienBilansComponent,
    SapTraumatismecranienActionsComponent,
    SapTraumatismecranienImportantComponent,
    SapTraumatismedesmembresSignesComponent,
    SapTraumatismedesmembresBilansComponent,
    SapTraumatismedesmembresActionsComponent,
    SapTraumatismedesmembresImportantComponent,
    SrPrincipesAccidentComponent,
    SrPrincipesRecoComponent,
    SrPrincipesIncComponent,
    SrPrincipesDecoupeComponent,
    SrPrincipesHybridesComponent,
    SrCalage_1Component,
    SrCalage_2Component,
    SrCalage_3Component,
    SrCalage_4Component,
    SrCalage_5Component,
    SrOuverturedeporte_1Component,
    SrOuverturedeporte_2Component,
    SrOuverturedeporte_3Component,
    SrOuverturedeporte_4Component,
    SrOuverturedeporte_5Component,
    SrOuverturedeporte_6Component,
    SrOuverturedeporte_7Component,
    SrOuverturelaterale_1Component,
    SrOuverturelaterale_2Component,
    SrOuverturelaterale_3Component,
    SrDepavillonnage_1Component,
    SrDepavillonnage_2Component,
    SrDepavillonnage_3Component,
    SrDemipavillonavant_1Component,
    SrDemipavillonavant_2Component,
    SrDemipavillonavant_3Component,
    SrDemipavillonarriere_1Component,
    SrDemipavillonarriere_2Component,
    SrDemipavillonarriere_3Component,
    SrCharniere_1Component,
    SrCharniere_2Component,
    SrCharniere_3Component,
    SrCoquilledhuitre_1Component,
    SrCoquilledhuitre_2Component,
    SrCoquilledhuitre_3Component,
    SrCoquilledhuitre_4Component,
    SrPortefeuille_1Component,
    SrPortefeuille_2Component,
    SrPortefeuille_3Component,
    SrRelevagetableaudebord_1Component,
    SrRelevagetableaudebord_2Component,
    SrRelevagetableaudebord_3Component,
    GocCadredordrePatracdrComponent,
    GocCadredordreDpifComponent,
    GocCadredordreSmesComponent,
    GocCadredordreSoiecComponent,
    GocMessageradioAmbianceComponent,
    GocMessageradioRenseignementComponent,
    GocFonctiondupcCrmComponent,
    GocFonctiondupcTransComponent,
    GocFonctiondupcRensComponent,
    PrevClassementdesbatimentsErpComponent,
    PrevClassementdesbatimentsIghComponent,
    PrevClassementdesbatimentsHabitationComponent,
    PrevClassementdesbatimentsTravailComponent,
    PrevVoiesenginsetechellesFonctionnementComponent,
    PrevVoiesenginsetechellesPrecautionComponent,
    PrevFacadesFonctionnementComponent,
    PrevFacadesPrecautionComponent,
    PrevGrosetsecondoeuvreFonctionnementComponent,
    PrevGrosetsecondoeuvrePrecautionComponent,
    PrevDegagementsaccesFonctionnementComponent,
    PrevDegagementsaccesPrecautionComponent,
    PrevLocauxarisquesFonctionnementComponent,
    PrevLocauxarisquesPrecautionComponent,
    PrevDesenfumageFonctionnementComponent,
    PrevDesenfumagePrecautionComponent,
    PrevMoyensdextinctionFonctionnementComponent,
    PrevMoyensdextinctionPrecautionComponent,
    PrevSsiapFonctionnementComponent,
    PrevSsiapPrecautionComponent,
    PrevSsiFonctionnementntComponent,
    PrevSsiPrecautionComponent,
    PrevGrosetsecondoeuvreDistributionComponent,
    PrevGrosetsecondoeuvreOuvertureComponent,
    PrevPlansdinterventionBaseComponent,
    PrevPlansdinterventionEauComponent,
    PrevPlansdinterventionIncComponent,
    PrevPlansdinterventionTechComponent,
    GnrAriReglesdebase_1Component,
    GnrAriReglesdebase_2Component,
    GnrAriReglesdebase_3Component,
    GnrAriContraintesCorporelComponent,
    GnrAriContraintesSensorielComponent,
    GnrAriContraintesRelationComponent,
    GnrLspccAmarrageetnoeudsPointsfixesComponent,
    GnrLspccAmarrageetnoeudsNoeudsComponent,
    GnrLspccOperationReglesdebasesAvantComponent,
    GnrLspccOperationReglesdebasesPendantComponent,
    GnrLspccOperationReglesdebasesApresComponent,
    GnrLspccOperationSauvetageexterieurChefdagresComponent,
    GnrLspccOperationSauvetageexterieurChefdequipeComponent,
    GnrLspccOperationSauvetageexterieurEquipierComponent,
    GnrLspccOperationSauvetageexterieur_2ebinomeComponent,
    GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent,
    GnrLspccOperationSauvetageexcavationExplorationEquipierComponent,
    GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent,
    GnrLspccOperationSauvetageexcavationRemonteEquipierComponent,
    GnrLspccOperationProtectionchutesChefdagresComponent,
    GnrLspccOperationProtectionchutesChefdequipeComponent,
    GnrLspccOperationProtectionchutesEquipierComponent,
    GnrLspccOperationProtectionchutes_2ebinomeComponent,
    GnrLspccOperationProtectionfixehumainChefdagresComponent,
    GnrLspccOperationProtectionfixehumainChefdequipeComponent,
    GnrLspccOperationProtectionfixehumainEquipierComponent,
    GnrLspccOperationProtectionfixehumain_2ebinomeComponent,
    GnretablissementlancesManoeuvresGnrM1ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM1ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM1EquipierComponent,
    GnretablissementlancesManoeuvresGnrM1BalComponent,
    GnretablissementlancesManoeuvresGnrM2ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM2ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM2EquipierComponent,
    GnretablissementlancesManoeuvresGnrM3ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM3ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM3EquipierComponent,
    GnretablissementlancesManoeuvresGnrM4ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM4ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM4EquipierComponent,
    GnretablissementlancesManoeuvresGnrM5ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM5ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM5EquipierComponent,
    GnretablissementlancesManoeuvresGnrM5BalComponent,
    GnretablissementlancesManoeuvresGnrM6ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM6ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM6EquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurChefdequipeComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurEquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurBalComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdequipeComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseEquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseBalComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneChefdequipeComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneEquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneBalComponent,
    GnretablissementlancesManoeuvrescomplLdvpoteauChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvpoteauBatComponent,
    GnretablissementlancesManoeuvrescomplLdvpoteauBalComponent,
    GnretablissementlancesTuyauxechevauxLdvprisedeauChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvprisedeauChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvprisedeauEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvattaqueChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvattaqueChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvattaqueEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurBalComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseBalComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationChefdagresComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationChefdequipeComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationEquipierComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationBalComponent,
    GnretablissementlancesSacdattaqueLdvexterieurChefdadresComponent,
    GnretablissementlancesSacdattaqueLdvexterieurChefdequipeComponent,
    GnretablissementlancesSacdattaqueLdvexterieurEquipierComponent,
    GnretablissementlancesSacdattaqueLdvexterieurBalComponent,
    GocOutilsgraphiquesSituationComponent,
    GocOutilsgraphiquesActionsComponent,
    GocOutilsgraphiquesMoyensComponent,
    GocOutilsgraphiquesCharteComponent,
    MaterielsSapSanglearaigneeDescriptionComponent,
    MaterielsSapSanglearaigneeIndicationsComponent,
    MaterielsSapSanglearaigneeRisquesComponent,
    MaterielsSapSanglearaigneeUtilisationComponent,
    MaterielsSapSanglearaigneePointsclesComponent,
    MaterielsSapSanglearaigneeCriteresdefficaciteComponent,
    MaterielSapAttelledetractionDescriptionComponent,
    MaterielSapAttelledetractionIndicationsComponent,
    MaterielSapAttelledetractionRisquesComponent,
    MaterielSapAttelledetractionUtilisationComponent,
    MaterielSapAttelledetractionPointsclesComponent,
    MaterielSapAttelledetractionEfficacitéComponent,
    MaterielsSapAspirateurdemucositesDescriptionComponent,
    MaterielsSapAspirateurdemucositesIndicationsComponent,
    MaterielsSapAspirateurdemucositesRisquesComponent,
    MaterielsSapAspirateurdemucositesUtilisationComponent,
    MaterielsSapAspirateurdemucositesPointsclesComponent,
    MaterielsSapAspirateurdemucositesEfficaciteComponent,
    MaterielsSapAttellecervicothoraciqueDescriptionComponent,
    MaterielsSapAttellecervicothoraciqueIndicationsComponent,
    MaterielsSapAttellecervicothoraciqueRisquesComponent,
    MaterielsSapAttellecervicothoraciqueUtilisationComponent,
    MaterielsSapAttellecervicothoraciquePointsclesComponent,
    MaterielsSapAttellecervicothoraciqueEfficaciteComponent,
    MaterielsSapBrancardcuillereDescriptionComponent,
    MaterielsSapBrancardcuillereIndicationsComponent,
    MaterielsSapBrancardcuillereRisquesComponent,
    MaterielsSapBrancardcuillereUtilisationComponent,
    MaterielsSapBrancardcuillerePointsclesComponent,
    MaterielsSapBrancardcuillereEfficaciteComponent,
    MaterielsSapDaeDescriptionComponent,
    MaterielsSapDaeIndicationsComponent,
    MaterielsSapDaeRisquesComponent,
    MaterielsSapDaeUtilisationComponent,
    MaterielsSapDaePointsclesComponent,
    MaterielsSapDaeEfficaciteComponent,
    MaterielsSapGarrotDescriptionComponent,
    MaterielsSapGarrotIndicationsComponent,
    MaterielsSapGarrotRisquesComponent,
    MaterielsSapGarrotUtilisationComponent,
    MaterielsSapGarrotPointsclesComponent,
    MaterielsSapGarrotEfficaciteComponent,
    MaterielsSapKitaesDescriptionComponent,
    MaterielsSapKitaesIndicationsComponent,
    MaterielsSapKitaesRisquesComponent,
    MaterielsSapKitaesUtilisationComponent,
    MaterielsSapKitbruluresDescriptionComponent,
    MaterielsSapKitbruluresIndicationsComponent,
    MaterielsSapKitbruluresRisquesComponent,
    MaterielsSapKitbruluresUtilisationComponent,
    MaterielsSapKitbruluresPointsclesComponent,
    MaterielsSapKitsectiondemembresDescriptionComponent,
    MaterielsSapKitsectiondemembresIndicationsComponent,
    MaterielsSapKitsectiondemembresRisquesComponent,
    MaterielsSapKitsectiondemembresUtilisationComponent,
    MaterielsSapKitsectiondemembresPointsclesComponent,
    MaterielsSapKitsectiondemembresEfficaciteComponent,
    MaterielsSapKitsinusDescriptionComponent,
    MaterielsSapKitsinusIndicationsComponent,
    MaterielsSapKitsinusRisquesComponent,
    MaterielsSapKitsinusUtilisationComponent,
    MaterielsSapLectureglycemieDescriptionComponent,
    MaterielsSapLectureglycemieIndicationsComponent,
    MaterielsSapLectureglycemieRisquesComponent,
    MaterielsSapLectureglycemieUtilisationComponent,
    MaterielsSapLectureglycemiePointsclesComponent,
    MaterielsSapLectureglycemieEfficaciteComponent,
    MaterielsSapMesurepressionarterielleDescriptionComponent,
    MaterielsSapMesurepressionarterielleIndicationsComponent,
    MaterielsSapMesurepressionarterielleRisquesComponent,
    MaterielsSapMesurepressionarterielleUtilisationComponent,
    MaterielsSapMesurepressionarteriellePointsclesComponent,
    MaterielsSapMesurepressionarterielleEfficaciteComponent,
    MaterielsSapOxymetredepoulsDescriptionComponent,
    MaterielsSapOxymetredepoulsIndicationsComponent,
    MaterielsSapOxymetredepoulsRisquesComponent,
    MaterielsSapOxymetredepoulsUtilisationComponent,
    MaterielsSapOxymetredepoulsPointsclesComponent,
    MaterielsSapOxymetredepoulsEfficaciteComponent,
    MaterielsSapThermometreDescriptionComponent,
    MaterielsSapThermometreIndicationsComponent,
    MaterielsSapThermometreRisquesComponent,
    MaterielsSapThermometreUtilisationComponent,
    MaterielsSapThermometrePointsclesComponent,
    MaterielsSapThermometreEfficaciteComponent,


],
	imports: [],
	exports: [
    FeuxCaveRisquesComponent,
    FeuxCaveStrategieComponent,
    FeuxCaveAccesComponent,
    FeuxCaveActionComponent,
    FeuxCaveAttentionComponent,
    FeuxCaveInterditComponent,
    FeuxCombleRisquesComponent,
    FeuxCombleStrategieComponent,
    FeuxCombleAccesComponent,
    FeuxCombleActionComponent,
    FeuxCombleAttentionComponent,
    FeuxCombleInterditComponent,
    FeuxTerrasseAccesComponent,
    FeuxTerrasseActionComponent,
    FeuxTerrasseAttentionComponent,
    FeuxTerrasseInterditComponent,
    FeuxTerrasseRisquesComponent,
    FeuxTerrasseStrategieComponent,
    FeuxCagedescalierAccesComponent,
    FeuxCagedescalierActionComponent,
    FeuxCagedescalierAttentionComponent,
    FeuxCagedescalierInterditComponent,
    FeuxCagedescalierRisquesComponent,
    FeuxCagedescalierStrategieComponent,
    FeuxChemineeAccesComponent,
    FeuxChemineeActionComponent,
    FeuxChemineeAttentionComponent,
    FeuxChemineeInterditComponent,
    FeuxChemineeRisquesComponent,
    FeuxChemineeStrategieComponent,
    FeuxPlanchersAccesComponent,
    FeuxPlanchersActionComponent,
    FeuxPlanchersAttentionComponent,
    FeuxPlanchersInterditComponent,
    FeuxPlanchersRisquesComponent,
    FeuxPlanchersStrategieComponent,
    FeuxJointdilatationAccesComponent,
    FeuxJointdilatationActionComponent,
    FeuxJointdilatationAttentionComponent,
    FeuxJointdilatationInterditComponent,
    FeuxJointdilatationRisquesComponent,
    FeuxJointdilatationStrategieComponent,
    FeuxParkingAccesComponent,
    FeuxParkingActionComponent,
    FeuxParkingAttentionComponent,
    FeuxParkingInterditComponent,
    FeuxParkingRisquesComponent,
    FeuxParkingStrategieComponent,
    FeuxMagasinAccesComponent,
    FeuxMagasinActionComponent,
    FeuxMagasinAttentionComponent,
    FeuxMagasinInterditComponent,
    FeuxMagasinRisquesComponent,
    FeuxMagasinStrategieComponent,
    FeuxDentrepôtAccesComponent,
    FeuxDentrepôtActionComponent,
    FeuxDentrepôtAttentionComponent,
    FeuxDentrepôtInterditComponent,
    FeuxDentrepôtRisquesComponent,
    FeuxDentrepôtStrategieComponent,
    FeuxErpTypeMAccesComponent,
    FeuxErpTypeMActionComponent,
    FeuxErpTypeMAttentionComponent,
    FeuxErpTypeMInterditComponent,
    FeuxErpTypeMRisquesComponent,
    FeuxErpTypeMStrategieComponent,
    FeuxErpTypeSvytAccesComponent,
    FeuxErpTypeSvytActionComponent,
    FeuxErpTypeSvytAttentionComponent,
    FeuxErpTypeSvytInterditComponent,
    FeuxErpTypeSvytRisquesComponent,
    FeuxErpTypeSvytStrategieComponent,
    FeuxErpTypeJuAccesComponent,
    FeuxErpTypeJuActionComponent,
    FeuxErpTypeJuAttentionComponent,
    FeuxErpTypeJuInterditComponent,
    FeuxErpTypeJuRisquesComponent,
    FeuxErpTypeJuStrategieComponent,
    FeuxIghAccesComponent,
    FeuxIghActionComponent,
    FeuxIghAttentionComponent,
    FeuxIghInterditComponent,
    FeuxIghRisquesComponent,
    FeuxIghStrategieComponent,
    FeuxFermeAccesComponent,
    FeuxFermeActionComponent,
    FeuxFermeAttentionComponent,
    FeuxFermeInterditComponent,
    FeuxFermeRisquesComponent,
    FeuxFermeStrategieComponent,
    FeuxCentreequestreAccesComponent,
    FeuxCentreequestreActionComponent,
    FeuxCentreequestreAttentionComponent,
    FeuxCentreequestreInterditComponent,
    FeuxCentreequestreRisquesComponent,
    FeuxCentreequestreStrategieComponent,
    FeuxRecolteAccesComponent,
    FeuxRecolteActionComponent,
    FeuxRecolteAttentionComponent,
    FeuxRecolteInterditComponent,
    FeuxRecolteRisquesComponent,
    FeuxRecolteStrategieComponent,
    FeuxDengraisAccesComponent,
    FeuxDengraisActionComponent,
    FeuxDengraisAttentionComponent,
    FeuxDengraisInterditComponent,
    FeuxDengraisRisquesComponent,
    FeuxDengraisStrategieComponent,
    FeuxChaufferieAccesComponent,
    FeuxChaufferieActionComponent,
    FeuxChaufferieAttentionComponent,
    FeuxChaufferieInterditComponent,
    FeuxChaufferieRisquesComponent,
    FeuxChaufferieStrategieComponent,
    FeuxChaufferieindustrielleAccesComponent,
    FeuxChaufferieindustrielleActionComponent,
    FeuxChaufferieindustrielleAttentionComponent,
    FeuxChaufferieindustrielleInterditComponent,
    FeuxChaufferieindustrielleRisquesComponent,
    FeuxChaufferieindustrielleStrategieComponent,
    FeuxTransformateurAccesComponent,
    FeuxTransformateurActionComponent,
    FeuxTransformateurAttentionComponent,
    FeuxTransformateurInterditComponent,
    FeuxTransformateurRisquesComponent,
    FeuxTransformateurStrategieComponent,
    FeuxRadiologiqueAccesComponent,
    FeuxRadiologiqueActionComponent,
    FeuxRadiologiqueAttentionComponent,
    FeuxRadiologiqueInterditComponent,
    FeuxRadiologiqueRisquesComponent,
    FeuxRadiologiqueStrategieComponent,
    FeuxChimiqueAccesComponent,
    FeuxChimiqueActionComponent,
    FeuxChimiqueAttentionComponent,
    FeuxChimiqueInterditComponent,
    FeuxChimiqueRisquesComponent,
    FeuxChimiqueStrategieComponent,
    FeuxMatieresplastiquesAccesComponent,
    FeuxMatieresplastiquesActionComponent,
    FeuxMatieresplastiquesAttentionComponent,
    FeuxMatieresplastiquesInterditComponent,
    FeuxMatieresplastiquesRisquesComponent,
    FeuxMatieresplastiquesStrategieComponent,
    FeuxPesticidesAccesComponent,
    FeuxPesticidesActionComponent,
    FeuxPesticidesAttentionComponent,
    FeuxPesticidesInterditComponent,
    FeuxPesticidesRisquesComponent,
    FeuxPesticidesStrategieComponent,
    FeuxSilosAccesComponent,
    FeuxSilosActionComponent,
    FeuxSilosAttentionComponent,
    FeuxSilosInterditComponent,
    FeuxSilosRisquesComponent,
    FeuxSilosStrategieComponent,
    FeuxLiquidesinflammablesAccesComponent,
    FeuxLiquidesinflammablesActionComponent,
    FeuxLiquidesinflammablesAttentionComponent,
    FeuxLiquidesinflammablesInterditComponent,
    FeuxLiquidesinflammablesRisquesComponent,
    FeuxLiquidesinflammablesStrategieComponent,
    FeuxMetauxAccesComponent,
    FeuxMetauxActionComponent,
    FeuxMetauxAttentionComponent,
    FeuxMetauxInterditComponent,
    FeuxMetauxRisquesComponent,
    FeuxMetauxStrategieComponent,
    FeuxVehiculesAccesComponent,
    FeuxVehiculesActionComponent,
    FeuxVehiculesAttentionComponent,
    FeuxVehiculesInterditComponent,
    FeuxVehiculesRisquesComponent,
    FeuxVehiculesStrategieComponent,
    FeuxTrainAccesComponent,
    FeuxTrainActionComponent,
    FeuxTrainAttentionComponent,
    FeuxTrainInterditComponent,
    FeuxTrainRisquesComponent,
    FeuxTrainStrategieComponent,
    FeuxBateauAccesComponent,
    FeuxBateauActionComponent,
    FeuxBateauAttentionComponent,
    FeuxBateauInterditComponent,
    FeuxBateauRisquesComponent,
    FeuxBateauStrategieComponent,
    FeuxPenicheAccesComponent,
    FeuxPenicheActionComponent,
    FeuxPenicheAttentionComponent,
    FeuxPenicheInterditComponent,
    FeuxPenicheRisquesComponent,
    FeuxPenicheStrategieComponent,
    FeuxAeronefAccesComponent,
    FeuxAeronefActionComponent,
    FeuxAeronefAttentionComponent,
    FeuxAeronefInterditComponent,
    FeuxAeronefRisquesComponent,
    FeuxAeronefStrategieComponent,
    FeuxTunnelAccesComponent,
    FeuxTunnelActionComponent,
    FeuxTunnelAttentionComponent,
    FeuxTunnelInterditComponent,
    FeuxTunnelRisquesComponent,
    FeuxTunnelStrategieComponent,
    FeuxCamionciterneAccesComponent,
    FeuxCamionciterneActionComponent,
    FeuxCamionciterneAttentionComponent,
    FeuxCamionciterneInterditComponent,
    FeuxCamionciterneRisquesComponent,
    FeuxCamionciterneStrategieComponent,
    SapAcrSignesComponent,
    SapAcrBilansComponent,
    SapAcrActionsComponent,
    SapAcrImportantComponent,
    SapBilansCirconstancielComponent,
    SapBilansVitalComponent,
    SapBilansLesionnelComponent,
    SapBilansFonctionnelComponent,
    SapBilansRadioComponent,
    SapBilansEvolutifComponent,
    SapAvcSignesComponent,
    SapAvcBilansComponent,
    SapAvcActionsComponent,
    SapAvcImportantComponent,
    SapAccouchementSignesComponent,
    SapAccouchementBilansComponent,
    SapAccouchementActionsComponent,
    SapAccouchementImportantComponent,
    SapInfarctusSignesComponent,
    SapInfarctusBilansComponent,
    SapInfarctusActionsComponent,
    SapInfarctusImportantComponent,
    SapBruluresSignesComponent,
    SapBruluresBilansComponent,
    SapBruluresActionsComponent,
    SapBruluresImportantComponent,
    SapConvulsionsSignesComponent,
    SapConvulsionsBilansComponent,
    SapConvulsionsActionsComponent,
    SapConvulsionsImportantComponent,
    SapCrisedasthmesSignesComponent,
    SapCrisedasthmesBilansComponent,
    SapCrisedasthmesActionsComponent,
    SapCrisedasthmesImportantComponent,
    SapDesincarserationSignesComponent,
    SapDesincarserationBilansComponent,
    SapDesincarserationActionsComponent,
    SapDesincarserationImportantComponent,
    SapDiabetiqueSignesComponent,
    SapDiabetiqueBilansComponent,
    SapDiabetiqueActionsComponent,
    SapDiabetiqueImportantComponent,
    SapEcrasementSignesComponent,
    SapEcrasementBilansComponent,
    SapEcrasementActionsComponent,
    SapEcrasementImportantComponent,
    SapElectrisationSignesComponent,
    SapElectrisationBilansComponent,
    SapElectrisationActionsComponent,
    SapElectrisationImportantComponent,
    SapEpilepsieSignesComponent,
    SapEpilepsieBilansComponent,
    SapEpilepsieActionsComponent,
    SapEpilepsieImportantComponent,
    SapHemorragieSignesComponent,
    SapHemorragieBilansComponent,
    SapHemorragieActionsComponent,
    SapHemorragieImportantComponent,
    SapIntoxicationcoSignesComponent,
    SapIntoxicationcoBilansComponent,
    SapIntoxicationcoActionsComponent,
    SapIntoxicationcoImportantComponent,
    SapIntoxicationmedSignesComponent,
    SapIntoxicationmedBilansComponent,
    SapIntoxicationmedActionsComponent,
    SapIntoxicationmedImportantComponent,
    SapIvresseSignesComponent,
    SapIvresseBilansComponent,
    SapIvresseActionsComponent,
    SapIvresseImportantComponent,
    SapMalaisesSignesComponent,
    SapMalaisesBilansComponent,
    SapMalaisesActionsComponent,
    SapMalaisesImportantComponent,
    SapNoyadeSignesComponent,
    SapNoyadeBilansComponent,
    SapNoyadeActionsComponent,
    SapNoyadeImportantComponent,
    SapOedemepoumonSignesComponent,
    SapOedemepoumonBilansComponent,
    SapOedemepoumonActionsComponent,
    SapOedemepoumonImportantComponent,
    SapOedemedequinckeSignesComponent,
    SapOedemedequinckeBilansComponent,
    SapOedemedequinckeActionsComponent,
    SapOedemedequinckeImportantComponent,
    SapPendaisonSignesComponent,
    SapPendaisonBilansComponent,
    SapPendaisonActionsComponent,
    SapPendaisonImportantComponent,
    SapPiqureshymenopteresSignesComponent,
    SapPiqureshymenopteresBilansComponent,
    SapPiqureshymenopteresActionsComponent,
    SapPiqureshymenopteresImportantComponent,
    SapPlaiesSignesComponent,
    SapPlaiesBilansComponent,
    SapPlaiesActionsComponent,
    SapPlaiesImportantComponent,
    SapPolytraumatiseSignesComponent,
    SapPolytraumatiseBilansComponent,
    SapPolytraumatiseActionsComponent,
    SapPolytraumatiseImportantComponent,
    SapTetaniespasmophilieSignesComponent,
    SapTetaniespasmophilieBilansComponent,
    SapTetaniespasmophilieActionsComponent,
    SapTetaniespasmophilieImportantComponent,
    SapTraumatismecolonnevertebraleSignesComponent,
    SapTraumatismecolonnevertebraleBilansComponent,
    SapTraumatismecolonnevertebraleActionsComponent,
    SapTraumatismecolonnevertebraleImportantComponent,
    SapTraumatismecranienSignesComponent,
    SapTraumatismecranienBilansComponent,
    SapTraumatismecranienActionsComponent,
    SapTraumatismecranienImportantComponent,
    SapTraumatismedesmembresSignesComponent,
    SapTraumatismedesmembresBilansComponent,
    SapTraumatismedesmembresActionsComponent,
    SapTraumatismedesmembresImportantComponent,
    SrPrincipesAccidentComponent,
    SrPrincipesRecoComponent,
    SrPrincipesIncComponent,
    SrPrincipesDecoupeComponent,
    SrPrincipesHybridesComponent,
    SrCalage_1Component,
    SrCalage_2Component,
    SrCalage_3Component,
    SrCalage_4Component,
    SrCalage_5Component,
    SrOuverturedeporte_1Component,
    SrOuverturedeporte_2Component,
    SrOuverturedeporte_3Component,
    SrOuverturedeporte_4Component,
    SrOuverturedeporte_5Component,
    SrOuverturedeporte_6Component,
    SrOuverturedeporte_7Component,
    SrOuverturelaterale_1Component,
    SrOuverturelaterale_2Component,
    SrOuverturelaterale_3Component,
    SrDepavillonnage_1Component,
    SrDepavillonnage_2Component,
    SrDepavillonnage_3Component,
    SrDemipavillonavant_1Component,
    SrDemipavillonavant_2Component,
    SrDemipavillonavant_3Component,
    SrDemipavillonarriere_1Component,
    SrDemipavillonarriere_2Component,
    SrDemipavillonarriere_3Component,
    SrCharniere_1Component,
    SrCharniere_2Component,
    SrCharniere_3Component,
    SrCoquilledhuitre_1Component,
    SrCoquilledhuitre_2Component,
    SrCoquilledhuitre_3Component,
    SrCoquilledhuitre_4Component,
    SrPortefeuille_1Component,
    SrPortefeuille_2Component,
    SrPortefeuille_3Component,
    SrRelevagetableaudebord_1Component,
    SrRelevagetableaudebord_2Component,
    SrRelevagetableaudebord_3Component,
    GocCadredordrePatracdrComponent,
    GocCadredordreDpifComponent,
    GocCadredordreSmesComponent,
    GocCadredordreSoiecComponent,
    GocMessageradioAmbianceComponent,
    GocMessageradioRenseignementComponent,
    GocFonctiondupcCrmComponent,
    GocFonctiondupcTransComponent,
    GocFonctiondupcRensComponent,
    PrevClassementdesbatimentsErpComponent,
    PrevClassementdesbatimentsIghComponent,
    PrevClassementdesbatimentsHabitationComponent,
    PrevClassementdesbatimentsTravailComponent,
    PrevVoiesenginsetechellesFonctionnementComponent,
    PrevVoiesenginsetechellesPrecautionComponent,
    PrevFacadesFonctionnementComponent,
    PrevFacadesPrecautionComponent,
    PrevGrosetsecondoeuvreFonctionnementComponent,
    PrevGrosetsecondoeuvrePrecautionComponent,
    PrevDegagementsaccesFonctionnementComponent,
    PrevDegagementsaccesPrecautionComponent,
    PrevLocauxarisquesFonctionnementComponent,
    PrevLocauxarisquesPrecautionComponent,
    PrevDesenfumageFonctionnementComponent,
    PrevDesenfumagePrecautionComponent,
    PrevMoyensdextinctionFonctionnementComponent,
    PrevMoyensdextinctionPrecautionComponent,
    PrevSsiapFonctionnementComponent,
    PrevSsiapPrecautionComponent,
    PrevSsiFonctionnementntComponent,
    PrevSsiPrecautionComponent,
    PrevGrosetsecondoeuvreDistributionComponent,
    PrevGrosetsecondoeuvreOuvertureComponent,
    PrevPlansdinterventionBaseComponent,
    PrevPlansdinterventionEauComponent,
    PrevPlansdinterventionIncComponent,
    PrevPlansdinterventionTechComponent,
    GnrAriReglesdebase_1Component,
    GnrAriReglesdebase_2Component,
    GnrAriReglesdebase_3Component,
    GnrAriContraintesCorporelComponent,
    GnrAriContraintesSensorielComponent,
    GnrAriContraintesRelationComponent,
    GnrLspccAmarrageetnoeudsPointsfixesComponent,
    GnrLspccAmarrageetnoeudsNoeudsComponent,
    GnrLspccOperationReglesdebasesAvantComponent,
    GnrLspccOperationReglesdebasesPendantComponent,
    GnrLspccOperationReglesdebasesApresComponent,
    GnrLspccOperationSauvetageexterieurChefdagresComponent,
    GnrLspccOperationSauvetageexterieurChefdequipeComponent,
    GnrLspccOperationSauvetageexterieurEquipierComponent,
    GnrLspccOperationSauvetageexterieur_2ebinomeComponent,
    GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent,
    GnrLspccOperationSauvetageexcavationExplorationEquipierComponent,
    GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent,
    GnrLspccOperationSauvetageexcavationRemonteEquipierComponent,
    GnrLspccOperationProtectionchutesChefdagresComponent,
    GnrLspccOperationProtectionchutesChefdequipeComponent,
    GnrLspccOperationProtectionchutesEquipierComponent,
    GnrLspccOperationProtectionchutes_2ebinomeComponent,
    GnrLspccOperationProtectionfixehumainChefdagresComponent,
    GnrLspccOperationProtectionfixehumainChefdequipeComponent,
    GnrLspccOperationProtectionfixehumainEquipierComponent,
    GnrLspccOperationProtectionfixehumain_2ebinomeComponent,
    GnretablissementlancesManoeuvresGnrM1ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM1ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM1EquipierComponent,
    GnretablissementlancesManoeuvresGnrM1BalComponent,
    GnretablissementlancesManoeuvresGnrM2ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM2ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM2EquipierComponent,
    GnretablissementlancesManoeuvresGnrM3ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM3ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM3EquipierComponent,
    GnretablissementlancesManoeuvresGnrM4ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM4ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM4EquipierComponent,
    GnretablissementlancesManoeuvresGnrM5ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM5ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM5EquipierComponent,
    GnretablissementlancesManoeuvresGnrM5BalComponent,
    GnretablissementlancesManoeuvresGnrM6ChefdagresComponent,
    GnretablissementlancesManoeuvresGnrM6ChefdequipeComponent,
    GnretablissementlancesManoeuvresGnrM6EquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurChefdequipeComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurEquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvexterieurBalComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseChefdequipeComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseEquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvechellecoulisseBalComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneChefdequipeComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneEquipierComponent,
    GnretablissementlancesManoeuvrescomplLdvcolonneBalComponent,
    GnretablissementlancesManoeuvrescomplLdvpoteauChefdagresComponent,
    GnretablissementlancesManoeuvrescomplLdvpoteauBatComponent,
    GnretablissementlancesManoeuvrescomplLdvpoteauBalComponent,
    GnretablissementlancesTuyauxechevauxLdvprisedeauChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvprisedeauChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvprisedeauEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvattaqueChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvattaqueChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvattaqueEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvexterieurBalComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdagresComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseChefdequipeComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseEquipierComponent,
    GnretablissementlancesTuyauxechevauxLdvechelleacoulisseBalComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationChefdagresComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationChefdequipeComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationEquipierComponent,
    GnretablissementlancesSacdattaqueLdvcommunicationBalComponent,
    GnretablissementlancesSacdattaqueLdvexterieurChefdadresComponent,
    GnretablissementlancesSacdattaqueLdvexterieurChefdequipeComponent,
    GnretablissementlancesSacdattaqueLdvexterieurEquipierComponent,
    GnretablissementlancesSacdattaqueLdvexterieurBalComponent,
    GocOutilsgraphiquesSituationComponent,
    GocOutilsgraphiquesActionsComponent,
    GocOutilsgraphiquesMoyensComponent,
    GocOutilsgraphiquesCharteComponent,
    MaterielsSapSanglearaigneeDescriptionComponent,
    MaterielsSapSanglearaigneeIndicationsComponent,
    MaterielsSapSanglearaigneeRisquesComponent,
    MaterielsSapSanglearaigneeUtilisationComponent,
    MaterielsSapSanglearaigneePointsclesComponent,
    MaterielsSapSanglearaigneeCriteresdefficaciteComponent,
    MaterielSapAttelledetractionDescriptionComponent,
    MaterielSapAttelledetractionIndicationsComponent,
    MaterielSapAttelledetractionRisquesComponent,
    MaterielSapAttelledetractionUtilisationComponent,
    MaterielSapAttelledetractionPointsclesComponent,
    MaterielSapAttelledetractionEfficacitéComponent,
    MaterielsSapAspirateurdemucositesDescriptionComponent,
    MaterielsSapAspirateurdemucositesIndicationsComponent,
    MaterielsSapAspirateurdemucositesRisquesComponent,
    MaterielsSapAspirateurdemucositesUtilisationComponent,
    MaterielsSapAspirateurdemucositesPointsclesComponent,
    MaterielsSapAspirateurdemucositesEfficaciteComponent,
    MaterielsSapAttellecervicothoraciqueDescriptionComponent,
    MaterielsSapAttellecervicothoraciqueIndicationsComponent,
    MaterielsSapAttellecervicothoraciqueRisquesComponent,
    MaterielsSapAttellecervicothoraciqueUtilisationComponent,
    MaterielsSapAttellecervicothoraciquePointsclesComponent,
    MaterielsSapAttellecervicothoraciqueEfficaciteComponent,
    MaterielsSapBrancardcuillereDescriptionComponent,
    MaterielsSapBrancardcuillereIndicationsComponent,
    MaterielsSapBrancardcuillereRisquesComponent,
    MaterielsSapBrancardcuillereUtilisationComponent,
    MaterielsSapBrancardcuillerePointsclesComponent,
    MaterielsSapBrancardcuillereEfficaciteComponent,
    MaterielsSapDaeDescriptionComponent,
    MaterielsSapDaeIndicationsComponent,
    MaterielsSapDaeRisquesComponent,
    MaterielsSapDaeUtilisationComponent,
    MaterielsSapDaePointsclesComponent,
    MaterielsSapDaeEfficaciteComponent,
    MaterielsSapGarrotDescriptionComponent,
    MaterielsSapGarrotIndicationsComponent,
    MaterielsSapGarrotRisquesComponent,
    MaterielsSapGarrotUtilisationComponent,
    MaterielsSapGarrotPointsclesComponent,
    MaterielsSapGarrotEfficaciteComponent,
    MaterielsSapKitaesDescriptionComponent,
    MaterielsSapKitaesIndicationsComponent,
    MaterielsSapKitaesRisquesComponent,
    MaterielsSapKitaesUtilisationComponent,
    MaterielsSapKitbruluresDescriptionComponent,
    MaterielsSapKitbruluresIndicationsComponent,
    MaterielsSapKitbruluresRisquesComponent,
    MaterielsSapKitbruluresUtilisationComponent,
    MaterielsSapKitbruluresPointsclesComponent,
    MaterielsSapKitsectiondemembresDescriptionComponent,
    MaterielsSapKitsectiondemembresIndicationsComponent,
    MaterielsSapKitsectiondemembresRisquesComponent,
    MaterielsSapKitsectiondemembresUtilisationComponent,
    MaterielsSapKitsectiondemembresPointsclesComponent,
    MaterielsSapKitsectiondemembresEfficaciteComponent,
    MaterielsSapKitsinusDescriptionComponent,
    MaterielsSapKitsinusIndicationsComponent,
    MaterielsSapKitsinusRisquesComponent,
    MaterielsSapKitsinusUtilisationComponent,
    MaterielsSapLectureglycemieDescriptionComponent,
    MaterielsSapLectureglycemieIndicationsComponent,
    MaterielsSapLectureglycemieRisquesComponent,
    MaterielsSapLectureglycemieUtilisationComponent,
    MaterielsSapLectureglycemiePointsclesComponent,
    MaterielsSapLectureglycemieEfficaciteComponent,
    MaterielsSapMesurepressionarterielleDescriptionComponent,
    MaterielsSapMesurepressionarterielleIndicationsComponent,
    MaterielsSapMesurepressionarterielleRisquesComponent,
    MaterielsSapMesurepressionarterielleUtilisationComponent,
    MaterielsSapMesurepressionarteriellePointsclesComponent,
    MaterielsSapMesurepressionarterielleEfficaciteComponent,
    MaterielsSapOxymetredepoulsDescriptionComponent,
    MaterielsSapOxymetredepoulsIndicationsComponent,
    MaterielsSapOxymetredepoulsRisquesComponent,
    MaterielsSapOxymetredepoulsUtilisationComponent,
    MaterielsSapOxymetredepoulsPointsclesComponent,
    MaterielsSapOxymetredepoulsEfficaciteComponent,
    MaterielsSapThermometreDescriptionComponent,
    MaterielsSapThermometreIndicationsComponent,
    MaterielsSapThermometreRisquesComponent,
    MaterielsSapThermometreUtilisationComponent,
    MaterielsSapThermometrePointsclesComponent,
    MaterielsSapThermometreEfficaciteComponent,

]
})
export class ComponentsModule {}
