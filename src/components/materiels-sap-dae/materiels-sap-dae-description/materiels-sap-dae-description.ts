import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapDaeDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-dae-description',
  templateUrl: 'materiels-sap-dae-description.html'
})
export class MaterielsSapDaeDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapDaeDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
