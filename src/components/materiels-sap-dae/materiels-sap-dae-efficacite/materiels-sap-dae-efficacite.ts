import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapDaeEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-dae-efficacite',
  templateUrl: 'materiels-sap-dae-efficacite.html'
})
export class MaterielsSapDaeEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapDaeEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
