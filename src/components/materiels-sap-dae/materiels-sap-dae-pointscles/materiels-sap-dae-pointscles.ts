import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapDaePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-dae-pointscles',
  templateUrl: 'materiels-sap-dae-pointscles.html'
})
export class MaterielsSapDaePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapDaePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
