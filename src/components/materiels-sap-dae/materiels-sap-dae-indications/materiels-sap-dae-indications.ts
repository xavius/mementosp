import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapDaeIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-dae-indications',
  templateUrl: 'materiels-sap-dae-indications.html'
})
export class MaterielsSapDaeIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapDaeIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
