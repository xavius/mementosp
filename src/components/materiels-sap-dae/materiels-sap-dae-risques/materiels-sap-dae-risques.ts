import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapDaeRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-dae-risques',
  templateUrl: 'materiels-sap-dae-risques.html'
})
export class MaterielsSapDaeRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapDaeRisquesComponent Component');
    this.text = 'Hello World';
  }

}
