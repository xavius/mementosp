import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapDaeUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-dae-utilisation',
  templateUrl: 'materiels-sap-dae-utilisation.html'
})
export class MaterielsSapDaeUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapDaeUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
