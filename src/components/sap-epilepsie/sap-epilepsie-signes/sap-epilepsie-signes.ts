import { Component } from '@angular/core';

/**
 * Generated class for the SapEpilepsieSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-epilepsie-signes',
  templateUrl: 'sap-epilepsie-signes.html'
})
export class SapEpilepsieSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapEpilepsieSignesComponent Component');
    this.text = 'Hello World';
  }

}
