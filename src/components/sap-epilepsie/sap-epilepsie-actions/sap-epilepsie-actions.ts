import { Component } from '@angular/core';

/**
 * Generated class for the SapEpilepsieActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-epilepsie-actions',
  templateUrl: 'sap-epilepsie-actions.html'
})
export class SapEpilepsieActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapEpilepsieActionsComponent Component');
    this.text = 'Hello World';
  }

}
