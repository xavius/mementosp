import { Component } from '@angular/core';

/**
 * Generated class for the SapEpilepsieBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-epilepsie-bilans',
  templateUrl: 'sap-epilepsie-bilans.html'
})
export class SapEpilepsieBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapEpilepsieBilansComponent Component');
    this.text = 'Hello World';
  }

}
