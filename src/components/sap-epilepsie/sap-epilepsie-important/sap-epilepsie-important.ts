import { Component } from '@angular/core';

/**
 * Generated class for the SapEpilepsieImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-epilepsie-important',
  templateUrl: 'sap-epilepsie-important.html'
})
export class SapEpilepsieImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapEpilepsieImportantComponent Component');
    this.text = 'Hello World';
  }

}
