import { Component } from '@angular/core';

/**
 * Generated class for the FeuxSilosStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-silos-strategie',
  templateUrl: 'feux-silos-strategie.html'
})
export class FeuxSilosStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxSilosStrategieComponent Component');
    this.text = 'Hello World';
  }

}
