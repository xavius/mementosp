import { Component } from '@angular/core';

/**
 * Generated class for the FeuxSilosAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-silos-attention',
  templateUrl: 'feux-silos-attention.html'
})
export class FeuxSilosAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxSilosAttentionComponent Component');
    this.text = 'Hello World';
  }

}
