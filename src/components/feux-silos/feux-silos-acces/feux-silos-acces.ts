import { Component } from '@angular/core';

/**
 * Generated class for the FeuxSilosAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-silos-acces',
  templateUrl: 'feux-silos-acces.html'
})
export class FeuxSilosAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxSilosAccesComponent Component');
    this.text = 'Hello World';
  }

}
