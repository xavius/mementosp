import { Component } from '@angular/core';

/**
 * Generated class for the FeuxSilosActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-silos-action',
  templateUrl: 'feux-silos-action.html'
})
export class FeuxSilosActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxSilosActionComponent Component');
    this.text = 'Hello World';
  }

}
