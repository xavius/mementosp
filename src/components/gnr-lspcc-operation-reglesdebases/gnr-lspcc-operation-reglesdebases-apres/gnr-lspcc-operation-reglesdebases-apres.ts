import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationReglesdebasesApresComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-reglesdebases-apres',
  templateUrl: 'gnr-lspcc-operation-reglesdebases-apres.html'
})
export class GnrLspccOperationReglesdebasesApresComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationReglesdebasesApresComponent Component');
    this.text = 'Hello World';
  }

}
