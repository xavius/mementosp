import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationReglesdebasesPendantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-reglesdebases-pendant',
  templateUrl: 'gnr-lspcc-operation-reglesdebases-pendant.html'
})
export class GnrLspccOperationReglesdebasesPendantComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationReglesdebasesPendantComponent Component');
    this.text = 'Hello World';
  }

}
