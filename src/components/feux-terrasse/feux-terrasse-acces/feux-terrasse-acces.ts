import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTerrasseAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-terrasse-acces',
  templateUrl: 'feux-terrasse-acces.html'
})
export class FeuxTerrasseAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTerrasseAccesComponent Component');
    this.text = 'Hello World';
  }

}
