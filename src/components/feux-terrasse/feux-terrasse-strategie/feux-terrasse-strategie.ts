import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTerrasseStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-terrasse-strategie',
  templateUrl: 'feux-terrasse-strategie.html'
})
export class FeuxTerrasseStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTerrasseStrategieComponent Component');
    this.text = 'Hello World';
  }

}
