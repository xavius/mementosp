import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTerrasseActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-terrasse-action',
  templateUrl: 'feux-terrasse-action.html'
})
export class FeuxTerrasseActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTerrasseActionComponent Component');
    this.text = 'Hello World';
  }

}
