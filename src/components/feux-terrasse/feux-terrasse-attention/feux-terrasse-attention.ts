import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTerrasseAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-terrasse-attention',
  templateUrl: 'feux-terrasse-attention.html'
})
export class FeuxTerrasseAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTerrasseAttentionComponent Component');
    this.text = 'Hello World';
  }

}
