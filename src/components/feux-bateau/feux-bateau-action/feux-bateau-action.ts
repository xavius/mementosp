import { Component } from '@angular/core';

/**
 * Generated class for the FeuxBateauActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-bateau-action',
  templateUrl: 'feux-bateau-action.html'
})
export class FeuxBateauActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxBateauActionComponent Component');
    this.text = 'Hello World';
  }

}
