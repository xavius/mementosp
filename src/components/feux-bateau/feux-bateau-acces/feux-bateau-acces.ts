import { Component } from '@angular/core';

/**
 * Generated class for the FeuxBateauAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-bateau-acces',
  templateUrl: 'feux-bateau-acces.html'
})
export class FeuxBateauAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxBateauAccesComponent Component');
    this.text = 'Hello World';
  }

}
