import { Component } from '@angular/core';

/**
 * Generated class for the FeuxBateauAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-bateau-attention',
  templateUrl: 'feux-bateau-attention.html'
})
export class FeuxBateauAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxBateauAttentionComponent Component');
    this.text = 'Hello World';
  }

}
