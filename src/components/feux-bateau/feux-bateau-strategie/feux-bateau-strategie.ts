import { Component } from '@angular/core';

/**
 * Generated class for the FeuxBateauStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-bateau-strategie',
  templateUrl: 'feux-bateau-strategie.html'
})
export class FeuxBateauStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxBateauStrategieComponent Component');
    this.text = 'Hello World';
  }

}
