import { Component } from '@angular/core';

/**
 * Generated class for the SrDepavillonnage_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-depavillonnage-2',
  templateUrl: 'sr-depavillonnage-2.html'
})
export class SrDepavillonnage_2Component {

  text: string;

  constructor() {
    console.log('Hello SrDepavillonnage_2Component Component');
    this.text = 'Hello World';
  }

}
