import { Component } from '@angular/core';

/**
 * Generated class for the SrDepavillonnage_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-depavillonnage-3',
  templateUrl: 'sr-depavillonnage-3.html'
})
export class SrDepavillonnage_3Component {

  text: string;

  constructor() {
    console.log('Hello SrDepavillonnage_3Component Component');
    this.text = 'Hello World';
  }

}
