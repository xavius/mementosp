import { Component } from '@angular/core';

/**
 * Generated class for the SrDepavillonnage_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-depavillonnage-1',
  templateUrl: 'sr-depavillonnage-1.html'
})
export class SrDepavillonnage_1Component {

  text: string;

  constructor() {
    console.log('Hello SrDepavillonnage_1Component Component');
    this.text = 'Hello World';
  }

}
