import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapOxymetredepoulsEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-oxymetredepouls-efficacite',
  templateUrl: 'materiels-sap-oxymetredepouls-efficacite.html'
})
export class MaterielsSapOxymetredepoulsEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapOxymetredepoulsEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
