import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapOxymetredepoulsRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-oxymetredepouls-risques',
  templateUrl: 'materiels-sap-oxymetredepouls-risques.html'
})
export class MaterielsSapOxymetredepoulsRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapOxymetredepoulsRisquesComponent Component');
    this.text = 'Hello World';
  }

}
