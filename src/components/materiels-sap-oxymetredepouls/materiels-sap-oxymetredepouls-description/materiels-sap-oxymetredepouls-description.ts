import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapOxymetredepoulsDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-oxymetredepouls-description',
  templateUrl: 'materiels-sap-oxymetredepouls-description.html'
})
export class MaterielsSapOxymetredepoulsDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapOxymetredepoulsDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
