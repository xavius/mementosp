import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapOxymetredepoulsPointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-oxymetredepouls-pointscles',
  templateUrl: 'materiels-sap-oxymetredepouls-pointscles.html'
})
export class MaterielsSapOxymetredepoulsPointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapOxymetredepoulsPointsclesComponent Component');
    this.text = 'Hello World';
  }

}
