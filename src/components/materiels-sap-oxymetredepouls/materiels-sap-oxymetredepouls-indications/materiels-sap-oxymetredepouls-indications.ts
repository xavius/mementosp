import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapOxymetredepoulsIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-oxymetredepouls-indications',
  templateUrl: 'materiels-sap-oxymetredepouls-indications.html'
})
export class MaterielsSapOxymetredepoulsIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapOxymetredepoulsIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
