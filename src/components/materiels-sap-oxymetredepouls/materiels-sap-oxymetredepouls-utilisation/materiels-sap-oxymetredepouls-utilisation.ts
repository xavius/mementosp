import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapOxymetredepoulsUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-oxymetredepouls-utilisation',
  templateUrl: 'materiels-sap-oxymetredepouls-utilisation.html'
})
export class MaterielsSapOxymetredepoulsUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapOxymetredepoulsUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
