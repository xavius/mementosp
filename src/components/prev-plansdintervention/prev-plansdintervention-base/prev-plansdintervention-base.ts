import { Component } from '@angular/core';

/**
 * Generated class for the PrevPlansdinterventionBaseComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'prev-plansdintervention-base',
  templateUrl: 'prev-plansdintervention-base.html'
})
export class PrevPlansdinterventionBaseComponent {

  text: string;

  constructor() {
    console.log('Hello PrevPlansdinterventionBaseComponent Component');
    this.text = 'Hello World';
  }

}
