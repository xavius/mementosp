import { Component } from '@angular/core';

/**
 * Generated class for the GocOutilsgraphiquesActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'goc-outilsgraphiques-actions',
  templateUrl: 'goc-outilsgraphiques-actions.html'
})
export class GocOutilsgraphiquesActionsComponent {

  text: string;

  constructor() {
    console.log('Hello GocOutilsgraphiquesActionsComponent Component');
    this.text = 'Hello World';
  }

}
