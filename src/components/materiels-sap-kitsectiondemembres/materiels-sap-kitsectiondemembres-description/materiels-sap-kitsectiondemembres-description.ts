import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitsectiondemembresDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitsectiondemembres-description',
  templateUrl: 'materiels-sap-kitsectiondemembres-description.html'
})
export class MaterielsSapKitsectiondemembresDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitsectiondemembresDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
