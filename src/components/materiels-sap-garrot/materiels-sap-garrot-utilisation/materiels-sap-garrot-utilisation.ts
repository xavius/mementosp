import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapGarrotUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-garrot-utilisation',
  templateUrl: 'materiels-sap-garrot-utilisation.html'
})
export class MaterielsSapGarrotUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapGarrotUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
