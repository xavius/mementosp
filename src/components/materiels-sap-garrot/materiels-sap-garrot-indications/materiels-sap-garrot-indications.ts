import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapGarrotIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-garrot-indications',
  templateUrl: 'materiels-sap-garrot-indications.html'
})
export class MaterielsSapGarrotIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapGarrotIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
