import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapGarrotRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-garrot-risques',
  templateUrl: 'materiels-sap-garrot-risques.html'
})
export class MaterielsSapGarrotRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapGarrotRisquesComponent Component');
    this.text = 'Hello World';
  }

}
