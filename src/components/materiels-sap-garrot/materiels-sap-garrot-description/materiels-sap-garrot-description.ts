import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapGarrotDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-garrot-description',
  templateUrl: 'materiels-sap-garrot-description.html'
})
export class MaterielsSapGarrotDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapGarrotDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
