import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapGarrotPointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-garrot-pointscles',
  templateUrl: 'materiels-sap-garrot-pointscles.html'
})
export class MaterielsSapGarrotPointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapGarrotPointsclesComponent Component');
    this.text = 'Hello World';
  }

}
