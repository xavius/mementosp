import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapGarrotEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-garrot-efficacite',
  templateUrl: 'materiels-sap-garrot-efficacite.html'
})
export class MaterielsSapGarrotEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapGarrotEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
