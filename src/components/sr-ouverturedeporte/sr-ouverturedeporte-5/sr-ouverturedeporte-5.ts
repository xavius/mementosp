import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_5Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-5',
  templateUrl: 'sr-ouverturedeporte-5.html'
})
export class SrOuverturedeporte_5Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_5Component Component');
    this.text = 'Hello World';
  }

}
