import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-3',
  templateUrl: 'sr-ouverturedeporte-3.html'
})
export class SrOuverturedeporte_3Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_3Component Component');
    this.text = 'Hello World';
  }

}
