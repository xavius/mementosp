import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_4Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-4',
  templateUrl: 'sr-ouverturedeporte-4.html'
})
export class SrOuverturedeporte_4Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_4Component Component');
    this.text = 'Hello World';
  }

}
