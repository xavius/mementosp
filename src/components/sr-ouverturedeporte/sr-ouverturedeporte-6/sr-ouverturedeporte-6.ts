import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_6Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-6',
  templateUrl: 'sr-ouverturedeporte-6.html'
})
export class SrOuverturedeporte_6Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_6Component Component');
    this.text = 'Hello World';
  }

}
