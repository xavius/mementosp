import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_7Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-7',
  templateUrl: 'sr-ouverturedeporte-7.html'
})
export class SrOuverturedeporte_7Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_7Component Component');
    this.text = 'Hello World';
  }

}
