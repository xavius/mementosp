import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-1',
  templateUrl: 'sr-ouverturedeporte-1.html'
})
export class SrOuverturedeporte_1Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_1Component Component');
    this.text = 'Hello World';
  }

}
