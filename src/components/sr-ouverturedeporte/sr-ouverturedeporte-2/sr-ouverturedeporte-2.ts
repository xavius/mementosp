import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturedeporte_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturedeporte-2',
  templateUrl: 'sr-ouverturedeporte-2.html'
})
export class SrOuverturedeporte_2Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturedeporte_2Component Component');
    this.text = 'Hello World';
  }

}
