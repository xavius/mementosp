import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturelaterale_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturelaterale-2',
  templateUrl: 'sr-ouverturelaterale-2.html'
})
export class SrOuverturelaterale_2Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturelaterale_2Component Component');
    this.text = 'Hello World';
  }

}
