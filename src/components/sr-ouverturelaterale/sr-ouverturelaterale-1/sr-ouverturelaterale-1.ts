import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturelaterale_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturelaterale-1',
  templateUrl: 'sr-ouverturelaterale-1.html'
})
export class SrOuverturelaterale_1Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturelaterale_1Component Component');
    this.text = 'Hello World';
  }

}
