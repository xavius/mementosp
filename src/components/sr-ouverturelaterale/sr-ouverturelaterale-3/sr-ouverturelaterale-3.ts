import { Component } from '@angular/core';

/**
 * Generated class for the SrOuverturelaterale_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-ouverturelaterale-3',
  templateUrl: 'sr-ouverturelaterale-3.html'
})
export class SrOuverturelaterale_3Component {

  text: string;

  constructor() {
    console.log('Hello SrOuverturelaterale_3Component Component');
    this.text = 'Hello World';
  }

}
