import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMagasinInterditComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-magasin-interdit',
  templateUrl: 'feux-magasin-interdit.html'
})
export class FeuxMagasinInterditComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMagasinInterditComponent Component');
    this.text = 'Hello World';
  }

}
