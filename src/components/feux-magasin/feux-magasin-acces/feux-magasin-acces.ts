import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMagasinAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-magasin-acces',
  templateUrl: 'feux-magasin-acces.html'
})
export class FeuxMagasinAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMagasinAccesComponent Component');
    this.text = 'Hello World';
  }

}
