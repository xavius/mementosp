import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMagasinStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-magasin-strategie',
  templateUrl: 'feux-magasin-strategie.html'
})
export class FeuxMagasinStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMagasinStrategieComponent Component');
    this.text = 'Hello World';
  }

}
