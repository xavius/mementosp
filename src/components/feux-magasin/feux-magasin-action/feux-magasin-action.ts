import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMagasinActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-magasin-action',
  templateUrl: 'feux-magasin-action.html'
})
export class FeuxMagasinActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMagasinActionComponent Component');
    this.text = 'Hello World';
  }

}
