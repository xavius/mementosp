import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMagasinAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-magasin-attention',
  templateUrl: 'feux-magasin-attention.html'
})
export class FeuxMagasinAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMagasinAttentionComponent Component');
    this.text = 'Hello World';
  }

}
