import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAppartementRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-appartement-risques',
  templateUrl: 'feux-appartement-risques.html'
})
export class FeuxAppartementRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAppartementRisquesComponent Component');
    this.text = 'Hello World';
  }

}
