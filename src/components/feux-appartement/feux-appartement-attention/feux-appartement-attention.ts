import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAppartementAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-appartement-attention',
  templateUrl: 'feux-appartement-attention.html'
})
export class FeuxAppartementAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAppartementAttentionComponent Component');
    this.text = 'Hello World';
  }

}
