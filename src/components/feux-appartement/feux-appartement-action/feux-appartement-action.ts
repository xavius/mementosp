import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAppartementActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-appartement-action',
  templateUrl: 'feux-appartement-action.html'
})
export class FeuxAppartementActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAppartementActionComponent Component');
    this.text = 'Hello World';
  }

}
