import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAppartementStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-appartement-strategie',
  templateUrl: 'feux-appartement-strategie.html'
})
export class FeuxAppartementStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAppartementStrategieComponent Component');
    this.text = 'Hello World';
  }

}
