import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAppartementAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-appartement-acces',
  templateUrl: 'feux-appartement-acces.html'
})
export class FeuxAppartementAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAppartementAccesComponent Component');
    this.text = 'Hello World';
  }

}
