import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAppartementInterditComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-appartement-interdit',
  templateUrl: 'feux-appartement-interdit.html'
})
export class FeuxAppartementInterditComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAppartementInterditComponent Component');
    this.text = 'Hello World';
  }

}
