import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCombleAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-comble-acces',
  templateUrl: 'feux-comble-acces.html'
})
export class FeuxCombleAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCombleAccesComponent Component');
    this.text = 'Hello World';
  }

}
