import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCombleStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-comble-strategie',
  templateUrl: 'feux-comble-strategie.html'
})
export class FeuxCombleStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCombleStrategieComponent Component');
    this.text = 'Hello World';
  }

}
