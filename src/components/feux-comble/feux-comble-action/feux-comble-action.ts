import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCombleActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-comble-action',
  templateUrl: 'feux-comble-action.html'
})
export class FeuxCombleActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCombleActionComponent Component');
    this.text = 'Hello World';
  }

}
