import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCombleAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-comble-attention',
  templateUrl: 'feux-comble-attention.html'
})
export class FeuxCombleAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCombleAttentionComponent Component');
    this.text = 'Hello World';
  }

}
