import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitbruluresRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitbrulures-risques',
  templateUrl: 'materiels-sap-kitbrulures-risques.html'
})
export class MaterielsSapKitbruluresRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitbruluresRisquesComponent Component');
    this.text = 'Hello World';
  }

}
