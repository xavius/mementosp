import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitbruluresUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitbrulures-utilisation',
  templateUrl: 'materiels-sap-kitbrulures-utilisation.html'
})
export class MaterielsSapKitbruluresUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitbruluresUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
