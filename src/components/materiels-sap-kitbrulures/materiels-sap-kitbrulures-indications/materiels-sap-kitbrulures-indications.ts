import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitbruluresIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitbrulures-indications',
  templateUrl: 'materiels-sap-kitbrulures-indications.html'
})
export class MaterielsSapKitbruluresIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitbruluresIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
