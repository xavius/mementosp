import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitbruluresPointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitbrulures-pointscles',
  templateUrl: 'materiels-sap-kitbrulures-pointscles.html'
})
export class MaterielsSapKitbruluresPointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitbruluresPointsclesComponent Component');
    this.text = 'Hello World';
  }

}
