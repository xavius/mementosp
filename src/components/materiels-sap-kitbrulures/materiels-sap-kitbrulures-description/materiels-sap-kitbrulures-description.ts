import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitbruluresDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitbrulures-description',
  templateUrl: 'materiels-sap-kitbrulures-description.html'
})
export class MaterielsSapKitbruluresDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitbruluresDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
