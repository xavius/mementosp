import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecranienActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecranien-actions',
  templateUrl: 'sap-traumatismecranien-actions.html'
})
export class SapTraumatismecranienActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecranienActionsComponent Component');
    this.text = 'Hello World';
  }

}
