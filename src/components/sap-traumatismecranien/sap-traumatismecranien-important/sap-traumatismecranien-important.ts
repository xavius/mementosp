import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecranienImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecranien-important',
  templateUrl: 'sap-traumatismecranien-important.html'
})
export class SapTraumatismecranienImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecranienImportantComponent Component');
    this.text = 'Hello World';
  }

}
