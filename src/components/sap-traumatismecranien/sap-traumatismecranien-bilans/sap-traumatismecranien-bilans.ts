import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecranienBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecranien-bilans',
  templateUrl: 'sap-traumatismecranien-bilans.html'
})
export class SapTraumatismecranienBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecranienBilansComponent Component');
    this.text = 'Hello World';
  }

}
