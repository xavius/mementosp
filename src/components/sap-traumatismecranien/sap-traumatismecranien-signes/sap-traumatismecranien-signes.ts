import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecranienSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecranien-signes',
  templateUrl: 'sap-traumatismecranien-signes.html'
})
export class SapTraumatismecranienSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecranienSignesComponent Component');
    this.text = 'Hello World';
  }

}
