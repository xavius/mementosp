import { Component } from '@angular/core';

/**
 * Generated class for the SrCoquilledhuitre_4Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-coquilledhuitre-4',
  templateUrl: 'sr-coquilledhuitre-4.html'
})
export class SrCoquilledhuitre_4Component {

  text: string;

  constructor() {
    console.log('Hello SrCoquilledhuitre_4Component Component');
    this.text = 'Hello World';
  }

}
