import { Component } from '@angular/core';

/**
 * Generated class for the SrCoquilledhuitre_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-coquilledhuitre-2',
  templateUrl: 'sr-coquilledhuitre-2.html'
})
export class SrCoquilledhuitre_2Component {

  text: string;

  constructor() {
    console.log('Hello SrCoquilledhuitre_2Component Component');
    this.text = 'Hello World';
  }

}
