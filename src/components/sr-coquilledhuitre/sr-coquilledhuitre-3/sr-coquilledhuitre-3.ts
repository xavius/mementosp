import { Component } from '@angular/core';

/**
 * Generated class for the SrCoquilledhuitre_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-coquilledhuitre-3',
  templateUrl: 'sr-coquilledhuitre-3.html'
})
export class SrCoquilledhuitre_3Component {

  text: string;

  constructor() {
    console.log('Hello SrCoquilledhuitre_3Component Component');
    this.text = 'Hello World';
  }

}
