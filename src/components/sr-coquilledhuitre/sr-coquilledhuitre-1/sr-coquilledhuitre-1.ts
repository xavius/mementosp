import { Component } from '@angular/core';

/**
 * Generated class for the SrCoquilledhuitre_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-coquilledhuitre-1',
  templateUrl: 'sr-coquilledhuitre-1.html'
})
export class SrCoquilledhuitre_1Component {

  text: string;

  constructor() {
    console.log('Hello SrCoquilledhuitre_1Component Component');
    this.text = 'Hello World';
  }

}
