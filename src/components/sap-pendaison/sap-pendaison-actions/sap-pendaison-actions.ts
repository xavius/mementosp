import { Component } from '@angular/core';

/**
 * Generated class for the SapPendaisonActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-pendaison-actions',
  templateUrl: 'sap-pendaison-actions.html'
})
export class SapPendaisonActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapPendaisonActionsComponent Component');
    this.text = 'Hello World';
  }

}
