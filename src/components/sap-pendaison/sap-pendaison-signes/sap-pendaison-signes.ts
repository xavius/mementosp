import { Component } from '@angular/core';

/**
 * Generated class for the SapPendaisonSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-pendaison-signes',
  templateUrl: 'sap-pendaison-signes.html'
})
export class SapPendaisonSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapPendaisonSignesComponent Component');
    this.text = 'Hello World';
  }

}
