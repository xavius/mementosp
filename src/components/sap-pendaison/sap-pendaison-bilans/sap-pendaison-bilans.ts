import { Component } from '@angular/core';

/**
 * Generated class for the SapPendaisonBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-pendaison-bilans',
  templateUrl: 'sap-pendaison-bilans.html'
})
export class SapPendaisonBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapPendaisonBilansComponent Component');
    this.text = 'Hello World';
  }

}
