import { Component } from '@angular/core';

/**
 * Generated class for the SapPendaisonImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-pendaison-important',
  templateUrl: 'sap-pendaison-important.html'
})
export class SapPendaisonImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapPendaisonImportantComponent Component');
    this.text = 'Hello World';
  }

}
