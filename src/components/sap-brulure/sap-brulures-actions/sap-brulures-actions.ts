import { Component } from '@angular/core';

/**
 * Generated class for the SapBruluresActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-brulures-actions',
  templateUrl: 'sap-brulures-actions.html'
})
export class SapBruluresActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapBruluresActionsComponent Component');
    this.text = 'Hello World';
  }

}
