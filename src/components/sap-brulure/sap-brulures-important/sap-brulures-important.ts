import { Component } from '@angular/core';

/**
 * Generated class for the SapBruluresImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-brulures-important',
  templateUrl: 'sap-brulures-important.html'
})
export class SapBruluresImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapBruluresImportantComponent Component');
    this.text = 'Hello World';
  }

}
