import { Component } from '@angular/core';

/**
 * Generated class for the SapBruluresBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-brulures-bilans',
  templateUrl: 'sap-brulures-bilans.html'
})
export class SapBruluresBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapBruluresBilansComponent Component');
    this.text = 'Hello World';
  }

}
