import { Component } from '@angular/core';

/**
 * Generated class for the SapBruluresSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-brulures-signes',
  templateUrl: 'sap-brulures-signes.html'
})
export class SapBruluresSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapBruluresSignesComponent Component');
    this.text = 'Hello World';
  }

}
