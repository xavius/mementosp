import { Component } from '@angular/core';

/**
 * Generated class for the FeuxDentrepôtAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-dentrepôt-attention',
  templateUrl: 'feux-dentrepôt-attention.html'
})
export class FeuxDentrepôtAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxDentrepôtAttentionComponent Component');
    this.text = 'Hello World';
  }

}
