import { Component } from '@angular/core';

/**
 * Generated class for the FeuxDentrepôtStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-dentrepôt-strategie',
  templateUrl: 'feux-dentrepôt-strategie.html'
})
export class FeuxDentrepôtStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxDentrepôtStrategieComponent Component');
    this.text = 'Hello World';
  }

}
