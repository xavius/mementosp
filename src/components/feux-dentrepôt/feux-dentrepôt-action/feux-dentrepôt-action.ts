import { Component } from '@angular/core';

/**
 * Generated class for the FeuxDentrepôtActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-dentrepôt-action',
  templateUrl: 'feux-dentrepôt-action.html'
})
export class FeuxDentrepôtActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxDentrepôtActionComponent Component');
    this.text = 'Hello World';
  }

}
