import { Component } from '@angular/core';

/**
 * Generated class for the FeuxDentrepôtAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-dentrepôt-acces',
  templateUrl: 'feux-dentrepôt-acces.html'
})
export class FeuxDentrepôtAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxDentrepôtAccesComponent Component');
    this.text = 'Hello World';
  }

}
