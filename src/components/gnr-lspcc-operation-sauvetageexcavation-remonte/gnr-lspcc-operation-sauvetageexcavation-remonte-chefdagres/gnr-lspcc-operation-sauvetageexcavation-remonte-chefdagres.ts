import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexcavation-remonte-chefdagres',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-remonte-chefdagres.html'
})
export class GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexcavationRemonteChefdagresComponent Component');
    this.text = 'Hello World';
  }

}
