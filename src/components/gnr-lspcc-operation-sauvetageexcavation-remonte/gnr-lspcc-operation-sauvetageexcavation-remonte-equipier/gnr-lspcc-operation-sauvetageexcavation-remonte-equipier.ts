import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationRemonteEquipierComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexcavation-remonte-equipier',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-remonte-equipier.html'
})
export class GnrLspccOperationSauvetageexcavationRemonteEquipierComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexcavationRemonteEquipierComponent Component');
    this.text = 'Hello World';
  }

}
