import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexcavation-exploration-chefdequipe',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-exploration-chefdequipe.html'
})
export class GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexcavationExplorationChefdequipeComponent Component');
    this.text = 'Hello World';
  }

}
