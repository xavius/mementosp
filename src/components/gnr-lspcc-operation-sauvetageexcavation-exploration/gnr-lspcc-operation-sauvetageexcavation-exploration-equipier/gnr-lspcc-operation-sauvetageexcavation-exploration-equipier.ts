import { Component } from '@angular/core';

/**
 * Generated class for the GnrLspccOperationSauvetageexcavationExplorationEquipierComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-lspcc-operation-sauvetageexcavation-exploration-equipier',
  templateUrl: 'gnr-lspcc-operation-sauvetageexcavation-exploration-equipier.html'
})
export class GnrLspccOperationSauvetageexcavationExplorationEquipierComponent {

  text: string;

  constructor() {
    console.log('Hello GnrLspccOperationSauvetageexcavationExplorationEquipierComponent Component');
    this.text = 'Hello World';
  }

}
