import { Component } from '@angular/core';

/**
 * Generated class for the SapPiqureshymenopteresImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-piqureshymenopteres-important',
  templateUrl: 'sap-piqureshymenopteres-important.html'
})
export class SapPiqureshymenopteresImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapPiqureshymenopteresImportantComponent Component');
    this.text = 'Hello World';
  }

}
