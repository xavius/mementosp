import { Component } from '@angular/core';

/**
 * Generated class for the SapPiqureshymenopteresActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-piqureshymenopteres-actions',
  templateUrl: 'sap-piqureshymenopteres-actions.html'
})
export class SapPiqureshymenopteresActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapPiqureshymenopteresActionsComponent Component');
    this.text = 'Hello World';
  }

}
