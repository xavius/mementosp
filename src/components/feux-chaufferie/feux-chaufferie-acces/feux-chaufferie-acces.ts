import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferie-acces',
  templateUrl: 'feux-chaufferie-acces.html'
})
export class FeuxChaufferieAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieAccesComponent Component');
    this.text = 'Hello World';
  }

}
