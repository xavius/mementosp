import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferie-strategie',
  templateUrl: 'feux-chaufferie-strategie.html'
})
export class FeuxChaufferieStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieStrategieComponent Component');
    this.text = 'Hello World';
  }

}
