import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferie-attention',
  templateUrl: 'feux-chaufferie-attention.html'
})
export class FeuxChaufferieAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieAttentionComponent Component');
    this.text = 'Hello World';
  }

}
