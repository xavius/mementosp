import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferie-action',
  templateUrl: 'feux-chaufferie-action.html'
})
export class FeuxChaufferieActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieActionComponent Component');
    this.text = 'Hello World';
  }

}
