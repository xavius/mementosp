import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitsinusRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitsinus-risques',
  templateUrl: 'materiels-sap-kitsinus-risques.html'
})
export class MaterielsSapKitsinusRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitsinusRisquesComponent Component');
    this.text = 'Hello World';
  }

}
