import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitsinusIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitsinus-indications',
  templateUrl: 'materiels-sap-kitsinus-indications.html'
})
export class MaterielsSapKitsinusIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitsinusIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
