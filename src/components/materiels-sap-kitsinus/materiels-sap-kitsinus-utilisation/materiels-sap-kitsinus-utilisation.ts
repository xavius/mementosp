import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitsinusUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitsinus-utilisation',
  templateUrl: 'materiels-sap-kitsinus-utilisation.html'
})
export class MaterielsSapKitsinusUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitsinusUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
