import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapKitsinusDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-kitsinus-description',
  templateUrl: 'materiels-sap-kitsinus-description.html'
})
export class MaterielsSapKitsinusDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapKitsinusDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
