import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeSvytAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-svyt-attention',
  templateUrl: 'feux-erp-type-svyt-attention.html'
})
export class FeuxErpTypeSvytAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeSvytAttentionComponent Component');
    this.text = 'Hello World';
  }

}
