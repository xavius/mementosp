import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeSvytActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-svyt-action',
  templateUrl: 'feux-erp-type-svyt-action.html'
})
export class FeuxErpTypeSvytActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeSvytActionComponent Component');
    this.text = 'Hello World';
  }

}
