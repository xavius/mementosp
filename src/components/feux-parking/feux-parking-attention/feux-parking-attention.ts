import { Component } from '@angular/core';

/**
 * Generated class for the FeuxParkingAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-parking-attention',
  templateUrl: 'feux-parking-attention.html'
})
export class FeuxParkingAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxParkingAttentionComponent Component');
    this.text = 'Hello World';
  }

}
