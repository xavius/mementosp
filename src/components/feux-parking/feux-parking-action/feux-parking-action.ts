import { Component } from '@angular/core';

/**
 * Generated class for the FeuxParkingActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-parking-action',
  templateUrl: 'feux-parking-action.html'
})
export class FeuxParkingActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxParkingActionComponent Component');
    this.text = 'Hello World';
  }

}
