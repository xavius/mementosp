import { Component } from '@angular/core';

/**
 * Generated class for the FeuxParkingStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-parking-strategie',
  templateUrl: 'feux-parking-strategie.html'
})
export class FeuxParkingStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxParkingStrategieComponent Component');
    this.text = 'Hello World';
  }

}
