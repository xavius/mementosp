import { Component } from '@angular/core';

/**
 * Generated class for the FeuxParkingAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-parking-acces',
  templateUrl: 'feux-parking-acces.html'
})
export class FeuxParkingAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxParkingAccesComponent Component');
    this.text = 'Hello World';
  }

}
