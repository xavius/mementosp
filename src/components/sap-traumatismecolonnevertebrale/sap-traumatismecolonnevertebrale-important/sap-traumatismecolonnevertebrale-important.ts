import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecolonnevertebraleImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecolonnevertebrale-important',
  templateUrl: 'sap-traumatismecolonnevertebrale-important.html'
})
export class SapTraumatismecolonnevertebraleImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecolonnevertebraleImportantComponent Component');
    this.text = 'Hello World';
  }

}
