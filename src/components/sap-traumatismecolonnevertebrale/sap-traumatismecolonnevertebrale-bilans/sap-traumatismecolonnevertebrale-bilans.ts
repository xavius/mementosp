import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecolonnevertebraleBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecolonnevertebrale-bilans',
  templateUrl: 'sap-traumatismecolonnevertebrale-bilans.html'
})
export class SapTraumatismecolonnevertebraleBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecolonnevertebraleBilansComponent Component');
    this.text = 'Hello World';
  }

}
