import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecolonnevertebraleSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecolonnevertebrale-signes',
  templateUrl: 'sap-traumatismecolonnevertebrale-signes.html'
})
export class SapTraumatismecolonnevertebraleSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecolonnevertebraleSignesComponent Component');
    this.text = 'Hello World';
  }

}
