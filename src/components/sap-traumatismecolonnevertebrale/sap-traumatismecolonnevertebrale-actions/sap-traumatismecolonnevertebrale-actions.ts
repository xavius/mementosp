import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismecolonnevertebraleActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismecolonnevertebrale-actions',
  templateUrl: 'sap-traumatismecolonnevertebrale-actions.html'
})
export class SapTraumatismecolonnevertebraleActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismecolonnevertebraleActionsComponent Component');
    this.text = 'Hello World';
  }

}
