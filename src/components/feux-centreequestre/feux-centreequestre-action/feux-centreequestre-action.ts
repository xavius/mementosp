import { Component } from '@angular/core';

/**
 * Generated class for the FeuxCentreequestreActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-centreequestre-action',
  templateUrl: 'feux-centreequestre-action.html'
})
export class FeuxCentreequestreActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxCentreequestreActionComponent Component');
    this.text = 'Hello World';
  }

}
