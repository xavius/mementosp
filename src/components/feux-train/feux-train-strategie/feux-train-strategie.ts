import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTrainStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-train-strategie',
  templateUrl: 'feux-train-strategie.html'
})
export class FeuxTrainStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTrainStrategieComponent Component');
    this.text = 'Hello World';
  }

}
