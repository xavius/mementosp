import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTrainAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-train-acces',
  templateUrl: 'feux-train-acces.html'
})
export class FeuxTrainAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTrainAccesComponent Component');
    this.text = 'Hello World';
  }

}
