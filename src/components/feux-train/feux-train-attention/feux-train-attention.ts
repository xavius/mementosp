import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTrainAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-train-attention',
  templateUrl: 'feux-train-attention.html'
})
export class FeuxTrainAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTrainAttentionComponent Component');
    this.text = 'Hello World';
  }

}
