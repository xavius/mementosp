import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTrainActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-train-action',
  templateUrl: 'feux-train-action.html'
})
export class FeuxTrainActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTrainActionComponent Component');
    this.text = 'Hello World';
  }

}
