import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapBrancardcuillereIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-brancardcuillere-indications',
  templateUrl: 'materiels-sap-brancardcuillere-indications.html'
})
export class MaterielsSapBrancardcuillereIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapBrancardcuillereIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
