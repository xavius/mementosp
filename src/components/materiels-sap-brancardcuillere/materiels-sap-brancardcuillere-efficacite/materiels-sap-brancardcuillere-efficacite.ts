import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapBrancardcuillereEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-brancardcuillere-efficacite',
  templateUrl: 'materiels-sap-brancardcuillere-efficacite.html'
})
export class MaterielsSapBrancardcuillereEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapBrancardcuillereEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
