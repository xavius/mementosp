import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapBrancardcuillerePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-brancardcuillere-pointscles',
  templateUrl: 'materiels-sap-brancardcuillere-pointscles.html'
})
export class MaterielsSapBrancardcuillerePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapBrancardcuillerePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
