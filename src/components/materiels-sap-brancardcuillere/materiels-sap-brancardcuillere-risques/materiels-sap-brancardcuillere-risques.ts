import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapBrancardcuillereRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-brancardcuillere-risques',
  templateUrl: 'materiels-sap-brancardcuillere-risques.html'
})
export class MaterielsSapBrancardcuillereRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapBrancardcuillereRisquesComponent Component');
    this.text = 'Hello World';
  }

}
