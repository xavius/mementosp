import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapBrancardcuillereDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-brancardcuillere-description',
  templateUrl: 'materiels-sap-brancardcuillere-description.html'
})
export class MaterielsSapBrancardcuillereDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapBrancardcuillereDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
