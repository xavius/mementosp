import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapBrancardcuillereUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-brancardcuillere-utilisation',
  templateUrl: 'materiels-sap-brancardcuillere-utilisation.html'
})
export class MaterielsSapBrancardcuillereUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapBrancardcuillereUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
