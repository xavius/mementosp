import { Component } from '@angular/core';

/**
 * Generated class for the SapAvcBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-avc-bilans',
  templateUrl: 'sap-avc-bilans.html'
})
export class SapAvcBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapAvcBilansComponent Component');
    this.text = 'Hello World';
  }

}
