import { Component } from '@angular/core';

/**
 * Generated class for the SapAvcSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-avc-signes',
  templateUrl: 'sap-avc-signes.html'
})
export class SapAvcSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapAvcSignesComponent Component');
    this.text = 'Hello World';
  }

}
