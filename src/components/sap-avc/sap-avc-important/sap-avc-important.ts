import { Component } from '@angular/core';

/**
 * Generated class for the SapAvcImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-avc-important',
  templateUrl: 'sap-avc-important.html'
})
export class SapAvcImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapAvcImportantComponent Component');
    this.text = 'Hello World';
  }

}
