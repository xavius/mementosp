import { Component } from '@angular/core';

/**
 * Generated class for the SapAvcActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-avc-actions',
  templateUrl: 'sap-avc-actions.html'
})
export class SapAvcActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapAvcActionsComponent Component');
    this.text = 'Hello World';
  }

}
