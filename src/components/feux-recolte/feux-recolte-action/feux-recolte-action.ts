import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRecolteActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-recolte-action',
  templateUrl: 'feux-recolte-action.html'
})
export class FeuxRecolteActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRecolteActionComponent Component');
    this.text = 'Hello World';
  }

}
