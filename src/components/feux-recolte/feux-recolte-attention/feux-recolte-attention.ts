import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRecolteAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-recolte-attention',
  templateUrl: 'feux-recolte-attention.html'
})
export class FeuxRecolteAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRecolteAttentionComponent Component');
    this.text = 'Hello World';
  }

}
