import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRecolteAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-recolte-acces',
  templateUrl: 'feux-recolte-acces.html'
})
export class FeuxRecolteAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRecolteAccesComponent Component');
    this.text = 'Hello World';
  }

}
