import { Component } from '@angular/core';

/**
 * Generated class for the FeuxRecolteStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-recolte-strategie',
  templateUrl: 'feux-recolte-strategie.html'
})
export class FeuxRecolteStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxRecolteStrategieComponent Component');
    this.text = 'Hello World';
  }

}
