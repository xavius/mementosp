import { Component } from '@angular/core';

/**
 * Generated class for the FeuxLiquidesinflammablesActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-liquidesinflammables-action',
  templateUrl: 'feux-liquidesinflammables-action.html'
})
export class FeuxLiquidesinflammablesActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxLiquidesinflammablesActionComponent Component');
    this.text = 'Hello World';
  }

}
