import { Component } from '@angular/core';

/**
 * Generated class for the FeuxLiquidesinflammablesAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-liquidesinflammables-attention',
  templateUrl: 'feux-liquidesinflammables-attention.html'
})
export class FeuxLiquidesinflammablesAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxLiquidesinflammablesAttentionComponent Component');
    this.text = 'Hello World';
  }

}
