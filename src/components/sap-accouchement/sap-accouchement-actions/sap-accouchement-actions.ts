import { Component } from '@angular/core';

/**
 * Generated class for the SapAccouchementActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-accouchement-actions',
  templateUrl: 'sap-accouchement-actions.html'
})
export class SapAccouchementActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapAccouchementActionsComponent Component');
    this.text = 'Hello World';
  }

}
