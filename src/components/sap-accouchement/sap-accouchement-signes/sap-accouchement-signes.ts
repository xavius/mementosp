import { Component } from '@angular/core';

/**
 * Generated class for the SapAccouchementSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-accouchement-signes',
  templateUrl: 'sap-accouchement-signes.html'
})
export class SapAccouchementSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapAccouchementSignesComponent Component');
    this.text = 'Hello World';
  }

}
