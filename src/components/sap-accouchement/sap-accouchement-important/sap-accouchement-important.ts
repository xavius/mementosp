import { Component } from '@angular/core';

/**
 * Generated class for the SapAccouchementImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-accouchement-important',
  templateUrl: 'sap-accouchement-important.html'
})
export class SapAccouchementImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapAccouchementImportantComponent Component');
    this.text = 'Hello World';
  }

}
