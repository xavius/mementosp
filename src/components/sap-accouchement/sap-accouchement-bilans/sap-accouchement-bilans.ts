import { Component } from '@angular/core';

/**
 * Generated class for the SapAccouchementBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-accouchement-bilans',
  templateUrl: 'sap-accouchement-bilans.html'
})
export class SapAccouchementBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapAccouchementBilansComponent Component');
    this.text = 'Hello World';
  }

}
