import { NgModule } from '@angular/core';
import { HeaderMenuComponent } from '../header-menu/header-menu';
import { FeuxAppartementInterditComponent } from '../feux-appartement/feux-appartement-interdit/feux-appartement-interdit';
import { FeuxAppartementStrategieComponent } from '../feux-appartement/feux-appartement-strategie/feux-appartement-strategie';
import { FeuxAppartementRisquesComponent } from '../feux-appartement/feux-appartement-risques/feux-appartement-risques';
import { FeuxAppartementAccesComponent } from '../feux-appartement/feux-appartement-acces/feux-appartement-acces';
import { FeuxAppartementActionComponent } from '../feux-appartement/feux-appartement-action/feux-appartement-action';
import { FeuxAppartementAttentionComponent } from '../feux-appartement/feux-appartement-attention/feux-appartement-attention';
import { FeuxMaisonRisquesComponent } from '../feux-maison/feux-maison-risques/feux-maison-risques';
import { FeuxMaisonStrategieComponent } from '../feux-maison/feux-maison-strategie/feux-maison-strategie';
import { FeuxMaisonAccesComponent } from '../feux-maison/feux-maison-acces/feux-maison-acces';
import { FeuxMaisonActionComponent } from '../feux-maison/feux-maison-action/feux-maison-action';
import { FeuxMaisonAttentionComponent } from '../feux-maison/feux-maison-attention/feux-maison-attention';
import { FeuxMaisonInterditComponent } from '../feux-maison/feux-maison-interdit/feux-maison-interdit';


@NgModule({
	declarations: [HeaderMenuComponent,
    FeuxAppartementInterditComponent,
    FeuxAppartementStrategieComponent,
    FeuxAppartementRisquesComponent,
    FeuxAppartementAccesComponent,
    FeuxAppartementActionComponent,
    FeuxAppartementAttentionComponent,
    FeuxMaisonRisquesComponent,
    FeuxMaisonStrategieComponent,
    FeuxMaisonAccesComponent,
    FeuxMaisonActionComponent,
    FeuxMaisonAttentionComponent,
    FeuxMaisonInterditComponent],
	imports: [],
	exports: [HeaderMenuComponent,
    FeuxAppartementInterditComponent,
    FeuxAppartementStrategieComponent,
    FeuxAppartementRisquesComponent,
    FeuxAppartementAccesComponent,
    FeuxAppartementActionComponent,
    FeuxAppartementAttentionComponent,
    FeuxMaisonRisquesComponent,
    FeuxMaisonStrategieComponent,
    FeuxMaisonAccesComponent,
    FeuxMaisonActionComponent,
    FeuxMaisonAttentionComponent,
    FeuxMaisonInterditComponent],
})
export class ComponentsModule {}
