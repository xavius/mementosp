import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMaisonAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-maison-attention',
  templateUrl: 'feux-maison-attention.html'
})
export class FeuxMaisonAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMaisonAttentionComponent Component');
    this.text = 'Hello World';
  }

}
