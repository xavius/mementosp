import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMaisonAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-maison-acces',
  templateUrl: 'feux-maison-acces.html'
})
export class FeuxMaisonAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMaisonAccesComponent Component');
    this.text = 'Hello World';
  }

}
