import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMaisonActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-maison-action',
  templateUrl: 'feux-maison-action.html'
})
export class FeuxMaisonActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMaisonActionComponent Component');
    this.text = 'Hello World';
  }

}
