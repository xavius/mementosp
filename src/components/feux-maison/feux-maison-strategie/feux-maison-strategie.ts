import { Component } from '@angular/core';

/**
 * Generated class for the FeuxMaisonStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-maison-strategie',
  templateUrl: 'feux-maison-strategie.html'
})
export class FeuxMaisonStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxMaisonStrategieComponent Component');
    this.text = 'Hello World';
  }

}
