import { Component } from '@angular/core';

/**
 * Generated class for the GocCadredordreSmesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'goc-cadredordre-smes',
  templateUrl: 'goc-cadredordre-smes.html'
})
export class GocCadredordreSmesComponent {

  text: string;

  constructor() {
    console.log('Hello GocCadredordreSmesComponent Component');
    this.text = 'Hello World';
  }

}
