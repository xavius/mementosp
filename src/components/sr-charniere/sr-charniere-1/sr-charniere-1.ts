import { Component } from '@angular/core';

/**
 * Generated class for the SrCharniere_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-charniere-1',
  templateUrl: 'sr-charniere-1.html'
})
export class SrCharniere_1Component {

  text: string;

  constructor() {
    console.log('Hello SrCharniere_1Component Component');
    this.text = 'Hello World';
  }

}
