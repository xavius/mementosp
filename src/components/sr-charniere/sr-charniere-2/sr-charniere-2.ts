import { Component } from '@angular/core';

/**
 * Generated class for the SrCharniere_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-charniere-2',
  templateUrl: 'sr-charniere-2.html'
})
export class SrCharniere_2Component {

  text: string;

  constructor() {
    console.log('Hello SrCharniere_2Component Component');
    this.text = 'Hello World';
  }

}
