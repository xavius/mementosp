import { Component } from '@angular/core';

/**
 * Generated class for the SrCharniere_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-charniere-3',
  templateUrl: 'sr-charniere-3.html'
})
export class SrCharniere_3Component {

  text: string;

  constructor() {
    console.log('Hello SrCharniere_3Component Component');
    this.text = 'Hello World';
  }

}
