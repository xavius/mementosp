import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTunnelAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-tunnel-attention',
  templateUrl: 'feux-tunnel-attention.html'
})
export class FeuxTunnelAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTunnelAttentionComponent Component');
    this.text = 'Hello World';
  }

}
