import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTunnelAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-tunnel-acces',
  templateUrl: 'feux-tunnel-acces.html'
})
export class FeuxTunnelAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTunnelAccesComponent Component');
    this.text = 'Hello World';
  }

}
