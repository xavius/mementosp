import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTunnelActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-tunnel-action',
  templateUrl: 'feux-tunnel-action.html'
})
export class FeuxTunnelActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTunnelActionComponent Component');
    this.text = 'Hello World';
  }

}
