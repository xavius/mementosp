import { Component } from '@angular/core';

/**
 * Generated class for the FeuxTunnelStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-tunnel-strategie',
  templateUrl: 'feux-tunnel-strategie.html'
})
export class FeuxTunnelStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxTunnelStrategieComponent Component');
    this.text = 'Hello World';
  }

}
