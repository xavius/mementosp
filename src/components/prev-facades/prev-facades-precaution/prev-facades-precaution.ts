import { Component } from '@angular/core';

/**
 * Generated class for the PrevFacadesPrecautionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'prev-facades-precaution',
  templateUrl: 'prev-facades-precaution.html'
})
export class PrevFacadesPrecautionComponent {

  text: string;

  constructor() {
    console.log('Hello PrevFacadesPrecautionComponent Component');
    this.text = 'Hello World';
  }

}
