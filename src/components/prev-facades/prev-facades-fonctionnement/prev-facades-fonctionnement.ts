import { Component } from '@angular/core';

/**
 * Generated class for the PrevFacadesFonctionnementComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'prev-facades-fonctionnement',
  templateUrl: 'prev-facades-fonctionnement.html'
})
export class PrevFacadesFonctionnementComponent {

  text: string;

  constructor() {
    console.log('Hello PrevFacadesFonctionnementComponent Component');
    this.text = 'Hello World';
  }

}
