import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the HeaderMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'header-menu',
  templateUrl: 'header-menu.html'
})
export class HeaderMenuComponent {

  text: string;

  constructor(public navCtrl: NavController) {
    console.log('Hello HeaderMenuComponent Component');
    this.text = 'Hello World';
  }

  backToHome(item){
    this.navCtrl.setRoot(HomePage);
}

}
