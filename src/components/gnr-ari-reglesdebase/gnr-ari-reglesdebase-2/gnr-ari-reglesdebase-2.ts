import { Component } from '@angular/core';

/**
 * Generated class for the GnrAriReglesdebase_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-ari-reglesdebase-2',
  templateUrl: 'gnr-ari-reglesdebase-2.html'
})
export class GnrAriReglesdebase_2Component {

  text: string;

  constructor() {
    console.log('Hello GnrAriReglesdebase_2Component Component');
    this.text = 'Hello World';
  }

}
