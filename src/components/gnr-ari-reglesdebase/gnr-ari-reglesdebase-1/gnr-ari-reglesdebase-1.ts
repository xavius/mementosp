import { Component } from '@angular/core';

/**
 * Generated class for the GnrAriReglesdebase_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-ari-reglesdebase-1',
  templateUrl: 'gnr-ari-reglesdebase-1.html'
})
export class GnrAriReglesdebase_1Component {

  text: string;

  constructor() {
    console.log('Hello GnrAriReglesdebase_1Component Component');
    this.text = 'Hello World';
  }

}
