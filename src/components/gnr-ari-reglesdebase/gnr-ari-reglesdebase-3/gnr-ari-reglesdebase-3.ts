import { Component } from '@angular/core';

/**
 * Generated class for the GnrAriReglesdebase_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gnr-ari-reglesdebase-3',
  templateUrl: 'gnr-ari-reglesdebase-3.html'
})
export class GnrAriReglesdebase_3Component {

  text: string;

  constructor() {
    console.log('Hello GnrAriReglesdebase_3Component Component');
    this.text = 'Hello World';
  }

}
