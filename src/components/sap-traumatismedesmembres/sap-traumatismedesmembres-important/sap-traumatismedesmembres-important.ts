import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismedesmembresImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismedesmembres-important',
  templateUrl: 'sap-traumatismedesmembres-important.html'
})
export class SapTraumatismedesmembresImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismedesmembresImportantComponent Component');
    this.text = 'Hello World';
  }

}
