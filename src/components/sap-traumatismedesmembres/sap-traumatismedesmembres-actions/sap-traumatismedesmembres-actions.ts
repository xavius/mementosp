import { Component } from '@angular/core';

/**
 * Generated class for the SapTraumatismedesmembresActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-traumatismedesmembres-actions',
  templateUrl: 'sap-traumatismedesmembres-actions.html'
})
export class SapTraumatismedesmembresActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapTraumatismedesmembresActionsComponent Component');
    this.text = 'Hello World';
  }

}
