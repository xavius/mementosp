import { Component } from '@angular/core';

/**
 * Generated class for the SapInfarctusBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-infarctus-bilans',
  templateUrl: 'sap-infarctus-bilans.html'
})
export class SapInfarctusBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapInfarctusBilansComponent Component');
    this.text = 'Hello World';
  }

}
