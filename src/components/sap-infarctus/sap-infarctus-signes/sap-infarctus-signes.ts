import { Component } from '@angular/core';

/**
 * Generated class for the SapInfarctusSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-infarctus-signes',
  templateUrl: 'sap-infarctus-signes.html'
})
export class SapInfarctusSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapInfarctusSignesComponent Component');
    this.text = 'Hello World';
  }

}
