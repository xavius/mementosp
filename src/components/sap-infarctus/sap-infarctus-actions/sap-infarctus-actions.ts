import { Component } from '@angular/core';

/**
 * Generated class for the SapInfarctusActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-infarctus-actions',
  templateUrl: 'sap-infarctus-actions.html'
})
export class SapInfarctusActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapInfarctusActionsComponent Component');
    this.text = 'Hello World';
  }

}
