import { Component } from '@angular/core';

/**
 * Generated class for the SapInfarctusImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-infarctus-important',
  templateUrl: 'sap-infarctus-important.html'
})
export class SapInfarctusImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapInfarctusImportantComponent Component');
    this.text = 'Hello World';
  }

}
