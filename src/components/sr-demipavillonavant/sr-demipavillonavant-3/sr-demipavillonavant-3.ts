import { Component } from '@angular/core';

/**
 * Generated class for the SrDemipavillonavant_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-demipavillonavant-3',
  templateUrl: 'sr-demipavillonavant-3.html'
})
export class SrDemipavillonavant_3Component {

  text: string;

  constructor() {
    console.log('Hello SrDemipavillonavant_3Component Component');
    this.text = 'Hello World';
  }

}
