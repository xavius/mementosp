import { Component } from '@angular/core';

/**
 * Generated class for the SrDemipavillonavant_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-demipavillonavant-1',
  templateUrl: 'sr-demipavillonavant-1.html'
})
export class SrDemipavillonavant_1Component {

  text: string;

  constructor() {
    console.log('Hello SrDemipavillonavant_1Component Component');
    this.text = 'Hello World';
  }

}
