import { Component } from '@angular/core';

/**
 * Generated class for the SrDemipavillonavant_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-demipavillonavant-2',
  templateUrl: 'sr-demipavillonavant-2.html'
})
export class SrDemipavillonavant_2Component {

  text: string;

  constructor() {
    console.log('Hello SrDemipavillonavant_2Component Component');
    this.text = 'Hello World';
  }

}
