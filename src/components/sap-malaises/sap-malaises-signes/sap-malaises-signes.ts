import { Component } from '@angular/core';

/**
 * Generated class for the SapMalaisesSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-malaises-signes',
  templateUrl: 'sap-malaises-signes.html'
})
export class SapMalaisesSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapMalaisesSignesComponent Component');
    this.text = 'Hello World';
  }

}
