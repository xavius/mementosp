import { Component } from '@angular/core';

/**
 * Generated class for the SapMalaisesActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-malaises-actions',
  templateUrl: 'sap-malaises-actions.html'
})
export class SapMalaisesActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapMalaisesActionsComponent Component');
    this.text = 'Hello World';
  }

}
