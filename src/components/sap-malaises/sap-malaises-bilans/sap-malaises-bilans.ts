import { Component } from '@angular/core';

/**
 * Generated class for the SapMalaisesBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-malaises-bilans',
  templateUrl: 'sap-malaises-bilans.html'
})
export class SapMalaisesBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapMalaisesBilansComponent Component');
    this.text = 'Hello World';
  }

}
