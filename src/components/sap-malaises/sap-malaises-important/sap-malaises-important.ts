import { Component } from '@angular/core';

/**
 * Generated class for the SapMalaisesImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-malaises-important',
  templateUrl: 'sap-malaises-important.html'
})
export class SapMalaisesImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapMalaisesImportantComponent Component');
    this.text = 'Hello World';
  }

}
