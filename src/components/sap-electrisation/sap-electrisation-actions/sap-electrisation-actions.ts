import { Component } from '@angular/core';

/**
 * Generated class for the SapElectrisationActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-electrisation-actions',
  templateUrl: 'sap-electrisation-actions.html'
})
export class SapElectrisationActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapElectrisationActionsComponent Component');
    this.text = 'Hello World';
  }

}
