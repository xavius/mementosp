import { Component } from '@angular/core';

/**
 * Generated class for the SapElectrisationImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-electrisation-important',
  templateUrl: 'sap-electrisation-important.html'
})
export class SapElectrisationImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapElectrisationImportantComponent Component');
    this.text = 'Hello World';
  }

}
