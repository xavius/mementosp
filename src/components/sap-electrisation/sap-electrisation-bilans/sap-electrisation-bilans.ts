import { Component } from '@angular/core';

/**
 * Generated class for the SapElectrisationBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-electrisation-bilans',
  templateUrl: 'sap-electrisation-bilans.html'
})
export class SapElectrisationBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapElectrisationBilansComponent Component');
    this.text = 'Hello World';
  }

}
