import { Component } from '@angular/core';

/**
 * Generated class for the SapElectrisationSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-electrisation-signes',
  templateUrl: 'sap-electrisation-signes.html'
})
export class SapElectrisationSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapElectrisationSignesComponent Component');
    this.text = 'Hello World';
  }

}
