import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeJuAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-ju-acces',
  templateUrl: 'feux-erp-type-ju-acces.html'
})
export class FeuxErpTypeJuAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeJuAccesComponent Component');
    this.text = 'Hello World';
  }

}
