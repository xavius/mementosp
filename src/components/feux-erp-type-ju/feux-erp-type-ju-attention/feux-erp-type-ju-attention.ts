import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeJuAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-ju-attention',
  templateUrl: 'feux-erp-type-ju-attention.html'
})
export class FeuxErpTypeJuAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeJuAttentionComponent Component');
    this.text = 'Hello World';
  }

}
