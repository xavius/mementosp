import { Component } from '@angular/core';

/**
 * Generated class for the FeuxErpTypeJuActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-erp-type-ju-action',
  templateUrl: 'feux-erp-type-ju-action.html'
})
export class FeuxErpTypeJuActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxErpTypeJuActionComponent Component');
    this.text = 'Hello World';
  }

}
