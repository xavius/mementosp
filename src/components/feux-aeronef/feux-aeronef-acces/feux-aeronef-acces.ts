import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAeronefAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-aeronef-acces',
  templateUrl: 'feux-aeronef-acces.html'
})
export class FeuxAeronefAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAeronefAccesComponent Component');
    this.text = 'Hello World';
  }

}
