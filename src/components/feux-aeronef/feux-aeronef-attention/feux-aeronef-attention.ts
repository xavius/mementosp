import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAeronefAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-aeronef-attention',
  templateUrl: 'feux-aeronef-attention.html'
})
export class FeuxAeronefAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAeronefAttentionComponent Component');
    this.text = 'Hello World';
  }

}
