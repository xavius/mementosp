import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAeronefActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-aeronef-action',
  templateUrl: 'feux-aeronef-action.html'
})
export class FeuxAeronefActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAeronefActionComponent Component');
    this.text = 'Hello World';
  }

}
