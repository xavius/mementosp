import { Component } from '@angular/core';

/**
 * Generated class for the FeuxAeronefStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-aeronef-strategie',
  templateUrl: 'feux-aeronef-strategie.html'
})
export class FeuxAeronefStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxAeronefStrategieComponent Component');
    this.text = 'Hello World';
  }

}
