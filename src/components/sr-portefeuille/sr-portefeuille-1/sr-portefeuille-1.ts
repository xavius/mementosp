import { Component } from '@angular/core';

/**
 * Generated class for the SrPortefeuille_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-portefeuille-1',
  templateUrl: 'sr-portefeuille-1.html'
})
export class SrPortefeuille_1Component {

  text: string;

  constructor() {
    console.log('Hello SrPortefeuille_1Component Component');
    this.text = 'Hello World';
  }

}
