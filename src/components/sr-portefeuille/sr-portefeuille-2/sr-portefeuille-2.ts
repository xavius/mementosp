import { Component } from '@angular/core';

/**
 * Generated class for the SrPortefeuille_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-portefeuille-2',
  templateUrl: 'sr-portefeuille-2.html'
})
export class SrPortefeuille_2Component {

  text: string;

  constructor() {
    console.log('Hello SrPortefeuille_2Component Component');
    this.text = 'Hello World';
  }

}
