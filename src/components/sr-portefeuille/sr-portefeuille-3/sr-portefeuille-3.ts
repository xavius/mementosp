import { Component } from '@angular/core';

/**
 * Generated class for the SrPortefeuille_3Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-portefeuille-3',
  templateUrl: 'sr-portefeuille-3.html'
})
export class SrPortefeuille_3Component {

  text: string;

  constructor() {
    console.log('Hello SrPortefeuille_3Component Component');
    this.text = 'Hello World';
  }

}
