import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChemineeAccesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cheminee-acces',
  templateUrl: 'feux-cheminee-acces.html'
})
export class FeuxChemineeAccesComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChemineeAccesComponent Component');
    this.text = 'Hello World';
  }

}
