import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChemineeStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cheminee-strategie',
  templateUrl: 'feux-cheminee-strategie.html'
})
export class FeuxChemineeStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChemineeStrategieComponent Component');
    this.text = 'Hello World';
  }

}
