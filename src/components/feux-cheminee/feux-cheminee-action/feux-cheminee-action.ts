import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChemineeActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cheminee-action',
  templateUrl: 'feux-cheminee-action.html'
})
export class FeuxChemineeActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChemineeActionComponent Component');
    this.text = 'Hello World';
  }

}
