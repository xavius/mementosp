import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChemineeAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-cheminee-attention',
  templateUrl: 'feux-cheminee-attention.html'
})
export class FeuxChemineeAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChemineeAttentionComponent Component');
    this.text = 'Hello World';
  }

}
