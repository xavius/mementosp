import { Component } from '@angular/core';

/**
 * Generated class for the SapEcrasementActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ecrasement-actions',
  templateUrl: 'sap-ecrasement-actions.html'
})
export class SapEcrasementActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapEcrasementActionsComponent Component');
    this.text = 'Hello World';
  }

}
