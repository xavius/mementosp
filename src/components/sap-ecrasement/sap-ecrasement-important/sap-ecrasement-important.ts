import { Component } from '@angular/core';

/**
 * Generated class for the SapEcrasementImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ecrasement-important',
  templateUrl: 'sap-ecrasement-important.html'
})
export class SapEcrasementImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapEcrasementImportantComponent Component');
    this.text = 'Hello World';
  }

}
