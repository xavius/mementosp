import { Component } from '@angular/core';

/**
 * Generated class for the SapEcrasementSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ecrasement-signes',
  templateUrl: 'sap-ecrasement-signes.html'
})
export class SapEcrasementSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapEcrasementSignesComponent Component');
    this.text = 'Hello World';
  }

}
