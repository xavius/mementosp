import { Component } from '@angular/core';

/**
 * Generated class for the SapEcrasementBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-ecrasement-bilans',
  templateUrl: 'sap-ecrasement-bilans.html'
})
export class SapEcrasementBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapEcrasementBilansComponent Component');
    this.text = 'Hello World';
  }

}
