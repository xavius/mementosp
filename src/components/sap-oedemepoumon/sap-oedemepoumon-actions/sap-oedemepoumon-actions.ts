import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemepoumonActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemepoumon-actions',
  templateUrl: 'sap-oedemepoumon-actions.html'
})
export class SapOedemepoumonActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemepoumonActionsComponent Component');
    this.text = 'Hello World';
  }

}
