import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemepoumonSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemepoumon-signes',
  templateUrl: 'sap-oedemepoumon-signes.html'
})
export class SapOedemepoumonSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemepoumonSignesComponent Component');
    this.text = 'Hello World';
  }

}
