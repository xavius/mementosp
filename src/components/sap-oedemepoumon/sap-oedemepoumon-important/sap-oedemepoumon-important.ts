import { Component } from '@angular/core';

/**
 * Generated class for the SapOedemepoumonImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-oedemepoumon-important',
  templateUrl: 'sap-oedemepoumon-important.html'
})
export class SapOedemepoumonImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapOedemepoumonImportantComponent Component');
    this.text = 'Hello World';
  }

}
