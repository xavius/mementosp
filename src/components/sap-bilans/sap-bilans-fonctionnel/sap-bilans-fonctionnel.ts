import { Component } from '@angular/core';

/**
 * Generated class for the SapBilansFonctionnelComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-bilans-fonctionnel',
  templateUrl: 'sap-bilans-fonctionnel.html'
})
export class SapBilansFonctionnelComponent {

  text: string;

  constructor() {
    console.log('Hello SapBilansFonctionnelComponent Component');
    this.text = 'Hello World';
  }

}
