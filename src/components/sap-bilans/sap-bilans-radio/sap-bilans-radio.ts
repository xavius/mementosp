import { Component } from '@angular/core';

/**
 * Generated class for the SapBilansRadioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-bilans-radio',
  templateUrl: 'sap-bilans-radio.html'
})
export class SapBilansRadioComponent {

  text: string;

  constructor() {
    console.log('Hello SapBilansRadioComponent Component');
    this.text = 'Hello World';
  }

}
