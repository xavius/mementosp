import { Component } from '@angular/core';

/**
 * Generated class for the SapBilansCirconstancielComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-bilans-circonstanciel',
  templateUrl: 'sap-bilans-circonstanciel.html'
})
export class SapBilansCirconstancielComponent {

  text: string;

  constructor() {
    console.log('Hello SapBilansCirconstancielComponent Component');
    this.text = 'Hello World';
  }

}
