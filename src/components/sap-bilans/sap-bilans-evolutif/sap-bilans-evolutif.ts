import { Component } from '@angular/core';

/**
 * Generated class for the SapBilansEvolutifComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-bilans-evolutif',
  templateUrl: 'sap-bilans-evolutif.html'
})
export class SapBilansEvolutifComponent {

  text: string;

  constructor() {
    console.log('Hello SapBilansEvolutifComponent Component');
    this.text = 'Hello World';
  }

}
