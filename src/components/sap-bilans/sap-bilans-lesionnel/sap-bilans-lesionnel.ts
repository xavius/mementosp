import { Component } from '@angular/core';

/**
 * Generated class for the SapBilansLesionnelComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-bilans-lesionnel',
  templateUrl: 'sap-bilans-lesionnel.html'
})
export class SapBilansLesionnelComponent {

  text: string;

  constructor() {
    console.log('Hello SapBilansLesionnelComponent Component');
    this.text = 'Hello World';
  }

}
