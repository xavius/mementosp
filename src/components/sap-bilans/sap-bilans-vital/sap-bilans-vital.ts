import { Component } from '@angular/core';

/**
 * Generated class for the SapBilansVitalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-bilans-vital',
  templateUrl: 'sap-bilans-vital.html'
})
export class SapBilansVitalComponent {

  text: string;

  constructor() {
    console.log('Hello SapBilansVitalComponent Component');
    this.text = 'Hello World';
  }

}
