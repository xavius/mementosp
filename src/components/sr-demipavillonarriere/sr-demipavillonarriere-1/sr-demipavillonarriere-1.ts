import { Component } from '@angular/core';

/**
 * Generated class for the SrDemipavillonarriere_1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sr-demipavillonarriere-1',
  templateUrl: 'sr-demipavillonarriere-1.html'
})
export class SrDemipavillonarriere_1Component {

  text: string;

  constructor() {
    console.log('Hello SrDemipavillonarriere_1Component Component');
    this.text = 'Hello World';
  }

}
