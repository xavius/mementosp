import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapLectureglycemieUtilisationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-lectureglycemie-utilisation',
  templateUrl: 'materiels-sap-lectureglycemie-utilisation.html'
})
export class MaterielsSapLectureglycemieUtilisationComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapLectureglycemieUtilisationComponent Component');
    this.text = 'Hello World';
  }

}
