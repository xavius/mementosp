import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapLectureglycemieIndicationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-lectureglycemie-indications',
  templateUrl: 'materiels-sap-lectureglycemie-indications.html'
})
export class MaterielsSapLectureglycemieIndicationsComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapLectureglycemieIndicationsComponent Component');
    this.text = 'Hello World';
  }

}
