import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapLectureglycemiePointsclesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-lectureglycemie-pointscles',
  templateUrl: 'materiels-sap-lectureglycemie-pointscles.html'
})
export class MaterielsSapLectureglycemiePointsclesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapLectureglycemiePointsclesComponent Component');
    this.text = 'Hello World';
  }

}
