import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapLectureglycemieRisquesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-lectureglycemie-risques',
  templateUrl: 'materiels-sap-lectureglycemie-risques.html'
})
export class MaterielsSapLectureglycemieRisquesComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapLectureglycemieRisquesComponent Component');
    this.text = 'Hello World';
  }

}
