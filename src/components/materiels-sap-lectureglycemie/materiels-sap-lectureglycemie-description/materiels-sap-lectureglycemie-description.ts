import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapLectureglycemieDescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-lectureglycemie-description',
  templateUrl: 'materiels-sap-lectureglycemie-description.html'
})
export class MaterielsSapLectureglycemieDescriptionComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapLectureglycemieDescriptionComponent Component');
    this.text = 'Hello World';
  }

}
