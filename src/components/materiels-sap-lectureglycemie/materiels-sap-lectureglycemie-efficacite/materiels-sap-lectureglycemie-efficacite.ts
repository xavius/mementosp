import { Component } from '@angular/core';

/**
 * Generated class for the MaterielsSapLectureglycemieEfficaciteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'materiels-sap-lectureglycemie-efficacite',
  templateUrl: 'materiels-sap-lectureglycemie-efficacite.html'
})
export class MaterielsSapLectureglycemieEfficaciteComponent {

  text: string;

  constructor() {
    console.log('Hello MaterielsSapLectureglycemieEfficaciteComponent Component');
    this.text = 'Hello World';
  }

}
