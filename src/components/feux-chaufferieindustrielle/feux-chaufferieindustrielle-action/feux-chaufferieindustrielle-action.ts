import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieindustrielleActionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferieindustrielle-action',
  templateUrl: 'feux-chaufferieindustrielle-action.html'
})
export class FeuxChaufferieindustrielleActionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieindustrielleActionComponent Component');
    this.text = 'Hello World';
  }

}
