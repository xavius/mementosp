import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieindustrielleAttentionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferieindustrielle-attention',
  templateUrl: 'feux-chaufferieindustrielle-attention.html'
})
export class FeuxChaufferieindustrielleAttentionComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieindustrielleAttentionComponent Component');
    this.text = 'Hello World';
  }

}
