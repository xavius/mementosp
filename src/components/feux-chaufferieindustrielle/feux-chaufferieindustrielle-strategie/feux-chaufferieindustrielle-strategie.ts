import { Component } from '@angular/core';

/**
 * Generated class for the FeuxChaufferieindustrielleStrategieComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feux-chaufferieindustrielle-strategie',
  templateUrl: 'feux-chaufferieindustrielle-strategie.html'
})
export class FeuxChaufferieindustrielleStrategieComponent {

  text: string;

  constructor() {
    console.log('Hello FeuxChaufferieindustrielleStrategieComponent Component');
    this.text = 'Hello World';
  }

}
