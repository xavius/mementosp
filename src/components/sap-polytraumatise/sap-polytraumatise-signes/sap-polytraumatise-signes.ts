import { Component } from '@angular/core';

/**
 * Generated class for the SapPolytraumatiseSignesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-polytraumatise-signes',
  templateUrl: 'sap-polytraumatise-signes.html'
})
export class SapPolytraumatiseSignesComponent {

  text: string;

  constructor() {
    console.log('Hello SapPolytraumatiseSignesComponent Component');
    this.text = 'Hello World';
  }

}
