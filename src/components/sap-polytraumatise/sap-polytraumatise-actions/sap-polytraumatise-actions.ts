import { Component } from '@angular/core';

/**
 * Generated class for the SapPolytraumatiseActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-polytraumatise-actions',
  templateUrl: 'sap-polytraumatise-actions.html'
})
export class SapPolytraumatiseActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapPolytraumatiseActionsComponent Component');
    this.text = 'Hello World';
  }

}
