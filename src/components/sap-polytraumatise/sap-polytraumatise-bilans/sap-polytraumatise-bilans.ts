import { Component } from '@angular/core';

/**
 * Generated class for the SapPolytraumatiseBilansComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-polytraumatise-bilans',
  templateUrl: 'sap-polytraumatise-bilans.html'
})
export class SapPolytraumatiseBilansComponent {

  text: string;

  constructor() {
    console.log('Hello SapPolytraumatiseBilansComponent Component');
    this.text = 'Hello World';
  }

}
