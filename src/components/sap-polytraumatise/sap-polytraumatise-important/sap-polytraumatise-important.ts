import { Component } from '@angular/core';

/**
 * Generated class for the SapPolytraumatiseImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-polytraumatise-important',
  templateUrl: 'sap-polytraumatise-important.html'
})
export class SapPolytraumatiseImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapPolytraumatiseImportantComponent Component');
    this.text = 'Hello World';
  }

}
