import { Component } from '@angular/core';

/**
 * Generated class for the SapIntoxicationmedActionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-intoxicationmed-actions',
  templateUrl: 'sap-intoxicationmed-actions.html'
})
export class SapIntoxicationmedActionsComponent {

  text: string;

  constructor() {
    console.log('Hello SapIntoxicationmedActionsComponent Component');
    this.text = 'Hello World';
  }

}
