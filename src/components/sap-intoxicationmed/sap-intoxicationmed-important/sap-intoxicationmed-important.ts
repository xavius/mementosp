import { Component } from '@angular/core';

/**
 * Generated class for the SapIntoxicationmedImportantComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sap-intoxicationmed-important',
  templateUrl: 'sap-intoxicationmed-important.html'
})
export class SapIntoxicationmedImportantComponent {

  text: string;

  constructor() {
    console.log('Hello SapIntoxicationmedImportantComponent Component');
    this.text = 'Hello World';
  }

}
